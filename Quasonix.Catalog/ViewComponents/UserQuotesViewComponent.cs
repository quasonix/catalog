﻿using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.ViewComponents
{
    public class UserQuotesViewComponent : ViewComponent
    {
        private readonly ICatalogRepository<Quote> quoteRepo;
        private readonly ICatalogRepository<User> userRepo;

        public UserQuotesViewComponent(ICatalogRepository<Quote> quoteRepo,
            ICatalogRepository<User> userRepo)
        {
            this.quoteRepo = quoteRepo;
            this.userRepo = userRepo;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = await userRepo.GetAsync(x => x.UserName == User.Identity.Name);
            return View(user.Quotes.ToList());
        }
    }
}
