﻿using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.ViewComponents
{
    public class QuoteCartViewComponent : ViewComponent
    {
        private readonly ICatalogRepository<Quote> quoteRepo;
        private readonly ICatalogRepository<User> userRepo;

        public QuoteCartViewComponent(ICatalogRepository<Quote> quoteRepo,
            ICatalogRepository<User> userRepo)
        {
            this.quoteRepo = quoteRepo;
            this.userRepo = userRepo;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = await userRepo.GetAsync(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);

            return View(activeQuote);
        }
    }
}
