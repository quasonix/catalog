﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class EvtmController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public EvtmController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var evtms = repo.GetAll<Evtm>();
            return View(evtms);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new EvtmViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(EvtmViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map evtm from model
                var evtm = mapper.Map<Evtm>(model);

                // create evtm
                unitOfWork.BeginTransaction();
                repo.Create(evtm);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var evtm = repo.Get<Evtm>(id);
            var model = mapper.Map<EvtmViewModel>(evtm);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(EvtmViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get existing evtm and map from model
                var evtm = repo.Get<Evtm>(model.Id);
                mapper.Map(model, evtm);

                // update evtm
                unitOfWork.BeginTransaction();
                repo.Update(evtm);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var evtm = repo.Get<Evtm>(id);
            var model = mapper.Map<EvtmDetailViewModel>(evtm);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var evtm = repo.Get<Evtm>(id);
            var model = mapper.Map<DeleteViewModel>(evtm);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var evtm = repo.Get<Evtm>(model.Id);

            unitOfWork.BeginTransaction();
            repo.Delete(evtm);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(EvtmViewModel model)
        {
            // check if evtm with name already exists, add error if not
            bool exists = repo.GetGroup<Evtm>(x => x.PartNumber == model.PartNumber && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"An evtm product with the part number {model.PartNumber} already exists.");
        }
    }
}