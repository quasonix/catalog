﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class TxFootprintController : Controller
    {
        private readonly ICatalogRepository<TxFootprint> footprintRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TxFootprintController(ICatalogRepository<TxFootprint> footprintRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.footprintRepo = footprintRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var footprints = footprintRepo.GetAll();
            return View(footprints);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new FootprintViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(FootprintViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var footprint = mapper.Map<TxFootprint>(model);

                // clear values for other shape, we don't want both
                if (footprint.Shape == FootprintShape.Round)
                {
                    footprint.Length = null;
                    footprint.Width = null;
                } 
                else
                {
                    footprint.Diameter = null;
                }

                // create footprint
                unitOfWork.BeginTransaction();
                footprintRepo.Create(footprint);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            // get footprint, map props from model
            var footprint = footprintRepo.Get(id);
            var model = mapper.Map<FootprintViewModel>(footprint);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(FootprintViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get footprint, map props from model
                var footprint = footprintRepo.Get(model.Id);
                mapper.Map(model, footprint);

                // clear values for other shape, we don't want both
                if (footprint.Shape == FootprintShape.Round)
                {
                    footprint.Length = null;
                    footprint.Width = null;
                }
                else
                {
                    footprint.Diameter = null;
                }

                // update in db
                unitOfWork.BeginTransaction();
                footprintRepo.Update(footprint);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var footprint = footprintRepo.Get(id);
            var model = mapper.Map<FootprintDetailViewModel>(footprint);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var footprint = footprintRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(footprint);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var footprint = footprintRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            footprintRepo.Delete(footprint);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(FootprintViewModel model)
        {
            // check if footprint with name already exists, add error if not
            bool exists = footprintRepo.GetGroup(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A footprint with the name {model.Name} already exists.");
        }
    }
}