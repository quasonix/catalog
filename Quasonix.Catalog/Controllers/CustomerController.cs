﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using AutoMapper;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Mvc;
//using Quasonix.Catalog.Entities;
//using Quasonix.Catalog.Models;

//namespace Quasonix.Catalog.Controllers
//{
//    [Authorize]
//    public class CustomerController : Controller
//    {
//        private readonly ICatalogRepository repo;
//        private readonly ICatalogUnitOfWork unitOfWork;
//        private readonly IMapper mapper;

//        public CustomerController(ICatalogRepository repo,
//            ICatalogUnitOfWork unitOfWork,
//            IMapper mapper)
//        {
//            this.repo = repo;
//            this.unitOfWork = unitOfWork;
//            this.mapper = mapper;
//        }

//        [HttpGet]
//        public IActionResult Index()
//        {
//            var customers = repo.GetAll<Customer>();
//            return View(customers);
//        }

//        [HttpGet]
//        public IActionResult Create()
//        {
//            var model = new CustomerViewModel();
//            return View(model);
//        }

//        [HttpPost]
//        public IActionResult Create(CustomerViewModel model)
//        {
//            ValidateViewModel(model);

//            if (ModelState.IsValid)
//            {
//                // map customer from model
//                var customer = mapper.Map<Customer>(model);

//                // create customer
//                unitOfWork.BeginTransaction();
//                repo.Create(customer);
//                unitOfWork.Commit();

//                return RedirectToAction("Index");
//            }

//            return View(model);
//        }

//        [HttpGet]
//        public IActionResult Edit(Guid id)
//        {
//            var customer = repo.Get<Customer>(id);
//            var model = mapper.Map<CustomerViewModel>(customer);

//            return View(model);
//        }

//        [HttpPost]
//        public IActionResult Edit(CustomerViewModel model)
//        {
//            ValidateViewModel(model);

//            if (ModelState.IsValid)
//            {
//                // get existing customer and map from model
//                var customer = repo.Get<Customer>(model.Id);
//                mapper.Map(model, customer);

//                // update customer
//                unitOfWork.BeginTransaction();
//                repo.Update(customer);
//                unitOfWork.Commit();

//                return RedirectToAction("Index");
//            }

//            return View(model);
//        }

//        [HttpGet]
//        public IActionResult Detail(Guid id)
//        {
//            var customer = repo.Get<Customer>(id);
//            var model = mapper.Map<CustomerDetailViewModel>(customer);
//            return View(model);
//        }

//        [HttpGet]
//        public IActionResult Delete(Guid id)
//        {
//            var customer = repo.Get<Customer>(id);
//            var model = mapper.Map<DeleteViewModel>(customer);
//            return View(model);
//        }

//        [HttpPost]
//        public IActionResult Delete(DeleteViewModel model)
//        {
//            var customer = repo.Get<Customer>(model.Id);

//            unitOfWork.BeginTransaction();
//            repo.Delete(customer);
//            unitOfWork.Commit();

//            return RedirectToAction("Index");
//        }

//        private void ValidateViewModel(CustomerViewModel model)
//        {
//            // check if customer with name already exists, add error if not
//            bool exists = repo.GetGroup<Customer>(x => x.Name == model.Name && x.Id != model.Id).Any();
//            if (exists)
//                ModelState.AddModelError("ExistsError", $"A customer with the name {model.Name} already exists.");
//        }
//    }
//}