﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class RfConnectorController : Controller
    {
        private readonly ICatalogRepository<TxRadioConnector> rfConnectorRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public RfConnectorController(ICatalogRepository<TxRadioConnector> rfConnectorRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.rfConnectorRepo = rfConnectorRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var rfConnectors = rfConnectorRepo.GetAll();
            return View(rfConnectors);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new RfConnectorViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(RfConnectorViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var rfConnector = mapper.Map<TxRadioConnector>(model);

                unitOfWork.BeginTransaction();
                rfConnectorRepo.Create(rfConnector);
                unitOfWork.Commit();

                return RedirectToAction("Index"); 
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var rfConnector = rfConnectorRepo.Get(id);
            var model = mapper.Map<RfConnectorViewModel>(rfConnector);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(RfConnectorViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var rfConnector = rfConnectorRepo.Get(model.Id);
                mapper.Map(model, rfConnector);

                unitOfWork.BeginTransaction();
                rfConnectorRepo.Update(rfConnector);
                unitOfWork.Commit();

                return RedirectToAction("Index"); 
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var rfConnector = rfConnectorRepo.Get(id);
            var model = mapper.Map<RfConnectorDetailViewModel>(rfConnector);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var rfConnector = rfConnectorRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(rfConnector);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var rfConnector = rfConnectorRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            rfConnectorRepo.Delete(rfConnector);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(RfConnectorViewModel model)
        {
            // check if connector with name already exists, add error if not
            bool exists = rfConnectorRepo.GetGroup(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"An RF connector with the name {model.Name} already exists.");
        }
    }
}