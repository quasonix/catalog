﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class AntWiringController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public AntWiringController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var wirings = repo.GetAll<AntWiring>();
            return View(wirings);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new AntWiringViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(AntWiringViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map wiring from model
                var wiring = mapper.Map<AntWiring>(model);

                // create wiring
                unitOfWork.BeginTransaction();
                repo.Create(wiring);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var wiring = repo.Get<AntWiring>(id);
            var model = mapper.Map<AntWiringViewModel>(wiring);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(AntWiringViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get existing wiring and map from model
                var wiring = repo.Get<AntWiring>(model.Id);
                mapper.Map(model, wiring);

                // update wiring
                unitOfWork.BeginTransaction();
                repo.Update(wiring);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var wiring = repo.Get<AntWiring>(id);
            var model = mapper.Map<AntWiringDetailViewModel>(wiring);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var wiring = repo.Get<AntWiring>(id);
            var model = mapper.Map<DeleteViewModel>(wiring);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var wiring = repo.Get<AntWiring>(model.Id);

            unitOfWork.BeginTransaction();
            repo.Delete(wiring);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(AntWiringViewModel model)
        {
            // check if wiring with name already exists, add error if not
            bool exists = repo.GetGroup<AntWiring>(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A wiring with the name {model.Name} already exists.");
        }
    }
}