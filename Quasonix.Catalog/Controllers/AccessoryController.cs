﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DynamicVML;
using DynamicVML.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class AccessoryController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public AccessoryController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var accs = repo.GetAll<Accessory>();
            return View(accs);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new AccessoryViewModel();
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(AccessoryViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map entity from model
                var acc = mapper.Map<Accessory>(model);

                // set product category
                acc.Category = repo.Get<Category>(x => x.ProductType == ProductType.Accessory);

                foreach (var catId in model.CategoryChecklist.SelectedValues)
                {
                    // add selected category
                    var category = repo.Get<AccCategory>(catId);
                    acc.Categories.Add(category);

                    // add acc to category, need both sides
                    category.Accessories.Add(acc);
                }

                foreach (var funcId in model.FunctionChecklist.SelectedValues)
                {
                    // add selected function
                    var function = repo.Get<AccFunction>(funcId);
                    acc.Functions.Add(function);

                    // add acc to function, need both sides
                    function.Accessories.Add(acc);
                }

                // create in db
                unitOfWork.BeginTransaction();
                repo.Create(acc);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var acc = repo.Get<Accessory>(id);
            var model = mapper.Map<AccessoryViewModel>(acc);

            InflateViewModel(model, acc);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(AccessoryViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get from db, map from model
                var acc = repo.Get<Accessory>(model.Id);
                mapper.Map(model, acc);

                acc.ClearCategories();
                foreach (var catId in model.CategoryChecklist.SelectedValues)
                {
                    // add selected category
                    var category = repo.Get<AccCategory>(catId);
                    acc.Categories.Add(category);

                    // add acc to category, need both sides
                    category.Accessories.Add(acc);
                }

                acc.ClearFunctions();
                foreach (var funcId in model.FunctionChecklist.SelectedValues)
                {
                    // add selected function
                    var function = repo.Get<AccFunction>(funcId);
                    acc.Functions.Add(function);

                    // add acc to function, need both sides
                    function.Accessories.Add(acc);
                }

                // update
                unitOfWork.BeginTransaction();
                repo.Update(acc);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var other = repo.Get<Accessory>(id);
            var model = mapper.Map<AccessoryDetailViewModel>(other);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var other = repo.Get<Accessory>(id);
            var model = mapper.Map<DeleteViewModel>(other);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var acc = repo.Get<Accessory>(model.Id);
            acc.ClearFunctions();

            unitOfWork.BeginTransaction();
            repo.Update(acc);
            repo.Delete(acc);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void InflateViewModel(AccessoryViewModel model)
        {
            model.CategoryChecklist = repo.GetAll<AccCategory>()
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, model.CategoryChecklist.SelectedValues);

            model.FunctionChecklist = repo.GetAll<AccFunction>()
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, model.FunctionChecklist.SelectedValues);
        }

        private void InflateViewModel(AccessoryViewModel model, Accessory accessory)
        {
            model.CategoryChecklist = repo.GetAll<AccCategory>()
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, accessory.Categories.Select(x => x.Id));

            model.FunctionChecklist = repo.GetAll<AccFunction>()
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, accessory.Functions.Select(x => x.Id));
        }

        private void ValidateViewModel(AccessoryViewModel model)
        {
            // check if acc with name already exists, add error if not
            bool exists = repo.GetGroup<Accessory>(x => x.PartNumber == model.PartNumber && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"An accessory with the part number {model.PartNumber} already exists.");
        }
    }
}