﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class FrontPanelInterfaceController : Controller
    {
        private readonly ICatalogRepository<RxFrontPanelInterface> panelInterfaceRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public FrontPanelInterfaceController(ICatalogRepository<RxFrontPanelInterface> panelInterfaceRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.panelInterfaceRepo = panelInterfaceRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var panelInterfaces = panelInterfaceRepo.GetAll();
            return View(panelInterfaces);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new FrontPanelInterfaceViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(FrontPanelInterfaceViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map panelInterface from model
                var panelInterface = mapper.Map<RxFrontPanelInterface>(model);

                // create panelInterface
                unitOfWork.BeginTransaction();
                panelInterfaceRepo.Create(panelInterface);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var panelInterface = panelInterfaceRepo.Get(id);
            var model = mapper.Map<FrontPanelInterfaceViewModel>(panelInterface);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(FrontPanelInterfaceViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get existing panelInterface and map from model
                var panelInterface = panelInterfaceRepo.Get(model.Id);
                mapper.Map(model, panelInterface);

                // update panelInterface
                unitOfWork.BeginTransaction();
                panelInterfaceRepo.Update(panelInterface);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var panelInterface = panelInterfaceRepo.Get(id);
            var model = mapper.Map<FrontPanelInterfaceDetailViewModel>(panelInterface);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var panelInterface = panelInterfaceRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(panelInterface);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var panelInterface = panelInterfaceRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            panelInterfaceRepo.Delete(panelInterface);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(FrontPanelInterfaceViewModel model)
        {
            // check if panelInterface with name already exists, add error if not
            bool exists = panelInterfaceRepo.GetGroup(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A panelInterface with the name {model.Name} already exists.");
        }
    }
}