﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Markdig;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class ReleaseController : Controller
    {
        private readonly IWebHostEnvironment webHostEnvironment;

        public ReleaseController(IWebHostEnvironment webHostEnvironment)
        {
            this.webHostEnvironment = webHostEnvironment;
        }

        // GET: Release
        public ActionResult Index()
        {
            var files = Directory.GetFiles($@"{webHostEnvironment.WebRootPath}/release_notes")
                .Select(x => Path.GetFileNameWithoutExtension(x))
                .OrderByDescending(x => x).ToList();

            var model = new List<ReleaseNotesViewModel>();

            foreach (var file in files)
            {
                var fileDateNum = Convert.ToInt32(file.Substring(file.LastIndexOf('_') + 1));

                model.Add(new ReleaseNotesViewModel()
                {
                    Title = file,
                    Date = new DateTime(2000, 1, 1).AddDays(fileDateNum)
                });
            }

            model = model.OrderByDescending(x => x.Date).ToList();

            return View(model);
        }

        // GET: Release/Notes
        public ActionResult Notes(string version)
        {
            string filePath = $@"{webHostEnvironment.WebRootPath}/release_notes/{version}.md";

            // build model as if it were incorrect
            var model = new ReleaseNotesViewModel();
            model.Title = version.Replace('_', '.');
            model.Html = "No release notes for this version";

            // bail if the file doesn't exist
            if (!System.IO.File.Exists(filePath))
                return View(model);

            var fileContents = System.IO.File.ReadAllText(filePath);

            model.Html = Markdown.ToHtml(fileContents);

            return View(model);
        }
    }
}