﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class TxBasebandController : Controller
    {
        private readonly ICatalogRepository<TxBaseband> basebandRepo;
        private readonly ICatalogRepository<TxPin> pinRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TxBasebandController(ICatalogRepository<TxBaseband> basebandRepo,
            ICatalogRepository<TxPin> pinRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.basebandRepo = basebandRepo;
            this.pinRepo = pinRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var basebands = basebandRepo.GetAll();
            return View(basebands);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new BasebandViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(BasebandViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var baseband = mapper.Map<TxBaseband>(model);

                unitOfWork.BeginTransaction();
                basebandRepo.Create(baseband);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var baseband = basebandRepo.Get(id);
            var model = mapper.Map<BasebandViewModel>(baseband);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(BasebandViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var baseband = basebandRepo.Get(model.Id);
                mapper.Map(model, baseband);

                unitOfWork.BeginTransaction();
                basebandRepo.Update(baseband);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var baseband = basebandRepo.Get(id);
            var model = mapper.Map<BasebandDetailViewModel>(baseband);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var baseband = basebandRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(baseband);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var baseband = basebandRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            basebandRepo.Delete(baseband);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(BasebandViewModel model)
        {
            // check if baseband with name already exists, add error if not
            bool exists = basebandRepo.GetGroup(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A baseband with the name {model.Name} already exists.");
        }
    }
}