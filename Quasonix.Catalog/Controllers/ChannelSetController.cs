﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class ChannelSetController : Controller
    {
        private readonly ICatalogRepository<RxChannelSet> channelSetRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public ChannelSetController(ICatalogRepository<RxChannelSet> channelSetRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.channelSetRepo = channelSetRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var channelSets = channelSetRepo.GetAll();
            var model = mapper.Map<IEnumerable<ChannelSetViewModel>>(channelSets);
            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new ChannelSetViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(ChannelSetViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var channelSet = mapper.Map<RxChannelSet>(model);

                unitOfWork.BeginTransaction();
                channelSetRepo.Create(channelSet);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var channelSet = channelSetRepo.Get(id);
            var model = mapper.Map<ChannelSetViewModel>(channelSet);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(ChannelSetViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var channelSet = channelSetRepo.Get(model.Id);
                mapper.Map(model, channelSet);

                unitOfWork.BeginTransaction();
                channelSetRepo.Update(channelSet);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var channelSet = channelSetRepo.Get(id);
            var model = mapper.Map<ChannelSetDetailViewModel>(channelSet);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var channelSet = channelSetRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(channelSet);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var channelSet = channelSetRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            channelSetRepo.Delete(channelSet);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(ChannelSetViewModel model)
        {
            // check if channel set with name already exists, add error if not
            bool exists = channelSetRepo.GetGroup(x => x.Description == model.Description && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A channel set with the description {model.Description} already exists.");
        }
    }
}