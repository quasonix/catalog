﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class PriceVersionController : Controller
    {
        private readonly ICatalogRepository<PriceVersion> priceVersionRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public PriceVersionController(ICatalogRepository<PriceVersion> priceVersionRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.priceVersionRepo = priceVersionRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var priceVersions = priceVersionRepo.GetAll();
            return View(priceVersions);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new PriceVersionViewModel();
            model.EffectiveDate = DateTime.Now.Date;
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(PriceVersionViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map price version from model
                var priceVersion = mapper.Map<PriceVersion>(model);

                // create price version
                unitOfWork.BeginTransaction();
                priceVersionRepo.Create(priceVersion);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var priceVersion = priceVersionRepo.Get(id);
            var model = mapper.Map<PriceVersionViewModel>(priceVersion);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(PriceVersionViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get existing price version and map from model
                var priceVersion = priceVersionRepo.Get(model.Id);
                mapper.Map(model, priceVersion);

                // update price version
                unitOfWork.BeginTransaction();
                priceVersionRepo.Update(priceVersion);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var priceVersion = priceVersionRepo.Get(id);
            var model = mapper.Map<PriceVersionDetailViewModel>(priceVersion);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var priceVersion = priceVersionRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(priceVersion);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var priceVersion = priceVersionRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            priceVersionRepo.Delete(priceVersion);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(PriceVersionViewModel model)
        {
            // check if price version with daTE already exists, add error if not
            bool exists = priceVersionRepo.GetGroup(x => x.EffectiveDate == model.EffectiveDate && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A price version with an effective date of \"{model.EffectiveDate.ToShortDateString()}\" already exists.");
        }
    }
}