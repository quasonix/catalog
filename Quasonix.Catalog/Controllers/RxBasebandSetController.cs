﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DynamicVML;
using DynamicVML.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class RxBasebandSetController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public RxBasebandSetController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var basebandSets = repo.GetAll<RxBasebandSet>();
            return View(basebandSets);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new RxBasebandSetViewModel();
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(RxBasebandSetViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map basebandSet from model
                var basebandSet = new RxBasebandSet()
                {
                    Description = model.Description,
                    RsConnectorSet = repo.Get<RxConnectorSet>(model.RsConnectorSetId),
                    TtlConnectorSet = repo.Get<RxConnectorSet>(model.TtlConnectorSetId)
                };

                // create basebandSet
                unitOfWork.BeginTransaction();
                repo.Create<RxBasebandSet>(basebandSet);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var basebandSet = repo.Get<RxBasebandSet>(id);
            var model = mapper.Map<RxBasebandSetViewModel>(basebandSet);

            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(RxBasebandSetViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get existing basebandSet and map from model
                var basebandSet = repo.Get<RxBasebandSet>(model.Id);
                basebandSet.Description = model.Description;
                basebandSet.RsConnectorSet = repo.Get<RxConnectorSet>(model.RsConnectorSetId);
                basebandSet.TtlConnectorSet = repo.Get<RxConnectorSet>(model.TtlConnectorSetId);

                // update basebandSet
                unitOfWork.BeginTransaction();
                repo.Create<RxBasebandSet>(basebandSet);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var basebandSet = repo.Get<RxBasebandSet>(id);
            var model = mapper.Map<RxBasebandSetDetailViewModel>(basebandSet);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var basebandSet = repo.Get<RxBasebandSet>(id);
            var model = mapper.Map<DeleteViewModel>(basebandSet);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var basebandSet = repo.Get<RxBasebandSet>(model.Id);

            unitOfWork.BeginTransaction();
            repo.Delete<RxBasebandSet>(basebandSet);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void InflateViewModel(RxBasebandSetViewModel model)
        {
            model.ConnectorSetOptions = repo.GetAll<RxConnectorSet>().ToSelectItemList(x => x.Description, x => x.Id);
        }

        private void ValidateViewModel(RxBasebandSetViewModel model)
        {
            // check if baseband set with name already exists, add error if not
            bool exists = repo.GetGroup<RxBasebandSet>(x => x.Description == model.Description && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A baseband set with the description {model.Description} already exists.");
        }
    }
}