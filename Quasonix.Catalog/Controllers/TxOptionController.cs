﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DynamicVML;
using DynamicVML.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class TxOptionController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TxOptionController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var options = repo.GetAll<TxOption>();
            return View(options);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new OptionViewModel();
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(OptionViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map entity from model
                var option = mapper.Map<TxOption>(model);

                LoadPricesToComponent(model, option);

                // create in db
                unitOfWork.BeginTransaction();
                repo.Create(option);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var option = repo.Get<TxOption>(id);
            var model = mapper.Map<OptionViewModel>(option);

            model.Prices = option.Prices.OrderBy(x => x.PriceVersion.EffectiveDate).ToDynamicList(x =>
                mapper.Map<PriceViewModel>(x));

            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(OptionViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get from db, map from model
                var option = repo.Get<TxOption>(model.Id);
                mapper.Map(model, option);

                LoadPricesToComponent(model, option);

                // update
                unitOfWork.BeginTransaction();
                repo.Update(option);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var other = repo.Get<TxOption>(id);
            var model = mapper.Map<OptionDetailViewModel>(other);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var other = repo.Get<TxOption>(id);
            var model = mapper.Map<DeleteViewModel>(other);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var other = repo.Get<TxOption>(model.Id);

            unitOfWork.BeginTransaction();
            repo.Delete(other);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void LoadPricesToComponent(ComponentViewModel model, Component component)
        {
            component.ClearPrices();
            foreach (var priceModel in model.Prices)
            {
                var price = new Price();
                price.Value = priceModel.ViewModel.Value;
                price.Component = component;
                price.PriceVersion = repo.Get<PriceVersion>(priceModel.ViewModel.PriceVersionId);
                component.Prices.Add(price);
            }
        }

        public IActionResult AddPrice(AddNewDynamicItem parameters)
        {
            var model = new PriceViewModel();
            model.PriceVersionOptions = repo.GetAll<PriceVersion>().OrderBy(x => x.EffectiveDate)
                .ToSelectItemList(x => $"({x.EffectiveDate.ToShortDateString()}) - {x.Note}", x => x.Id);
            return this.PartialView(model, parameters);
        }

        public IActionResult AddBinding(AddNewDynamicItem parameters)
        {
            var model = new OptionBindingViewModel();
            model.Options = repo.GetAll<TxOption>().ToSelectItemList(x => x.Name, x => x.Id);
            return this.PartialView(model, parameters);
        }

        private void InflateViewModel(OptionViewModel model)
        {
            //model.PinOptions = pinRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);
            var priceVersionOptions = repo.GetAll<PriceVersion>().ToSelectItemList(x => $"({x.EffectiveDate.ToShortDateString()}) - {x.Note}", x => x.Id);
            var options = repo.GetAll<TxOption>().ToSelectItemList(x => x.Name, x => x.Id);

            foreach (var price in model.Prices)
            {
                price.ViewModel.PriceVersionOptions = priceVersionOptions;
            }
            foreach (var option in model.Bindings)
            {
                option.ViewModel.Options = options;
            }
        }

        private void ValidateViewModel(OptionViewModel model)
        {
            // check if option with name already exists, add error if not
            bool exists = repo.GetGroup<TxOption>(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"An option with the name {model.Name} already exists.");

            // price validation
            if (model.Prices == null || model.Prices.Count < 1)
                ModelState.AddModelError("NoPriceError", $"Must have a price.");
            else
            {
                var lowestVersion = repo.GetAll<PriceVersion>().OrderBy(x => x.EffectiveDate).First();
                var versionIds = model.Prices.Select(x => x.ViewModel.PriceVersionId);
                if (!versionIds.Contains(lowestVersion.Id))
                    ModelState.AddModelError("NeedLowestError", $"Must have a price for the lowest price version ({lowestVersion.EffectiveDate.ToShortDateString()}).");
                if (versionIds.Count() > versionIds.Distinct().Count())
                    ModelState.AddModelError("NoMultipleError", $"Cannot have multiple prices for the same price version");
            }
        }
    }
}