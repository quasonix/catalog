﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class AntBandCombinationController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public AntBandCombinationController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var bandCombos = repo.GetAll<AntBandCombination>();
            return View(bandCombos);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new BandCombinationViewModel();
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(BandCombinationViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var bandCombo = mapper.Map<AntBandCombination>(model);

                foreach (var bandId in model.BandChecklist.SelectedValues)
                {
                    // add selected band
                    var selectedBand = repo.Get<AntBand>(bandId);
                    bandCombo.Bands.Add(selectedBand);

                    // add band combo to band, need both sides
                    selectedBand.BandCombinations.Add(bandCombo);
                }

                // create combo
                unitOfWork.BeginTransaction();
                repo.Create(bandCombo);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var bandCombo = repo.Get<AntBandCombination>(id);
            var model = mapper.Map<BandCombinationViewModel>(bandCombo);
            InflateViewModel(model, bandCombo);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(BandCombinationViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var bandCombo = repo.Get<AntBandCombination>(model.Id);
                mapper.Map(model, bandCombo);

                bandCombo.ClearBands();
                foreach (var bandId in model.BandChecklist.SelectedValues)
                {
                    // add selected band
                    var selectedBand = repo.Get<AntBand>(bandId);
                    bandCombo.Bands.Add(selectedBand);

                    // add band combo to band, need both sides
                    selectedBand.BandCombinations.Add(bandCombo);
                }

                // update combo
                unitOfWork.BeginTransaction();
                repo.Update(bandCombo);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var bandCombo = repo.Get<AntBandCombination>(id);
            var model = mapper.Map<BandCombinationDetailViewModel>(bandCombo);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var bandCombo = repo.Get<AntBandCombination>(id);
            var model = mapper.Map<DeleteViewModel>(bandCombo);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var bandCombo = repo.Get<AntBandCombination>(model.Id);

            unitOfWork.BeginTransaction();
            repo.Delete(bandCombo);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void InflateViewModel(BandCombinationViewModel model)
        {
            model.BandChecklist = repo.GetAll<AntBand>().ToMultiSelectChecklist(x => x.Name, x => x.Id, model.BandChecklist.SelectedValues);
        }

        private void InflateViewModel(BandCombinationViewModel model, AntBandCombination bandCombo)
        {
            model.BandChecklist = repo.GetAll<AntBand>().ToMultiSelectChecklist(x => x.Name, x => x.Id, bandCombo.Bands.Select(x => x.Id));
        }

        private void ValidateViewModel(BandCombinationViewModel model)
        {
            // check if band combo with name already exists, add error if not
            bool exists = repo.GetGroup<AntBandCombination>(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A band combination with the name {model.Name} already exists.");

            // check that at least one band selected
            if (model.BandChecklist.SelectedValues.Count == 0)
                ModelState.AddModelError("NoBandError", $"Must select at least one band");
        }
    }
}