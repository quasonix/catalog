﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class AntFocusArrangementController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public AntFocusArrangementController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var focuss = repo.GetAll<AntFocusArrangement>();
            return View(focuss);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new AntFocusArrangementViewModel();

            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(AntFocusArrangementViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map focus from model
                var focus = mapper.Map<AntFocusArrangement>(model);

                foreach (var feedId in model.FeedChecklist.SelectedValues)
                {
                    // add selected focus combo
                    var feed = repo.Get<AntFeed>(feedId);
                    focus.Feeds.Add(feed);

                    // add pedestal to focus combo, need both sides
                    feed.FocusArrangements.Add(focus);
                }

                // create focus
                unitOfWork.BeginTransaction();
                repo.Create(focus);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var focus = repo.Get<AntFocusArrangement>(id);
            var model = mapper.Map<AntFocusArrangementViewModel>(focus);

            InflateViewModel(model, focus);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(AntFocusArrangementViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get existing focus and map from model
                var focus = repo.Get<AntFocusArrangement>(model.Id);
                mapper.Map(model, focus);

                focus.ClearFeeds();
                foreach (var feedId in model.FeedChecklist.SelectedValues)
                {
                    // add selected focus combo
                    var feed = repo.Get<AntFeed>(feedId);
                    focus.Feeds.Add(feed);

                    // add pedestal to focus combo, need both sides
                    feed.FocusArrangements.Add(focus);
                }

                // update focus
                unitOfWork.BeginTransaction();
                repo.Update(focus);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var focus = repo.Get<AntFocusArrangement>(id);
            var model = mapper.Map<BandDetailViewModel>(focus);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var focus = repo.Get<AntFocusArrangement>(id);
            var model = mapper.Map<DeleteViewModel>(focus);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var focus = repo.Get<AntFocusArrangement>(model.Id);

            unitOfWork.BeginTransaction();
            repo.Delete(focus);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void InflateViewModel(AntFocusArrangementViewModel model)
        {
            model.FeedChecklist = repo.GetAll<AntFeed>()
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, model.FeedChecklist.SelectedValues);
        }

        private void InflateViewModel(AntFocusArrangementViewModel model, AntFocusArrangement focus)
        {
            model.FeedChecklist = repo.GetAll<AntFeed>()
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, focus.Feeds.Select(x => x.Id));
        }

        private void ValidateViewModel(AntFocusArrangementViewModel model)
        {
            // check if focus with name already exists, add error if not
            bool exists = repo.GetGroup<AntFocusArrangement>(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A focus with the name {model.Name} already exists.");
        }
    }
}