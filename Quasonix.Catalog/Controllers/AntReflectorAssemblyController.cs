﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class AntReflectorAssemblyController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public AntReflectorAssemblyController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var dishes = repo.GetAll<AntReflectorAssembly>();
            return View(dishes);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new AntReflectorAssemblyViewModel();

            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(AntReflectorAssemblyViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map dish from model
                var dish = mapper.Map<AntReflectorAssembly>(model);
                dish.FocusArrangement = repo.Get<AntFocusArrangement>(model.FocusArrangementId);

                // create dish
                unitOfWork.BeginTransaction();
                repo.Create(dish);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var dish = repo.Get<AntReflectorAssembly>(id);
            var model = mapper.Map<AntReflectorAssemblyViewModel>(dish);

            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(AntReflectorAssemblyViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get existing dish and map from model
                var dish = repo.Get<AntReflectorAssembly>(model.Id);
                mapper.Map(model, dish);
                dish.FocusArrangement = repo.Get<AntFocusArrangement>(model.FocusArrangementId);

                // update dish
                unitOfWork.BeginTransaction();
                repo.Update(dish);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var dish = repo.Get<AntReflectorAssembly>(id);
            var model = mapper.Map<BandDetailViewModel>(dish);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var dish = repo.Get<AntReflectorAssembly>(id);
            var model = mapper.Map<DeleteViewModel>(dish);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var dish = repo.Get<AntReflectorAssembly>(model.Id);

            unitOfWork.BeginTransaction();
            repo.Delete(dish);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void InflateViewModel(AntReflectorAssemblyViewModel model)
        {
            model.FocusArrangementOptions = repo.GetAll<AntFocusArrangement>().ToSelectItemList(x => x.Name, x => x.Id);
        }

        private void ValidateViewModel(AntReflectorAssemblyViewModel model)
        {
            // check if dish with name already exists, add error if not
            bool exists = repo.GetGroup<AntReflectorAssembly>(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A dish with the name {model.Name} already exists.");
        }
    }
}