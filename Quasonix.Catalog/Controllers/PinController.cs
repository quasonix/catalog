﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class PinController : Controller
    {
        private readonly ICatalogRepository<TxPin> pinRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public PinController(ICatalogRepository<TxPin> pinRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.pinRepo = pinRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var pins = pinRepo.GetAll();
            return View(pins);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new PinViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(PinViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var pin = mapper.Map<TxPin>(model);

                unitOfWork.BeginTransaction();
                pinRepo.Create(pin);
                unitOfWork.Commit();

                return RedirectToAction("Index"); 
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var pin = pinRepo.Get(id);
            var model = mapper.Map<PinViewModel>(pin);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(PinViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var pin = pinRepo.Get(model.Id);
                mapper.Map(model, pin);

                unitOfWork.BeginTransaction();
                pinRepo.Update(pin);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var pin = pinRepo.Get(id);
            var model = mapper.Map<PinDetailViewModel>(pin);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var pin = pinRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(pin);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var pin = pinRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            pinRepo.Delete(pin);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(PinViewModel model)
        {
            // check if pin with name already exists, add error if not
            bool exists = pinRepo.GetGroup(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A pin with the name {model.Name} already exists.");
        }
    }
}