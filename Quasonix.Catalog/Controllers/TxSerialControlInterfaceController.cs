﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class TxSerialControlInterfaceController : Controller
    {
        private readonly ICatalogRepository<TxSerialControlInterface> serialRepo;
        private readonly ICatalogRepository<TxPin> pinRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TxSerialControlInterfaceController(ICatalogRepository<TxSerialControlInterface> serialRepo,
            ICatalogRepository<TxPin> pinRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.serialRepo = serialRepo;
            this.pinRepo = pinRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var serials = serialRepo.GetAll();
            return View(serials);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new SerialControlInterfaceViewModel();
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(SerialControlInterfaceViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var serial = mapper.Map<TxSerialControlInterface>(model);

                unitOfWork.BeginTransaction();
                serialRepo.Create(serial);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var serial = serialRepo.Get(id);
            var model = mapper.Map<SerialControlInterfaceViewModel>(serial);
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(SerialControlInterfaceViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var serial = serialRepo.Get(model.Id);
                mapper.Map(model, serial);

                unitOfWork.BeginTransaction();
                serialRepo.Update(serial);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var serial = serialRepo.Get(id);
            var model = mapper.Map<SerialControlInterfaceDetailViewModel>(serial);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var serial = serialRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(serial);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var serial = serialRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            serialRepo.Delete(serial);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void InflateViewModel(SerialControlInterfaceViewModel model)
        {
        }

        private void ValidateViewModel(SerialControlInterfaceViewModel model)
        {
            // check if serial with name already exists, add error if not
            bool exists = serialRepo.GetGroup(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A serial interface with the name {model.Name} already exists.");
        }
    }
}