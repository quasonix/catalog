﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using DynamicVML;
using DynamicVML.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class TxPinoutController : Controller
    {
        private readonly ICatalogRepository<TxPinout> pinoutRepo;
        private readonly ICatalogRepository<TxDataConnector> dataConnectorRepo;
        private readonly ICatalogRepository<TxPin> pinRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TxPinoutController(ICatalogRepository<TxPinout> pinoutRepo,
            ICatalogRepository<TxDataConnector> dataConnectorRepo,
            ICatalogRepository<TxPin> pinRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.pinoutRepo = pinoutRepo;
            this.dataConnectorRepo = dataConnectorRepo;
            this.pinRepo = pinRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var pinouts = pinoutRepo.GetAll();
            return View(pinouts);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new PinoutViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(PinoutViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                unitOfWork.BeginTransaction();

                var pinout = new TxPinout()
                {
                    Name = model.Name
                };

                //foreach (var dataConnModel in model.DataConnectors)
                //{
                //    var dataConn = dataConnectorRepo.Get(dataConnModel.ViewModel.DataConnectorId);
                //    var dataConnForPinout = new DataConnectorForPinout()
                //    {
                //        Pinout = pinout,
                //        Rank = dataConnModel.ViewModel.Rank,
                //        DataConnector = dataConn,
                //        Pins = mapper.Map<List<TxDataConnectorPinUsage>>(dataConnModel.ViewModel.Pins).ToHashSet()
                //    };
                //    foreach (var pin in dataConnForPinout.Pins)
                //    {
                //        pin.DataConnectorForPinout = dataConnForPinout;
                //    }

                //    // add to both sides or won't save correctly
                //    pinout.DataConnectors.Add(dataConnForPinout);
                //    dataConn.DataConnectorsForPinouts.Add(dataConnForPinout);
                //}

                pinoutRepo.Create(pinout);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        public IActionResult Edit(Guid id)
        {
            var pinout = pinoutRepo.Get(id);
            var model = mapper.Map<PinoutViewModel>(pinout);
            var dataConnectorOptions = dataConnectorRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);
            var pinOptions = pinRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);

            //model.DataConnectors = pinout.DataConnectors.ToDynamicList(x =>
            //    mapper.Map(x, new DataConnectorForPinoutViewModel() { DataConnectorOptions = dataConnectorOptions, PinOptions = pinOptions }));

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(PinoutViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                unitOfWork.BeginTransaction();

                var pinout = pinoutRepo.Get(model.Id);
                pinout.Name = model.Name;

                //pinout.ClearDataConnectors();
                //foreach (var dataConnModel in model.DataConnectors)
                //{
                //    var dataConn = dataConnectorRepo.Get(dataConnModel.ViewModel.DataConnectorId);
                //    var dataConnForPinout = new DataConnectorForPinout()
                //    {
                //        Pinout = pinout,
                //        Rank = dataConnModel.ViewModel.Rank,
                //        DataConnector = dataConn,
                //        Pins = mapper.Map<List<TxDataConnectorPinUsage>>(dataConnModel.ViewModel.Pins).ToHashSet()
                //    };
                //    foreach (var pin in dataConnForPinout.Pins)
                //    {
                //        pin.DataConnectorForPinout = dataConnForPinout;
                //    }

                //    // add to both sides or won't save correctly
                //    pinout.DataConnectors.Add(dataConnForPinout);
                //    dataConn.DataConnectorsForPinouts.Add(dataConnForPinout);
                //}

                pinoutRepo.Update(pinout);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var pinout = pinoutRepo.Get(id);
            var model = mapper.Map<PinoutDetailViewModel>(pinout);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var pinout = pinoutRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(pinout);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var pinout = pinoutRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            pinoutRepo.Delete(pinout);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        public IActionResult AddDataConnector(AddNewDynamicItem parameters)
        {
            var model = new DataConnectorForPinoutViewModel();
            model.DataConnectorOptions = dataConnectorRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);
            model.PinOptions = pinRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);

            // use first connector, as that will be selected by default
            var firstConnector = dataConnectorRepo.Get(Guid.Parse(model.DataConnectorOptions.First().Value));
            for (int i = 0; i < firstConnector.PinCount; i++)
            {
                model.Pins.Add(new DataConnectorIncludedPinViewModel() { PinNumber = i + 1 });
            }
            return this.PartialView(model, parameters);
        }

        public IActionResult ChangeConnector (int connectorId)
        {
            var model = new DataConnectorForPinoutViewModel();
            model.PinOptions = pinRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);
            var connector = dataConnectorRepo.Get(connectorId);
            for (int i = 0; i < connector.PinCount; i++)
            {
                model.Pins.Add(new DataConnectorIncludedPinViewModel() { PinNumber = i + 1 });
            }
            return PartialView("_Pins", model);
        }

        private void InflateViewModel(PinoutViewModel model)
        {
            // get connector options
            var dataConnectorOptions = dataConnectorRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);
            var pinOptions = pinRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);

            foreach (var conn in model.DataConnectors)
            {
                conn.ViewModel.DataConnectorOptions = dataConnectorOptions;
                conn.ViewModel.PinOptions = pinOptions;
            }
        }

        private void ValidateViewModel(PinoutViewModel model)
        {
            // check if pinout with name already exists, add error if not
            bool exists = pinoutRepo.GetGroup(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A pinout with the name {model.Name} already exists.");

            // data conn validation
            if (model.DataConnectors == null || model.DataConnectors.Count < 1)
                ModelState.AddModelError("DataConnCountError", $"Must have at least one data connector.");
            else
            {
                var ranks = model.DataConnectors.ViewModels.Select(x => x.Rank);
                if (!ranks.Contains(ConnectorRank.Primary))
                    ModelState.AddModelError("NoPrimaryDataConnError", $"Must have a primary data connector.");
                if (ranks.Count(x => x == ConnectorRank.Primary) > 1)
                    ModelState.AddModelError("NoPrimaryDataConnError", $"Must have only one primary data connector.");
            }
        }
    }
}