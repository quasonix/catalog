﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class AntBandController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public AntBandController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var bands = repo.GetAll<AntBand>();
            return View(bands);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new BandViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(BandViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map band from model
                var band = mapper.Map<AntBand>(model);

                // create band
                unitOfWork.BeginTransaction();
                repo.Create(band);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var band = repo.Get<AntBand>(id);
            var model = mapper.Map<BandViewModel>(band);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(BandViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get existing band and map from model
                var band = repo.Get<AntBand>(model.Id);
                mapper.Map(model, band);

                // update band
                unitOfWork.BeginTransaction();
                repo.Update(band);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var band = repo.Get<AntBand>(id);
            var model = mapper.Map<BandDetailViewModel>(band);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var band = repo.Get<AntBand>(id);
            var model = mapper.Map<DeleteViewModel>(band);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var band = repo.Get<AntBand>(model.Id);

            unitOfWork.BeginTransaction();
            repo.Delete(band);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(BandViewModel model)
        {
            // check if band with name already exists, add error if not
            bool exists = repo.GetGroup<AntBand>(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A band with the name {model.Name} already exists.");
        }
    }
}