﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.AspNetCore.Mvc.Rendering;
//using Microsoft.Extensions.Logging;
//using Quasonix.Catalog.Entities;
//using Quasonix.Catalog.Models;

//namespace Quasonix.Catalog.Controllers
//{
//    [Authorize]
//    public class ConfigurationController : Controller
//    {
//        private readonly ILogger<HomeController> logger;
//        private readonly ICatalogRepository<Package> packageRepo;
//        private readonly ICatalogRepository<Stack> stackRepo;
//        private readonly ICatalogRepository<Pinout> pinoutRepo;
//        private readonly ICatalogRepository<ConfigurationOption> optionRepo;
//        private readonly ICatalogUnitOfWork unitOfWork;

//        public ConfigurationController(ILogger<HomeController> logger,
//            ICatalogRepository<Package> packageRepo,
//            ICatalogRepository<Stack> stackRepo,
//            ICatalogRepository<Pinout> pinoutRepo,
//            ICatalogRepository<ConfigurationOption> optionRepo,
//            ICatalogUnitOfWork unitOfWork)
//        {
//            this.logger = logger;
//            this.packageRepo = packageRepo;
//            this.stackRepo = stackRepo;
//            this.pinoutRepo = pinoutRepo;
//            this.optionRepo = optionRepo;
//            this.unitOfWork = unitOfWork;
//        }

//        public IActionResult Index()
//        {
//            var model = new ConfigureViewModel();
//            InflateModel(model);
//            return View(model);
//        }

//        [HttpPost]
//        public IActionResult Index(ConfigureViewModel model)
//        {
//            InflateModel(model);
//            return View(model);
//        }

//        private void InflateModel(ConfigureViewModel model)
//        {
//            var product = new ConfiguredProduct();

//            // get instances of selected options
//            var selectedOptions = optionRepo.GetGroup(x => model.SelectedOptions.Contains(x.Id)).ToList();
//            var stacks = stackRepo.GetAll().ToList();

//            if (selectedOptions.Count > 0)
//            {
//                // make sure stacks contain all selected options
//                stacks = stacks.Where(x => x.ConfigurationOptions.Intersect(selectedOptions).Count() == selectedOptions.Count).ToList();

//                product.BandCombination = (BandCombination)selectedOptions.FirstOrDefault(x => x.Type == OptionType.BandCombination);
//                product.Baseband = (Baseband)selectedOptions.FirstOrDefault(x => x.Type == OptionType.Baseband);
//                product.SerialControlInterface = (SerialControlInterface)selectedOptions.FirstOrDefault(x => x.Type == OptionType.Serial);
//                product.RFPower = (RFPower)selectedOptions.FirstOrDefault(x => x.Type == OptionType.Power);
//            }

//            // get packages from stacks
//            var packages = stacks.SelectMany(x => x.Packages);

//            if (model.SelectedFootprint != null)
//                packages = packages.Where(x => x.Footprint.Id == model.SelectedFootprint);
//            if (model.SelectedHeight != null)
//                packages = packages.Where(x => x.Height.Id == model.SelectedHeight);
//            if (model.SelectedDataConnectorPosition != null)
//                packages = packages.Where(x => x.DataConnectors.Any(dc => dc.Rank == ConnectorRank.Primary && dc.Position == (ConnectorPosition)model.SelectedDataConnectorPosition));
//            if (model.SelectedRfConnectorPosition != null)
//                packages = packages.Where(x => x.RfConnectors.Any(rfc => rfc.Rank == ConnectorRank.Primary && rfc.Position == (ConnectorPosition)model.SelectedRfConnectorPosition));
            
//            model.HeightOptions = packages.Select(x => x.Height).Distinct().ToSelectItemList(x => x.Name, x => x.Id);
//            model.FootprintOptions = packages.Select(x => x.Footprint).Distinct().ToSelectItemList(x => x.Name, x => x.Id);
//            model.DataConnectorPositionOptions = packages.SelectMany(x => x.DataConnectors)
//                .Where(x => x.Rank == ConnectorRank.Primary)
//                .Select(x => x.Position).Distinct()
//                .ToSelectItemList(x => x.ToString(), x => (int)x);
//            model.RfConnectorPositionOptions = packages.SelectMany(x => x.RfConnectors)
//                .Where(x => x.Rank == ConnectorRank.Primary)
//                .Select(x => x.Position).Distinct()
//                .ToSelectItemList(x => x.ToString(), x => (int)x);

//            // reload stacks after packages filtered
//            stacks = packages.SelectMany(x => x.Stacks).Distinct().ToList();

//            // get options from stacks
//            var options = stacks.SelectMany(x => x.ConfigurationOptions).Distinct().ToList();

//            // loop through option matrix to remove any conflicts
//            foreach (var option in selectedOptions)
//            {
//                options.RemoveAll(x => !option.Options.Contains(x));
//            }

//            model.ClockInterfaceOptions = options.Where(x => x.Type == OptionType.Baseband).ToSelectItemList(x => x.Name, x => x.Id);
//            model.SerialControlOptions = options.Where(x => x.Type == OptionType.Serial).ToSelectItemList(x => x.Name, x => x.Id);
//            model.RfPowerOptions = options.Where(x => x.Type == OptionType.Power).ToSelectItemList(x => x.Name, x => x.Id);
//            model.HardwareSpecificOptions = options.Where(x => x.Type == OptionType.Other).ToSelectItemList(x => x.Name, x => x.Id);

//            var bandCombos = options.Where(x => x.Type == OptionType.BandCombination)
//                .Select(x => (BandCombination)x);

//            foreach (var band in model.SelectedBands)
//            {
//                bandCombos = bandCombos.Where(x => x.Bands.Any(x => x.Name == band));
//            }

//            foreach (var combo in bandCombos)
//            {
//                model.BandCombos.Add(new BandComboSelectionViewModel()
//                {
//                    HasLowerL = combo.Bands.Any(x => x.Name == "Lower L"),
//                    HasUpperL = combo.Bands.Any(x => x.Name == "Upper L"),
//                    HasLowerS = combo.Bands.Any(x => x.Name == "Lower S"),
//                    HasUpperS = combo.Bands.Any(x => x.Name == "Upper S"),
//                    HasC = combo.Bands.Any(x => x.Name == "C Band"),
//                    HasMidC = combo.Bands.Any(x => x.Name == "Mid C"),
//                    HasEuroMidC = combo.Bands.Any(x => x.Name == "Euro Mid C"),
//                    Name = combo.Name,
//                    Id = combo.Id
//                });
//            }

//            var reqPins = selectedOptions.SelectMany(x => x.Pins).ToList();
//            var pinouts = pinoutRepo.GetAll().ToList();
//            if (reqPins.Count > 0)
//                pinouts = pinouts.Where(x => x.DataConnectors.SelectMany(x => x.Pins.Select(p => p.Pin)).Any(reqPins.Contains)).ToList();

//            if (packages.Count() == 1)
//            {
//                product.Package = packages.First();
//                foreach (var dataConn in product.Package.DataConnectors)
//                {
//                    pinouts = pinouts.Where(x => x.DataConnectors.Any(x => x.Rank == dataConn.Rank
//                        && x.DataConnector == dataConn.DataConnector)).ToList();
//                }

//                if (pinouts.Count > 0)
//                    product.Pinout = pinouts.First();
//            }

//            product.HasArtm0 = model.HasArtm0;
//            product.HasArtm1 = model.HasArtm1;
//            product.HasArtm2 = model.HasArtm2;
//            product.HasBPSK = model.HasBPSK;

//            model.Product = product;
//        }
//    }
//}