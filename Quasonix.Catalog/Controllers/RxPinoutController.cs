﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class RxPinoutController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public RxPinoutController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var pinouts = repo.GetAll<RxPinout>();
            return View(pinouts);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new RxPinoutViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(RxPinoutViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map pinout from model
                var pinout = mapper.Map<RxPinout>(model);

                // create pinout
                unitOfWork.BeginTransaction();
                repo.Create(pinout);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var pinout = repo.Get<RxPinout>(id);
            var model = mapper.Map<RxPinoutViewModel>(pinout);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(RxPinoutViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get existing pinout and map from model
                var pinout = repo.Get<RxPinout>(model.Id);
                mapper.Map(model, pinout);

                // update pinout
                unitOfWork.BeginTransaction();
                repo.Update(pinout);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var pinout = repo.Get<RxPinout>(id);
            var model = mapper.Map<BandDetailViewModel>(pinout);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var pinout = repo.Get<RxPinout>(id);
            var model = mapper.Map<DeleteViewModel>(pinout);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var pinout = repo.Get<RxPinout>(model.Id);

            unitOfWork.BeginTransaction();
            repo.Delete(pinout);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(RxPinoutViewModel model)
        {
            // check if pinout with name already exists, add error if not
            bool exists = repo.GetGroup<RxPinout>(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A pinout with the name {model.Name} already exists.");
        }
    }
}