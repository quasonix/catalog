﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class AntPedestalController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public AntPedestalController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var pedestals = repo.GetAll<AntPedestal>();
            return View(pedestals);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new AntPedestalViewModel();
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(AntPedestalViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map pedestal from model
                var pedestal = mapper.Map<AntPedestal>(model);

                foreach (var dishId in model.ReflectorAssemblyChecklist.SelectedValues)
                {
                    // add selected band combo
                    var dish = repo.Get<AntReflectorAssembly>(dishId);
                    pedestal.ReflectorAssemblies.Add(dish);

                    // add pedestal to band combo, need both sides
                    dish.Pedestals.Add(pedestal);
                }

                foreach (var wiringId in model.WiringChecklist.SelectedValues)
                {
                    // add selected band combo
                    var wiring = repo.Get<AntWiring>(wiringId);
                    pedestal.Wirings.Add(wiring);

                    // add pedestal to band combo, need both sides
                    wiring.Pedestals.Add(pedestal);
                }

                foreach (var optionId in model.OptionChecklist.SelectedValues)
                {
                    // add selected band combo
                    var option = repo.Get<AntOption>(optionId);
                    pedestal.Options.Add(option);

                    // add pedestal to band combo, need both sides
                    option.Pedestals.Add(pedestal);
                }

                // create pedestal
                unitOfWork.BeginTransaction();
                repo.Create(pedestal);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var pedestal = repo.Get<AntPedestal>(id);
            var model = mapper.Map<AntPedestalViewModel>(pedestal);
            InflateViewModel(model, pedestal);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(AntPedestalViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get existing pedestal and map from model
                var pedestal = repo.Get<AntPedestal>(model.Id);
                mapper.Map(model, pedestal);

                pedestal.ClearReflectorAssemblies();
                foreach (var dishId in model.ReflectorAssemblyChecklist.SelectedValues)
                {
                    // add selected band combo
                    var dish = repo.Get<AntReflectorAssembly>(dishId);
                    pedestal.ReflectorAssemblies.Add(dish);

                    // add pedestal to band combo, need both sides
                    dish.Pedestals.Add(pedestal);
                }

                pedestal.ClearWirings();
                foreach (var wiringId in model.WiringChecklist.SelectedValues)
                {
                    // add selected band combo
                    var wiring = repo.Get<AntWiring>(wiringId);
                    pedestal.Wirings.Add(wiring);

                    // add pedestal to band combo, need both sides
                    wiring.Pedestals.Add(pedestal);
                }

                pedestal.ClearOptions();
                foreach (var optionId in model.OptionChecklist.SelectedValues)
                {
                    // add selected band combo
                    var option = repo.Get<AntOption>(optionId);
                    pedestal.Options.Add(option);

                    // add pedestal to band combo, need both sides
                    option.Pedestals.Add(pedestal);
                }

                // update pedestal
                unitOfWork.BeginTransaction();
                repo.Update(pedestal);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var pedestal = repo.Get<AntPedestal>(id);
            var model = mapper.Map<AntPedestalDetailViewModel>(pedestal);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var pedestal = repo.Get<AntPedestal>(id);
            var model = mapper.Map<DeleteViewModel>(pedestal);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var pedestal = repo.Get<AntPedestal>(model.Id);

            unitOfWork.BeginTransaction();
            repo.Delete(pedestal);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void InflateViewModel(AntPedestalViewModel model)
        {
            model.ReflectorAssemblyChecklist = repo.GetAll<AntReflectorAssembly>().OrderBy(x => x.Diameter)
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, model.ReflectorAssemblyChecklist.SelectedValues);
            model.WiringChecklist = repo.GetAll<AntWiring>()
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, model.ReflectorAssemblyChecklist.SelectedValues);
            model.OptionChecklist = repo.GetAll<AntOption>()
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, model.ReflectorAssemblyChecklist.SelectedValues);
        }

        private void InflateViewModel(AntPedestalViewModel model, AntPedestal pedestal)
        {
            model.ReflectorAssemblyChecklist = repo.GetAll<AntReflectorAssembly>()
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, pedestal.ReflectorAssemblies.Select(x => x.Id));
            model.WiringChecklist = repo.GetAll<AntWiring>()
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, pedestal.Wirings.Select(x => x.Id));
            model.OptionChecklist = repo.GetAll<AntOption>()
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, pedestal.Options.Select(x => x.Id));
        }

        private void ValidateViewModel(AntPedestalViewModel model)
        {
            // check if pedestal with name already exists, add error if not
            bool exists = repo.GetGroup<AntPedestal>(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A pedestal with the name {model.Name} already exists.");
        }
    }
}