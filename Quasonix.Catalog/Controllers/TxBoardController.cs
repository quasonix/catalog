﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class TxBoardController : Controller
    {
        private readonly ICatalogRepository<TxBoard> boardRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TxBoardController(ICatalogRepository<TxBoard> boardRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.boardRepo = boardRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var boards = boardRepo.GetAll();
            var model = mapper.Map<IEnumerable<BoardViewModel>>(boards);
            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new BoardViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(BoardViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var board = mapper.Map<TxBoard>(model);

                unitOfWork.BeginTransaction();
                boardRepo.Create(board);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var board = boardRepo.Get(id);
            var model = mapper.Map<BoardViewModel>(board);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(BoardViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var board = boardRepo.Get(model.Id);
                mapper.Map(model, board);

                unitOfWork.BeginTransaction();
                boardRepo.Update(board);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var board = boardRepo.Get(id);
            var model = mapper.Map<BoardDetailViewModel>(board);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var board = boardRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(board);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var board = boardRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            boardRepo.Delete(board);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(BoardViewModel model)
        {
            // check if board with name already exists, add error if not
            bool exists = boardRepo.GetGroup(x => x.ManufacturingName == model.ManufacturingName && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A board with the manufacturing name {model.ManufacturingName} already exists.");
        }
    }
}