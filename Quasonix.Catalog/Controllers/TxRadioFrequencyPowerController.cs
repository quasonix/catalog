﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class TxRadioFrequencyPowerController : Controller
    {
        private readonly ICatalogRepository<TxRadioFrequencyPower> rfRepo;
        private readonly ICatalogRepository<TxPin> pinRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TxRadioFrequencyPowerController(ICatalogRepository<TxRadioFrequencyPower> rfRepo,
            ICatalogRepository<TxPin> pinRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.rfRepo = rfRepo;
            this.pinRepo = pinRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var rfPowers = rfRepo.GetAll();
            return View(rfPowers);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new RfPowerViewModel();
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(RfPowerViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var rfPower = mapper.Map<TxRadioFrequencyPower>(model);

                unitOfWork.BeginTransaction();
                rfRepo.Create(rfPower);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var rfPower = rfRepo.Get(id);
            var model = mapper.Map<RfPowerViewModel>(rfPower);
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(RfPowerViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var rfPower = rfRepo.Get(model.Id);
                mapper.Map(model, rfPower);

                unitOfWork.BeginTransaction();
                rfRepo.Update(rfPower);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var rfPower = rfRepo.Get(id);
            var model = mapper.Map<RfPowerDetailViewModel>(rfPower);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var rfPower = rfRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(rfPower);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var rfPower = rfRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            rfRepo.Delete(rfPower);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void InflateViewModel(RfPowerViewModel model)
        {
        }

        private void ValidateViewModel(RfPowerViewModel model)
        {
            // check if rf power with name already exists, add error if not
            bool exists = rfRepo.GetGroup(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"An RF power with the name {model.Name} already exists.");
        }
    }
}