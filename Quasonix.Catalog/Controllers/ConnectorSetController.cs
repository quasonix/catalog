﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using DynamicVML;
using DynamicVML.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class ConnectorSetController : Controller
    {
        private readonly ICatalogRepository<RxConnectorSet> connSetRepo;
        private readonly ICatalogRepository<Connector> connRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public ConnectorSetController(ICatalogRepository<RxConnectorSet> connSetRepo,
            ICatalogRepository<Connector> connRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.connSetRepo = connSetRepo;
            this.connRepo = connRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var connSets = connSetRepo.GetAll();
            return View(connSets);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new ConnectorSetViewModel();
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(ConnectorSetViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                unitOfWork.BeginTransaction();

                var connSet = mapper.Map<RxConnectorSet>(model);

                foreach (var connModel in model.Connectors)
                {
                    var conn = connRepo.Get(connModel.ViewModel.ConnectorId);
                    var connForSet = new ConnectorForConnectorSet()
                    {
                        ConnectorSet = connSet,
                        Connector = conn,
                        Count = connModel.ViewModel.Count
                    };
                    connSet.Connectors.Add(connForSet);
                    conn.ConnectorsForSet.Add(connForSet);
                }

                connSetRepo.Create(connSet);

                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        public IActionResult Edit(Guid id)
        {
            var connSet = connSetRepo.Get(id);
            var model = mapper.Map<ConnectorSetViewModel>(connSet);

            model.Connectors = connSet.Connectors.ToDynamicList(x =>
                mapper.Map<ConnectorForConnectorSetViewModel>(x));

            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(ConnectorSetViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                unitOfWork.BeginTransaction();

                var connSet = connSetRepo.Get(model.Id);
                connSet.Description = model.Description;

                connSet.ClearConnectors();
                foreach (var connModel in model.Connectors)
                {
                    var conn = connRepo.Get(connModel.ViewModel.ConnectorId);
                    var connForSet = new ConnectorForConnectorSet()
                    {
                        ConnectorSet = connSet,
                        Connector = conn,
                        Count = connModel.ViewModel.Count
                    };
                    connSet.Connectors.Add(connForSet);
                    conn.ConnectorsForSet.Add(connForSet);
                }

                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            return View();
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var connSet = connSetRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(connSet);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var connSet = connSetRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            connSet.ClearConnectors();
            connSetRepo.Update(connSet);
            connSetRepo.Delete(connSet);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        public IActionResult AddConnector(AddNewDynamicItem parameters)
        {
            var model = new ConnectorForConnectorSetViewModel();
            model.ConnectorOptions = connRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);
            model.Count = 1;
            return this.PartialView(model, parameters);
        }

        private void InflateViewModel(ConnectorSetViewModel model)
        {
            var connOptions = connRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);

            foreach (var dataConn in model.Connectors)
            {
                dataConn.ViewModel.ConnectorOptions = connOptions;
            }
        }

        private void ValidateViewModel(ConnectorSetViewModel model)
        {
            if (model.Connectors.Any(x => x.ViewModel.Count <= 0))
                ModelState.AddModelError("ConnCountError", $"Connector count must be greater than 0.");
        }
    }
}