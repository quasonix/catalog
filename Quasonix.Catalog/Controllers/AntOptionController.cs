﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DynamicVML;
using DynamicVML.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class AntOptionController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public AntOptionController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var options = repo.GetAll<AntOption>();
            return View(options);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new OptionViewModel();
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(OptionViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map entity from model
                var option = mapper.Map<AntOption>(model);

                // create in db
                unitOfWork.BeginTransaction();
                repo.Create(option);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var option = repo.Get<AntOption>(id);
            var model = mapper.Map<OptionViewModel>(option);

            model.Prices = option.Prices.OrderBy(x => x.PriceVersion.EffectiveDate).ToDynamicList(x =>
                mapper.Map<PriceViewModel>(x));

            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(OptionViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get from db, map from model
                var option = repo.Get<AntOption>(model.Id);
                mapper.Map(model, option);

                // update
                unitOfWork.BeginTransaction();
                repo.Update(option);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var other = repo.Get<AntOption>(id);
            var model = mapper.Map<OptionDetailViewModel>(other);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var other = repo.Get<AntOption>(id);
            var model = mapper.Map<DeleteViewModel>(other);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var other = repo.Get<AntOption>(model.Id);

            unitOfWork.BeginTransaction();
            repo.Delete(other);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        public IActionResult AddBinding(AddNewDynamicItem parameters)
        {
            var model = new OptionBindingViewModel();
            model.Options = repo.GetAll<AntOption>().ToSelectItemList(x => x.Name, x => x.Id);
            return this.PartialView(model, parameters);
        }

        private void InflateViewModel(OptionViewModel model)
        {
            var options = repo.GetAll<AntOption>().ToSelectItemList(x => x.Name, x => x.Id);

            foreach (var option in model.Bindings)
            {
                option.ViewModel.Options = options;
            }
        }

        private void ValidateViewModel(OptionViewModel model)
        {
            // check if option with name already exists, add error if not
            bool exists = repo.GetGroup<AntOption>(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"An option with the name {model.Name} already exists.");
        }
    }
}