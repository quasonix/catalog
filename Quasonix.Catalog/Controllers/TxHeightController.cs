﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class TxHeightController : Controller
    {
        private readonly ICatalogRepository<TxHeight> heightRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TxHeightController(ICatalogRepository<TxHeight> heightRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.heightRepo = heightRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var heights = heightRepo.GetAll();
            return View(heights);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new HeightViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(HeightViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map from model
                var height = mapper.Map<TxHeight>(model);

                // create height in db
                unitOfWork.BeginTransaction();
                heightRepo.Create(height);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            // get from db, map from view model
            var height = heightRepo.Get(id);
            var model = mapper.Map<HeightViewModel>(height);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(HeightViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get from db, map from view model
                var height = heightRepo.Get(model.Id);
                mapper.Map(model, height);

                // update
                unitOfWork.BeginTransaction();
                heightRepo.Update(height);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var height = heightRepo.Get(id);
            var model = mapper.Map<HeightDetailViewModel>(height);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var height = heightRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(height);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var height = heightRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            heightRepo.Delete(height);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(HeightViewModel model)
        {
            // check if height with name already exists, add error if not
            bool exists = heightRepo.GetGroup(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A height with the name {model.Name} already exists.");
        }
    }
}