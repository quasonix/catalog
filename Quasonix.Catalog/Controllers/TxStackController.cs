﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DynamicVML;
using DynamicVML.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class TxStackController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogRepository<TxStack> stackRepo;
        private readonly ICatalogRepository<TxBoard> boardRepo;
        private readonly ICatalogRepository<TxRadioFrequencyPower> powerRepo;
        private readonly ICatalogRepository<TxSerialControlInterface> serialRepo;
        private readonly ICatalogRepository<TxBaseband> basebandRepo;
        private readonly ICatalogRepository<TxBandCombination> bandComboRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TxStackController(ICatalogRepository repo,
            ICatalogRepository<TxStack> stackRepo,
            ICatalogRepository<TxBoard> boardRepo,
            ICatalogRepository<TxRadioFrequencyPower> powerRepo,
            ICatalogRepository<TxSerialControlInterface> serialRepo,
            ICatalogRepository<TxBaseband> basebandRepo,
            ICatalogRepository<TxBandCombination> bandComboRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.stackRepo = stackRepo;
            this.boardRepo = boardRepo;
            this.powerRepo = powerRepo;
            this.serialRepo = serialRepo;
            this.basebandRepo = basebandRepo;
            this.bandComboRepo = bandComboRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var stacks = stackRepo.GetAll()
                .OrderBy(x => x.Name);
            return View(stacks);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new TxStackViewModel();
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(TxStackViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var stack = mapper.Map<TxStack>(model);
                stack.RadioFrequencyPower = powerRepo.Get(model.RFPowerId);
                stack.BandCombination = bandComboRepo.Get(model.BandCombinationId);
                stack.Package = repo.Get<TxPackage>(model.PackageId);

                foreach (var boardModel in model.Boards)
                {
                    var board = repo.Get<TxBoard>(boardModel.ViewModel.Id);
                    stack.Boards.Add(board);
                    board.Stacks.Add(stack);
                }

                foreach (var basebandModel in model.Basebands)
                {
                    var baseband = repo.Get<TxBaseband>(basebandModel.ViewModel.Id);
                    baseband.Stacks.Add(stack);
                    stack.Basebands.Add(baseband);
                }
                
                foreach (var serialModel in model.SerialControlInterfaces)
                {
                    var serial = repo.Get<TxSerialControlInterface>(serialModel.ViewModel.Id);
                    serial.Stacks.Add(stack);
                    stack.SerialControlInterfaces.Add(serial);
                }

                unitOfWork.BeginTransaction();
                stackRepo.Create(stack);
                unitOfWork.Commit();

                return RedirectToAction("Index"); 
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var stack = stackRepo.Get(id);
            var model = mapper.Map<TxStackViewModel>(stack);
            model.Boards = stack.Boards.ToDynamicList(x =>
                mapper.Map<StackBoardViewModel>(x));
            model.Basebands = stack.Basebands.ToDynamicList(x =>
                mapper.Map<StackBasebandViewModel>(x));
            model.SerialControlInterfaces = stack.SerialControlInterfaces.ToDynamicList(x =>
                mapper.Map<StackSerialControlInterfaceViewModel>(x));
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(TxStackViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                var stack = stackRepo.Get(model.Id);
                stack.Name = model.Name;
                stack.RadioFrequencyPower = powerRepo.Get(model.RFPowerId);
                stack.BandCombination = bandComboRepo.Get(model.BandCombinationId);
                stack.Package = repo.Get<TxPackage>(model.PackageId);

                stack.ClearBoards();
                foreach (var boardModel in model.Boards)
                {
                    var board = repo.Get<TxBoard>(boardModel.ViewModel.Id);
                    stack.Boards.Add(board);
                    board.Stacks.Add(stack);
                }

                stack.ClearBasebands();
                foreach (var basebandModel in model.Basebands)
                {
                    var baseband = repo.Get<TxBaseband>(basebandModel.ViewModel.Id);
                    baseband.Stacks.Add(stack);
                    stack.Basebands.Add(baseband);
                }

                stack.ClearSerialControlInterfaces();
                foreach (var serialModel in model.SerialControlInterfaces)
                {
                    var serial = repo.Get<TxSerialControlInterface>(serialModel.ViewModel.Id);
                    serial.Stacks.Add(stack);
                    stack.SerialControlInterfaces.Add(serial);
                }

                unitOfWork.BeginTransaction();
                stackRepo.Update(stack);
                unitOfWork.Commit();

                return RedirectToAction("Index"); 
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var stack = stackRepo.Get(id);
            var model = mapper.Map<StackDetailViewModel>(stack);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var stack = stackRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(stack);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var stack = stackRepo.Get(model.Id);

            foreach (var board in stack.Boards)
            {
                board.Stacks.Remove(stack);
            }

            unitOfWork.BeginTransaction();
            stackRepo.Delete(stack);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        public IActionResult AddBoard(AddNewDynamicItem parameters)
        {
            var model = new StackBoardViewModel();
            model.BoardOptions = repo.GetAll<TxBoard>().ToSelectItemList(x => x.FullName, x => x.Id);
            return this.PartialView(model, parameters);
        }

        public IActionResult AddBaseband(AddNewDynamicItem parameters)
        {
            var model = new StackBasebandViewModel();
            model.BasebandOptions = repo.GetAll<TxBaseband>().ToSelectItemList(x => x.Name, x => x.Id);
            return this.PartialView(model, parameters);
        }

        public IActionResult AddSerialControlInterface(AddNewDynamicItem parameters)
        {
            var model = new StackSerialControlInterfaceViewModel();
            model.SerialControlInterfaceOptions = repo.GetAll<TxSerialControlInterface>().ToSelectItemList(x => x.Name, x => x.Id);
            return this.PartialView(model, parameters);
        }

        private void InflateViewModel(TxStackViewModel model)
        {
            model.BoardOptions = boardRepo.GetAll().ToSelectItemList(x => x.FullName, x => x.Id);
            model.RfPowerOptions = powerRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);
            model.BandCombinationOptions = bandComboRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);
            model.PackageOptions = repo.GetAll<TxPackage>().ToSelectItemList(x => x.Name, x => x.Id);

            var boardOptions = repo.GetAll<TxBoard>().ToSelectItemList(x => x.FullName, x => x.Id);
            var basebandOptions = repo.GetAll<TxBaseband>().ToSelectItemList(x => x.Name, x => x.Id);
            var serialOptions = repo.GetAll<TxSerialControlInterface>().ToSelectItemList(x => x.Name, x => x.Id);

            foreach (var board in model.Boards)
            {
                board.ViewModel.BoardOptions = boardOptions;
            }
            foreach (var baseband in model.Basebands)
            {
                baseband.ViewModel.BasebandOptions = basebandOptions;
            }
            foreach (var serial in model.SerialControlInterfaces)
            {
                serial.ViewModel.SerialControlInterfaceOptions = serialOptions;
            }
        }

        private void ValidateViewModel(TxStackViewModel model)
        {
            // check if stack with name already exists, add error if not
            bool exists = stackRepo.GetGroup(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A stack with the name {model.Name} already exists.");
        }
    }
}