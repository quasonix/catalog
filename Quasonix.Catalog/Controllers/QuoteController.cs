﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;
using Quasonix.Catalog.Services;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class QuoteController : Controller
    {
        private readonly ICatalogRepository<User> userRepo;
        private readonly ICatalogRepository<Product> productRepo;
        private readonly ICatalogRepository<TxStack> stackRepo;
        private readonly ICatalogRepository<TxPinout> pinoutRepo;
        private readonly ICatalogRepository<TxBandCombination> bandComboRepo;
        private readonly ICatalogRepository<TxBaseband> basebandRepo;
        private readonly ICatalogRepository<TxRadioFrequencyPower> powerRepo;
        private readonly ICatalogRepository<TxSerialControlInterface> serialRepo;
        private readonly ICatalogRepository<PriceVersion> priceVersionRepo;
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly ISpecSheetGenerator specSheetGenerator;
        private readonly IQuoteDocumentGenerator quoteDocumentGenerator;
        private readonly TransmitterFilter filter;
        private readonly IEmailSender emailSender;
        private readonly IMapper mapper;

        public QuoteController(ICatalogRepository<User> userRepo,
            ICatalogRepository<Product> productRepo,
            ICatalogRepository<TxStack> stackRepo,
            ICatalogRepository<TxPinout> pinoutRepo,
            ICatalogRepository<TxBandCombination> bandComboRepo,
            ICatalogRepository<TxBaseband> basebandRepo,
            ICatalogRepository<TxRadioFrequencyPower> powerRepo,
            ICatalogRepository<TxSerialControlInterface> serialRepo,
            ICatalogRepository<PriceVersion> priceVersionRepo,
            ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            ISpecSheetGenerator specSheetGenerator,
            IQuoteDocumentGenerator quoteDocumentGenerator,
            TransmitterFilter filter,
            IEmailSender emailSender,
            IMapper mapper)
        {
            this.userRepo = userRepo;
            this.productRepo = productRepo;
            this.stackRepo = stackRepo;
            this.pinoutRepo = pinoutRepo;
            this.bandComboRepo = bandComboRepo;
            this.basebandRepo = basebandRepo;
            this.powerRepo = powerRepo;
            this.serialRepo = serialRepo;
            this.priceVersionRepo = priceVersionRepo;
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.specSheetGenerator = specSheetGenerator;
            this.quoteDocumentGenerator = quoteDocumentGenerator;
            this.filter = filter;
            this.emailSender = emailSender;
            this.mapper = mapper;
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult Index()
        {
            var evtms = repo.GetAll<Quote>();
            return View(evtms);
        }

        [HttpPost]
        public IActionResult Create()
        {
            var user = userRepo.Get(x => x.UserName == User.Identity.Name);

            unitOfWork.BeginTransaction();
            foreach (var quote in user.Quotes)
            {
                quote.Active = false;
                repo.Update(quote);
            }

            // get where the quote number starts
            int numPosition = user.RepNumber.Length + 1;
            int quoteNum = 1000;

            // get highest number and increment
            if (user.Quotes.Any())
                quoteNum = user.Quotes.Select(x => Convert.ToInt32(x.Number[numPosition..])).Max() + 1;

            var newQuote = new Quote()
            {
                Number = $"{user.RepNumber}-{quoteNum}",
                User = user,
                Active = true,
                StartDate = DateTime.Now,
                PriceVersion = priceVersionRepo.GetAll().OrderByDescending(x => x.EffectiveDate).First()
            };
            repo.Create(newQuote);
            unitOfWork.Commit();

            return RedirectToAction("Cart");
        }

        [HttpGet]
        public IActionResult Cart(Guid id)
        {
            Quote quote;

            if (id != Guid.Empty && User.IsInRole("admin"))
            {
                quote = repo.Get<Quote>(id);
            }
            else
            {
                var user = userRepo.Get(x => x.UserName == User.Identity.Name);
                quote = user.Quotes.FirstOrDefault(x => x.Active);
            }

            if (quote == null)
                return RedirectToAction("Index", "Home");

            var model = mapper.Map<QuoteViewModel>(quote);

            foreach (var line in model.IncludedLines)
            {
                var quoteLine = quote.Lines.First(x => x.Id == line.Id);

                var product = productRepo.Get(line.ProductId);

                if (quote.ShippingZone != null)
                {
                    line.UnitPrice = product.GetPrice(quote.PriceVersion, line.Quantity, quote.ShippingZone);
                    line.ExtendedPrice = product.ExtendedPrice;
                    line.PriceDetails = product.PriceDetails;
                    line.HasPrice = true;
                }
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Open(Guid id)
        {
            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var quoteToActivate = user.Quotes.FirstOrDefault(x => x.Id == id);

            if (quoteToActivate != null)
            {
                unitOfWork.BeginTransaction();

                foreach (var quote in user.Quotes)
                {
                    quote.Active = quote == quoteToActivate ? true : false;
                    repo.Update(quote);
                }

                unitOfWork.Commit();
            }

            return RedirectToAction("Cart");
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var quoteToDelete = user.Quotes.FirstOrDefault(x => x.Id == id);

            if (quoteToDelete != null)
            {
                var model = mapper.Map<DeleteViewModel>(quoteToDelete);
                return View(model);
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var quoteToDelete = user.Quotes.FirstOrDefault(x => x.Id == model.Id);

            if (quoteToDelete != null)
            {
                unitOfWork.BeginTransaction();
                user.Quotes.Remove(quoteToDelete);
                quoteToDelete.ClearLines();
                repo.Delete(quoteToDelete);
                unitOfWork.Commit();
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public IActionResult UpdateCustomerInfo(QuoteViewModel model)
        {
            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Id == model.Id);

            if (activeQuote != null)
            {
                activeQuote.CustomerName = model.CustomerName;
                activeQuote.CustomerLocation = model.CustomerLocation;
                activeQuote.OrderType = model.OrderType;
                activeQuote.ShippingZone = model.ShippingZone;
                activeQuote.PurchaserEmail = model.PurchaserEmail;
                activeQuote.AdditionalInformation = model.AdditionalInformation;

                unitOfWork.BeginTransaction();
                repo.Update(activeQuote);
                unitOfWork.Commit();
            }

            return RedirectToAction("Cart");
        }

        [HttpPost]
        public IActionResult UpdatePaymentTerms(QuoteViewModel model)
        {
            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Id == model.Id);

            if (activeQuote != null)
            {
                activeQuote.PaymentTerms = model.PaymentTerms;

                unitOfWork.BeginTransaction();
                repo.Update(activeQuote);
                unitOfWork.Commit();
            }

            return RedirectToAction("Cart");
        }

        [HttpPost]
        public IActionResult UpdateCoverText(QuoteViewModel model)
        {
            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Id == model.Id);

            if (activeQuote != null)
            {
                activeQuote.CoverText = model.CoverText;

                unitOfWork.BeginTransaction();
                repo.Update(activeQuote);
                unitOfWork.Commit();
            }

            return RedirectToAction("Cart");
        }

        [HttpPost]
        public IActionResult UpdateDescription(QuoteViewModel model)
        {
            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Id == model.Id);

            if (activeQuote != null)
            {
                activeQuote.Description = model.Description;

                unitOfWork.BeginTransaction();
                repo.Update(activeQuote);
                unitOfWork.Commit();
            }

            return RedirectToAction("Cart");
        }

        [HttpGet]
        public IActionResult ConfigureTx(Guid lineId)
        {
            ConfigureTxViewModel model;
            if (lineId != Guid.Empty)
            {
                var user = userRepo.Get(x => x.UserName == User.Identity.Name);
                var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);
                var quoteLine = activeQuote.Lines.First(x => x.Id == lineId);
                model = JsonConvert.DeserializeObject<ConfigureTxViewModel>(quoteLine.ConfigurationJson);
                model.LineId = lineId;
            }
            else
            {
                model = new ConfigureTxViewModel();
            }

            InflateTxModel(model);

            return View(model);
        }

        [HttpGet]
        public IActionResult AddRx(Guid lineId)
        {
            ConfigureReceiverViewModel model;
            if (lineId != Guid.Empty)
            {
                var user = userRepo.Get(x => x.UserName == User.Identity.Name);
                var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);
                var quoteLine = activeQuote.Lines.First(x => x.Id == lineId);
                model = JsonConvert.DeserializeObject<ConfigureReceiverViewModel>(quoteLine.ConfigurationJson);
                model.LineId = lineId;
            }
            else
            {
                model = new ConfigureReceiverViewModel();
            }

            InflateRxModel(model);

            return View(model);
        }

        [HttpGet]
        public IActionResult AddAnt(Guid lineId)
        {
            ConfigureAntennaViewModel model;
            if (lineId != Guid.Empty)
            {
                var user = userRepo.Get(x => x.UserName == User.Identity.Name);
                var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);
                var quoteLine = activeQuote.Lines.First(x => x.Id == lineId);
                model = JsonConvert.DeserializeObject<ConfigureAntennaViewModel>(quoteLine.ConfigurationJson);
                model.LineId = lineId;
            }
            else
            {
                model = new ConfigureAntennaViewModel();
            }

            InflateAntModel(model);

            return View(model);
        }

        [HttpGet]
        public IActionResult AddEvtm(Guid lineId)
        {
            ConfigureEvtmViewModel model;
            if (lineId != Guid.Empty)
            {
                var user = userRepo.Get(x => x.UserName == User.Identity.Name);
                var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);
                var quoteLine = activeQuote.Lines.First(x => x.Id == lineId);
                model = JsonConvert.DeserializeObject<ConfigureEvtmViewModel>(quoteLine.ConfigurationJson);
                model.LineId = lineId;
            }
            else
            {
                model = new ConfigureEvtmViewModel();
            }

            InflateEvtmModel(model);

            return View(model);
        }

        [HttpGet]
        public IActionResult AddStatusLogger(Guid lineId)
        {
            ConfigureStatusLoggerViewModel model;
            if (lineId != Guid.Empty)
            {
                var user = userRepo.Get(x => x.UserName == User.Identity.Name);
                var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);
                var quoteLine = activeQuote.Lines.First(x => x.Id == lineId);
                model = JsonConvert.DeserializeObject<ConfigureStatusLoggerViewModel>(quoteLine.ConfigurationJson);
                model.LineId = lineId;
            }
            else
            {
                model = new ConfigureStatusLoggerViewModel();
            }

            InflateStatusLoggerModel(model);

            return View(model);
        }

        [HttpGet]
        public IActionResult AddAccessory()
        {
            ConfigureAccessoryViewModel model = new ConfigureAccessoryViewModel();
            InflateAccessoryViewModel(model);

            return View(model);
        }

        [HttpPost]
        public IActionResult AddItem(ConfigureTxViewModel model)
        {
            InflateTxModel(model);

            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);

            if (model.SaveForLater != null)
            {
                var json = JsonConvert.SerializeObject(model, Formatting.None, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                var tempProduct = repo.Get<Product>(x => x.PartNumber == "QSX-PLACEHOLDER");

                QuoteLine line;
                if (model.LineId == Guid.Empty)
                {
                    line = new QuoteLine()
                    {
                        FullyConfigured = false,
                        ConfigurationJson = json,
                        Quote = activeQuote,
                        Quantity = 1,
                        ProductName = model.PartNumber,
                        Product = tempProduct,
                        ProductType = ProductType.Transmitter
                    };

                    activeQuote.AddLine(line);
                }
                else
                {
                    line = activeQuote.Lines.First(x => x.Id == model.LineId);
                    line.ConfigurationJson = json;
                    line.ProductName = model.PartNumber;
                    line.FullyConfigured = false;
                }

                unitOfWork.BeginTransaction();
                repo.Update(activeQuote);
                unitOfWork.Commit();

                return RedirectToAction("Cart");
            }
            else if (model.AddToQuote != null)
            {
                var category = repo.Get<Category>(x => x.ProductType == ProductType.Transmitter);

                Transmitter product = repo.Get<Transmitter>(x => x.PartNumber == model.PartNumber);

                if (product == null)
                {
                    product = new Transmitter()
                    {
                        PartNumber = model.PartNumber,
                        BandCombination = model.BandCombination,
                        Baseband = model.Baseband,
                        Package = model.Package,
                        Pinout = model.Pinout,
                        RadioFrequencyPower = model.RadioPower,
                        SerialControlInterface = model.SerialControlInterface,
                        Options = model.Options.ToHashSet(),
                        Modulations = model.Modulations.ToHashSet(),
                        Category = category
                    };

                    foreach (var option in product.Options)
                    {
                        option.Transmitters.Add(product);
                    }

                    foreach (var mod in product.Modulations)
                    {
                        mod.Transmitters.Add(product);
                    }
                }

                var json = JsonConvert.SerializeObject(model, Formatting.None, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
                unitOfWork.BeginTransaction();

                repo.Create(product);

                QuoteLine line;
                if (model.LineId == Guid.Empty)
                {
                    line = new QuoteLine()
                    {
                        FullyConfigured = true,
                        ConfigurationJson = json,
                        Product = product,
                        Quote = activeQuote,
                        Quantity = 1,
                        ProductName = product.PartNumber,
                        IncludedInQuote = true,
                        ProductType = ProductType.Transmitter
                    };

                    activeQuote.AddLine(line);
                }
                else
                {
                    line = activeQuote.Lines.First(x => x.Id == model.LineId);
                    line.ConfigurationJson = json;
                    line.FullyConfigured = true;
                    line.Product = product;
                    line.Quantity = 1;
                    line.ProductName = product.PartNumber;
                    line.IncludedInQuote = true;
                    line.ProductType = ProductType.Transmitter;
                }

                repo.Update(activeQuote);
                unitOfWork.Commit();

                return RedirectToAction("Cart");
            }

            return PartialView("_Configure", model);
        }

        [HttpPost]
        public IActionResult ConfigureTx(ConfigureTxViewModel model)
        {
            InflateTxModel(model);

            return PartialView("_ConfigureTx", model);
        }

        [HttpPost]
        public IActionResult AddRx(ConfigureReceiverViewModel model)
        {
            InflateRxModel(model);

            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);

            if (model.SaveForLater != null)
            {
                var tempProduct = repo.Get<Product>(x => x.PartNumber == "QSX-PLACEHOLDER");

                var json = JsonConvert.SerializeObject(model, Formatting.None, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                unitOfWork.BeginTransaction();

                QuoteLine line;
                if (model.LineId == Guid.Empty)
                {
                    line = new QuoteLine()
                    {
                        FullyConfigured = false,
                        ConfigurationJson = json,
                        Quote = activeQuote,
                        Quantity = 1,
                        ProductType = ProductType.Receiver,
                        ProductName = model.PartNumber,
                        Product = tempProduct
                    };

                    activeQuote.AddLine(line);
                }
                else
                {
                    line = activeQuote.Lines.First(x => x.Id == model.LineId);
                    line.ConfigurationJson = json;
                    line.ProductName = model.PartNumber;
                    line.FullyConfigured = false;
                }

                repo.Update(activeQuote);
                unitOfWork.Commit();

                return RedirectToAction("Cart");
            }
            else if (model.AddToQuote != null)
            {
                var category = repo.Get<Category>(x => x.ProductType == ProductType.Receiver);

                Receiver product = repo.Get<Receiver>(x => x.PartNumber == model.PartNumber);

                if (product == null)
                {
                    product = new Receiver()
                    {
                        PartNumber = model.PartNumber,
                        Package = model.Package,
                        BandCombination = model.BandCombination,
                        Pinout = model.Pinout,
                        Demodulations = repo.GetGroup<RxDemodulation>(x => model.DemodulationChecklist.SelectedValues.Contains(x.Id)).ToHashSet(),
                        Options = model.SelectedOptions.ToHashSet(),
                        Category = category
                    };

                foreach (var option in product.Options)
                {
                    option.Receivers.Add(product);
                }

                foreach (var demod in product.Demodulations)
                {
                    demod.Receivers.Add(product);
                }
                }

                var json = JsonConvert.SerializeObject(model, Formatting.None, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                unitOfWork.BeginTransaction();

                repo.Create(product);

                QuoteLine line;
                if (model.LineId == Guid.Empty)
                {
                    line = new QuoteLine()
                    {
                        FullyConfigured = true,
                        ConfigurationJson = json,
                        Product = product,
                        Quote = activeQuote,
                        Quantity = 1,
                        ProductType = ProductType.Receiver,
                        ProductName = product.PartNumber,
                        IncludedInQuote = true
                    };

                    activeQuote.AddLine(line);
                }
                else
                {
                    line = activeQuote.Lines.First(x => x.Id == model.LineId);
                    line.ConfigurationJson = json;
                    line.FullyConfigured = true;
                    line.Product = product;
                    line.Quantity = 1;
                    line.ProductType = ProductType.Receiver;
                    line.ProductName = product.PartNumber;
                    line.IncludedInQuote = true;
                }

                repo.Update(activeQuote);
                unitOfWork.Commit();

                return RedirectToAction("Cart");
            }

            return PartialView("_ConfigureRx", model);
        }

        [HttpPost]
        public IActionResult AddAnt(ConfigureAntennaViewModel model)
        {
            InflateAntModel(model);

            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);

            if (model.SaveForLater != null)
            {
                var json = JsonConvert.SerializeObject(model, Formatting.None, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                var tempProduct = repo.Get<Product>(x => x.PartNumber == "QSX-PLACEHOLDER");

                unitOfWork.BeginTransaction();

                QuoteLine line;
                if (model.LineId == Guid.Empty)
                {
                    line = new QuoteLine()
                    {
                        FullyConfigured = false,
                        ConfigurationJson = json,
                        Quote = activeQuote,
                        Quantity = 1,
                        ProductType = ProductType.Antenna,
                        ProductName = model.PartNumber,
                        Product = tempProduct
                    };

                    activeQuote.AddLine(line);
                }
                else
                {
                    line = activeQuote.Lines.First(x => x.Id == model.LineId);
                    line.FullyConfigured = false;
                    line.ConfigurationJson = json;
                    line.ProductName = model.PartNumber;
                }

                repo.Update(activeQuote);
                unitOfWork.Commit();

                return RedirectToAction("Cart");
            }
            else if (model.AddToQuote != null)
            {
                var category = repo.Get<Category>(x => x.ProductType == ProductType.Antenna);

                Antenna product = repo.Get<Antenna>(x => x.PartNumber == model.PartNumber);

                if (product == null)
                {
                    product = new Antenna()
                    {
                        PartNumber = model.PartNumber,
                        Pedestal = model.Pedestal,
                        BandCombination = model.BandCombination,
                        Feed = model.Feed,
                        ReflectorAssembly = model.ReflectorAssembly,
                        Wiring = model.Wiring,
                        Options = model.SelectedOptions.ToHashSet(),
                        Category = category
                    };
                }

                var json = JsonConvert.SerializeObject(model, Formatting.None, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                unitOfWork.BeginTransaction();

                repo.Create(product);

                QuoteLine line;
                if (model.LineId == Guid.Empty)
                {
                    line = new QuoteLine()
                    {
                        FullyConfigured = true,
                        ConfigurationJson = json,
                        Product = product,
                        Quote = activeQuote,
                        Quantity = 1,
                        ProductType = ProductType.Antenna,
                        ProductName = product.PartNumber,
                        IncludedInQuote = true
                    };

                    activeQuote.AddLine(line);
                }
                else
                {
                    line = activeQuote.Lines.First(x => x.Id == model.LineId);
                    line.ConfigurationJson = json;
                    line.FullyConfigured = true;
                    line.Product = product;
                    line.Quantity = 1;
                    line.ProductType = ProductType.Antenna;
                    line.ProductName = product.PartNumber;
                    line.IncludedInQuote = true;
                }

                repo.Update(activeQuote);
                unitOfWork.Commit();

                return RedirectToAction("Cart");
            }

            return PartialView("_ConfigureAnt", model);
        }

        [HttpPost]
        public IActionResult AddEvtm(ConfigureEvtmViewModel model)
        {
            InflateEvtmModel(model);

            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);

            if (model.SaveForLater != null)
            {
                var json = JsonConvert.SerializeObject(model, Formatting.None, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                var tempProduct = repo.Get<Product>(x => x.PartNumber == "QSX-PLACEHOLDER");

                unitOfWork.BeginTransaction();

                QuoteLine line;
                if (model.LineId == Guid.Empty)
                {
                    line = new QuoteLine()
                    {
                        IncludedInQuote = false,
                        FullyConfigured = false,
                        ConfigurationJson = json,
                        Quote = activeQuote,
                        Quantity = 1,
                        ProductType = ProductType.EVTM,
                        ProductName = "QSX-EVTM-XXX",
                        Product = tempProduct
                    };

                    activeQuote.AddLine(line);
                }
                else
                {
                    line = activeQuote.Lines.First(x => x.Id == model.LineId);
                    line.FullyConfigured = false;
                    line.ConfigurationJson = json;
                    line.ProductName = "QSX-EVTM-XXX";
                }

                repo.Update(activeQuote);
                unitOfWork.Commit();

                return RedirectToAction("Cart");
            }
            else if (model.AddToQuote != null)
            {
                var product = model.Evtm;

                var json = JsonConvert.SerializeObject(model, Formatting.None, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                unitOfWork.BeginTransaction();

                repo.Create(product);

                QuoteLine line;
                if (model.LineId == Guid.Empty)
                {
                    line = new QuoteLine()
                    {
                        FullyConfigured = true,
                        ConfigurationJson = json,
                        Product = product,
                        Quote = activeQuote,
                        Quantity = 1,
                        ProductType = ProductType.EVTM,
                        ProductName = product.PartNumber,
                        IncludedInQuote = true,
                        Price = model.Evtm.Price
                    };

                    activeQuote.AddLine(line);
                }
                else
                {
                    line = activeQuote.Lines.First(x => x.Id == model.LineId);
                    line.ConfigurationJson = json;
                    line.FullyConfigured = true;
                    line.Product = product;
                    line.Quantity = 1;
                    line.ProductType = ProductType.EVTM;
                    line.ProductName = product.PartNumber;
                    line.IncludedInQuote = true;
                    line.Price = model.Evtm.Price;
                }

                repo.Update(activeQuote);
                unitOfWork.Commit();

                return RedirectToAction("Cart");
            }

            return PartialView("_ConfigureEvtm", model);
        }

        [HttpPost]
        public IActionResult AddStatusLogger(ConfigureStatusLoggerViewModel model)
        {
            InflateStatusLoggerModel(model);

            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);

            if (model.SaveForLater != null)
            {
                var json = JsonConvert.SerializeObject(model, Formatting.None, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                var tempProduct = repo.Get<Product>(x => x.PartNumber == "QSX-PLACEHOLDER");

                unitOfWork.BeginTransaction();

                QuoteLine line;
                if (model.LineId == Guid.Empty)
                {
                    line = new QuoteLine()
                    {
                        IncludedInQuote = false,
                        FullyConfigured = false,
                        ConfigurationJson = json,
                        Quote = activeQuote,
                        Quantity = 1,
                        ProductType = ProductType.StatusLogger,
                        ProductName = "QSX-RXSL-XXX",
                        Product = tempProduct
                    };

                    activeQuote.AddLine(line);
                }
                else
                {
                    line = activeQuote.Lines.First(x => x.Id == model.LineId);
                    line.ConfigurationJson = json;
                    line.ProductName = "QSX-RXSL-XXX";
                    line.FullyConfigured = false;
                }

                repo.Update(activeQuote);
                unitOfWork.Commit();

                return RedirectToAction("Cart");
            }
            else if (model.AddToQuote != null)
            {
                var product = model.StatusLogger;

                var json = JsonConvert.SerializeObject(model, Formatting.None, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                unitOfWork.BeginTransaction();

                repo.Create(product);

                QuoteLine line;
                if (model.LineId == Guid.Empty)
                {
                    line = new QuoteLine()
                    {
                        FullyConfigured = true,
                        ConfigurationJson = json,
                        Product = product,
                        Quote = activeQuote,
                        Quantity = 1,
                        ProductType = ProductType.StatusLogger,
                        ProductName = product.PartNumber,
                        IncludedInQuote = true,
                        Price = model.StatusLogger.Price
                    };

                    activeQuote.AddLine(line);
                }
                else
                {
                    line = activeQuote.Lines.First(x => x.Id == model.LineId);
                    line.ConfigurationJson = json;
                    line.FullyConfigured = true;
                    line.Product = product;
                    line.Quantity = 1;
                    line.ProductType = ProductType.StatusLogger;
                    line.ProductName = product.PartNumber;
                    line.IncludedInQuote = true;
                    line.Price = model.StatusLogger.Price;
                }

                repo.Update(activeQuote);
                unitOfWork.Commit();

                return RedirectToAction("Cart");
            }

            return PartialView("_ConfigureStatusLogger", model);
        }

        [HttpPost]
        public IActionResult AddAccessory(ConfigureAccessoryViewModel model)
        {
            InflateAccessoryViewModel(model);

            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);

            if (model.AddToQuote != null)
            {
                var product = repo.Get<Accessory>(model.AccessoryId);

                var line = new QuoteLine()
                {
                    FullyConfigured = true,
                    ConfigurationJson = "",
                    Product = product,
                    Quote = activeQuote,
                    Quantity = 1,
                    ProductType = ProductType.Accessory,
                    ProductName = product.PartNumber,
                    IncludedInQuote = true,
                    Price = product.Price
                };

                unitOfWork.BeginTransaction();
                activeQuote.AddLine(line);
                unitOfWork.Commit();

                return RedirectToAction("Cart");
            }

            return PartialView("_ConfigureAccessory", model);
        }

        [HttpPost]
        public IActionResult RemoveItem(Guid id)
        {
            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);
            var line = activeQuote.Lines.First(x => x.Id == id);
            activeQuote.Lines.Remove(line);

            unitOfWork.BeginTransaction();
            if (line.Product != null)
            {
                line.Product.QuoteLines.Remove(line);
                productRepo.Update(line.Product);
            }

            repo.Update(activeQuote);
            unitOfWork.Commit();
            return RedirectToAction("Cart");
        }

        [HttpPost]
        public IActionResult ExcludeItem(Guid id)
        {
            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);
            var line = activeQuote.Lines.First(x => x.Id == id);
            line.IncludedInQuote = false;
            unitOfWork.BeginTransaction();
            repo.Update(activeQuote);
            unitOfWork.Commit();
            return RedirectToAction("Cart");
        }

        [HttpPost]
        public IActionResult IncludeItem(Guid id)
        {
            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);
            var line = activeQuote.Lines.First(x => x.Id == id);
            line.IncludedInQuote = true;
            unitOfWork.BeginTransaction();
            repo.Update(activeQuote);
            unitOfWork.Commit();
            return RedirectToAction("Cart");
        }

        [HttpPost]
        public IActionResult UpdateQuantity(Guid id, int Quantity)
        {
            var user = userRepo.Get(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);
            var line = activeQuote.Lines.First(x => x.Id == id);
            line.Quantity = Quantity;
            unitOfWork.BeginTransaction();
            repo.Update(activeQuote);
            unitOfWork.Commit();
            return RedirectToAction("Cart");
        }

        private void InflateTxModel(ConfigureTxViewModel model)
        {
            filter.Filter(model);

            // this is crucial. without, the model binder will retain old values for list items
            ModelState.Clear();
        }

        private void InflateRxModel(ConfigureReceiverViewModel model)
        {
            var packages = repo.GetAll<RxPackage>().ToList();
            var bandCombos = repo.GetAll<RxBandCombination>().ToList();
            model.BandChecklist = bandCombos
                .SelectMany(x => x.Bands)
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, model.BandChecklist.SelectedValues);

            if (model.FormFactorChecklist.HasSelection)
            {
                packages = packages.Where(x => x.FormFactor.Id == model.FormFactorChecklist.SelectedValue.Value).ToList();
            }
            if (model.ChannelCountChecklist.HasSelection)
            {
                var channelSet = repo.Get<RxChannelSet>(model.ChannelCountChecklist.SelectedValue.Value);
                model.ChannelSet = channelSet;
                packages = packages.Where(x => x.ChannelSet.Id == model.ChannelCountChecklist.SelectedValue.Value).ToList();
            }
            if (model.IFInputChecklist.HasSelection)
            {
                packages = packages.Where(x => x.IfInputConnectorSet.Id == model.IFInputChecklist.SelectedValue.Value).ToList();
            }
            if (model.AnalogOutputChecklist.HasSelection)
            {
                packages = packages.Where(x => x.AnalogOutputConnectorSet.Id == model.AnalogOutputChecklist.SelectedValue.Value).ToList();
            }
            if (model.AMOutputChecklist.HasSelection)
            {
                packages = packages.Where(x => x.AmOutputConnectorSet.Id == model.AMOutputChecklist.SelectedValue.Value).ToList();
            }
            if (model.AGCOutputChecklist.HasSelection)
            {
                packages = packages.Where(x => x.AgcOutputConnectorSet.Id == model.AGCOutputChecklist.SelectedValue.Value).ToList();
            }
            if (model.BasebandChecklist.HasSelection)
            {
                packages = packages.Where(x => x.BasebandSet.Id == model.BasebandChecklist.SelectedValue.Value).ToList();
            }
            if (model.DisplayChecklist.HasSelection)
            {
                var display = repo.Get<RxFrontPanelDisplay>(x => x.Id == model.DisplayChecklist.SelectedValue.Value);
                packages = packages.Where(x => x.FrontPanelDisplay == display).ToList();
                if (display.Id == Guid.Parse("ebca40e9-b345-4f9d-87fe-9904f82ea1cc"))
                {
                    var ndOption = repo.Get<RxOption>(x => x.PartNumberDescriptor == "ND");
                    model.SelectedOptions.Add(ndOption);
                }
            }
            if (model.OptionsChecklist.HasSelection)
            {
                model.SelectedOptions = repo.GetGroup<RxOption>(x => model.OptionsChecklist.SelectedValues.Contains(x.Id)).ToList();
                packages = packages.Where(x => x.FormFactor.Options.Any(x => model.OptionsChecklist.SelectedValues.Contains(x.Id))).ToList();
            }
            if (model.BandChecklist.HasSelection)
            {
                bandCombos = bandCombos.Where(x => x.Bands.Select(b => b.Id).Intersect(model.BandChecklist.SelectedValues).Count() == model.BandChecklist.SelectedValues.Count).ToList();
            }
            if (model.BandComboChecklist.HasSelection)
            {
                bandCombos = bandCombos.Where(x => x.Id == model.BandComboChecklist.SelectedValue).ToList();

                string extendedText = model.BandRange == BandRange.Standard ? "0" : "1";
                model.BandCombinationText = bandCombos[0].Name + extendedText;
                model.BandCombination = bandCombos[0];
            }
            if (model.DemodulationChecklist.HasSelection)
            {
                var demods = repo.GetGroup<RxDemodulation>(x => model.DemodulationChecklist.SelectedValues.Contains(x.Id));
                model.Demodulations = demods.ToList();

                model.HasTier0 = demods.Any(x => x.Type == ModulationType.ARTM0);
                model.HasTier1 = demods.Any(x => x.Type == ModulationType.ARTM1);
                model.HasTier2 = demods.Any(x => x.Type == ModulationType.ARTM2);
                model.HasLegacy = demods.Any(x => x.Type == ModulationType.PSK);
            }
            if (model.PinoutChecklist.HasSelection)
            {
                model.Pinout = repo.Get<RxPinout>(model.PinoutChecklist.SelectedValue.Value);
                model.PinoutText = model.Pinout.PartNumberDescriptor;
                packages = packages.Where(x => x.FormFactor.Pinouts.Contains(model.Pinout)).ToList();
            }
            if (packages.Count == 1)
            {
                model.Package = packages[0];
                model.ChassisText = model.Package.PartNumberDescriptor;
            }

            model.FormFactorChecklist = packages.Select(x => x.FormFactor).Distinct()
                .OrderBy(x => x.Style)
                .ThenBy(x => x.Name)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.FormFactorChecklist?.SelectedValue);
            model.ChannelCountChecklist = packages.Select(x => x.ChannelSet)
                .OrderBy(x => x.TotalChannelCount)
                .ToSingleSelectGuidChecklist(x => x.Description, x => x.Id, model.ChannelCountChecklist?.SelectedValue);
            model.IFInputChecklist = packages.Select(x => x.IfInputConnectorSet)
                .OrderBy(x => x.Connectors.Select(x => x.Count).Sum())
                .ToSingleSelectGuidChecklist(x => x.Description, x => x.Id, model.IFInputChecklist?.SelectedValue);
            model.AnalogOutputChecklist = packages.Select(x => x.AnalogOutputConnectorSet)
                .OrderBy(x => x.Connectors.Select(x => x.Count).Sum())
                .ToSingleSelectGuidChecklist(x => x.Description, x => x.Id, model.AnalogOutputChecklist?.SelectedValue);
            model.AMOutputChecklist = packages.Select(x => x.AmOutputConnectorSet)
                .OrderBy(x => x.Connectors.Select(x => x.Count).Sum())
                .ToSingleSelectGuidChecklist(x => x.Description, x => x.Id, model.AMOutputChecklist?.SelectedValue);
            model.AGCOutputChecklist = packages.Select(x => x.AgcOutputConnectorSet)
                .OrderBy(x => x.Connectors.Select(x => x.Count).Sum())
                .ToSingleSelectGuidChecklist(x => x.Description, x => x.Id, model.AGCOutputChecklist?.SelectedValue);
            model.BasebandChecklist = packages.Select(x => x.BasebandSet)
                .OrderBy(x => x.Description)
                .ToSingleSelectGuidChecklist(x => x.Description, x => x.Id, model.BasebandChecklist?.SelectedValue);
            model.DisplayChecklist = packages.Select(x => x.FrontPanelDisplay)
                .OrderBy(x => x.Name)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.DisplayChecklist?.SelectedValue);
            model.BandChecklist = repo.GetAll<RxBand>()
                .OrderBy(x => x.MinimumFrequency)
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, model.BandChecklist.SelectedValues);
            model.BandComboChecklist = bandCombos
                .OrderBy(x => x.Name)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.BandComboChecklist?.SelectedValue);

            var formFactors = packages.Select(x => x.FormFactor);

            model.DemodulationChecklist = repo.GetAll<RxDemodulation>()
                .OrderBy(x => (int)x.Type)
                .ToMultiSelectChecklist(x => x.Description, x => x.Id, model.DemodulationChecklist?.SelectedValues);
            model.PinoutChecklist = formFactors.SelectMany(x => x.Pinouts)
                .OrderBy(x => x.PartNumberDescriptor)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.PinoutChecklist?.SelectedValue);

            var options = formFactors.SelectMany(x => x.Options).Distinct().ToList();
            var selectedOptionIds = model.OptionsChecklist.SelectedValues;
            foreach (var optionId in model.OptionsChecklist.SelectedValues)
            {
                //var option = repo.Get<ReceiverOption>(optionId);

                //if (!option.OptionBindings.Any())
                //    continue;

                //options.RemoveAll(x => option.ConflictingOptions.Contains(x));
                //selectedOptionIds.AddRange(option.RequiredOptions.Select(x => x.Id));
            }

            model.BandCheckmarks = new List<List<bool>>();
            foreach (var bandCombo in model.BandComboChecklist.Options)
            {
                var combo = repo.Get<RxBandCombination>(bandCombo.Id);
                var list = new List<bool>();
                foreach (var band in model.BandChecklist.Options)
                {
                    list.Add(combo.Bands.Any(x => x.Id == band.Id));
                }
                model.BandCheckmarks.Add(list);
            }

            model.OptionsChecklist = options
                .OrderBy(x => x.PartNumberDescriptor)
                .ToMultiSelectChecklist(x => $"{x.PartNumberDescriptor} - {x.Name}", x => x.Id, selectedOptionIds);

            if (model.ChannelCountChecklist.HasGhost)
            {
                var channelSet = repo.Get<RxChannelSet>(model.ChannelCountChecklist.GhostSelection.Value);
                model.ChannelSet = channelSet;
            }
            if (model.BandComboChecklist.HasGhost)
            {
                model.BandCombination = bandCombos.First(x => x.Id == model.BandComboChecklist.GhostSelection.Value);

                string extendedText = model.BandRange == BandRange.Standard ? "0" : "1";
                model.BandCombinationText = model.BandCombination.Name + extendedText;
            }
            if (model.PinoutChecklist.HasGhost)
            {
                model.Pinout = repo.Get<RxPinout>(model.PinoutChecklist.GhostSelection.Value);
                model.PinoutText = model.Pinout.PartNumberDescriptor;
            }

            // this is crucial. without, the model binder will retain old values for list items
            ModelState.Clear();
        }

        private class FlattenedAntenna
        {
            public AntPedestal Pedestal { get; set; }
            public AntWiring Wiring { get; set; }
            public AntReflectorAssembly ReflectorAssembly { get; set; }
            public AntFeed Feed { get; set; }
            public AntBandCombination BandCombination { get; set; }
            public List<AntOption> Options { get; set; }
        }

        private void InflateAntModel(ConfigureAntennaViewModel model)
        {
            var pedestals = repo.GetAll<AntPedestal>();

            List<FlattenedAntenna> filter = new List<FlattenedAntenna>();

            foreach (var pedestal in pedestals)
            {
                foreach (var wiring in pedestal.Wirings)
                {
                    foreach (var reflector in pedestal.ReflectorAssemblies)
                    {
                        foreach (var feed in reflector.FocusArrangement.Feeds)
                        {
                            foreach (var bandCombo in feed.BandCombinations)
                            {
                                filter.Add(new FlattenedAntenna()
                                {
                                    Pedestal = pedestal,
                                    Wiring = wiring,
                                    ReflectorAssembly = reflector,
                                    Feed = feed,
                                    BandCombination = bandCombo,
                                    Options = pedestal.Options.ToList()
                                });
                            }
                        }
                    }
                }
            }

            if (model.ReflectorSizeChecklist.HasSelection)
            {
                filter = filter.Where(x => x.ReflectorAssembly.Diameter == model.ReflectorSizeChecklist.SelectedValue).ToList();
            }
            if (model.ReflectorConstructionChecklist.HasSelection)
            {
                filter = filter.Where(x => (int)x.ReflectorAssembly.ConstructionType == model.ReflectorConstructionChecklist.SelectedValue).ToList();
            }
            if (model.FocusChecklist.HasSelection)
            {
                filter = filter.Where(x => x.ReflectorAssembly.FocusArrangement.Id == model.FocusChecklist.SelectedValue).ToList();
            }
            if (model.WiringChecklist.HasSelection)
            {
                model.Wiring = repo.Get<AntWiring>(model.WiringChecklist.SelectedValue);
                filter = filter.Where(x => x.Wiring == model.Wiring).ToList();
            }
            if (model.FeedChecklist.HasSelection)
            {
                model.Feed = repo.Get<AntFeed>(model.FeedChecklist.SelectedValue);
                filter = filter.Where(x => x.Feed == model.Feed).ToList();
            }
            if (model.BandComboChecklist.HasSelection)
            {
                model.BandCombination = repo.Get<AntBandCombination>(model.BandComboChecklist.SelectedValue);
                filter = filter.Where(x => x.BandCombination == model.BandCombination).ToList();
            }

            var options = filter.SelectMany(x => x.Options).ToList();
            if (model.OptionChecklist.HasSelection)
            {
                model.SelectedOptions = repo.GetGroup<AntOption>(x => model.OptionChecklist.SelectedValues.Contains(x.Id)).ToList();
                filter = filter.Where(x => x.Options.Intersect(model.SelectedOptions).Count() == model.SelectedOptions.Count()).ToList();
                options = filter.SelectMany(x => x.Options).Distinct().ToList();
                //foreach (var selectedOption in model.SelectedOptions)
                //{
                //    var conflicts = selectedOption.ConflictingOptions;
                //    foreach (var conflict in conflicts)
                //    {
                //        options.Remove((AntOption)conflict);
                //    }
                //}
            }

            model.ReflectorSizeChecklist = filter.Select(x => x.ReflectorAssembly.Diameter)
                .OrderBy(x => x)
                .ToSingleSelectChecklist(x => x.ToString(), x => x, model.ReflectorSizeChecklist.SelectedValue);
            model.ReflectorConstructionChecklist = filter.Select(x => x.ReflectorAssembly.ConstructionType)
                .ToSingleSelectChecklist(x => Enum.GetName(typeof(DishConstructionType), x), x => (int)x, model.ReflectorConstructionChecklist.SelectedValue);
            model.FocusChecklist = filter.Select(x => x.ReflectorAssembly.FocusArrangement)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.FocusChecklist.SelectedValue);
            model.WiringChecklist = filter.Select(x => x.Wiring)
                .OrderBy(x => x.Name)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.WiringChecklist.SelectedValue);
            model.FeedChecklist = filter.Select(x => x.Feed)
                .OrderBy(x => x.Name)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.FeedChecklist.SelectedValue);
            model.OptionChecklist = options
                .OrderBy(x => x.Name)
                .ToMultiSelectChecklist(x => $"{x.PartNumberDescriptor} - {x.LineItemText}", x => x.Id, model.OptionChecklist.SelectedValues);
            model.BandChecklist = repo.GetAll<AntBand>()
                .OrderBy(x => x.MinimumFrequency)
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, model.BandChecklist.SelectedValues);

            var bandCombos = filter.Select(x => x.BandCombination);
            if (model.BandChecklist.HasSelection)
            {
                var bands = repo.GetGroup<AntBand>(x => model.BandChecklist.SelectedValues.Contains(x.Id));
                bandCombos = bandCombos.Where(x => x.Bands.Intersect(bands).Count() == bands.Count());
            }
            model.BandComboChecklist = bandCombos
                .OrderBy(x => x.Name)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.BandComboChecklist.SelectedValue);

            model.BandCheckmarks = new List<List<bool>>();
            foreach (var bandCombo in model.BandComboChecklist.Options)
            {
                var combo = repo.Get<AntBandCombination>(bandCombo.Id);
                var list = new List<bool>();
                foreach (var band in model.BandChecklist.Options)
                {
                    list.Add(combo.Bands.Any(x => x.Id == band.Id));
                }
                model.BandCheckmarks.Add(list);
            }

            var refs = filter.Select(x => x.ReflectorAssembly).Distinct();
            if (refs.Count() == 1)
                model.ReflectorAssembly = refs.First();

            var peds = filter.Select(x => x.Pedestal).Distinct();
            if (peds.Count() == 1)
                model.Pedestal = peds.First();

            // this is crucial. without, the model binder will retain old values for list items
            ModelState.Clear();
        }

        private void InflateEvtmModel(ConfigureEvtmViewModel model)
        {
            var filter = repo.GetAll<Evtm>();

            if (model.FormFactorChecklist.HasSelection)
            {
                filter = filter.Where(x => (int)x.FormFactor == model.FormFactorChecklist.SelectedValue);
            }
            if (model.ClockDataChecklist.HasSelection)
            {
                filter = filter.Where(x => (int)x.ClockData == model.ClockDataChecklist.SelectedValue);
            }
            if (model.ChannelCountChecklist.HasSelection)
            {
                filter = filter.Where(x => x.ChannelCount == model.ChannelCountChecklist.SelectedValue);
            }
            if (model.OptionChecklist.HasSelection)
            {
                bool hasET = model.OptionChecklist.SelectedValue == 1;
                filter = filter.Where(x => x.HasET == hasET);
            }

            model.FormFactorChecklist = filter.Select(x => x.FormFactor)
                .ToSingleSelectChecklist(x => Enum.GetName(typeof(EvtmFormFactor), x), x => (int)x, model.FormFactorChecklist.SelectedValue);
            model.ClockDataChecklist = filter.Select(x => x.ClockData)
                .ToSingleSelectChecklist(x => Enum.GetName(typeof(EvtmClockData), x), x => (int)x, model.ClockDataChecklist.SelectedValue);
            model.ChannelCountChecklist = filter.Select(x => x.ChannelCount)
                .OrderBy(x => x)
                .ToSingleSelectChecklist(x => x.ToString(), x => x, model.ChannelCountChecklist.SelectedValue);
            model.OptionChecklist = filter.Select(x => x.HasET)
                .ToSingleSelectChecklist(x => x ? "Extended temperature range (-40 to +85 C)" : "None", x => x ? 1 : 0, model.OptionChecklist.SelectedValue);

            if (filter.Count() == 1)
            {
                model.Evtm = filter.First();

                // get the text after QSX-EVTM-
                model.PackageText = model.Evtm.PartNumber[9..];
            }

            // this is crucial. without, the model binder will retain old values for list items
            ModelState.Clear();
        }

        private void InflateStatusLoggerModel(ConfigureStatusLoggerViewModel model)
        {
            var filter = repo.GetAll<StatusLogger>();

            if (model.FormFactorChecklist.HasSelection)
            {
                filter = filter.Where(x => (int)x.FormFactor == model.FormFactorChecklist.SelectedValue);
            }
            if (model.ReceiverCountChecklist.HasSelection)
            {
                filter = filter.Where(x => (int)x.ReceiverCount == model.ReceiverCountChecklist.SelectedValue);
            }

            model.FormFactorChecklist = filter.Select(x => x.FormFactor)
                .ToSingleSelectChecklist(x => Enum.GetName(typeof(TestEqFormFactor), x), x => (int)x, model.FormFactorChecklist.SelectedValue);
            model.ReceiverCountChecklist = filter.Select(x => x.ReceiverCount)
                .OrderBy(x => x)
                .ToSingleSelectChecklist(x => x.ToString(), x => x, model.ReceiverCountChecklist.SelectedValue);

            if (filter.Count() == 1)
            {
                model.StatusLogger = filter.First();

                // get the text after QSX-EVTM-
                model.PackageText = model.StatusLogger.PartNumber[9..];
            }

            // this is crucial. without, the model binder will retain old values for list items
            ModelState.Clear();
        }

        private void InflateAccessoryViewModel(ConfigureAccessoryViewModel model)
        {
            var filter = repo.GetAll<Accessory>();

            if (model.CategoryChecklist.HasSelection)
            {
                var category = repo.Get<AccCategory>(model.CategoryChecklist.SelectedValue);
                filter = filter.Where(x => x.Categories.Contains(category));
            }
            if (model.FunctionChecklist.HasSelection)
            {
                var function = repo.Get<AccFunction>(model.FunctionChecklist.SelectedValue);
                filter = filter.Where(x => x.Functions.Contains(function));
            }
            if (model.SignalStandardChecklist.HasSelection)
            {
                filter = filter.Where(x => (int)x.SignalStandard == model.SignalStandardChecklist.SelectedValue);
            }

            model.CategoryChecklist = filter.SelectMany(x => x.Categories)
                .OrderBy(x => x.Name)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.CategoryChecklist.SelectedValue);
            model.FunctionChecklist = filter.SelectMany(x => x.Functions)
                .OrderBy(x => x.Name)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.FunctionChecklist.SelectedValue);
            model.SignalStandardChecklist = filter.Select(x => x.SignalStandard)
                .ToSingleSelectChecklist(x => Enum.GetName(typeof(AccessorySignalStandard), x), x => (int)x, model.SignalStandardChecklist.SelectedValue);

            model.Products = filter;

            // this is crucial. without, the model binder will retain old values for list items
            ModelState.Clear();
        }

        public IActionResult GenerateSpecSheet(Guid id)
        {
            var tx = (Transmitter)productRepo.Get(id);
            var bytes = specSheetGenerator.GenerateTransmitterSpecSheet(tx);

            return File(bytes, "application/pdf");
        }


        public IActionResult FinalizeQuote(Guid id)
        {
            var quote = repo.Get<Quote>(id);
            //quote.FinalizeQuote();

            //unitOfWork.BeginTransaction();
            //repo.Update(quote);
            //unitOfWork.Commit();

            var outstream = quoteDocumentGenerator.GenerateQuoteDocument(quote);

            //emailSender.SendEmailAsync("nmoore@quasonix.com", "Test", "quote");

            return File(outstream, "application/pdf", $"{quote.Number}.pdf");
        }

        public IActionResult GetPriceDetail(Guid lineId)
        {
            var quoteLine = repo.Get<QuoteLine>(lineId);
            var quote = quoteLine.Quote;
            quoteLine.Product.GetPrice(quote.PriceVersion, quoteLine.Quantity, quote.ShippingZone);
            return PartialView("_PriceDetail", quoteLine.Product);
        }
    }
}

public static class MyExtensions
{
    public static SingleSelectChecklistViewModel ToSingleSelectChecklist<T>(this IEnumerable<T> collection, Func<T, string> nameGetter, Func<T, int> idGetter, int? selectedId)
    {
        var model = new SingleSelectChecklistViewModel();
        model.Options = collection.Distinct().Select(m => new IntegerCheckboxViewModel
        {
            Id = idGetter(m),
            IsSelected = idGetter(m) == selectedId,
            LabelText = nameGetter(m)
        }).ToList();

        return model;
    }

    public static SingleSelectGuidChecklistViewModel ToSingleSelectGuidChecklist<T>(this IEnumerable<T> collection, Func<T, string> nameGetter, Func<T, Guid> idGetter, Guid? selectedId)
    {
        var model = new SingleSelectGuidChecklistViewModel();
        model.Options = collection.Distinct().Select(m => new GuidCheckboxViewModel
        {
            Id = idGetter(m),
            IsSelected = idGetter(m) == selectedId,
            LabelText = nameGetter(m)
        }).ToList();

        return model;
    }

    public static MultiSelectChecklistViewModel ToMultiSelectChecklist<T>(this IEnumerable<T> collection, Func<T, string> nameGetter, Func<T, Guid> idGetter, IEnumerable<Guid> selectedIds)
    {
        var model = new MultiSelectChecklistViewModel();
        model.Options = collection.Distinct().Select(m => new GuidCheckboxViewModel
        {
            Id = idGetter(m),
            IsSelected = selectedIds == null ? false : selectedIds.Contains(idGetter(m)),
            LabelText = nameGetter(m)
        }).ToList();

        return model;
    }

    public static MultiSelectChecklistViewModel ToMultiSelectChecklist<T>(this IEnumerable<T> collection, Func<T, string> nameGetter, Func<T, Guid> idGetter, IEnumerable<Guid> selectedIds, Dictionary<Guid, string> decorations)
    {
        var model = new MultiSelectChecklistViewModel();
        model.Options = collection.Distinct().Select(m => new GuidCheckboxViewModel
        {
            Id = idGetter(m),
            IsSelected = selectedIds == null ? false : selectedIds.Contains(idGetter(m)),
            LabelText = nameGetter(m),
            Decoration = decorations.ContainsKey(idGetter(m)) ? decorations[idGetter(m)] : null
        }).ToList();

        return model;
    }
}