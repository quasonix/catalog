﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class FrontPanelDisplayController : Controller
    {
        private readonly ICatalogRepository<RxFrontPanelDisplay> displayRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public FrontPanelDisplayController(ICatalogRepository<RxFrontPanelDisplay> displayRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.displayRepo = displayRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var displays = displayRepo.GetAll();
            return View(displays);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new FrontPanelDisplayViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(FrontPanelDisplayViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map display from model
                var display = mapper.Map<RxFrontPanelDisplay>(model);

                // create display
                unitOfWork.BeginTransaction();
                displayRepo.Create(display);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var display = displayRepo.Get(id);
            var model = mapper.Map<FrontPanelDisplayViewModel>(display);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(FrontPanelDisplayViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get existing display and map from model
                var display = displayRepo.Get(model.Id);
                mapper.Map(model, display);

                // update display
                unitOfWork.BeginTransaction();
                displayRepo.Update(display);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var display = displayRepo.Get(id);
            var model = mapper.Map<FrontPanelDisplayDetailViewModel>(display);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var display = displayRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(display);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var display = displayRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            displayRepo.Delete(display);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(FrontPanelDisplayViewModel model)
        {
            // check if display with name already exists, add error if not
            bool exists = displayRepo.GetGroup(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A display with the name {model.Name} already exists.");
        }
    }
}