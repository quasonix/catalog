﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using DynamicVML;
using DynamicVML.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class TxPackageController : Controller
    {
        private readonly ICatalogRepository<TxPackage> packageRepo;
        private readonly ICatalogRepository<TxFootprint> footprintRepo;
        private readonly ICatalogRepository<TxHeight> heightRepo;
        private readonly ICatalogRepository<TxStack> stackRepo;
        private readonly ICatalogRepository<TxDataConnector> dataConnectorRepo;
        private readonly ICatalogRepository<TxRadioConnector> rfConnectorRepo;
        private readonly ICatalogRepository<PriceVersion> priceVersionRepo;
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TxPackageController(ICatalogRepository<TxPackage> packageRepo,
            ICatalogRepository<TxFootprint> footprintRepo,
            ICatalogRepository<TxHeight> heightRepo,
            ICatalogRepository<TxStack> stackRepo,
            ICatalogRepository<TxDataConnector> dataConnectorRepo,
            ICatalogRepository<TxRadioConnector> rfConnectorRepo,
            ICatalogRepository<PriceVersion> priceVersionRepo,
            ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.packageRepo = packageRepo;
            this.footprintRepo = footprintRepo;
            this.heightRepo = heightRepo;
            this.stackRepo = stackRepo;
            this.dataConnectorRepo = dataConnectorRepo;
            this.rfConnectorRepo = rfConnectorRepo;
            this.priceVersionRepo = priceVersionRepo;
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var packages = packageRepo.GetAll();
            return View(packages);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new PackageViewModel();
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(PackageViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                unitOfWork.BeginTransaction();

                var package = new TxPackage()
                {
                    Name = model.Name,
                    Active = model.Active,
                    PrimaryConnectorPosition = model.PrimaryConnectorPosition,
                    RadioConnectorPosition = model.RadioConnectorPosition,
                    Footprint = repo.Get<TxFootprint>(model.FootprintId),
                    Height = repo.Get<TxHeight>(model.HeightId),
                    CoaxialModulationInput = repo.Get<TxCoaxialModulationInput>(model.CoaxialModulationInputId),
                    ConnectorSet = repo.Get<TxConnectorSet>(model.ConnectorSetId),
                    RadioConnector = repo.Get<TxRadioConnector>(model.RadioConnectorId)
                };

                foreach (var priceModel in model.Prices)
                {
                    var price = new Price();
                    price.Value = priceModel.ViewModel.Value;
                    price.Component = package;
                    price.PriceVersion = priceVersionRepo.Get(priceModel.ViewModel.PriceVersionId);
                    package.Prices.Add(price);
                }

                packageRepo.Create(package);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        public IActionResult Edit(Guid id)
        {
            var package = packageRepo.Get(id);
            var model = mapper.Map<PackageViewModel>(package);

            model.Prices = package.Prices.ToDynamicList(x =>
                mapper.Map<PriceViewModel>(x));

            InflateViewModel(model);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(PackageViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                unitOfWork.BeginTransaction();

                var package = packageRepo.Get(model.Id);
                package.Name = model.Name;
                package.Active = model.Active;
                package.PrimaryConnectorPosition = model.PrimaryConnectorPosition;
                package.RadioConnectorPosition = model.RadioConnectorPosition;
                package.Footprint = repo.Get<TxFootprint>(model.FootprintId);
                package.Height = repo.Get<TxHeight>(model.HeightId);
                package.CoaxialModulationInput = repo.Get<TxCoaxialModulationInput>(model.CoaxialModulationInputId);
                package.ConnectorSet = repo.Get<TxConnectorSet>(model.ConnectorSetId);
                package.RadioConnector = repo.Get<TxRadioConnector>(model.RadioConnectorId);

                package.ClearPrices();
                foreach (var priceModel in model.Prices)
                {
                    var price = new Price();
                    price.Value = priceModel.ViewModel.Value;
                    price.Component = package;
                    price.PriceVersion = priceVersionRepo.Get(priceModel.ViewModel.PriceVersionId);
                    package.Prices.Add(price);
                }

                packageRepo.Update(package);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var package = packageRepo.Get(id);
            var model = mapper.Map<PackageDetailViewModel>(package);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var package = packageRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(package);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var package = packageRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            //package.ClearStacks();
            packageRepo.Update(package);
            packageRepo.Delete(package);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        public IActionResult AddStack(AddNewDynamicItem parameters)
        {
            var model = new PackageStackViewModel();
            model.StackOptions = stackRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);
            return this.PartialView(model, parameters);
        }

        public IActionResult AddDataConnector(AddNewDynamicItem parameters)
        {
            var model = new DataConnectorForPackageViewModel();
            model.DataConnectorOptions = dataConnectorRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);
            return this.PartialView(model, parameters);
        }

        public IActionResult AddRfConnector(AddNewDynamicItem parameters)
        {
            var model = new RfConnectorForPackageViewModel();
            model.RfConnectorOptions = rfConnectorRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);
            return this.PartialView(model, parameters);
        }

        public IActionResult AddPrice(AddNewDynamicItem parameters)
        {
            var model = new PriceViewModel();
            model.PriceVersionOptions = priceVersionRepo.GetAll()
                .OrderBy(x => x.EffectiveDate)
                .ToSelectItemList(x => $"({x.EffectiveDate.ToShortDateString()}) - {x.Note}", x => x.Id);
            return this.PartialView(model, parameters);
        }

        private void InflateViewModel(PackageViewModel model)
        {
            model.FootprintOptions = repo.GetAll<TxFootprint>().ToSelectItemList(x => x.Name, x => x.Id);
            model.HeightOptions = repo.GetAll<TxHeight>().OrderBy(x => x.Measurement).ToSelectItemList(x => x.Name, x => x.Id);
            model.ConnectorSetOptions = repo.GetAll<TxConnectorSet>().OrderBy(x => x.Name).ToSelectItemList(x => x.Name, x => x.Id);
            model.RadioOptions = repo.GetAll<TxRadioConnector>().OrderBy(x => x.Name).ToSelectItemList(x => x.Name, x => x.Id);
            model.CoaxOptions = repo.GetAll<TxCoaxialModulationInput>().OrderBy(x => x.Name).ToSelectItemList(x => x.Name, x => x.Id);

            // get options
            var stackOptions = stackRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);
            var dataConnectorOptions = dataConnectorRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);
            var rfConnectorOptions = rfConnectorRepo.GetAll().ToSelectItemList(x => x.Name, x => x.Id);
            var priceVersionOptions = priceVersionRepo.GetAll().ToSelectItemList(x => $"({x.EffectiveDate.ToShortDateString()}) - {x.Note}", x => x.Id);

            foreach (var price in model.Prices)
            {
                price.ViewModel.PriceVersionOptions = priceVersionOptions;
            }
        }

        private void ValidateViewModel(PackageViewModel model)
        {
            // check if package with name already exists, add error if not
            bool exists = packageRepo.GetGroup(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A package with the name {model.Name} already exists.");

            // price validation
            //if (model.Prices == null || model.Prices.Count < 1)
            //    ModelState.AddModelError("NoPriceError", $"Must have a price.");
        }
    }
}