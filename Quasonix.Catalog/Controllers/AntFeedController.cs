﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class AntFeedController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public AntFeedController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var feeds = repo.GetAll<AntFeed>();
            return View(feeds);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new AntFeedViewModel();
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(AntFeedViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map feed from model
                var feed = mapper.Map<AntFeed>(model);

                foreach (var comboId in model.BandCombinationChecklist.SelectedValues)
                {
                    // add selected band combo
                    var bandCombo = repo.Get<AntBandCombination>(comboId);
                    feed.BandCombinations.Add(bandCombo);

                    // add feed to band combo, need both sides
                    bandCombo.Feeds.Add(feed);
                }

                // create feed
                unitOfWork.BeginTransaction();
                repo.Create(feed);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var feed = repo.Get<AntFeed>(id);
            var model = mapper.Map<AntFeedViewModel>(feed);
            InflateViewModel(model, feed);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(AntFeedViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get existing feed and map from model
                var feed = repo.Get<AntFeed>(model.Id);
                mapper.Map(model, feed);

                feed.ClearBandCombinations();
                foreach (var comboId in model.BandCombinationChecklist.SelectedValues)
                {
                    // add selected band combo
                    var bandCombo = repo.Get<AntBandCombination>(comboId);
                    feed.BandCombinations.Add(bandCombo);

                    // add feed to band combo, need both sides
                    bandCombo.Feeds.Add(feed);
                }

                // update feed
                unitOfWork.BeginTransaction();
                repo.Update(feed);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var feed = repo.Get<AntFeed>(id);
            var model = mapper.Map<AntFeedDetailViewModel>(feed);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var feed = repo.Get<AntFeed>(id);
            var model = mapper.Map<DeleteViewModel>(feed);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var feed = repo.Get<AntFeed>(model.Id);

            unitOfWork.BeginTransaction();
            repo.Delete(feed);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void InflateViewModel(AntFeedViewModel model)
        {
            model.BandCombinationChecklist = repo.GetAll<AntBandCombination>().ToMultiSelectChecklist(x => x.Name, x => x.Id, model.BandCombinationChecklist.SelectedValues);
        }

        private void InflateViewModel(AntFeedViewModel model, AntFeed feed)
        {
            model.BandCombinationChecklist = repo.GetAll<AntBandCombination>().ToMultiSelectChecklist(x => x.Name, x => x.Id, feed.BandCombinations.Select(x => x.Id));
        }

        private void ValidateViewModel(AntFeedViewModel model)
        {
            // check if feed with name already exists, add error if not
            bool exists = repo.GetGroup<AntFeed>(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A feed with the name {model.Name} already exists.");
        }
    }
}