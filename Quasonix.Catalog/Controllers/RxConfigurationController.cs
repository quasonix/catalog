﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DynamicVML;
using DynamicVML.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class RxConfigurationController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public RxConfigurationController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var rxConfig = repo.GetAll<RxPackage>();
            return View(rxConfig);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new RxPackageViewModel();
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(RxPackageViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map rxConfig from model
                var rxConfig = new RxPackage()
                {
                    Name = model.Name,
                    Description = model.Description,
                    FormFactor = repo.Get<RxFormFactor>(model.FormFactorId),
                    ChannelSet = repo.Get<RxChannelSet>(model.ChannelSetId),
                    FrontPanelDisplay = repo.Get<RxFrontPanelDisplay>(model.FrontPanelDisplayId),
                    FrontPanelInterface = repo.Get<RxFrontPanelInterface>(model.FrontPanelInterfaceId),
                    BasebandSet = repo.Get<RxBasebandSet>(model.BasebandSetId),
                    RfInputConnectorSet = repo.Get<RxConnectorSet>(model.RfInputConnectorSetId),
                    IfInputConnectorSet = repo.Get<RxConnectorSet>(model.IfInputConnectorSetId),
                    IfOutputConnectorSet = repo.Get<RxConnectorSet>(model.IfOutputConnectorSetId),
                    EthernetConnectorSet = repo.Get<RxConnectorSet>(model.EthernetConnectorSetId),
                    AnalogOutputConnectorSet = repo.Get<RxConnectorSet>(model.AnalogOutputConnectorSetId),
                    AmOutputConnectorSet = repo.Get<RxConnectorSet>(model.AmOutputConnectorSetId),
                    AgcOutputConnectorSet = repo.Get<RxConnectorSet>(model.AgcOutputConnectorSetId)
                };

                // create rxConfig
                unitOfWork.BeginTransaction();
                repo.Create<RxPackage>(rxConfig);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var rxConfig = repo.Get<RxPackage>(id);
            var model = mapper.Map<RxPackageViewModel>(rxConfig);

            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(RxPackageViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get existing rxConfig and map from model
                var rxConfig = repo.Get<RxPackage>(model.Id);
                rxConfig.Name = model.Name;
                rxConfig.Description = model.Description;
                rxConfig.FormFactor = repo.Get<RxFormFactor>(model.FormFactorId);
                rxConfig.ChannelSet = repo.Get<RxChannelSet>(model.ChannelSetId);
                rxConfig.FrontPanelDisplay = repo.Get<RxFrontPanelDisplay>(model.FrontPanelDisplayId);
                rxConfig.FrontPanelInterface = repo.Get<RxFrontPanelInterface>(model.FrontPanelInterfaceId);
                rxConfig.BasebandSet = repo.Get<RxBasebandSet>(model.BasebandSetId);
                rxConfig.RfInputConnectorSet = repo.Get<RxConnectorSet>(model.RfInputConnectorSetId);
                rxConfig.IfInputConnectorSet = repo.Get<RxConnectorSet>(model.IfInputConnectorSetId);
                rxConfig.IfOutputConnectorSet = repo.Get<RxConnectorSet>(model.IfOutputConnectorSetId);
                rxConfig.EthernetConnectorSet = repo.Get<RxConnectorSet>(model.EthernetConnectorSetId);
                rxConfig.AnalogOutputConnectorSet = repo.Get<RxConnectorSet>(model.AnalogOutputConnectorSetId);
                rxConfig.AmOutputConnectorSet = repo.Get<RxConnectorSet>(model.AmOutputConnectorSetId);
                rxConfig.AgcOutputConnectorSet = repo.Get<RxConnectorSet>(model.AgcOutputConnectorSetId);

                // update rxConfig
                unitOfWork.BeginTransaction();
                repo.Create<RxPackage>(rxConfig);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var rxConfig = repo.Get<RxPackage>(id);
            var model = mapper.Map<FormFactorDetailViewModel>(rxConfig);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var rxConfig = repo.Get<RxPackage>(id);
            var model = mapper.Map<DeleteViewModel>(rxConfig);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var rxConfig = repo.Get<RxPackage>(model.Id);

            unitOfWork.BeginTransaction();
            repo.Delete<RxPackage>(rxConfig);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void InflateViewModel(RxPackageViewModel model)
        {
            model.FormFactorOptions = repo.GetAll<RxFormFactor>().ToSelectItemList(x => x.Name, x => x.Id);
            model.ChannelSetOptions = repo.GetAll<RxChannelSet>().ToSelectItemList(x => x.Description, x => x.Id);
            model.FrontPanelDisplayOptions = repo.GetAll<RxFrontPanelDisplay>().ToSelectItemList(x => x.Name, x => x.Id);
            model.FrontPanelInterfaceOptions = repo.GetAll<RxFrontPanelInterface>().ToSelectItemList(x => x.Name, x => x.Id);
            model.ConnectorSetOptions = repo.GetAll<RxConnectorSet>().ToSelectItemList(x => x.Description, x => x.Id);
            model.BasebandSetOptions = repo.GetAll<RxBasebandSet>().ToSelectItemList(x => x.Description, x => x.Id);
        }

        private void ValidateViewModel(RxPackageViewModel model)
        {
            // check if rxConfig with name already exists, add error if not
            bool exists = repo.GetGroup<RxPackage>(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A rxConfig with the name {model.Name} already exists.");
        }
    }
}