﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class AddItemController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public AddItemController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Start()
        {
            return PartialView("_Start");
        }

        [HttpGet]
        public IActionResult TestEquipment()
        {
            return PartialView("_TestEquipment");
        }

        [HttpGet]
        public IActionResult RxAnalyzer()
        {
            var model = repo.GetAll<ReceiverAnalyzer>();
            return PartialView("_RxAnalyzer", model);
        }

        [HttpGet]
        public IActionResult FromQuote()
        {
            var model = new AddItemFromQuoteViewModel();
            InflateFromQuoteViewModel(model);
            return PartialView("_FromQuote", model);
        }

        [HttpPost]
        public IActionResult FromQuote(AddItemFromQuoteViewModel model)
        {
            InflateFromQuoteViewModel(model);
            return PartialView("_FromQuote", model);
        }

        [HttpPost]
        public IActionResult AddExistingItem(Guid quoteLineId)
        {
            var user = repo.Get<User>(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);

            var oldLine = repo.Get<QuoteLine>(quoteLineId);

            var quoteLine = new QuoteLine()
            {
                FullyConfigured = true,
                ConfigurationJson = oldLine.ConfigurationJson,
                Product = oldLine.Product,
                ProductType = oldLine.ProductType,
                Quote = activeQuote,
                Quantity = 1,
                ProductName = oldLine.Product.PartNumber,
                IncludedInQuote = true
            };

            unitOfWork.BeginTransaction();
            activeQuote.AddLine(quoteLine);
            repo.Update(quoteLine);
            unitOfWork.Commit();

            return RedirectToAction("Cart", "Quote");
        }

        private void InflateFromQuoteViewModel(AddItemFromQuoteViewModel model)
        {
            var user = repo.Get<User>(x => x.UserName == User.Identity.Name);

            model.Quotes = user.Quotes.ToSelectItemList(x => x.Number, x => x.Id);
            if (model.Quotes.Count > 0)
            {
                if (model.SelectedQuote == Guid.Empty)
                    model.SelectedQuote = Guid.Parse(model.Quotes[0].Value);

                var quote = repo.Get<Quote>(model.SelectedQuote);

                model.Products = quote.Lines.Select(x => new QuoteProductStubViewModel()
                {
                    PartNumber = x.Product.PartNumber,
                    Id = x.Id
                }).ToList();
            }
        }

        [HttpGet]
        public IActionResult FromPartNumber()
        {
            var model = new AddItemFromPartNumberViewModel();
            return PartialView("_FromPartNumber", model);
        }

        [HttpPost]
        public IActionResult FromPartNumber(AddItemFromPartNumberViewModel model)
        {
            if (string.IsNullOrWhiteSpace(model.PartNumber))
                return PartialView("_PartNumberResult", model);

            // always caps
            model.PartNumber = model.PartNumber.ToUpper();

            var parts = model.PartNumber.Split('-');

            if (parts.Length == 0 || parts[0] != "QSX")
            {
                return PartialView("_PartNumberResult", model);
            }

            if (parts.Length > 1)
            {
                // set optimistically
                model.IsRecognizedType = true;

                var part2 = parts[1];
                if (part2.StartsWith("V"))
                {
                    model.ProductType = ProductType.Transmitter;
                    DigestTxNumber(model);
                }
                else if (part2.StartsWith("RDMS"))
                {
                    model.ProductType = ProductType.Receiver;
                    DigestReceiverNumber(model);
                }
                else if (part2.StartsWith("PD"))
                {
                    model.ProductType = ProductType.Antenna;
                    DigestAntennaNumber(model);
                }
                else if (part2.StartsWith("EVTM"))
                {
                    model.ProductType = ProductType.EVTM;
                    DigestEvtmNumber(model);
                }
                else if (part2.StartsWith("RXSL"))
                {
                    model.ProductType = ProductType.StatusLogger;
                    DigestStatusLoggerNumber(model);
                }
                else if (part2.StartsWith("RXAN"))
                {
                    model.ProductType = ProductType.ReceiverAnalyzer;
                    DigestRxAnNumber(model);
                }
                else if (part2.StartsWith("AC"))
                {
                    model.ProductType = ProductType.Accessory;
                    DigestAccNumber(model);
                }
                else
                {
                    model.IsRecognizedType = false;
                }
            }

            return PartialView("_PartNumberResult", model);
        }

        private AddItemFromPartNumberViewModel DigestTxNumber(AddItemFromPartNumberViewModel model)
        {
            var parts = model.PartNumber.Split('-');

            var tx = new Transmitter();
            tx.PartNumber = model.PartNumber;

            // band combo
            if (parts.Length > 1 && parts[1].Length > 1)
            {
                var bandPart = parts[1][1].ToString();
                var bandCombo = repo.Get<TxBandCombination>(x => x.PartNumberDescriptor == bandPart);
                if (bandCombo != null)
                {
                    tx.BandCombination = bandCombo;
                    model.PartNumberDescriptors.Add(new LineItem($"({bandCombo.PartNumberDescriptor})", bandCombo.LineItemText));
                }
                else
                {
                    model.PartNumberDescriptors.Add(new LineItem(bandPart, "Invalid band"));
                }
            }
            else
            {
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing band"));
            }

            // baseband
            if (parts.Length > 1 && parts[1].Length > 2)
            {
                var basebandPart = parts[1][2].ToString();
                var baseband = repo.Get<TxBaseband>(x => x.PartNumberDescriptor == basebandPart);
                if (baseband != null)
                {
                    tx.Baseband = baseband;
                    model.PartNumberDescriptors.Add(new LineItem($"({baseband.PartNumberDescriptor})", baseband.LineItemText));
                }
                else
                {
                    model.PartNumberDescriptors.Add(new LineItem(basebandPart, "Invalid signal interface"));
                }
            }
            else
            {
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing signal interface"));
            }

            // serial
            if (parts.Length > 1 && parts[1].Length > 3)
            {
                var serialPart = parts[1][3..];
                var serial = repo.Get<TxSerialControlInterface>(x => x.PartNumberDescriptor == serialPart);
                if (serial != null)
                {
                    tx.SerialControlInterface = serial;
                    model.PartNumberDescriptors.Add(new LineItem($"({serial.PartNumberDescriptor})", serial.LineItemText));
                }
                else
                {
                    model.PartNumberDescriptors.Add(new LineItem(serialPart, "Invalid serial control interface"));
                }
            }
            else
            {
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing serial control interface"));
            }

            // modulation
            if (parts.Length > 2 && parts[2].Length > 0)
            {
                var modsPart = parts[2];
                var modChars = modsPart.ToCharArray();

                int max = Math.Max(4, modChars.Length);
                for (int i = 0; i < max; i++)
                {
                    // if number of mods less than the max of 4, add missing line
                    if (modChars.Length < (i + 1))
                    {
                        model.PartNumberDescriptors.Add(new LineItem("-", "Missing modulation"));
                        continue;
                    }

                    // if number of mods greater than the max of 4, add excess line with rest of string and bail from loop to avoid multiple lines
                    if (i >= 4)
                    {
                        string remainder = modsPart[4..];
                        model.PartNumberDescriptors.Add(new LineItem(remainder, "Excess modulation text"));
                        break;
                    }

                    // must be 0 or 1
                    string modString = modChars[i].ToString();
                    if (modString != "0" && modString != "1")
                    {
                        model.PartNumberDescriptors.Add(new LineItem(modString, "Invalid modulation text. Must be 0 or 1"));
                        continue;
                    }

                    // if we get here, everything is copacetic input-wise
                    // the modulation types are 1 based indexed 1-4. Get the one that matches our current loop position
                    var modPosType = (ModulationType)(i + 1);
                    var mod = repo.Get<TxModulation>(x => x.Type == modPosType);

                    // we know it's a 1 or 0 at this point, so if not a 1 it can be assumed 0
                    bool hasMod = modString == "1";
                    string hasModString = hasMod ? "Yes" : "No";

                    model.PartNumberDescriptors.Add(new LineItem(modString, $"{mod.Name} - {hasModString}"));

                    if (hasMod)
                        tx.Modulations.Add(mod);
                }
            }
            else
            {
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing modulation"));
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing modulation"));
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing modulation"));
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing modulation"));
            }

            // rf
            if (parts.Length > 3 && parts[3].Length > 0)
            {
                var rfPart = parts[3];
                var rf = repo.Get<TxRadioFrequencyPower>(x => x.PartNumberDescriptor == rfPart);
                if (rf != null)
                {
                    tx.RadioFrequencyPower = rf;
                    model.PartNumberDescriptors.Add(new LineItem($"({rf.PartNumberDescriptor})", rf.LineItemText));
                }
                else
                {
                    model.PartNumberDescriptors.Add(new LineItem(rfPart, "Invalid RF output power"));
                }
            }
            else
            {
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing RF output power"));
            }

            // pinout
            if (parts.Length > 4 && parts[4].Length > 0)
            {
                var pinoutPart = parts[4];
                var pinout = repo.Get<TxPinout>(x => x.Name == pinoutPart);
                if (pinout != null)
                {
                    tx.Pinout = pinout;
                    model.PartNumberDescriptors.Add(new LineItem($"({pinout.Name})", "Pinout"));
                }
                else
                {
                    model.PartNumberDescriptors.Add(new LineItem(pinoutPart, "Invalid pinout"));
                }
            }
            else
            {
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing pinout"));
            }

            // package
            if (parts.Length > 5 && parts[5].Length > 0)
            {
                var packagePart = parts[5];
                var package = repo.Get<TxPackage>(x => x.Name == packagePart);
                if (package != null)
                {
                    tx.Package = package;
                    double cubicSize = 0d;
                    string sizeString = "";
                    if (package.Footprint.Shape == FootprintShape.Rectangular)
                    {
                        cubicSize = Math.Round(package.Footprint.Length.Value * package.Footprint.Width.Value * package.Height.Measurement, 3);
                        sizeString = $"Package: {package.Footprint.Length.Value} x {package.Footprint.Width.Value} x {package.Height.Measurement} = {cubicSize} inches³";
                    }
                    else
                    {
                        double rad = package.Footprint.Diameter.Value / 2;
                        cubicSize = Math.Round((Math.PI * (rad * rad)) * package.Height.Measurement, 3);
                        sizeString = $"Package: {package.Footprint.Name} x {package.Height.Measurement} = {cubicSize} inches³";
                    }
                    //string connString = $"Primary Connector: {package.DataConnectors.First(x => x.Rank == ConnectorRank.Primary).DataConnector.Name}";
                    //model.PartNumberDescriptors.Add(new LineItem($"({package.Name})", $"{sizeString}\n{connString}"));
                }
                else
                {
                    model.PartNumberDescriptors.Add(new LineItem(packagePart, "Invalid pinout"));
                }
            }
            else
            {
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing pinout"));
            }

            // options
            if (parts.Length > 6 && parts[6].Length > 0)
            {
                // spin through remaining parts as it's options from here on out
                for (int i = 6; i < parts.Length; i++)
                {
                    var optionPart = parts[i];
                    var option = repo.Get<TxOption>(x => x.PartNumberDescriptor == optionPart);
                    if (option != null)
                    {
                        tx.Options.Add(option);
                        model.PartNumberDescriptors.Add(new LineItem($"({option.PartNumberDescriptor})", option.LineItemText));
                    }
                    else
                    {
                        model.PartNumberDescriptors.Add(new LineItem(optionPart, "Invalid option"));
                    }
                }
            }

            // check for mismatches
            if (tx.Package != null)
            {
                //if (tx.Pinout != null)
                //{
                //    var pinouts = repo.GetAll<TxPinout>().ToList();
                //    foreach (var dataConn in tx.Package.DataConnectors)
                //    {
                //        pinouts = pinouts.Where(x => x.DataConnectors.Any(x => x.Rank == dataConn.Rank
                //            && x.DataConnector == dataConn.DataConnector)).ToList();
                //    }

                //    if (!pinouts.Contains(tx.Pinout))
                //        model.Errors.Add($"Pinout {tx.Pinout.Name} not compatible with package {tx.Package.Name}");
                //}

                var stacks = tx.Package.Stacks.ToList();
                var filteredStacks = stacks;
                if (tx.BandCombination != null)
                {
                    if (!stacks.Select(x => x.BandCombination).Contains(tx.BandCombination))
                        model.Errors.Add($"Band combination {tx.BandCombination.Name} not compatible with package {tx.Package.Name}");

                    filteredStacks = filteredStacks.Where(x => x.BandCombination == tx.BandCombination).ToList();
                }

                if (tx.Baseband != null && filteredStacks.Count > 0)
                {
                    if (!stacks.SelectMany(x => x.Basebands).Contains(tx.Baseband))
                        model.Errors.Add($"Signal interface {tx.Baseband.Name} not compatible with package {tx.Package.Name}");

                    filteredStacks = filteredStacks.Where(x => x.Basebands.Contains(tx.Baseband)).ToList();
                    if (filteredStacks.Count == 0)
                        model.Errors.Add($"Invalid combination of band combination, signal interface, RF power, and/or serial control interface");
                }

                if (tx.SerialControlInterface != null && filteredStacks.Count > 0)
                {
                    if (!stacks.SelectMany(x => x.SerialControlInterfaces).Contains(tx.SerialControlInterface))
                        model.Errors.Add($"Signal interface {tx.SerialControlInterface.Name} not compatible with package {tx.Package.Name}");

                    filteredStacks = filteredStacks.Where(x => x.SerialControlInterfaces.Contains(tx.SerialControlInterface)).ToList();
                    if (filteredStacks.Count == 0)
                        model.Errors.Add($"Invalid combination of band combination, signal interface, RF power, and/or serial control interface");
                }

                if (tx.RadioFrequencyPower != null && filteredStacks.Count > 0)
                {
                    if (!stacks.Select(x => x.RadioFrequencyPower).Contains(tx.RadioFrequencyPower))
                        model.Errors.Add($"Signal interface {tx.SerialControlInterface.Name} not compatible with package {tx.Package.Name}");

                    filteredStacks = filteredStacks.Where(x => x.RadioFrequencyPower == tx.RadioFrequencyPower).ToList();
                    if (filteredStacks.Count == 0)
                        model.Errors.Add($"Invalid combination of band combination, signal interface, RF power, and/or serial control interface");
                }
            }

            model.IsValidPartNumber = tx.BandCombination != null &&
                tx.Baseband != null &&
                tx.SerialControlInterface != null &&
                tx.RadioFrequencyPower != null &&
                tx.Package != null &&
                tx.Pinout != null &&
                tx.Modulations.Any() &&
                model.Errors.Count == 0;

            model.ProductType = ProductType.Transmitter;
            model.Product = tx;

            return model;
        }

        private AddItemFromPartNumberViewModel DigestReceiverNumber(AddItemFromPartNumberViewModel model)
        {
            var parts = model.PartNumber.Split('-');

            var receiver = new Receiver();
            receiver.PartNumber = model.PartNumber;

            // package
            if (parts.Length > 2 && parts[2].Length > 0)
            {
                var packagePart = parts[2];
                var package = repo.Get<RxPackage>(x => x.PartNumberDescriptor == packagePart);
                if (package != null)
                {
                    receiver.Package = package;
                    model.PartNumberDescriptors.Add(new LineItem(package.PartNumberDescriptor, package.Description));
                }
                else
                {
                    model.PartNumberDescriptors.Add(new LineItem(packagePart, "Invalid package"));
                }
            }
            else
            {
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing package"));
            }

            // band combo 
            if (parts.Length > 3)
            {
                var bandExtPart = parts[3];
                var bandPart = bandExtPart.Length > 0 ? bandExtPart.Substring(0, 1) : "";
                var extPart = bandExtPart.Length > 1 ? bandExtPart[1..] : "";

                var bandCombo = repo.Get<RxBandCombination>(x => x.PartNumberDescriptor == bandPart);
                if (bandCombo != null && extPart.Length == 1 && (extPart == "0" || extPart == "1"))
                {
                    receiver.BandCombination = bandCombo;
                    string rangeString = extPart == "0" ? "standard range" : "extended range";
                    model.PartNumberDescriptors.Add(new LineItem(bandCombo.PartNumberDescriptor, $"{bandCombo.PartNumberDescriptor} band, {rangeString}"));
                }
                else
                {
                    model.PartNumberDescriptors.Add(new LineItem(bandExtPart, "Invalid band"));
                }
            }
            else
            {
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing band"));
            }

            // demodulation
            if (parts.Length > 4 && parts[4].Length > 0)
            {
                var modsPart = parts[4];
                var modChars = modsPart.ToCharArray();

                int max = Math.Max(4, modChars.Length);
                for (int i = 0; i < max; i++)
                {
                    // if number of mods less than the max of 4, add missing line
                    if (modChars.Length < (i + 1))
                    {
                        model.PartNumberDescriptors.Add(new LineItem("-", "Missing demodulation"));
                        continue;
                    }

                    // if number of mods greater than the max of 4, add excess line with rest of string and bail from loop to avoid multiple lines
                    if (i >= 4)
                    {
                        string remainder = modsPart[4..];
                        model.PartNumberDescriptors.Add(new LineItem(remainder, "Excess demodulation text"));
                        break;
                    }

                    // must be 0 or 1
                    string modString = modChars[i].ToString();
                    if (modString != "0" && modString != "1")
                    {
                        model.PartNumberDescriptors.Add(new LineItem(modString, "Invalid demodulation text. Must be 0 or 1"));
                        continue;
                    }

                    // if we get here, everything is copacetic input-wise
                    // the modulation types are 1 based indexed 1-4. Get the one that matches our current loop position
                    var modPosType = (ModulationType)(i + 1);
                    var demodulation = repo.Get<RxDemodulation>(x => x.Type == modPosType);

                    // we know it's a 1 or 0 at this point, so if not a 1 it can be assumed 0
                    bool hasMod = modString == "1";
                    string hasModString = hasMod ? "Yes" : "No";

                    model.PartNumberDescriptors.Add(new LineItem(modString, $"{demodulation.Name} - {hasModString}"));

                    if (hasMod)
                        receiver.Demodulations.Add(demodulation);
                }
            }
            else
            {
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing demodulation"));
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing demodulation"));
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing demodulation"));
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing demodulation"));
            }

            // pinout
            if (parts.Length > 5 && parts[5].Length > 0)
            {
                var pinoutPart = parts[5];
                var pinout = repo.Get<RxPinout>(x => x.PartNumberDescriptor == pinoutPart);
                if (pinout != null)
                {
                    receiver.Pinout = pinout;
                    model.PartNumberDescriptors.Add(new LineItem(pinout.PartNumberDescriptor, pinout.Name));
                }
                else
                {
                    model.PartNumberDescriptors.Add(new LineItem(pinoutPart, "Invalid pinout"));
                }
            }
            else
            {
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing pinout"));
            }

            // options
            if (parts.Length > 6 && parts[6].Length > 0)
            {
                // spin through remaining parts as it's options from here on out
                for (int i = 6; i < parts.Length; i++)
                {
                    var optionPart = parts[i];
                    var option = repo.Get<RxOption>(x => x.PartNumberDescriptor == optionPart);
                    if (option != null)
                    {
                        receiver.Options.Add(option);
                        model.PartNumberDescriptors.Add(new LineItem(option.PartNumberDescriptor, option.Name));
                    }
                    else
                    {
                        model.PartNumberDescriptors.Add(new LineItem(optionPart, "Invalid option"));
                    }
                }
            }

            // check for mismatches
            if (receiver.Package != null)
            {
                if (receiver.Pinout != null && !receiver.Package.FormFactor.Pinouts.Contains(receiver.Pinout))
                {
                    model.Errors.Add($"Pinout {receiver.Pinout.PartNumberDescriptor} not compatible with package {receiver.Package.PartNumberDescriptor}");
                }
            }

            model.IsValidPartNumber = receiver.BandCombination != null &&
                receiver.Package != null &&
                receiver.Pinout != null &&
                receiver.Demodulations.Any() &&
                model.Errors.Count == 0;

            model.ProductType = ProductType.Receiver;
            model.Product = receiver;

            return model;
        }

        private AddItemFromPartNumberViewModel DigestAntennaNumber(AddItemFromPartNumberViewModel model)
        {
            var parts = model.PartNumber.Split('-');

            var antenna = new Antenna();
            antenna.PartNumber = model.PartNumber;

            var pedestalPart = parts[1];
            var pedestal = repo.Get<AntPedestal>(x => x.PartNumberDescriptor == pedestalPart);
            if (pedestal != null)
            {
                antenna.Pedestal = pedestal;
                model.PartNumberDescriptors.Add(new LineItem(pedestal.PartNumberDescriptor, $"Pedestal {pedestal.Name}"));
            }
            else
            {
                model.PartNumberDescriptors.Add(new LineItem(pedestalPart, "Invalid pedestal"));
            }

            if (parts.Length > 2)
            {
                var reflectorPart = parts[2];
                var reflector = repo.Get<AntReflectorAssembly>(x => x.PartNumberDescriptor == reflectorPart);
                if (reflector != null)
                {
                    antenna.ReflectorAssembly = reflector;
                    model.PartNumberDescriptors.Add(new LineItem(reflector.PartNumberDescriptor, reflector.Name));
                }
                else
                {
                    model.PartNumberDescriptors.Add(new LineItem(reflectorPart, "Invalid reflector assembly"));
                }
            }
            else
            {
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing reflector assembly"));
            }

            if (parts.Length > 3)
            {
                var wiringPart = parts[3];
                var wirings = repo.GetGroup<AntWiring>(x => x.PartNumberDescriptor == wiringPart);
                if (wirings.Any())
                {
                    if (pedestal != null)
                    {
                        foreach (var wiring in wirings)
                        {
                            if (pedestal.Wirings.Contains(wiring))
                            {
                                antenna.Wiring = wiring;
                                model.PartNumberDescriptors.Add(new LineItem(wiring.PartNumberDescriptor, wiring.Name));
                                break;
                            }
                        }
                    }
                }
                else
                {
                    model.PartNumberDescriptors.Add(new LineItem(wiringPart, "Invalid wiring"));
                }
            }
            else
            {
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing wiring"));
            }

            if (parts.Length > 4)
            {
                var feedBandPart = parts[4];

                var feedPart = feedBandPart.Substring(0, 1);
                var feed = repo.Get<AntFeed>(x => x.PartNumberDescriptor == feedPart);
                if (feed != null)
                {
                    antenna.Feed = feed;
                    model.PartNumberDescriptors.Add(new LineItem(feed.PartNumberDescriptor, feed.Name));
                }
                else
                {
                    model.PartNumberDescriptors.Add(new LineItem(feedPart, "Invalid feed"));
                }

                if (feedBandPart.Length > 1)
                {
                    var bandPart = feedBandPart[1..];
                    var bandCombo = repo.Get<AntBandCombination>(x => x.PartNumberDescriptor == bandPart);
                    if (bandCombo != null)
                    {
                        antenna.BandCombination = bandCombo;
                        model.PartNumberDescriptors.Add(new LineItem(bandCombo.PartNumberDescriptor, bandCombo.Name));
                    }
                    else
                    {
                        model.PartNumberDescriptors.Add(new LineItem(bandPart, "Invalid band combination"));
                    }
                }
                else
                {
                    model.PartNumberDescriptors.Add(new LineItem("-", "Missing band combination"));
                }
            }
            else
            {
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing feed"));
                model.PartNumberDescriptors.Add(new LineItem("-", "Missing band combination"));
            }

            if (parts.Length > 5)
            {
                for (int i = 5; i < parts.Length; i++)
                {
                    var optionPart = parts[i];
                    var options = repo.GetGroup<AntOption>(x => x.PartNumberDescriptor == optionPart);
                    if (options.Any())
                    {
                        if (pedestal != null)
                        {
                            foreach (var option in options)
                            {
                                if (pedestal.Options.Contains(option))
                                {
                                    antenna.Options.Add(option);
                                    model.PartNumberDescriptors.Add(new LineItem(option.PartNumberDescriptor, option.Name));
                                }
                            }
                        }
                    }
                    else
                    {
                        model.PartNumberDescriptors.Add(new LineItem(optionPart, "Invalid option"));
                    }
                }
            }

            // check for mismatches
            if (antenna.Pedestal != null)
            {
                if (antenna.ReflectorAssembly != null && !antenna.Pedestal.ReflectorAssemblies.Contains(antenna.ReflectorAssembly))
                {
                    model.Errors.Add($"Reflector {antenna.ReflectorAssembly.PartNumberDescriptor} not compatible with pedestal {antenna.Pedestal.PartNumberDescriptor}");
                }
                if (antenna.Wiring != null && !antenna.Pedestal.Wirings.Contains(antenna.Wiring))
                {
                    model.Errors.Add($"Wiring {antenna.Wiring.PartNumberDescriptor} not compatible with pedestal {antenna.Pedestal.PartNumberDescriptor}");
                }
            }

            if (antenna.ReflectorAssembly != null)
            {
                if (antenna.Feed != null && !antenna.ReflectorAssembly.FocusArrangement.Feeds.Contains(antenna.Feed))
                {
                    model.Errors.Add($"Feed {antenna.Feed.PartNumberDescriptor} not compatible with reflector {antenna.ReflectorAssembly.PartNumberDescriptor}");
                }
            }

            if (antenna.Feed != null)
            {
                if (antenna.BandCombination != null && !antenna.Feed.BandCombinations.Contains(antenna.BandCombination))
                {
                    model.Errors.Add($"Band combination {antenna.Feed.PartNumberDescriptor} not compatible with feed {antenna.ReflectorAssembly.PartNumberDescriptor}");
                }
            }

            model.IsValidPartNumber = antenna.BandCombination != null &&
                antenna.Feed != null &&
                antenna.Pedestal != null &&
                antenna.ReflectorAssembly != null &&
                antenna.Wiring != null &&
                model.Errors.Count == 0;

            model.ProductType = ProductType.Antenna;
            model.Product = antenna;

            return model;
        }

        private AddItemFromPartNumberViewModel DigestEvtmNumber(AddItemFromPartNumberViewModel model)
        {
            var parts = model.PartNumber.Split('-');

            // if there isn't a 3rd section, it's bs, so just bail
            if (parts.Length < 3)
                return model;

            var evtm = repo.Get<Evtm>(x => x.PartNumber == model.PartNumber);

            if (evtm != null)
            {
                model.PartNumberDescriptors.Add(new LineItem(evtm.PartNumber[9..], $"{evtm.Description}"));
                model.IsValidPartNumber = true;
            }
            else
            {
                string packageString = model.PartNumber.Length > 9 ? model.PartNumber[9..] : "--";
                model.PartNumberDescriptors.Add(new LineItem(packageString, "Invalid EVTM Package"));
                model.IsValidPartNumber = false;
            }

            model.ProductType = ProductType.EVTM;
            model.Product = evtm;

            return model;
        }

        private AddItemFromPartNumberViewModel DigestStatusLoggerNumber(AddItemFromPartNumberViewModel model)
        {
            var parts = model.PartNumber.Split('-');

            // if there isn't a 3rd section, it's bs, so just bail
            if (parts.Length < 3)
                return model;

            var logger = repo.Get<StatusLogger>(x => x.PartNumber == model.PartNumber);

            if (logger != null)
            {
                model.PartNumberDescriptors.Add(new LineItem(logger.PartNumber[9..], $"{logger.Description}"));
                model.IsValidPartNumber = true;
            }
            else
            {
                string packageString = model.PartNumber.Length > 9 ? model.PartNumber[9..] : "--";
                model.PartNumberDescriptors.Add(new LineItem(packageString, "Invalid Status Logger"));
                model.IsValidPartNumber = false;
            }

            model.ProductType = ProductType.StatusLogger;
            model.Product = logger;

            return model;
        }

        private AddItemFromPartNumberViewModel DigestRxAnNumber(AddItemFromPartNumberViewModel model)
        {
            var parts = model.PartNumber.Split('-');

            // if there isn't a 3rd section, it's bs, so just bail
            if (parts.Length < 3)
                return model;

            var rxan = repo.Get<ReceiverAnalyzer>(x => x.PartNumber == model.PartNumber);

            if (rxan != null)
            {
                model.PartNumberDescriptors.Add(new LineItem(rxan.PartNumber[9..], $"{rxan.Description}"));
                model.IsValidPartNumber = true;
            }
            else
            {
                string packageString = model.PartNumber.Length > 9 ? model.PartNumber[9..] : "--";
                model.PartNumberDescriptors.Add(new LineItem(packageString, "Invalid Receciver Analyzer"));
                model.IsValidPartNumber = false;
            }

            model.ProductType = ProductType.ReceiverAnalyzer;
            model.Product = rxan;

            return model;
        }

        private AddItemFromPartNumberViewModel DigestAccNumber(AddItemFromPartNumberViewModel model)
        {
            var parts = model.PartNumber.Split('-');

            // if there isn't a 3rd section, it's bs, so just bail
            if (parts.Length < 3)
                return model;

            var acc = repo.Get<Accessory>(x => x.PartNumber == model.PartNumber);

            if (acc != null)
            {
                model.PartNumberDescriptors.Add(new LineItem(acc.PartNumber[9..], $"{acc.Description}"));
                model.IsValidPartNumber = true;
            }
            else
            {
                string packageString = model.PartNumber.Length > 9 ? model.PartNumber[9..] : "--";
                model.PartNumberDescriptors.Add(new LineItem(packageString, "Invalid Accessory"));
                model.IsValidPartNumber = false;
            }

            model.ProductType = ProductType.Accessory;
            model.Product = acc;

            return model;
        }

        [HttpPost]
        public IActionResult AddReceiver(AddItemFromPartNumberViewModel model)
        {
            model.PartNumber = model.PartNumber.ToUpper();
            DigestReceiverNumber(model);
            Receiver receiver = (Receiver)model.Product;

            var configModel = new ConfigureReceiverViewModel();
            configModel.AGCOutputChecklist = new List<RxConnectorSet>() { receiver.Package.AgcOutputConnectorSet }
                .ToSingleSelectGuidChecklist(x => x.Description, x => x.Id, receiver.Package.AgcOutputConnectorSet.Id);
            configModel.AMOutputChecklist = new List<RxConnectorSet>() { receiver.Package.AmOutputConnectorSet }
                .ToSingleSelectGuidChecklist(x => x.Description, x => x.Id, receiver.Package.AmOutputConnectorSet.Id);
            configModel.AnalogOutputChecklist = new List<RxConnectorSet>() { receiver.Package.AnalogOutputConnectorSet }
                .ToSingleSelectGuidChecklist(x => x.Description, x => x.Id, receiver.Package.AnalogOutputConnectorSet.Id);
            configModel.BandComboChecklist = new List<RxBandCombination>() { receiver.BandCombination }
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, receiver.BandCombination.Id);
            configModel.BasebandChecklist = new List<RxBasebandSet>() { receiver.Package.BasebandSet }
                .ToSingleSelectGuidChecklist(x => x.Description, x => x.Id, receiver.Package.BasebandSet.Id);
            configModel.ChannelCountChecklist = new List<RxChannelSet>() { receiver.Package.ChannelSet }
                .ToSingleSelectGuidChecklist(x => x.Description, x => x.Id, receiver.Package.ChannelSet.Id);
            configModel.DemodulationChecklist = receiver.Demodulations
                .ToMultiSelectChecklist(x => x.Description, x => x.Id, receiver.Demodulations.Select(x => x.Id));
            configModel.DisplayChecklist = new List<RxFrontPanelDisplay>() { receiver.Package.FrontPanelDisplay }
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, receiver.Package.FrontPanelDisplay.Id);
            configModel.FormFactorChecklist = new List<RxFormFactor>() { receiver.Package.FormFactor }
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, receiver.Package.FormFactor.Id);
            configModel.IFInputChecklist = new List<RxConnectorSet>() { receiver.Package.IfInputConnectorSet }
                .ToSingleSelectGuidChecklist(x => x.Description, x => x.Id, receiver.Package.IfInputConnectorSet.Id);
            configModel.OptionsChecklist = receiver.Options
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, receiver.Options.Select(x => x.Id));
            configModel.PinoutChecklist = new List<RxPinout>() { receiver.Pinout }
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, receiver.Pinout.Id);

            var user = repo.Get<User>(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);

            var json = JsonConvert.SerializeObject(configModel, Formatting.None, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var existingReceiver = repo.Get<Receiver>(x => x.PartNumber == receiver.PartNumber);
            if (existingReceiver != null)
                receiver = existingReceiver;

            var line = new QuoteLine()
            {
                FullyConfigured = true,
                IncludedInQuote = true,
                ConfigurationJson = json,
                Quote = activeQuote,
                Quantity = 1,
                ProductType = ProductType.Receiver,
                Product = receiver,
                ProductName = receiver.PartNumber
            };

            unitOfWork.BeginTransaction();
            activeQuote.AddLine(line);
            repo.Update(activeQuote);
            unitOfWork.Commit();

            return RedirectToAction("Cart", "Quote");
        }

        [HttpPost]
        public IActionResult AddAntenna(AddItemFromPartNumberViewModel model)
        {
            model.PartNumber = model.PartNumber.ToUpper();
            DigestAntennaNumber(model);
            Antenna antenna = (Antenna)model.Product;

            var configModel = new ConfigureAntennaViewModel();
            configModel.BandComboChecklist = new List<AntBandCombination>() { antenna.BandCombination }
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, antenna.BandCombination.Id);
            configModel.FeedChecklist = new List<AntFeed>() { antenna.Feed }
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, antenna.Feed.Id);
            configModel.FocusChecklist = new List<AntFocusArrangement>() { antenna.ReflectorAssembly.FocusArrangement }
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, antenna.ReflectorAssembly.FocusArrangement.Id);
            configModel.OptionChecklist = antenna.Options.ToList()
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, antenna.Options.Select(x => x.Id));
            configModel.ReflectorConstructionChecklist = new List<DishConstructionType>() { antenna.ReflectorAssembly.ConstructionType }
                .ToSingleSelectChecklist(x => Enum.GetName(typeof(DishConstructionType), x), x => (int)x, (int)antenna.ReflectorAssembly.ConstructionType);
            configModel.ReflectorSizeChecklist = new List<int>() { antenna.ReflectorAssembly.Diameter }
                .ToSingleSelectChecklist(x => x.ToString(), x => x, antenna.ReflectorAssembly.Diameter);
            configModel.WiringChecklist = new List<AntWiring>() { antenna.Wiring }
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, antenna.Wiring.Id);

            var user = repo.Get<User>(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);

            var json = JsonConvert.SerializeObject(configModel, Formatting.None, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var existingAntenna = repo.Get<Antenna>(x => x.PartNumber == antenna.PartNumber);
            if (existingAntenna != null)
                antenna = existingAntenna;

            var line = new QuoteLine()
            {
                FullyConfigured = true,
                IncludedInQuote = true,
                ConfigurationJson = json,
                Quote = activeQuote,
                Quantity = 1,
                ProductType = ProductType.Antenna,
                Product = antenna,
                ProductName = antenna.PartNumber
            };

            unitOfWork.BeginTransaction();
            activeQuote.AddLine(line);
            repo.Update(activeQuote);
            unitOfWork.Commit();

            return RedirectToAction("Cart", "Quote");
        }

        [HttpPost]
        public IActionResult AddEvtmFromPartNumber(AddItemFromPartNumberViewModel model)
        {
            model.PartNumber = model.PartNumber.ToUpper();
            DigestEvtmNumber(model);
            Evtm evtm = (Evtm)model.Product;

            var configModel = new ConfigureEvtmViewModel();
            configModel.ChannelCountChecklist = new List<int>() { evtm.ChannelCount }
                .ToSingleSelectChecklist(x => x.ToString(), x => x, evtm.ChannelCount);
            configModel.ClockDataChecklist = new List<EvtmClockData>() { evtm.ClockData }
                .ToSingleSelectChecklist(x => x.ToString(), x => (int)x, (int)evtm.ClockData);
            configModel.FormFactorChecklist = new List<EvtmFormFactor>() { evtm.FormFactor }
                .ToSingleSelectChecklist(x => x.ToString(), x => (int)x, (int)evtm.FormFactor);

            configModel.ShowChannelCountSection = true;
            configModel.ShowClockDataSection = true;
            configModel.ShowFormFactorSection = true;

            var user = repo.Get<User>(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);

            var json = JsonConvert.SerializeObject(configModel, Formatting.None, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var line = new QuoteLine()
            {
                FullyConfigured = true,
                IncludedInQuote = true,
                ConfigurationJson = json,
                Quote = activeQuote,
                Quantity = 1,
                ProductType = ProductType.EVTM,
                Product = evtm,
                ProductName = evtm.PartNumber
            };

            unitOfWork.BeginTransaction();
            activeQuote.AddLine(line);
            repo.Update(activeQuote);
            unitOfWork.Commit();

            return RedirectToAction("Cart", "Quote");
        }

        [HttpPost]
        public IActionResult AddStatusLoggerFromPartNumber(AddItemFromPartNumberViewModel model)
        {
            model.PartNumber = model.PartNumber.ToUpper();
            DigestStatusLoggerNumber(model);
            StatusLogger logger = (StatusLogger)model.Product;

            var configModel = new ConfigureStatusLoggerViewModel();
            configModel.FormFactorChecklist = new List<TestEqFormFactor>() { logger.FormFactor }
                .ToSingleSelectChecklist(x => x.ToString(), x => (int)x, (int)logger.FormFactor);
            configModel.ReceiverCountChecklist = new List<int>() { logger.ReceiverCount }
                .ToSingleSelectChecklist(x => x.ToString(), x => x, logger.ReceiverCount);

            configModel.ShowFormFactorSection = true;
            configModel.ShowReceiverCountSection = true;

            var user = repo.Get<User>(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);

            var json = JsonConvert.SerializeObject(configModel, Formatting.None, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var line = new QuoteLine()
            {
                FullyConfigured = true,
                IncludedInQuote = true,
                ConfigurationJson = json,
                Quote = activeQuote,
                Quantity = 1,
                ProductType = ProductType.StatusLogger,
                Product = logger,
                ProductName = logger.PartNumber
            };

            unitOfWork.BeginTransaction();
            activeQuote.AddLine(line);
            repo.Update(activeQuote);
            unitOfWork.Commit();

            return RedirectToAction("Cart", "Quote");
        }

        [HttpPost]
        public IActionResult AddRxAnFromPartNumber(AddItemFromPartNumberViewModel model)
        {
            model.PartNumber = model.PartNumber.ToUpper();
            DigestRxAnNumber(model);
            ReceiverAnalyzer rxan = (ReceiverAnalyzer)model.Product;

            var user = repo.Get<User>(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);

            var line = new QuoteLine()
            {
                FullyConfigured = true,
                IncludedInQuote = true,
                ConfigurationJson = "",
                Quote = activeQuote,
                Quantity = 1,
                ProductType = ProductType.ReceiverAnalyzer,
                Product = rxan,
                ProductName = rxan.PartNumber
            };

            unitOfWork.BeginTransaction();
            activeQuote.AddLine(line);
            repo.Update(activeQuote);
            unitOfWork.Commit();

            return RedirectToAction("Cart", "Quote");
        }

        [HttpPost]
        public IActionResult AddAccFromPartNumber(AddItemFromPartNumberViewModel model)
        {
            model.PartNumber = model.PartNumber.ToUpper();
            DigestAccNumber(model);
            Accessory acc = (Accessory)model.Product;

            var user = repo.Get<User>(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);

            var line = new QuoteLine()
            {
                FullyConfigured = true,
                IncludedInQuote = true,
                ConfigurationJson = "",
                Quote = activeQuote,
                Quantity = 1,
                ProductType = ProductType.Accessory,
                Product = acc,
                ProductName = acc.PartNumber
            };

            unitOfWork.BeginTransaction();
            activeQuote.AddLine(line);
            repo.Update(activeQuote);
            unitOfWork.Commit();

            return RedirectToAction("Cart", "Quote");
        }

        [HttpPost]
        public IActionResult AddRxAnalyzer(Guid id)
        {
            var user = repo.Get<User>(x => x.UserName == User.Identity.Name);
            var activeQuote = user.Quotes.FirstOrDefault(x => x.Active);

            var rxan = repo.Get<ReceiverAnalyzer>(id);
            var line = new QuoteLine()
            {
                FullyConfigured = true,
                IncludedInQuote = true,
                ConfigurationJson = "",
                Quote = activeQuote,
                Quantity = 1,
                ProductType = ProductType.ReceiverAnalyzer,
                Product = rxan,
                ProductName = rxan.PartNumber
            };

            unitOfWork.BeginTransaction();
            activeQuote.AddLine(line);
            repo.Update(activeQuote);
            unitOfWork.Commit();

            return RedirectToAction("Cart", "Quote");
        }
    }
}