﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class TxBandController : Controller
    {
        private readonly ICatalogRepository<TxBand> bandRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TxBandController(ICatalogRepository<TxBand> bandRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.bandRepo = bandRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var bands = bandRepo.GetAll();
            return View(bands);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new BandViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(BandViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map band from model
                var band = mapper.Map<TxBand>(model);

                // create band
                unitOfWork.BeginTransaction();
                bandRepo.Create(band);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var band = bandRepo.Get(id);
            var model = mapper.Map<BandViewModel>(band);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(BandViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get existing band and map from model
                var band = bandRepo.Get(model.Id);
                mapper.Map(model, band);

                // update band
                unitOfWork.BeginTransaction();
                bandRepo.Update(band);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var band = bandRepo.Get(id);
            var model = mapper.Map<BandDetailViewModel>(band);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var band = bandRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(band);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var band = bandRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            bandRepo.Delete(band);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(BandViewModel model)
        {
            // check if band with name already exists, add error if not
            bool exists = bandRepo.GetGroup(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A band with the name {model.Name} already exists.");
        }
    }
}