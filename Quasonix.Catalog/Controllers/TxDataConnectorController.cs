﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class TxDataConnectorController : Controller
    {
        private readonly ICatalogRepository<TxDataConnector> dataConnectorRepo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TxDataConnectorController(ICatalogRepository<TxDataConnector> dataConnectorRepo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.dataConnectorRepo = dataConnectorRepo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var dataConnectors = dataConnectorRepo.GetAll();
            return View(dataConnectors);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new DataConnectorViewModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(DataConnectorViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map connector from model
                var dataConnector = mapper.Map<TxDataConnector>(model);

                // create conector
                unitOfWork.BeginTransaction();
                dataConnectorRepo.Create(dataConnector);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var dataConnector = dataConnectorRepo.Get(id);
            var model = mapper.Map<DataConnectorViewModel>(dataConnector);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(DataConnectorViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get connector and map from model
                var dataConnector = dataConnectorRepo.Get(model.Id);
                mapper.Map(model, dataConnector);

                // update connector in db
                unitOfWork.BeginTransaction();
                dataConnectorRepo.Update(dataConnector);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var dataConnector = dataConnectorRepo.Get(id);
            var model = mapper.Map<DataConnectorDetailViewModel>(dataConnector);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var dataConnector = dataConnectorRepo.Get(id);
            var model = mapper.Map<DeleteViewModel>(dataConnector);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var dataConnector = dataConnectorRepo.Get(model.Id);

            unitOfWork.BeginTransaction();
            dataConnectorRepo.Delete(dataConnector);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void ValidateViewModel(DataConnectorViewModel model)
        {
            // check if connector with name already exists, add error if not
            bool exists = dataConnectorRepo.GetGroup(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A connector with the name {model.Name} already exists.");
        }
    }
}