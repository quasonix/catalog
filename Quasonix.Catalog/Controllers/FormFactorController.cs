﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;

namespace Quasonix.Catalog.Controllers
{
    [Authorize]
    public class FormFactorController : Controller
    {
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public FormFactorController(ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.repo = repo;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var formFactors = repo.GetAll<RxFormFactor>();
            return View(formFactors);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new FormFactorViewModel();
            InflateViewModel(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(FormFactorViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // map formFactor from model
                var formFactor = mapper.Map<RxFormFactor>(model);

                foreach (var optionId in model.OptionChecklist.SelectedValues)
                {
                    // add selected option
                    var option = repo.Get<RxOption>(optionId);
                    formFactor.Options.Add(option);

                    // add form factor to option, need both sides
                    option.FormFactors.Add(formFactor);
                }

                foreach (var pinoutId in model.PinoutChecklist.SelectedValues)
                {
                    // add selected option
                    var pinout = repo.Get<RxPinout>(pinoutId);
                    formFactor.Pinouts.Add(pinout);

                    // add form factor to option, need both sides
                    pinout.FormFactors.Add(formFactor);
                }

                // create formFactor
                unitOfWork.BeginTransaction();
                repo.Create<RxFormFactor>(formFactor);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var formFactor = repo.Get<RxFormFactor>(id);
            var model = mapper.Map<FormFactorViewModel>(formFactor);
            InflateViewModel(model, formFactor);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(FormFactorViewModel model)
        {
            ValidateViewModel(model);

            if (ModelState.IsValid)
            {
                // get existing formFactor and map from model
                var formFactor = repo.Get<RxFormFactor>(model.Id);
                mapper.Map(model, formFactor);

                formFactor.ClearOptions();
                foreach (var optionId in model.OptionChecklist.SelectedValues)
                {
                    // add selected option
                    var option = repo.Get<RxOption>(optionId);
                    formFactor.Options.Add(option);

                    // add form factor to option, need both sides
                    option.FormFactors.Add(formFactor);
                }

                formFactor.ClearPinouts();
                foreach (var pinoutId in model.PinoutChecklist.SelectedValues)
                {
                    // add selected option
                    var pinout = repo.Get<RxPinout>(pinoutId);
                    formFactor.Pinouts.Add(pinout);

                    // add form factor to option, need both sides
                    pinout.FormFactors.Add(formFactor);
                }

                // update formFactor
                unitOfWork.BeginTransaction();
                repo.Update<RxFormFactor>(formFactor);
                unitOfWork.Commit();

                return RedirectToAction("Index");
            }

            InflateViewModel(model);
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var formFactor = repo.Get<RxFormFactor>(id);
            var model = mapper.Map<FormFactorDetailViewModel>(formFactor);
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            var formFactor = repo.Get<RxFormFactor>(id);
            var model = mapper.Map<DeleteViewModel>(formFactor);
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(DeleteViewModel model)
        {
            var formFactor = repo.Get<RxFormFactor>(model.Id);

            unitOfWork.BeginTransaction();
            repo.Delete<RxFormFactor>(formFactor);
            unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        private void InflateViewModel(FormFactorViewModel model)
        {
            model.OptionChecklist = repo.GetAll<RxOption>().ToMultiSelectChecklist(x => x.Name, x => x.Id, model.OptionChecklist.SelectedValues);
            model.PinoutChecklist = repo.GetAll<RxPinout>().ToMultiSelectChecklist(x => x.Name, x => x.Id, model.PinoutChecklist.SelectedValues);
        }

        private void InflateViewModel(FormFactorViewModel model, RxFormFactor formFactor)
        {
            model.OptionChecklist = repo.GetAll<RxOption>().ToMultiSelectChecklist(x => x.Name, x => x.Id, formFactor.Options.Select(x => x.Id));
            model.PinoutChecklist = repo.GetAll<RxPinout>().ToMultiSelectChecklist(x => x.Name, x => x.Id, formFactor.Pinouts.Select(x => x.Id));
        }

        private void ValidateViewModel(FormFactorViewModel model)
        {
            // check if formFactor with name already exists, add error if not
            bool exists = repo.GetGroup<RxFormFactor>(x => x.Name == model.Name && x.Id != model.Id).Any();
            if (exists)
                ModelState.AddModelError("ExistsError", $"A formFactor with the name {model.Name} already exists.");
        }
    }
}