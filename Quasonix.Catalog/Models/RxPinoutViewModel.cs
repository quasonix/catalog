﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class RxPinoutViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Part Number Description")]
        public string PartNumberDescriptor { get; set; }
    }

    public class RxPinoutDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
