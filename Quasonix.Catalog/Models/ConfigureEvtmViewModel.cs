﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class ConfigureEvtmViewModel
    {
        public string AddToQuote { get; set; }
        public string SaveForLater { get; set; }
        public Guid LineId { get; set; }

        public bool ShowFormFactorSection { get; set; }
        public bool ShowChannelCountSection { get; set; }
        public bool ShowClockDataSection { get; set; }
        public bool ShowOptionSection { get; set; }

        public SingleSelectChecklistViewModel FormFactorChecklist { get; set; } = new SingleSelectChecklistViewModel();
        public SingleSelectChecklistViewModel ChannelCountChecklist { get; set; } = new SingleSelectChecklistViewModel();
        public SingleSelectChecklistViewModel ClockDataChecklist { get; set; } = new SingleSelectChecklistViewModel();
        public SingleSelectChecklistViewModel OptionChecklist { get; set; } = new SingleSelectChecklistViewModel();

        public string PackageText { get; set; }

        [JsonIgnore]
        public Evtm Evtm { get; set; }

        public bool IsComplete => Evtm != null;

        public string PartNumber => Evtm?.PartNumber;

        public List<LineItem> LineItems
        {
            get
            {
                var lineItems = new List<LineItem>();
                lineItems.Add(new LineItem($"(EVTM)", "Stand-alone Ethernet Via Telemetry Encoder/Decoder"));

                if (Evtm != null)
                    lineItems.Add(new LineItem($"({PackageText})", Evtm.Description));

                return lineItems;
            }
        }
    }
}
