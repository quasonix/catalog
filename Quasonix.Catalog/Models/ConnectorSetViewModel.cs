﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class ConnectorSetViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Description { get; set; }
        public DynamicList<ConnectorForConnectorSetViewModel> Connectors { get; set; } = new DynamicList<ConnectorForConnectorSetViewModel>();
    }

    public class ConnectorForConnectorSetViewModel
    {
        public Guid Id { get; set; }
        public int Count { get; set; }
        [Display(Name = "Connector")]
        public Guid ConnectorId { get; set; }
        public IList<SelectListItem> ConnectorOptions { get; set; } = new List<SelectListItem>();
    }
}
