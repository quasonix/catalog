﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class ConfigureAntennaViewModel
    {
        public string AddToQuote { get; set; }
        public string SaveForLater { get; set; }
        public Guid LineId { get; set; }

        public bool ShowReflectorSizeSection { get; set; }
        public bool ShowReflectorConstructionSection { get; set; }
        public bool ShowFocusSection { get; set; }
        public bool ShowWiringSection { get; set; }
        public bool ShowFeedSection { get; set; }
        public bool ShowBandCombinationSection { get; set; }
        public bool ShowOptionSection { get; set; }

        public SingleSelectChecklistViewModel ReflectorSizeChecklist { get; set; } = new SingleSelectChecklistViewModel();
        public SingleSelectChecklistViewModel ReflectorConstructionChecklist { get; set; } = new SingleSelectChecklistViewModel();
        public SingleSelectGuidChecklistViewModel FocusChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel WiringChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel FeedChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();

        public MultiSelectChecklistViewModel OptionChecklist { get; set; } = new MultiSelectChecklistViewModel();

        public MultiSelectChecklistViewModel BandChecklist { get; set; } = new MultiSelectChecklistViewModel();
        public SingleSelectGuidChecklistViewModel BandComboChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public List<List<bool>> BandCheckmarks { get; set; } = new List<List<bool>>();

        [JsonIgnore]
        public AntPedestal Pedestal { get; set; }
        [JsonIgnore]
        public AntReflectorAssembly ReflectorAssembly { get; set; }
        [JsonIgnore]
        public AntWiring Wiring { get; set; }
        [JsonIgnore]
        public AntFeed Feed { get; set; }
        [JsonIgnore]
        public AntBandCombination BandCombination { get; set; }
        [JsonIgnore]
        public List<AntOption> SelectedOptions { get; set; } = new List<AntOption>();

        public bool IsComplete => Pedestal != null &&
            ReflectorAssembly != null &&
            Wiring != null &&
            Feed != null &&
            BandCombination != null;

        public string PedestalString => Pedestal != null ? Pedestal.PartNumberDescriptor : "PDXXX";
        public string ReflectorAssemblyString => ReflectorAssembly != null ? ReflectorAssembly.PartNumberDescriptor : "XXX";
        public string WiringString => Wiring != null ? Wiring.PartNumberDescriptor : "XX";
        public string FeedString => Feed != null ? Feed.PartNumberDescriptor : "X";
        public string BandCombinationString => BandCombination != null ? BandCombination.PartNumberDescriptor : "X";
        public string OptionString => string.Join(' ', SelectedOptions.Select(x => x.PartNumberDescriptor));
        public string OptionStringAlt => string.Concat(SelectedOptions.OrderBy(x => x.PartNumberDescriptor).Select(x => "-" + x.PartNumberDescriptor));

        public string PartNumber => $"QSX-{PedestalString}-{ReflectorAssemblyString}-{WiringString}-{FeedString}{BandCombinationString}{OptionStringAlt}";

        public List<LineItem> LineItems
        {
            get
            {
                var lineItems = new List<LineItem>();

                if (Pedestal != null)
                    lineItems.Add(new LineItem($"({Pedestal.PartNumberDescriptor})", $"Pedestal {Pedestal.Name}"));
                else
                    lineItems.Add(new LineItem($"({PedestalString})", $"Pedestal not selected"));

                if (ReflectorAssembly != null)
                    lineItems.Add(new LineItem($"({ReflectorAssembly.PartNumberDescriptor})", $"Reflector assembly {ReflectorAssembly.Name}"));
                else
                    lineItems.Add(new LineItem($"({ReflectorAssemblyString})", $"Reflector assembly not selected"));

                if (Wiring != null)
                    lineItems.Add(new LineItem($"({Wiring.PartNumberDescriptor})", $"{Wiring.Name}"));
                else
                    lineItems.Add(new LineItem($"({WiringString})", $"Wiring not selected"));

                if (Feed != null)
                    lineItems.Add(new LineItem($"({Feed.PartNumberDescriptor})", $"{Feed.Name}"));
                else
                    lineItems.Add(new LineItem($"({FeedString})", $"Feed not selected"));

                if (BandCombination != null)
                    lineItems.Add(new LineItem($"({BandCombination.PartNumberDescriptor})", $"{BandCombination.Name}"));
                else
                    lineItems.Add(new LineItem($"({BandCombinationString})", $"Band combination not selected"));

                foreach (var option in SelectedOptions.OrderBy(x => x.PartNumberDescriptor))
                {
                    lineItems.Add(new LineItem($"({option.PartNumberDescriptor})", $"{option.Name}"));
                }

                return lineItems;
            }
        }
    }
}
