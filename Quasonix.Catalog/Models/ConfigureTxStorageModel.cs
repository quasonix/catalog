﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class ConfigureTxStorageModel
    {
        public Guid LineId { get; set; }

        public SingleSelectGuidChecklistViewModel BandCombinationChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel BasebandChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectChecklistViewModel DataConnectorPositionChecklist { get; set; } = new SingleSelectChecklistViewModel();
        public SingleSelectGuidChecklistViewModel FootprintChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel HeightChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel OptionCombinationChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectChecklistViewModel RadioConnectorPositionChecklist { get; set; } = new SingleSelectChecklistViewModel();
        public SingleSelectGuidChecklistViewModel RadioPowerChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel SerialChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();

        public MultiSelectChecklistViewModel BandChecklist { get; set; } = new MultiSelectChecklistViewModel();
        public MultiSelectChecklistViewModel HardwareOptionChecklist { get; set; } = new MultiSelectChecklistViewModel();
        public MultiSelectChecklistViewModel ModulationChecklist { get; set; } = new MultiSelectChecklistViewModel();
        public MultiSelectChecklistViewModel OptionChecklist { get; set; } = new MultiSelectChecklistViewModel();

        public List<List<bool>> BandCheckmarks { get; set; }
        public List<List<bool>> HardwareOptionCheckmarks { get; set; }

        public List<Guid> OptionCombinationFilter { get; set; }

        public bool ShowBandCombinationSection { get; set; }
        public bool ShowBasebandSection { get; set; }
        public bool ShowDataConnectorSection { get; set; }
        public bool ShowFootprintSection { get; set; }
        public bool ShowHeightSection { get; set; }
        public bool ShowModulationSection { get; set; }
        public bool ShowOptionSection { get; set; }
        public bool ShowOptionCombinationSection { get; set; }
        public bool ShowRadioConnectorSection { get; set; }
        public bool ShowRadioPowerSection { get; set; }
        public bool ShowSerialSection { get; set; }
    }
}
