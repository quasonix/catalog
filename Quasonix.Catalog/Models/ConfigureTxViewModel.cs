﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class ConfigureTxViewModel
    {
        public string AddToQuote { get; set; }
        public string SaveForLater { get; set; }

        public Guid LineId { get; set; }

        public SingleSelectGuidChecklistViewModel BandCombinationChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel BasebandChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectChecklistViewModel DataConnectorPositionChecklist { get; set; } = new SingleSelectChecklistViewModel();
        public SingleSelectGuidChecklistViewModel FootprintChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel HeightChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel OptionCombinationChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectChecklistViewModel RadioConnectorPositionChecklist { get; set; } = new SingleSelectChecklistViewModel();
        public SingleSelectGuidChecklistViewModel RadioPowerChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel SerialChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();

        public MultiSelectChecklistViewModel BandChecklist { get; set; } = new MultiSelectChecklistViewModel();
        public MultiSelectChecklistViewModel HardwareOptionChecklist { get; set; } = new MultiSelectChecklistViewModel();
        public MultiSelectChecklistViewModel ModulationChecklist { get; set; } = new MultiSelectChecklistViewModel();
        public MultiSelectChecklistViewModel OptionChecklist { get; set; } = new MultiSelectChecklistViewModel();

        public List<List<bool>> BandCheckmarks { get; set; }
        public List<List<bool>> HardwareOptionCheckmarks { get; set; }

        public List<Guid> OptionCombinationFilter { get; set; }

        public bool ShowBandCombinationSection { get; set; }
        public bool ShowBasebandSection { get; set; }
        public bool ShowDataConnectorSection { get; set; }
        public bool ShowFootprintSection { get; set; }
        public bool ShowHeightSection { get; set; }
        public bool ShowModulationSection { get; set; }
        public bool ShowOptionSection { get; set; }
        public bool ShowOptionCombinationSection { get; set; }
        public bool ShowRadioConnectorSection { get; set; }
        public bool ShowRadioPowerSection { get; set; }
        public bool ShowSerialSection { get; set; }

        public List<string> HardwareOptionDefinitions { get; set; } = new List<string>();

        [JsonIgnore]
        public TxBandCombination BandCombination { get; set; }
        [JsonIgnore]
        public TxBaseband Baseband { get; set; }
        [JsonIgnore]
        public ConnectorPosition DataConnectorPosition { get; set; }
        [JsonIgnore]
        public TxFootprint Footprint { get; set; }
        [JsonIgnore]
        public TxHeight Height { get; set; }
        [JsonIgnore]
        public TxOptionCombination OptionCombination { get; set; }
        [JsonIgnore]
        public List<TxModulation> Modulations { get; set; } = new List<TxModulation>();
        [JsonIgnore]
        public List<TxOption> Options { get; set; } = new List<TxOption>();
        [JsonIgnore]
        public TxPackage Package { get; set; }
        [JsonIgnore]
        public TxPinout Pinout { get; set; }
        [JsonIgnore]
        public ConnectorPosition RadioConnectorPosition { get; set; }
        [JsonIgnore]
        public TxRadioFrequencyPower RadioPower { get; set; }
        [JsonIgnore]
        public TxSerialControlInterface SerialControlInterface { get; set; }
        [JsonIgnore]
        public TxStack Stack { get; set; }

        public bool HasArtm0 => Modulations.Any(x => x.Type == ModulationType.ARTM0);
        public bool HasArtm1 => Modulations.Any(x => x.Type == ModulationType.ARTM1);
        public bool HasArtm2 => Modulations.Any(x => x.Type == ModulationType.ARTM2);
        public bool HasBPSK => Modulations.Any(x => x.Type == ModulationType.PSK);

        public string ModulationString
        {
            get
            {
                string modString = "????";

                if (Modulations.Any())
                {
                    string tier0 = HasArtm0 ? "1" : "0";
                    string tier1 = HasArtm1 ? "1" : "0";
                    string tier2 = HasArtm2 ? "1" : "0";
                    string legacy = HasBPSK ? "1" : "0";

                    modString = $"{tier0}{tier1}{tier2}{legacy}";
                }

                return modString;
            }
        }

        public bool IsComplete => BandCombination != null
            && Baseband != null
            && SerialControlInterface != null
            && RadioPower != null
            && Package != null
            && Pinout != null
            && (HasArtm0 || HasArtm1 || HasArtm2 || HasBPSK);

        [JsonIgnore]
        public List<LineItem> LineItems
        {
            get
            {
                var lineItems = new List<LineItem>();
                lineItems.Add(new LineItem($"(V)", "Variable Rate, auto-scaling of deviation and filter BW"));

                if (BandCombination != null)
                    lineItems.Add(new LineItem($"({BandCombination.PartNumberDescriptor})", BandCombination.LineItemText));
                else
                    lineItems.Add(new LineItem($"(?)", "Band combination not selected"));

                if (Baseband != null)
                    lineItems.Add(new LineItem($"({Baseband.PartNumberDescriptor})", Baseband.LineItemText));
                else
                    lineItems.Add(new LineItem($"(?)", "Signal interface not selected"));

                if (SerialControlInterface != null)
                    lineItems.Add(new LineItem($"({SerialControlInterface.PartNumberDescriptor})", SerialControlInterface.LineItemText));
                else
                    lineItems.Add(new LineItem($"(?)", "Serial control not selected"));

                if (Modulations.Any())
                {
                    lineItems.Add(new LineItem(HasArtm0 ? "(1)" : $"({emptyMod})", "ARTM Tier 0 (PCM/FM), 0.05 - 14 Mbps: " + (HasArtm0 ? "YES" : "NO")));
                    lineItems.Add(new LineItem(HasArtm1 ? "(1)" : $"({emptyMod})", "ARTM Tier I (SOQPSK-TG), 0.1 - 28 Mbps: " + (HasArtm1 ? "YES" : "NO")));
                    lineItems.Add(new LineItem(HasArtm2 ? "(1)" : $"({emptyMod})", "ARTM Tier II (Multi-h CPM), 0.1 - 28 Mbps: " + (HasArtm2 ? "YES" : "NO")));
                    lineItems.Add(new LineItem(HasBPSK ? "(1)" : $"({emptyMod})", "BPSK, QPSK, OQPSK 0.05 - 14 Mbps: " + (HasBPSK ? "YES" : "NO")));
                }
                else
                {
                    lineItems.Add(new LineItem(HasArtm0 ? "(1)" : $"({emptyMod})", "ARTM Tier 0 (PCM/FM), 0.05 - 14 Mbps: NO SELECTION"));
                    lineItems.Add(new LineItem(HasArtm1 ? "(1)" : $"({emptyMod})", "ARTM Tier I (SOQPSK-TG), 0.1 - 28 Mbps: NO SELECTION"));
                    lineItems.Add(new LineItem(HasArtm2 ? "(1)" : $"({emptyMod})", "ARTM Tier II (Multi-h CPM), 0.1 - 28 Mbps: NO SELECTION"));
                    lineItems.Add(new LineItem(HasBPSK ? "(1)" : $"({emptyMod})", "BPSK, QPSK, OQPSK 0.05 - 14 Mbps: NO SELECTION"));
                }

                if (RadioPower != null)
                    lineItems.Add(new LineItem($"({RadioPower.PartNumberDescriptor})", RadioPower.LineItemText));
                else
                    lineItems.Add(new LineItem($"(??)", "RF output power not selected"));

                if (Pinout != null && Pinout.Name != "XX")
                    lineItems.Add(new LineItem($"({Pinout.Name})", "Pinout Configuration"));
                else if (Pinout != null && Pinout.Name == "XX")
                    lineItems.Add(new LineItem($"({Pinout.Name})", "The options chosen require a new/custom pinout; Quasonix will verify feasibility"));
                else
                    lineItems.Add(new LineItem($"(??)", "Pinout not yet determined"));

                if (Package != null)
                {
                    double cubicSize = 0d;
                    string sizeString = "";
                    if (Package.Footprint.Shape == FootprintShape.Rectangular)
                    {
                        cubicSize = Math.Round(Package.Footprint.Length.Value * Package.Footprint.Width.Value * Package.Height.Measurement, 3);
                        sizeString = $"Package: {Package.Footprint.Length.Value} x {Package.Footprint.Width.Value} x {Package.Height.Measurement} = {cubicSize} inches³";
                    }
                    else
                    {
                        double rad = Package.Footprint.Diameter.Value / 2;
                        cubicSize = Math.Round((Math.PI * (rad * rad)) * Package.Height.Measurement, 3);
                        sizeString = $"Package: {Package.Footprint.Name} x {Package.Height.Measurement} = {cubicSize} inches³";
                    }
                    string connString = $"Primary Connector: {Package.ConnectorSet.ConnectorSetDataConnectors.First(x => x.Rank == ConnectorRank.Primary).DataConnector.Name}";
                    string rfString = $"RF Output Connector:";
                    lineItems.Add(new LineItem($"({Package.Name})", $"{sizeString}\n{connString}\n{rfString}"));
                }
                else
                    lineItems.Add(new LineItem($"(????)", "Package not yet determined"));


                foreach (var option in Options.OrderBy(x => x.Name))
                {
                    lineItems.Add(new LineItem($"({option.PartNumberDescriptor})", option.LineItemText));
                }

                return lineItems;
            }
        }

        private string bandComboPart => BandCombination == null ? "?" : BandCombination.PartNumberDescriptor;
        private string basebandPart => Baseband == null ? "?" : Baseband.PartNumberDescriptor;
        private string serialPart => SerialControlInterface == null ? "?" : SerialControlInterface.PartNumberDescriptor;
        // use x's if no mod selected, otherwise 0, to keep consistency with other unselecteds
        private string emptyMod => Modulations.Any() ? "0" : "?";
        private string modulationPart => (HasArtm0 ? "1" : emptyMod) + (HasArtm1 ? "1" : emptyMod) + (HasArtm2 ? "1" : emptyMod) + (HasBPSK ? "1" : emptyMod);
        private string rfPart => RadioPower == null ? "??" : RadioPower.PartNumberDescriptor;
        private string pinoutPart => Pinout == null ? "??" : Pinout.Name;
        private string packagePart => Package == null ? "????" : Package.Name;
        public string OptionStringAlt => Options == null ? "" : string.Concat(Options?.OrderBy(x => x.PartNumberDescriptor).Select(x => "-" + x.PartNumberDescriptor));

        public string PartNumber => $"QSX-V{bandComboPart}{basebandPart}{serialPart}-{modulationPart}-{rfPart}-{pinoutPart}-{packagePart}{OptionStringAlt}";
    }
}
