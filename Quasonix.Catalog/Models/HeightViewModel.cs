﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class HeightViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Range(0.0001d, double.MaxValue, ErrorMessage = "{0} must be greater than {1}")]
        public double? Measurement { get; set; }
    }

    public class HeightDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double? Measurement { get; set; }
        [Display(Name = "Packages Where Used")]
        public List<PackageDetailViewModel> Packages { get; set; }
    }
}
