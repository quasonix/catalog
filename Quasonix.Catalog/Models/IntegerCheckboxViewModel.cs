﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class IntegerCheckboxViewModel
    {
        public int Id { set; get; }
        public string LabelText { get; set; }
        public bool IsSelected { set; get; }
    }

    public class GuidCheckboxViewModel
    {
        public Guid Id { set; get; }
        public string LabelText { get; set; }
        public bool IsSelected { set; get; }
        public string Decoration { get; set; }
    }

    public class EnumCheckboxViewModel<T> where T : struct, Enum
    {
        public T Value { set; get; }
        public string LabelText { get; set; }
        public bool IsSelected { set; get; }
    }
}
