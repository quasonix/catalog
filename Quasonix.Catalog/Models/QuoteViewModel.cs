﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class QuoteViewModel
    {
        public Guid Id { get; set; }
        public string Number { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public List<QuoteLineViewModel> Lines { get; set; }

        public List<QuoteLineViewModel> IncludedLines => Lines?.Where(x => x.IncludedInQuote).ToList();
        public List<QuoteLineViewModel> ExcludedLines => Lines?.Where(x => !x.IncludedInQuote).ToList();

        public bool HasIncludedLines => IncludedLines?.Count > 0;
        public bool HasExcludedLines => ExcludedLines?.Count > 0;

        public int? Total => IncludedLines?.Sum(x => x.ExtendedPrice);
        public string TotalString => String.Format("${0:n0}", Total);

        public int? GsaDiscount => Total.HasValue ? (int)(Total * .01) : 0;
        public string GsaString => String.Format("(${0:n0})", GsaDiscount);

        public int? PaymentTerms { get; set; }
        public string CoverText { get; set; }
        public string Description { get; set; }

        public double DiscountMultiplier { get; set; }

        [Required]
        public string CustomerName { get; set; }
        [Required]
        public string CustomerLocation { get; set; }
        [Required]
        [Display(Name = "Order Type")]
        public OrderType OrderType { get; set; }
        [Required]
        [Display(Name = "Shipping Zone")]
        public ShippingZone? ShippingZone { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Purchaser Email")]
        public string PurchaserEmail { get; set; }
        [Display(Name = "Additional Info")]
        public string AdditionalInformation { get; set; }

        public bool CustomerInfoComplete => !string.IsNullOrWhiteSpace(CustomerName) &&
            !string.IsNullOrWhiteSpace(CustomerLocation) &&
            !string.IsNullOrWhiteSpace(PurchaserEmail) &&
            ShippingZone != null;
    }

    public class CustomerInfo
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Location { get; set; }
        [Required]
        [Display(Name = "Order Type")]
        public OrderType OrderType { get; set; }
        [Required]
        [Display(Name = "Shipping Zone")]
        public string ShippingZone { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Purchaser Email")]
        public string PurchaserEmail { get; set; }
        [Display(Name = "Additional Info")]
        public string AdditionalInfo { get; set; }

        public bool Valid => !string.IsNullOrWhiteSpace(Name) &&
            !string.IsNullOrWhiteSpace(Location) &&
            !string.IsNullOrWhiteSpace(ShippingZone) &&
            !string.IsNullOrWhiteSpace(PurchaserEmail);


    }

    public class QuoteLineViewModel
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
        public bool FullyConfigured { get; set; }
        public bool IncludedInQuote { get; set; }
        public string ProductName { get; set; }
        public int UnitPrice { get; set; }
        public int ExtendedPrice { get; set; }
        public int Shipping { get; set; }
        public ProductType ProductType { get; set; }
        public string Number { get; set; }

        public bool HasPrice { get; set; }

        public List<PriceDetail> PriceDetails { get; set; }
    }
}
