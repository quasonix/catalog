﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class SingleSelectChecklistViewModel
    {
        public List<IntegerCheckboxViewModel> Options { get; set; } = new List<IntegerCheckboxViewModel>();
        public int? SelectedValue => Options.FirstOrDefault(x => x.IsSelected)?.Id;
        public bool HasSelection => Options.Any(x => x.IsSelected);
        public int? GhostSelection => Options.Count == 1 ? Options[0].Id : (int?)null;
        public bool HasGhost => GhostSelection != null && !HasSelection;
    }

    public class SingleSelectGuidChecklistViewModel
    {
        public List<GuidCheckboxViewModel> Options { get; set; } = new List<GuidCheckboxViewModel>();
        public Guid? SelectedValue => Options.FirstOrDefault(x => x.IsSelected)?.Id;
        public bool HasSelection => Options.Any(x => x.IsSelected);
        public Guid? GhostSelection => Options.Count == 1 ? Options[0].Id : (Guid?)null;
        public bool HasGhost => GhostSelection != null && !HasSelection;

        public void RemoveOption(Guid id)
        {
            var option = Options.First(x => x.Id == id);
            Options.Remove(option);
        }
    }

    public class MultiSelectChecklistViewModel
    {
        public List<GuidCheckboxViewModel> Options { get; set; } = new List<GuidCheckboxViewModel>();
        public List<Guid> SelectedValues => Options.Where(x => x.IsSelected).Select(x => x.Id).ToList();
        public bool HasSelection => Options.Any(x => x.IsSelected);

        public Dictionary<Guid, string> Ghosts = new Dictionary<Guid, string>();

        public void RemoveOption(Guid id)
        {
            var option = Options.First(x => x.Id == id);
            Options.Remove(option);
        }

        public void GhostSelect(Guid id, string message)
        {
            Ghosts.Add(id, message);
        }
    }
}
