﻿using AutoMapper;
using DynamicVML;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models.Mapping
{
    public class DetailProfile : Profile
    {
        public DetailProfile()
        {
            CreateMap<TxBand, BandDetailViewModel>().ReverseMap();
            CreateMap<AntBand, BandDetailViewModel>().ReverseMap();
            CreateMap<RxBand, BandDetailViewModel>().ReverseMap();
            CreateMap<TxBandCombination, BandCombinationDetailViewModel>().ReverseMap();
            CreateMap<AntBandCombination, BandCombinationDetailViewModel>().ReverseMap();
            CreateMap<RxBandCombination, BandCombinationDetailViewModel>().ReverseMap();
            CreateMap<TxBaseband, BasebandDetailViewModel>().ReverseMap();
            CreateMap<TxBoard, BoardDetailViewModel>().ReverseMap();
            CreateMap<TxDataConnector, DataConnectorDetailViewModel>().ReverseMap();
            CreateMap<TxDataConnectorPinUsage, DataConnectorIncludedPinDetailViewModel>().ReverseMap();
            CreateMap<TxFootprint, FootprintDetailViewModel>().ReverseMap();
            CreateMap<TxHeight, HeightDetailViewModel>().ReverseMap();
            CreateMap<TxPackage, PackageDetailViewModel>().ReverseMap();
            CreateMap<TxPin, PinDetailViewModel>().ReverseMap();
            CreateMap<TxPinout, PinoutDetailViewModel>().ReverseMap();
            CreateMap<TxRadioConnector, RfConnectorDetailViewModel>().ReverseMap();
            CreateMap<TxRadioFrequencyPower, RfPowerDetailViewModel>().ReverseMap();
            CreateMap<TxSerialControlInterface, SerialControlInterfaceDetailViewModel>().ReverseMap();
            CreateMap<TxStack, StackDetailViewModel>().ReverseMap();
            CreateMap<PriceVersion, PriceVersionDetailViewModel>().ReverseMap();
            CreateMap<RxFrontPanelDisplay, FrontPanelDisplayDetailViewModel>().ReverseMap();
            CreateMap<RxFrontPanelInterface, FrontPanelInterfaceDetailViewModel>().ReverseMap();
            CreateMap<RxFormFactor, FormFactorDetailViewModel>().ReverseMap();
            CreateMap<RxChannelSet, ChannelSetDetailViewModel>().ReverseMap();
            CreateMap<RxPackage, RxPackageDetailViewModel>().ReverseMap();
            CreateMap<RxBasebandSet, RxBasebandSetDetailViewModel>().ReverseMap();
            CreateMap<RxPinout, RxPinoutDetailViewModel>().ReverseMap();
            CreateMap<AntOption, OptionDetailViewModel>().ReverseMap();
            CreateMap<AntWiring, AntWiringDetailViewModel>().ReverseMap();
            CreateMap<AntFeed, AntFeedDetailViewModel>().ReverseMap();
            CreateMap<AntReflectorAssemblyViewModel, AntReflectorAssemblyDetailViewModel>().ReverseMap();
            CreateMap<AntPedestal, AntPedestalDetailViewModel>().ReverseMap();
            CreateMap<AntFocusArrangement, AntFocusArrangementDetailViewModel>().ReverseMap();
            CreateMap<Evtm, EvtmDetailViewModel>().ReverseMap();
            CreateMap<Accessory, AccessoryDetailViewModel>().ReverseMap();
        }
    }
}
