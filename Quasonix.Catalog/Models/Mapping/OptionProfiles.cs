﻿using AutoMapper;
using DynamicVML;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models.Mapping
{
    public class OptionProfile : Profile
    {
        public OptionProfile()
        {
            CreateMap<RxOption, OptionViewModel>()
                .ForMember(x => x.Prices, x => x.Ignore());
            CreateMap<OptionViewModel, RxOption>()
                .ForMember(x => x.Prices, x => x.Ignore());
            CreateMap<TxPin, PinViewModel>().ReverseMap();
            CreateMap<TxDataConnectorPinUsage, DataConnectorIncludedPinViewModel>().ReverseMap();
            CreateMap<TxBaseband, BasebandViewModel>().ReverseMap();
            CreateMap<TxSerialControlInterface, SerialControlInterfaceViewModel>().ReverseMap();
            CreateMap<TxRadioFrequencyPower, RfPowerViewModel>().ReverseMap();
            CreateMap<TxBandCombination, BandCombinationViewModel>().ReverseMap();
            CreateMap<AntBandCombination, BandCombinationViewModel>().ReverseMap();
            CreateMap<RxBandCombination, BandCombinationViewModel>().ReverseMap();
            CreateMap<TxHeight, HeightViewModel>().ReverseMap();
            CreateMap<TxFootprint, FootprintViewModel>().ReverseMap();
            CreateMap<TxDataConnector, DataConnectorViewModel>().ReverseMap();
            CreateMap<TxRadioConnector, RfConnectorViewModel>().ReverseMap();
            CreateMap<TxBoard, BoardViewModel>().ReverseMap();
            CreateMap<TxBand, BandViewModel>().ReverseMap();
            CreateMap<AntBand, BandViewModel>().ReverseMap();
            CreateMap<TxStack, TxStackViewModel>()
                .ForMember(x => x.Boards, x => x.Ignore())
                .ForMember(x => x.Basebands, x => x.Ignore())
                .ForMember(x => x.SerialControlInterfaces, x => x.Ignore()).ReverseMap();
            CreateMap<TxStack, PackageStackViewModel>().ReverseMap();
            CreateMap<TxPackage, PackageViewModel>()
                .ForMember(x => x.Prices, x => x.Ignore()).ReverseMap();
            CreateMap<TxPinout, PinoutViewModel>()
                .ForMember(x => x.DataConnectors, x => x.Ignore()).ReverseMap();
            CreateMap<Quote, QuoteViewModel>();
            CreateMap<QuoteLine, QuoteLineViewModel>();
            CreateMap<PriceVersion, PriceVersionViewModel>().ReverseMap();
            CreateMap<Price, PriceViewModel>().ReverseMap();
            CreateMap<RxFrontPanelDisplay, FrontPanelDisplayViewModel>().ReverseMap();
            CreateMap<RxFrontPanelInterface, FrontPanelInterfaceViewModel>().ReverseMap();
            CreateMap<RxFormFactor, FormFactorViewModel>().ReverseMap();
            CreateMap<RxConnectorSet, ConnectorSetViewModel>()
                .ForMember(x => x.Connectors, x => x.Ignore());
            CreateMap<ConnectorSetViewModel, RxConnectorSet>()
                .ForMember(x => x.Connectors, x => x.Ignore());
            CreateMap<ConnectorForConnectorSet, ConnectorForConnectorSetViewModel>().ReverseMap();
            CreateMap<RxChannelSet, ChannelSetViewModel>().ReverseMap();
            CreateMap<RxBand, ReceiverBandViewModel>().ReverseMap();
            CreateMap<RxPackage, RxPackageViewModel>()
                .ForMember(x => x.FormFactorOptions, x => x.Ignore());
            CreateMap<RxBasebandSet, RxBasebandSetViewModel>();
            CreateMap<RxPinout, RxPinoutViewModel>().ReverseMap();
            CreateMap<AntOption, OptionViewModel>()
                .ForMember(x => x.Prices, x => x.Ignore());
            CreateMap<OptionViewModel, AntOption>();
            CreateMap<TxOption, OptionViewModel>()
                .ForMember(x => x.Prices, x => x.Ignore());
            CreateMap<OptionViewModel, TxOption>()
                .ForMember(x => x.Prices, x => x.Ignore());
            CreateMap<AntWiring, AntWiringViewModel>().ReverseMap();
            CreateMap<AntFeed, AntFeedViewModel>().ReverseMap();
            CreateMap<AntReflectorAssembly, AntReflectorAssemblyViewModel>();
            CreateMap<AntReflectorAssemblyViewModel, AntReflectorAssembly>()
                .ForMember(x => x.FocusArrangement, x => x.Ignore());
            CreateMap<AntPedestal, AntPedestalViewModel>().ReverseMap();
            CreateMap<AntFocusArrangement, AntFocusArrangementViewModel>().ReverseMap();
            CreateMap<TxBoard, StackBoardViewModel>().ReverseMap();
            CreateMap<TxBaseband, StackBasebandViewModel>().ReverseMap();
            CreateMap<TxSerialControlInterface, StackSerialControlInterfaceViewModel>().ReverseMap();
            CreateMap<Evtm, EvtmViewModel>().ReverseMap();
            CreateMap<Accessory, AccessoryViewModel>().ReverseMap();
        }
    }
}
