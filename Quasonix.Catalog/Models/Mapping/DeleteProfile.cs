﻿using AutoMapper;
using DynamicVML;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models.Mapping
{
    public class DeleteProfile : Profile
    {
        public DeleteProfile()
        {
            CreateMap<Quote, DeleteViewModel>()
                .ForMember(x => x.CanDelete, opt => opt.MapFrom(x => !x.Submitted));
            CreateMap<TxBand, DeleteViewModel>()
                .ForMember(x => x.CanDelete, opt => opt.MapFrom(x => !x.BandCombinations.Any()));
            CreateMap<AntBand, DeleteViewModel>();
            CreateMap<TxBandCombination, DeleteViewModel>();
            CreateMap<AntBandCombination, DeleteViewModel>();
            CreateMap<RxBandCombination, DeleteViewModel>();
            CreateMap<TxBaseband, DeleteViewModel>();
            CreateMap<TxBoard, DeleteViewModel>()
                .ForMember(x => x.CanDelete, opt => opt.MapFrom(x => !x.Stacks.Any()));
            CreateMap<TxDataConnector, DeleteViewModel>()
                .ForMember(x => x.CanDelete, opt => opt.MapFrom(x => x.ConnectorSetDataConnectors.Any()));
            CreateMap<TxFootprint, DeleteViewModel>()
                .ForMember(x => x.CanDelete, opt => opt.MapFrom(x => !x.Packages.Any()));
            CreateMap<TxHeight, DeleteViewModel>()
                .ForMember(x => x.CanDelete, opt => opt.MapFrom(x => !x.Packages.Any()));
            CreateMap<TxPackage, DeleteViewModel>();
            CreateMap<TxPin, DeleteViewModel>()
                .ForMember(x => x.CanDelete, opt => opt.MapFrom(x => !x.PinUsages.Any()));
            CreateMap<TxPinout, DeleteViewModel>();
            CreateMap<TxRadioConnector, DeleteViewModel>()
                .ForMember(x => x.CanDelete, opt => opt.MapFrom(x => !x.Packages.Any()));
            CreateMap<TxRadioFrequencyPower, DeleteViewModel>();
            CreateMap<TxSerialControlInterface, DeleteViewModel>();
            CreateMap<TxStack, DeleteViewModel>()
                .ForMember(x => x.CanDelete, opt => opt.MapFrom(x => true));//!x.Packages.Any()));
            CreateMap<PriceVersion, DeleteViewModel>();
            CreateMap<RxFrontPanelDisplay, DeleteViewModel>();
            CreateMap<RxFrontPanelInterface, DeleteViewModel>();
            CreateMap<RxFormFactor, DeleteViewModel>().ReverseMap();
            CreateMap<RxConnectorSet, DeleteViewModel>().ReverseMap();
            CreateMap<RxChannelSet, DeleteViewModel>().ReverseMap();
            CreateMap<RxBand, DeleteViewModel>().ReverseMap();
            CreateMap<RxBasebandSet, DeleteViewModel>().ReverseMap();
            CreateMap<RxPackage, DeleteViewModel>().ReverseMap();
            CreateMap<RxPinout, DeleteViewModel>().ReverseMap();
            CreateMap<AntOption, DeleteViewModel>().ReverseMap();
            CreateMap<AntWiring, DeleteViewModel>().ReverseMap();
            CreateMap<AntFeed, DeleteViewModel>().ReverseMap();
            CreateMap<AntReflectorAssembly, DeleteViewModel>().ReverseMap();
            CreateMap<AntPedestal, DeleteViewModel>().ReverseMap();
            CreateMap<AntFocusArrangement, DeleteViewModel>().ReverseMap();
            CreateMap<Evtm, DeleteViewModel>().ReverseMap();
            CreateMap<Accessory, DeleteViewModel>().ReverseMap();
        }
    }
}
