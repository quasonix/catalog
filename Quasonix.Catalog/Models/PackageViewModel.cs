﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class PackageViewModel
    {
        public Guid Id { get; set; }
        public bool Active { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Footprint")]
        public Guid FootprintId { get; set; }
        [Required]
        [Display(Name = "Height")]
        public Guid HeightId { get; set; }
        [Required]
        [Display(Name = "Coaxial Modulation Input")]
        public Guid CoaxialModulationInputId { get; set; }
        [Required]
        [Display(Name = "Connector Set")]
        public Guid ConnectorSetId { get; set; }
        [Required]
        [Display(Name = "Primary Connector Position")]
        public ConnectorPosition PrimaryConnectorPosition { get; set; }
        [Required]
        [Display(Name = "RF Connector")]
        public Guid RadioConnectorId { get; set; }
        [Required]
        [Display(Name = "RF Connector Position")]
        public ConnectorPosition RadioConnectorPosition { get; set; }

        public DynamicList<PriceViewModel> Prices { get; set; } = new DynamicList<PriceViewModel>();

        public IList<SelectListItem> FootprintOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> HeightOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> CoaxOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> RadioOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> ConnectorSetOptions { get; set; } = new List<SelectListItem>();
    }

    public class PackageDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public FootprintDetailViewModel Footprint { get; set; }
        public HeightDetailViewModel Height { get; set; }
        [Display(Name = "Board Stacks")]
        public List<StackDetailViewModel> Stacks { get; set; }
        [Display(Name = "Data Connectors")]
        public List<DataConnectorForPackageDetailViewModel> DataConnectors { get; set; }
        [Display(Name = "RF Connnectors")]
        public List<RfConnectorForPackageDetailViewModel> RfConnectors { get; set; }
    }

    public class DataConnectorForPackageViewModel
    {
        public Guid Id { get; set; }
        public ConnectorRank Rank { get; set; }
        public ConnectorPosition Position { get; set; }
        [Display(Name = "Connector")]
        public Guid DataConnectorId { get; set; }
        public IList<SelectListItem> DataConnectorOptions { get; set; } = new List<SelectListItem>();
    }

    public class DataConnectorForPackageDetailViewModel
    {
        public Guid Id { get; set; }
        public ConnectorRank Rank { get; set; }
        public ConnectorPosition Position { get; set; }
        public DataConnectorDetailViewModel DataConnector { get; set; }
        public PackageDetailViewModel Package { get; set; }
    }

    public class RfConnectorForPackageViewModel
    {
        public Guid Id { get; set; }
        public Guid RfConnectorId { get; set; }
        public ConnectorRank Rank { get; set; }
        public ConnectorPosition Position { get; set; }
        [Display(Name = "Connector")]
        public IList<SelectListItem> RfConnectorOptions { get; set; } = new List<SelectListItem>();
    }

    public class RfConnectorForPackageDetailViewModel
    {
        public Guid Id { get; set; }
        public ConnectorRank Rank { get; set; }
        public ConnectorPosition Position { get; set; }
        public RfConnectorDetailViewModel RfConnector { get; set; }
        public PackageDetailViewModel Package { get; set; }
    }

    public class PackageStackViewModel
    {
        public Guid Id { get; set; }
        public IList<SelectListItem> StackOptions { get; set; } = new List<SelectListItem>();
    }
}
