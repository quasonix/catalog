﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class AntFocusArrangementViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Display(Name = "Available Feeds")]
        public MultiSelectChecklistViewModel FeedChecklist { get; set; } = new MultiSelectChecklistViewModel();
    }

    public class AntFocusArrangementDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<BandCombinationDetailViewModel> BandCombinations { get; set; }
    }
}
