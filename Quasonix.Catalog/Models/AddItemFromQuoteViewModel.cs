﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class AddItemFromQuoteViewModel
    {
        [Display(Name = "Select a Quote")]
        public Guid SelectedQuote { get; set; }
        public IList<SelectListItem> Quotes { get; set; } = new List<SelectListItem>();
        public IList<QuoteProductStubViewModel> Products { get; set; } = new List<QuoteProductStubViewModel>();
    }

    public class QuoteProductStubViewModel
    {
        public string PartNumber { get; set; }
        public Guid Id { get; set; }
    }
}
