﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class PinViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
    }

    public class PinDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<DataConnectorIncludedPinDetailViewModel> DataConnectorIncludedPins { get; set; }

        [Display(Name = "Pinouts Where Used")]
        public List<PinoutDetailViewModel> Pinouts => DataConnectorIncludedPins.Select(x => x.DataConnectorForPinout.Pinout).ToList();
    }
}
