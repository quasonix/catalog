﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class RxPackageViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Part Number Descriptor")]
        public string PartNumberDescriptor { get; set; }
        [Required]
        [Display(Name = "Form Factor")]
        public Guid FormFactorId { get; set; }
        [Required]
        [Display(Name = "Channel Set")]
        public Guid ChannelSetId { get; set; }
        [Required]
        [Display(Name = "Front Panel Display")]
        public Guid FrontPanelDisplayId { get; set; }
        [Required]
        [Display(Name = "Front Panel Interface")]
        public Guid FrontPanelInterfaceId { get; set; }
        [Required]
        [Display(Name = "Baseband Connector")]
        public Guid BasebandSetId { get; set; }
        [Required]
        [Display(Name = "RF Input Connector")]
        public Guid RfInputConnectorSetId { get; set; }
        [Required]
        [Display(Name = "IF Input Connector")]
        public Guid IfInputConnectorSetId { get; set; }
        [Required]
        [Display(Name = "IF Output Connector")]
        public Guid IfOutputConnectorSetId { get; set; }
        [Required]
        [Display(Name = "Ethernet Connector")]
        public Guid EthernetConnectorSetId { get; set; }
        [Required]
        [Display(Name = "Analog Output Connector")]
        public Guid AnalogOutputConnectorSetId { get; set; }
        [Required]
        [Display(Name = "AM Output Connector")]
        public Guid AmOutputConnectorSetId { get; set; }
        [Required]
        [Display(Name = "AGC Output Connector")]
        public Guid AgcOutputConnectorSetId { get; set; }

        public IList<SelectListItem> ChannelSetOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> FormFactorOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> FrontPanelDisplayOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> FrontPanelInterfaceOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> ConnectorSetOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> BasebandSetOptions { get; set; } = new List<SelectListItem>();
    }

    public class RxPackageDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
