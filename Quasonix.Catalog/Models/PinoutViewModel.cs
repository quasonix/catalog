﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class PinoutViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public DynamicList<DataConnectorForPinoutViewModel> DataConnectors { get; set; } = new DynamicList<DataConnectorForPinoutViewModel>();
    }

    public class PinoutDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<DataConnectorForPinoutDetailViewModel> DataConnectors { get; set; }
    }

    public class DataConnectorForPinoutViewModel
    {
        public Guid Id { get; set; }
        public ConnectorRank Rank { get; set; }
        [Display(Name = "Connector")]
        public Guid DataConnectorId { get; set; }
        public int PinCount { get; set; }
        public IList<SelectListItem> DataConnectorOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> PinOptions { get; set; } = new List<SelectListItem>();
        public IList<DataConnectorIncludedPinViewModel> Pins { get; set; } = new List<DataConnectorIncludedPinViewModel>();
    }

    public class DataConnectorForPinoutDetailViewModel
    {
        public Guid Id { get; set; }
        public ConnectorRank Rank { get; set; }
        public DataConnectorDetailViewModel DataConnector { get; set; }
        public int PinCount { get; set; }
        public List<DataConnectorIncludedPinDetailViewModel> Pins { get; set; }
        public PinoutDetailViewModel Pinout { get; set; }
    }

    public class DataConnectorIncludedPinViewModel
    {
        public Guid PinId { get; set; }
        public int PinNumber { get; set; }
    }

    public class DataConnectorIncludedPinDetailViewModel
    {
        public PinDetailViewModel Pin { get; set; }
        public DataConnectorForPinoutDetailViewModel DataConnectorForPinout { get; set; }
        public int PinNumber { get; set; }
    }
}
