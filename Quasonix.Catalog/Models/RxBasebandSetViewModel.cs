﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class RxBasebandSetViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Display(Name = "TTL Connector")]
        public Guid TtlConnectorSetId { get; set; }
        [Required]
        [Display(Name = "RS-422 Connector")]
        public Guid RsConnectorSetId { get; set; }

        public IList<SelectListItem> ConnectorSetOptions { get; set; } = new List<SelectListItem>();
    }

    public class RxBasebandSetDetailViewModel
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
    }
}
