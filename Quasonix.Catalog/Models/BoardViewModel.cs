﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class BoardViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Engineering Name")]
        public string EngineeringName { get; set; }
        [Required]
        [Display(Name = "Manufacturing Name")]
        public string ManufacturingName { get; set; }
        public string FullName { get; set; }
        [Required]
        [Display(Name = "Board Type")]
        public BoardType Type { get; set; }
    }

    public class BoardDetailViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Engineering Name")]
        public string EngineeringName { get; set; }
        [Display(Name = "Manufacturing Name")]
        public string ManufacturingName { get; set; }
        public string FullName { get; set; }
        [Display(Name = "Board Type")]
        public BoardType Type { get; set; }
        [Display(Name = "Stacks Where Used")]
        public List<StackDetailViewModel> Stacks { get; set; }
    }
}
