﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Quasonix.Catalog.Models
{
    public class PriceViewModel
    {
        public Guid Id { get; set; }
        public Guid PriceVersionId { get; set; }
        [Required]
        public int Value { get; set; }
        public IList<SelectListItem> PriceVersionOptions { get; set; } = new List<SelectListItem>();
    }
}
