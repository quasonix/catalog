﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class FormFactorViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Range(1, double.MaxValue, ErrorMessage = "Width must be greater than 0")]
        public double Width { get; set; }
        [Required]
        [Range(1, double.MaxValue, ErrorMessage = "Height must be greater than 0")]
        public double Height { get; set; }
        [Required]
        [Range(1, double.MaxValue, ErrorMessage = "Depth must be greater than 0")]
        public double Depth { get; set; }
        [Required]
        public ReceiverStyle Style { get; set; }
        [Required]
        [Display(Name = "Dimension Description")]
        public string DimensionDescription { get; set; }

        [Display(Name = "Available Options")]
        public MultiSelectChecklistViewModel OptionChecklist { get; set; } = new MultiSelectChecklistViewModel();
        [Display(Name = "Available Pinouts")]
        public MultiSelectChecklistViewModel PinoutChecklist { get; set; } = new MultiSelectChecklistViewModel();
    }

    public class FormFactorDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
