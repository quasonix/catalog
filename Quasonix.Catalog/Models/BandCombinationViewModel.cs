﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class BandCombinationViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Part Number Descriptor")]
        public string PartNumberDescriptor { get; set; }
        public MultiSelectChecklistViewModel BandChecklist { get; set; } = new MultiSelectChecklistViewModel();
    }

    public class BandCombinationDetailViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Line Item Text")]
        public string LineItemText { get; set; }
        public string Name { get; set; }
        public List<ConfigurationOptionDetailViewModel> Options { get; set; }
        [Display(Name = "Part Number Descriptor")]
        public string PartNumberDescriptor { get; set; }
        [Display(Name = "Included Bands")]
        public List<BandDetailViewModel> Bands { get; set; }
    }
}