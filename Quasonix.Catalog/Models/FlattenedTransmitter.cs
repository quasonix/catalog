﻿using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class FlattenedTransmitter
    {
        public TxFootprint Footprint { get; set; }
        public TxHeight Height { get; set; }
        public TxRadioFrequencyPower RFPower { get; set; }
        public TxSerialControlInterface SerialControlInterface { get; set; }
        public TxBaseband Baseband { get; set; }
        public TxBandCombination BandCombination { get; set; }
        public ConnectorPosition DataConnectorPosition { get; set; }
        public ConnectorPosition RadioConnectorPosition { get; set; }
        public TxPackage Package { get; set; }
        public TxStack Stack { get; set; }
        public List<TxOption> Options { get; set; }
        public TxOptionCombination OptionCombination { get; set; }
        public List<TxModulation> Modulations { get; set; }
        public TxPinout Pinout { get; set; }

        public List<Component> Components
        {
            get
            {
                var compList = new List<Component>()
                    {
                        Footprint, Height, RFPower, SerialControlInterface, Baseband, BandCombination, Package, Stack,
                        Package.CoaxialModulationInput, Package.ConnectorSet
                    };

                compList.AddRange(BandCombination.Bands);
                compList.AddRange(Stack.Boards);
                compList.AddRange(Package.ConnectorSet.ConnectorSetDataConnectors.Select(x => x.DataConnector));

                return compList;
            }
        }
    }
}
