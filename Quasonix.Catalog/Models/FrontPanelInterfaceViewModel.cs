﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class FrontPanelInterfaceViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
    }

    public class FrontPanelInterfaceDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
