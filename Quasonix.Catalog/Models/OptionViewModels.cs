﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class ConfigurationOptionViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Part Number Descriptor")]
        public string PartNumberDescriptor { get; set; }
        [Required]
        [Display(Name = "Line Item Text")]
        public string LineItemText { get; set; }
        public IList<SelectListItem> PinOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> BandCombinationOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> SerialControlOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> RfPowerOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> BasebandOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> OtherOptions { get; set; } = new List<SelectListItem>();
        public IList<int> SelectedOptions { get; set; } = new List<int>();
        public IList<int> SelectedPins { get; set; } = new List<int>();

        public bool ShowBandCombinations { get; set; } = true;
        public bool ShowSerials { get; set; } = true;
        public bool ShowRfPowers { get; set; } = true;
        public bool ShowBasebands { get; set; } = true;
    }

    public class ConfigurationOptionDetailViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Line Item Text")]
        public string LineItemText { get; set; }
        public string Name { get; set; }
        public List<ConfigurationOptionDetailViewModel> Options { get; set; }
        [Display(Name = "Part Number Descriptor")]
        public string PartNumberDescriptor { get; set; }
        [Display(Name = "Required Pins")]
        public List<PinDetailViewModel> Pins { get; set; }
        public OptionType Type { get; set; }

        [Display(Name = "Associated Band Combinations")]
        public List<ConfigurationOptionDetailViewModel> BandCombinations => Options.Where(x => x.Type == OptionType.BandCombination).ToList();
        [Display(Name = "Associated Basebands")]
        public List<ConfigurationOptionDetailViewModel> Basebands => Options.Where(x => x.Type == OptionType.Baseband).ToList();
        [Display(Name = "Associated Options")]
        public List<ConfigurationOptionDetailViewModel> Others => Options.Where(x => x.Type == OptionType.Other).ToList();
        [Display(Name = "Associated RF Powers")]
        public List<ConfigurationOptionDetailViewModel> RfPowers => Options.Where(x => x.Type == OptionType.Power).ToList();
        [Display(Name = "Associated Serial Ints.")]
        public List<ConfigurationOptionDetailViewModel> Serials => Options.Where(x => x.Type == OptionType.Serial).ToList();
    }
}
