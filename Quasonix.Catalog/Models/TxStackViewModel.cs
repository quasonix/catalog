﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class TxStackViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public IList<SelectListItem> BandCombinationOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> SerialControlOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> RfPowerOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> BasebandOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> OtherOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> BoardOptions { get; set; } = new List<SelectListItem>();
        public IList<SelectListItem> PackageOptions { get; set; } = new List<SelectListItem>();
        [Required]
        [Display(Name = "Package")]
        public Guid PackageId { get; set; }
        [Required]
        [Display(Name = "RF Power")]
        public Guid RFPowerId { get; set; }
        [Required]
        [Display(Name = "Serial Control Interface")]
        public Guid SerialControlInterfaceId { get; set; }
        [Required]
        [Display(Name = "Baseband")]
        public Guid BasebandId { get; set; }
        [Required]
        [Display(Name = "Band Combination")]
        public Guid BandCombinationId { get; set; }

        [Display(Name = "Boards")]
        public DynamicList<StackBoardViewModel> Boards { get; set; } = new DynamicList<StackBoardViewModel>();
        [Display(Name = "Available Basebands")]
        public DynamicList<StackBasebandViewModel> Basebands { get; set; } = new DynamicList<StackBasebandViewModel>();
        [Display(Name = "Available Serial Control Interfaces")]
        public DynamicList<StackSerialControlInterfaceViewModel> SerialControlInterfaces { get; set; } = new DynamicList<StackSerialControlInterfaceViewModel>();
    }

    public class StackDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<ConfigurationOptionDetailViewModel> ConfigurationOptions { get; set; }
        [Display(Name = "Included Boards")]
        public List<BoardDetailViewModel> Boards { get; set; }
        [Display(Name = "Packages Where Used")]
        public List<PackageDetailViewModel> Packages { get; set; }
    }

    public class StackBasebandViewModel
    {
        public Guid Id { get; set; }
        public IList<SelectListItem> BasebandOptions { get; set; } = new List<SelectListItem>();
    }

    public class StackSerialControlInterfaceViewModel
    {
        public Guid Id { get; set; }
        public IList<SelectListItem> SerialControlInterfaceOptions { get; set; } = new List<SelectListItem>();
    }

    public class StackBoardViewModel
    {
        public Guid Id { get; set; }
        public IList<SelectListItem> BoardOptions { get; set; } = new List<SelectListItem>();
    }
}
