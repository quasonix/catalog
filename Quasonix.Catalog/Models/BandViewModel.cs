﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class BandViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Minimum Frequency (MHz)")]
        public double MinimumFrequency { get; set; }
        [Required]
        [Display(Name = "Maximum Frequency (MHz)")]
        public double MaximumFrequency { get; set; }
    }

    public class BandDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<BandCombinationDetailViewModel> BandCombinations { get; set; }
    }

    public class ReceiverBandViewModel : BandViewModel
    {
        [Required]
        [Display(Name = "Extended Minimum Frequency (MHz)")]
        public double ExtendedMinimumFrequency { get; set; }
        [Required]
        [Display(Name = "Extended Maximum Frequency (MHz)")]
        public double ExtendedMaximumFrequency { get; set; }
        [Display(Name = "Requires IF Input")]
        public bool RequiresIFInput { get; set; }
    }
}
