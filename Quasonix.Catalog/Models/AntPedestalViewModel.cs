﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class AntPedestalViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Part Number Descriptor")]
        public string PartNumberDescriptor { get; set; }

        [Display(Name = "Available Reflector Assemblies")]
        public MultiSelectChecklistViewModel ReflectorAssemblyChecklist { get; set; } = new MultiSelectChecklistViewModel();
        [Display(Name = "Available Wiring")]
        public MultiSelectChecklistViewModel WiringChecklist { get; set; } = new MultiSelectChecklistViewModel();
        [Display(Name = "Available Options")]
        public MultiSelectChecklistViewModel OptionChecklist { get; set; } = new MultiSelectChecklistViewModel();
    }

    public class AntPedestalDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        [Display(Name = "Part Number Descriptor")]
        public string PartNumberDescriptor { get; set; }
    }
}
