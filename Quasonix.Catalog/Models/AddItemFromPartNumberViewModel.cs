﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class AddItemFromPartNumberViewModel
    {
        [Display(Name = "Part Number")]
        public string PartNumber { get; set; }

        public List<LineItem> PartNumberDescriptors { get; set; } = new List<LineItem>();
        public List<string> Errors { get; set; } = new List<string>();

        public bool IsValidPartNumber { get; set; }

        public ProductType ProductType { get; set; }

        public bool IsRecognizedType { get; set; }

        public Product Product { get; set; }
    }
}
