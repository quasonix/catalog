﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class DataConnectorViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "{0} must be greater than 0")]
        [Display(Name = "Pin Count")]
        public int PinCount { get; set; }
    }

    public class DataConnectorDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int PinCount { get; set; }
        public List<DataConnectorForPackageDetailViewModel> DataConnectorsForPackages { get; set; }

        [Display(Name = "Packages Where Used")]
        public List<PackageDetailViewModel> Packages => DataConnectorsForPackages.Select(x => x.Package).Distinct().ToList();
    }
}
