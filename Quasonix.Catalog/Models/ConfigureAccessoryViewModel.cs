﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class ConfigureAccessoryViewModel
    {
        public string AddToQuote { get; set; }
        public Guid AccessoryId { get; set; }

        public bool ShowCategorySection { get; set; } = true;
        public bool ShowFunctionSection { get; set; } = true;
        public bool ShowSignalStandardSection { get; set; } = true;

        public SingleSelectGuidChecklistViewModel CategoryChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel FunctionChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectChecklistViewModel SignalStandardChecklist { get; set; } = new SingleSelectChecklistViewModel();

        public IEnumerable<Accessory> Products { get; set; }
    }
}
