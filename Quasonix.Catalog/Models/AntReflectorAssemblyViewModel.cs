﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class AntReflectorAssemblyViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Part Number Descriptor")]
        public string PartNumberDescriptor { get; set; }
        [Required]
        public int Diameter { get; set; }
        [Required]
        [Display(Name = "Construction")]
        public DishConstructionType ConstructionType { get; set; }
        [Required]
        [Display(Name = "Focus Arrangement")]
        public Guid FocusArrangementId { get; set; }

        public IList<SelectListItem> FocusArrangementOptions { get; set; } = new List<SelectListItem>();
    }

    public class AntReflectorAssemblyDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
