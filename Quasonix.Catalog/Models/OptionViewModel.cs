﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class OptionViewModel : ComponentViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Part Number Descriptor")]
        public string PartNumberDescriptor { get; set; }
        [Required]
        [Display(Name = "Line Item Text")]
        public string LineItemText { get; set; }

        [Display(Name = "Option Bindings")]
        public DynamicList<OptionBindingViewModel> Bindings { get; set; } = new DynamicList<OptionBindingViewModel>();
    }

    public class OptionDetailViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Line Item Text")]
        public string LineItemText { get; set; }
        public string Name { get; set; }
        public List<ConfigurationOptionDetailViewModel> Options { get; set; }
        [Display(Name = "Part Number Descriptor")]
        public string PartNumberDescriptor { get; set; }
        //[Display(Name = "Required Pins")]
        //public List<PinDetailViewModel> Pins { get; set; }
    }

    public class ComponentViewModel
    {
        public DynamicList<PriceViewModel> Prices { get; set; } = new DynamicList<PriceViewModel>();
    }

    public class OptionBindingViewModel
    {
        public Guid Id { get; set; }
        public Guid BoundOptionId { get; set; }
        public OptionBindingType BindingType { get; set; }
        public IList<SelectListItem> Options { get; set; } = new List<SelectListItem>();
    }
}
