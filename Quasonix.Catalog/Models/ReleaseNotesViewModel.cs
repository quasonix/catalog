﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class ReleaseNotesViewModel
    {
        public DateTime Date { get; set; }
        public string Html { get; set; }
        public string Title { get; set; }
    }
}
