﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class PriceVersionViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public int Number { get; set; }
        [Required]
        [IsFutureDate(ErrorMessage = "Effective date cannot be in the past.")]
        public DateTime EffectiveDate { get; set; }
        public string Note { get; set; }
    }

    public class PriceVersionDetailViewModel
    {
        public Guid Id { get; set; }
        public int Number { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string Note { get; set; }
    }
}
