﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class FootprintViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [RequiredIf("Shape", FootprintShape.Rectangular)]
        [Range(0.0001d, double.MaxValue, ErrorMessage = "{0} must be greater than {1}")]
        public double? Length { get; set; }
        [RequiredIf("Shape", FootprintShape.Rectangular)]
        [Range(0.0001d, double.MaxValue, ErrorMessage = "{0} must be greater than {1}")]
        public double? Width { get; set; }
        [RequiredIf("Shape", FootprintShape.Round)]
        [Range(0.0001d, double.MaxValue, ErrorMessage = "{0} must be greater than {1}")]
        public double? Diameter { get; set; }
        public FootprintShape Shape { get; set; }
    }
    
    public class FootprintDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double? Length { get; set; }
        public double? Width { get; set; }
        public double? Diameter { get; set; }
        public FootprintShape Shape { get; set; }
        [Display(Name = "Packages Where Used")]
        public List<PackageDetailViewModel> Packages { get; set; }
    }
}
