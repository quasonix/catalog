﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class EvtmViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string PartNumber { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Form Factor")]
        public EvtmFormFactor FormFactor { get; set; }
        [Required]
        [Display(Name = "Channel Count")]
        public int ChannelCount { get; set; }
        [Required]
        [Display(Name = "Clock & Data")]
        public EvtmClockData ClockData { get; set; }
        [Required]
        [Display(Name = "Price")]
        public int Price { get; set; }
        [Display(Name = "Has Option ET")]
        public bool HasET { get; set; }
    }

    public class EvtmDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
