﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class ConfigureViewModel
    {
        public string AddToQuote { get; set; }
        public string SaveForLater { get; set; }

        public Guid LineId { get; set; }

        public SingleSelectGuidChecklistViewModel SignalInterfaceChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel SerialControlChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public MultiSelectChecklistViewModel ModulationChecklist { get; set; } = new MultiSelectChecklistViewModel();
        public SingleSelectGuidChecklistViewModel PowerChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel FootprintChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel HeightChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectChecklistViewModel DataConnectorPositionChecklist { get; set; } = new SingleSelectChecklistViewModel();
        public SingleSelectChecklistViewModel RfConnectorPositionChecklist { get; set; } = new SingleSelectChecklistViewModel();

        public MultiSelectChecklistViewModel OptionChecklist { get; set; } = new MultiSelectChecklistViewModel();

        public MultiSelectChecklistViewModel BandChecklist { get; set; } = new MultiSelectChecklistViewModel();
        public SingleSelectGuidChecklistViewModel BandComboChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public List<List<bool>> BandCheckmarks { get; set; } = new List<List<bool>>();

        public bool ShowFootprintSection { get; set; }
        public bool ShowRfPowerSection { get; set; }
        public bool ShowHeightSection { get; set; }
        public bool ShowBandCombinationSection { get; set; }
        public bool ShowClockInterfaceSection { get; set; }
        public bool ShowClockConnectorSection { get; set; }
        public bool ShowRfConnectorSection { get; set; }
        public bool ShowOptionSection { get; set; }
        public bool ShowSerialControlSection { get; set; }
        public bool ShowModulationSection { get; set; }

        public string StackString { get; set; }

        public bool IsComplete => BandCombination != null
            && Baseband != null
            && SerialControlInterface != null
            && RFPower != null
            && Package != null
            && Pinout != null
            && (HasArtm0 || HasArtm1 || HasArtm2 || HasBPSK);

        [JsonIgnore]
        public TxBandCombination BandCombination { get; set; }
        [JsonIgnore]
        public TxBaseband Baseband { get; set; }
        [JsonIgnore]
        public TxSerialControlInterface SerialControlInterface { get; set; }
        [JsonIgnore]
        public TxRadioFrequencyPower RFPower { get; set; }
        [JsonIgnore]
        public TxPackage Package { get; set; }
        [JsonIgnore]
        public TxPinout Pinout { get; set; }
        [JsonIgnore]
        public List<TxOption> SelectedOptions { get; set; } = new List<TxOption>();
        [JsonIgnore]
        public List<TxModulation> Modulations { get; set; } = new List<TxModulation>();

        public bool HasArtm0 { get; set; }
        public bool HasArtm1 { get; set; }
        public bool HasArtm2 { get; set; }
        public bool HasBPSK { get; set; }

        public List<LineItem> LineItems
        {
            get
            {
                var lineItems = new List<LineItem>();
                lineItems.Add(new LineItem($"(V)", "Variable Rate, auto-scaling of deviation and filter BW"));

                if (BandCombination != null)
                    lineItems.Add(new LineItem($"({BandCombination.PartNumberDescriptor})", BandCombination.LineItemText));
                else
                    lineItems.Add(new LineItem($"(?)", "Band combination not selected"));

                if (Baseband != null)
                    lineItems.Add(new LineItem($"({Baseband.PartNumberDescriptor})", Baseband.LineItemText));
                else
                    lineItems.Add(new LineItem($"(?)", "Signal interface not selected"));

                if (SerialControlInterface != null)
                    lineItems.Add(new LineItem($"({SerialControlInterface.PartNumberDescriptor})", SerialControlInterface.LineItemText));
                else
                    lineItems.Add(new LineItem($"(?)", "Serial control not selected"));

                lineItems.Add(new LineItem(HasArtm0 ? "(1)" : "(0)", "ARTM Tier 0 (PCM/FM), 0.05 - 14 Mbps: " + (HasArtm0 ? "YES" : "NO")));
                lineItems.Add(new LineItem(HasArtm1 ? "(1)" : "(0)", "ARTM Tier I (SOQPSK-TG), 0.1 - 28 Mbps: " + (HasArtm1 ? "YES" : "NO")));
                lineItems.Add(new LineItem(HasArtm2 ? "(1)" : "(0)", "ARTM Tier II (Multi-h CPM), 0.1 - 28 Mbps: " + (HasArtm2 ? "YES" : "NO")));
                lineItems.Add(new LineItem(HasBPSK ? "(1)" : "(0)", "BPSK, QPSK, OQPSK 0.05 - 14 Mbps: " + (HasBPSK ? "YES" : "NO")));

                if (RFPower != null)
                    lineItems.Add(new LineItem($"({RFPower.PartNumberDescriptor})", RFPower.LineItemText));
                else
                    lineItems.Add(new LineItem($"(??)", "RF output power not selected"));

                if (Pinout != null)
                    lineItems.Add(new LineItem($"({Pinout.Name})", "Pinout Configuration"));
                else
                    lineItems.Add(new LineItem($"(??)", "Pinout not yet determined"));

                if (Package != null)
                {
                    double cubicSize = 0d;
                    string sizeString = "";
                    if (Package.Footprint.Shape == FootprintShape.Rectangular)
                    {
                        cubicSize = Math.Round(Package.Footprint.Length.Value * Package.Footprint.Width.Value * Package.Height.Measurement, 3);
                        sizeString = $"Package: {Package.Footprint.Length.Value} x {Package.Footprint.Width.Value} x {Package.Height.Measurement} = {cubicSize} inches³";
                    }
                    else
                    {
                        double rad = Package.Footprint.Diameter.Value / 2;
                        cubicSize = Math.Round((Math.PI * (rad * rad)) * Package.Height.Measurement, 3);
                        sizeString = $"Package: {Package.Footprint.Name} x {Package.Height.Measurement} = {cubicSize} inches³";
                    }
                    //string connString = $"Primary Connector: {Package.DataConnectors.First(x => x.Rank == ConnectorRank.Primary).DataConnector.Name}";
                    string rfString = $"RF Output Connector:";
                    //lineItems.Add(new LineItem($"({Package.Name})", $"{sizeString}\n{connString}\n{rfString}"));
                }
                else
                    lineItems.Add(new LineItem($"(????)", "Package not yet determined"));


                foreach (var option in SelectedOptions.OrderBy(x => x.Name))
                {
                    lineItems.Add(new LineItem($"({option.PartNumberDescriptor})", option.LineItemText));
                }

                return lineItems;
            }
        }

        private string bandComboPart => BandCombination == null ? "?" : BandCombination.PartNumberDescriptor;
        private string basebandPart => Baseband == null ? "?" : Baseband.PartNumberDescriptor;
        private string serialPart => SerialControlInterface == null ? "?" : SerialControlInterface.PartNumberDescriptor;
        private string modulationPart => (HasArtm0 ? "1" : "0") + (HasArtm1 ? "1" : "0") + (HasArtm2 ? "1" : "0") + (HasBPSK ? "1" : "0");
        private string rfPart => RFPower == null ? "??" : RFPower.PartNumberDescriptor;
        private string pinoutPart => Pinout == null ? "??" : Pinout.Name;
        private string packagePart => Package == null ? "????" : Package.Name;
        public string OptionStringAlt => string.Concat(SelectedOptions.OrderBy(x => x.PartNumberDescriptor).Select(x => "-" + x.PartNumberDescriptor));

        public string PartNumber => $"QSX-V{bandComboPart}{basebandPart}{serialPart}-{modulationPart}-{rfPart}-{pinoutPart}-{packagePart}{OptionStringAlt}";
    }

    public class LineItem
    {
        public string PartNumberDescriptor { get; set; }
        public string LineItemText { get; set; }

        public LineItem(string descriptor, string text)
        {
            PartNumberDescriptor = descriptor;
            LineItemText = text;
        }
    }
}
