﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class ConfigureReceiverViewModel
    {
        public string AddToQuote { get; set; }
        public string SaveForLater { get; set; }
        public Guid LineId { get; set; }

        public bool ShowFormFactorSection { get; set; }
        public bool ShowChannelCountSection { get; set; }
        public bool ShowIFInputSection { get; set; }
        public bool ShowAnalogOutputSection { get; set; }
        public bool ShowAMOutputSection { get; set; }
        public bool ShowAGCOutputSection { get; set; }
        public bool ShowBasebandSection { get; set; }
        public bool ShowDisplaySection { get; set; }
        public bool ShowBandComboSection { get; set; }
        public bool ShowDemodulationsSection { get; set; }
        public bool ShowPinoutSection { get; set; }
        public bool ShowOptionsSection { get; set; }

        public bool HasTier0 { get; set; }
        public bool HasTier1 { get; set; }
        public bool HasTier2 { get; set; }
        public bool HasLegacy { get; set; }

        public SingleSelectGuidChecklistViewModel FormFactorChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel ChannelCountChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel IFInputChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel AnalogOutputChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel AMOutputChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel AGCOutputChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel DisplayChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel BasebandChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public SingleSelectGuidChecklistViewModel PinoutChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();

        public MultiSelectChecklistViewModel DemodulationChecklist { get; set; } = new MultiSelectChecklistViewModel();
        public MultiSelectChecklistViewModel OptionsChecklist { get; set; } = new MultiSelectChecklistViewModel();

        public MultiSelectChecklistViewModel BandChecklist { get; set; } = new MultiSelectChecklistViewModel();
        public SingleSelectGuidChecklistViewModel BandComboChecklist { get; set; } = new SingleSelectGuidChecklistViewModel();
        public List<List<bool>> BandCheckmarks { get; set; } = new List<List<bool>>();
        public BandRange BandRange { get; set; } = BandRange.Standard;

        public string ChassisText { get; set; } = "XXXX";
        public string BandCombinationText { get; set; } = "XX";
        public string PinoutText { get; set; } = "XX";
        public string DemodulationText
        {
            get
            {
                string modString = "XXXX";

                if (Demodulations.Any())
                {
                    string tier0 = HasTier0 ? "1" : "0";
                    string tier1 = HasTier1 ? "1" : "0";
                    string tier2 = HasTier2 ? "1" : "0";
                    string legacy = HasLegacy ? "1" : "0";

                    modString = $"{tier0}{tier1}{tier2}{legacy}";
                }

                return modString;
            }
        }
        public string Options { get; set; }

        public bool HasEvtm { get; set; }

        [JsonIgnore]
        public RxChannelSet ChannelSet { get; set; }
        [JsonIgnore]
        public RxPackage Package { get; set; }
        [JsonIgnore]
        public RxBandCombination BandCombination { get; set; }
        [JsonIgnore]
        public RxPinout Pinout { get; set; }
        [JsonIgnore]
        public List<RxOption> SelectedOptions { get; set; } = new List<RxOption>();
        [JsonIgnore]
        public List<RxDemodulation> Demodulations { get; set; } = new List<RxDemodulation>();

        public bool IsComplete => ChannelSet != null &&
            Package != null &&
            BandCombination != null &&
            Pinout != null &&
            Demodulations.Any();

        public string PartNumber
        {
            get
            {
                string pinoutPart = Pinout != null ? Pinout.PartNumberDescriptor : "XX";

                string optionPart = "";
                foreach (var option in SelectedOptions.OrderBy(x => x.PartNumberDescriptor))
                {
                    optionPart += $"-{option.PartNumberDescriptor}";
                }

                return $"QSX-RDMS-{ChassisText}-{BandCombinationText}-{DemodulationText}-{pinoutPart}{optionPart}";
            }
        }

        public List<LineItem> LineItems
        {
            get
            {
                var lineItems = new List<LineItem>();
                lineItems.Add(new LineItem($"(RDMS)", "Third-Generation RDMS™ Telemetry Receiver / Demodulator / Synchronizer"));

                if (Package != null)
                {
                    string chassisDesc = Package.Description;
                    if (HasEvtm)
                        chassisDesc += " (EVTM)";

                    lineItems.Add(new LineItem($"({ChassisText})", chassisDesc));
                    lineItems.Add(new LineItem("", $"DISPLAYS: {Package.FrontPanelDisplay.Name}"));
                    lineItems.Add(new LineItem("", $"FRONT PANEL CONTROLS: {Package.FrontPanelInterface.Name}"));
                    lineItems.Add(new LineItem("", "GRAPHICS: Eye pattern, Constellation, Spectrum"));
                    lineItems.Add(new LineItem("", "DATA QUALITY ENCAPSULATION: Included, bypassable"));
                }
                else
                    lineItems.Add(new LineItem("(XXX)", "Chassis not selected"));

                if (BandCombination != null)
                {
                    lineItems.Add(new LineItem($"({BandCombinationText})", "FREQUENCY BANDS, TUNED IN 1Hz STEPS:"));

                    var orderedBands = BandCombination.Bands.OrderBy(x => x.MinimumFrequency);
                    foreach (var band in orderedBands)
                    {
                        if (BandRange == BandRange.Standard)
                            lineItems.Add(new LineItem("", band.RangeText));
                        else
                            lineItems.Add(new LineItem("", band.ExtendedRangeText));
                    }
                }
                else
                    lineItems.Add(new LineItem($"(XX)", "Band combination not selected"));

                if (Demodulations.Any())
                {
                    lineItems.Add(new LineItem($"({(HasTier0 ? "1" : "0")})", $"ARTM Tier 0 (PCM/FM), 24 kbps - 23 Mbps: {(HasTier0 ? "YES" : "NO")}"));
                    lineItems.Add(new LineItem($"({(HasTier1 ? "1" : "0")})", $"ARTM Tier I (SOQPSK-TG), 100 kbps - 46 Mbps: {(HasTier1 ? "YES" : "NO")}"));
                    lineItems.Add(new LineItem("", $"Space-Time Coding Demod with SOQPSK-TG: {(HasTier1 ? "YES" : "NO")}"));
                    lineItems.Add(new LineItem("", $"LDPC Decoder with SOQPSK-TG: {(HasTier1 ? "YES" : "NO")}"));
                    lineItems.Add(new LineItem($"({(HasTier2 ? "1" : "0")})", $"ARTM Tier II (Multi-h CPM), 1 Mbps - 46 Mbps: {(HasTier2 ? "YES" : "NO")}"));
                    lineItems.Add(new LineItem($"({(HasLegacy ? "1" : "0")})", $"BPSK, QPSK, OQPSK, AQPSK, UQPSK, AUQPSK, Digital PM: {(HasLegacy ? "YES" : "NO")}"));
                }
                else
                {
                    lineItems.Add(new LineItem($"({(HasTier0 ? "1" : "0")})", $"ARTM Tier 0 (PCM/FM), 24 kbps - 23 Mbps: NO SELECTION"));
                    lineItems.Add(new LineItem($"({(HasTier1 ? "1" : "0")})", $"ARTM Tier I (SOQPSK-TG), 100 kbps - 46 Mbps: NO SELECTION"));
                    lineItems.Add(new LineItem("", $"Space-Time Coding Demod with SOQPSK-TG: NO SELECTION"));
                    lineItems.Add(new LineItem("", $"LDPC Decoder with SOQPSK-TG: NO SELECTION"));
                    lineItems.Add(new LineItem($"({(HasTier2 ? "1" : "0")})", $"ARTM Tier II (Multi-h CPM), 1 Mbps - 46 Mbps: NO SELECTION"));
                    lineItems.Add(new LineItem($"({(HasLegacy ? "1" : "0")})", $"BPSK, QPSK, OQPSK, AQPSK, UQPSK, AUQPSK, Digital PM: NO SELECTION"));
                }


                if (Pinout != null)
                    lineItems.Add(new LineItem($"({Pinout.PartNumberDescriptor})", $"PINOUT CUSTOMIZATION: {Pinout.Name}"));
                else
                    lineItems.Add(new LineItem($"(XX)", $"PINOUT CUSTOMIZATION: NO SELECTION"));

                foreach (var option in SelectedOptions.OrderBy(x => x.Name))
                {
                    lineItems.Add(new LineItem($"({option.PartNumberDescriptor})", option.Name));
                }

                return lineItems;
            }
        }
    }

    public enum BandRange
    {
        Standard, Extended
    }
}
