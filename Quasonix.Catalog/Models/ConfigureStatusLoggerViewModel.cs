﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class ConfigureStatusLoggerViewModel
    {
        public string AddToQuote { get; set; }
        public string SaveForLater { get; set; }
        public Guid LineId { get; set; }

        public bool ShowFormFactorSection { get; set; }
        public bool ShowReceiverCountSection { get; set; }

        public SingleSelectChecklistViewModel FormFactorChecklist { get; set; } = new SingleSelectChecklistViewModel();
        public SingleSelectChecklistViewModel ReceiverCountChecklist { get; set; } = new SingleSelectChecklistViewModel();

        public string PackageText { get; set; }

        [JsonIgnore]
        public StatusLogger StatusLogger { get; set; }

        public bool IsComplete => StatusLogger != null;

        public string PartNumber => StatusLogger?.PartNumber;

        public List<LineItem> LineItems
        {
            get
            {
                var lineItems = new List<LineItem>();
                lineItems.Add(new LineItem($"(RXSL)", "RDMS™ Status Logger"));

                if (StatusLogger != null)
                    lineItems.Add(new LineItem($"({PackageText})", StatusLogger.Description));

                return lineItems;
            }
        }
    }
}
