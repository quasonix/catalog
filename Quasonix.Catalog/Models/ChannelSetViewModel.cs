﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class ChannelSetViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Total channel count must be greater than 0")]
        [Display(Name = "Total Channels")]
        public int TotalChannelCount { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Total channel count must be greater than 0")]
        [Display(Name = "Input Channels")]
        public int InputChannelCount { get; set; }
    }

    public class ChannelSetDetailViewModel
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        [Display(Name = "Total Channels")]
        public int TotalChannelCount { get; set; }
        [Display(Name = "Input Channels")]
        public int InputChannelCount { get; set; }
    }
}
