﻿using DynamicVML;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Models
{
    public class AccessoryViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string PartNumber { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Signal Standard")]
        public AccessorySignalStandard SignalStandard { get; set; }
        [Required]
        public int Price { get; set; }

        [Display(Name = "Categories")]
        public MultiSelectChecklistViewModel CategoryChecklist { get; set; } = new MultiSelectChecklistViewModel();
        [Display(Name = "Functions")]
        public MultiSelectChecklistViewModel FunctionChecklist { get; set; } = new MultiSelectChecklistViewModel();
    }

    public class AccessoryDetailViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
