﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string htmlMessage);
    }

    public class EmailSender : IEmailSender
    {
        private static SmtpClient smtp = new SmtpClient("192.168.0.8", 25)
        {
            Credentials = new System.Net.NetworkCredential("nmoore", "nmoore")
        };
        private static MailAddress defaultSender = new MailAddress("ecat@quasonix.com", "Quasonix eCat");

        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            MailMessage message = new MailMessage();
            message.To.Add(email);
            message.Subject = subject;
            message.From = defaultSender;
            message.Body = htmlMessage;
            message.IsBodyHtml = true;
            return smtp.SendMailAsync(message);
        }
    }
}
