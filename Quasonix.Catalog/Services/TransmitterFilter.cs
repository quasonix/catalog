﻿using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Quasonix.Catalog.Controllers.QuoteController;

namespace Quasonix.Catalog.Services
{
    public class TransmitterFilter
    {
        public List<FlattenedTransmitter> Transmitters { get; set; } = new List<FlattenedTransmitter>();

        private List<TxModulation> ModulationOptions { get; set; }

        private IEnumerable<TxBandCombination> BandCombinationOptions => Transmitters.Select(x => x.BandCombination).Distinct();
        private IEnumerable<TxBaseband> BasebandOptions => Transmitters.Select(x => x.Baseband).Distinct();
        private IEnumerable<ConnectorPosition> DataConnectorPositionOptions => Transmitters.Select(x => x.DataConnectorPosition).Distinct();
        private IEnumerable<TxFootprint> FootprintOptions => Transmitters.Select(x => x.Footprint).Distinct();
        private IEnumerable<TxHeight> HeightOptions => Transmitters.Select(x => x.Height).Distinct();
        private IEnumerable<TxOptionCombination> OptionCombinationOptions => Transmitters.Select(x => x.OptionCombination).Distinct();
        private IEnumerable<TxPackage> PackageOptions => Transmitters.Select(x => x.Package).Distinct();
        private IEnumerable<ConnectorPosition> RadioConnectorPositionOptions => Transmitters.Select(x => x.RadioConnectorPosition).Distinct();
        private IEnumerable<TxRadioFrequencyPower> RadioPowerOptions => Transmitters.Select(x => x.RFPower).Distinct();
        private IEnumerable<TxSerialControlInterface> SerialOptions => Transmitters.Select(x => x.SerialControlInterface).Distinct();

        private TxBaseband SelectedBaseband => BasebandOptions.Count() == 1 ? BasebandOptions.First() : null;
        private TxBandCombination SelectedBandCombination => BandCombinationOptions.Count() == 1 ? BandCombinationOptions.First() : null;
        private ConnectorPosition? SelectedDataConnectorPosition => DataConnectorPositionOptions.Count() == 1 ? DataConnectorPositionOptions.First() : (ConnectorPosition?)null;
        private TxFootprint SelectedFootprint => FootprintOptions.Count() == 1 ? FootprintOptions.First() : null;
        private TxHeight SelectedHeight => HeightOptions.Count() == 1 ? HeightOptions.First() : null;
        private TxOptionCombination SelectedOptionCombination => OptionCombinationOptions.Count() == 1 ? OptionCombinationOptions.First() : null;
        private TxPackage SelectedPackage => PackageOptions.Count() == 1 ? PackageOptions.First() : null;
        private ConnectorPosition? SelectedRadioConnectorPosition => RadioConnectorPositionOptions.Count() == 1 ? RadioConnectorPositionOptions.First() : (ConnectorPosition?)null;
        private TxRadioFrequencyPower SelectedRadioPower => RadioPowerOptions.Count() == 1 ? RadioPowerOptions.First() : null;
        private TxSerialControlInterface SelectedSerial => SerialOptions.Count() == 1 ? SerialOptions.First() : null;

        private IEnumerable<TxComponent> SelectedComponents => new List<TxComponent>()
        {
            SelectedBandCombination, SelectedBaseband, SelectedFootprint, SelectedHeight, SelectedPackage, SelectedRadioPower, SelectedSerial
        }.Where(x => x != null);

        private readonly ICatalogRepository repo;
        private readonly IProductDescriptionBuilder descriptionBuilder;

        public TransmitterFilter(ICatalogRepository repo,
            IProductDescriptionBuilder descriptionBuilder)
        {
            this.repo = repo;
            this.descriptionBuilder = descriptionBuilder;
        }

        public void Filter(ConfigureTxViewModel model)
        {
            InitTransmitterList();

            var allPOS = repo.GetAll<TxPinout>().ToList();
            var pos = allPOS.Where(x => !x.IncludedPins.Any(x => x.Name == "RF On/Off")).ToList();

            // we don't want these to change, so get them before any filtering
            var hardwareOptions = Transmitters.Select(x => x.OptionCombination).SelectMany(x => x.Options).Distinct().OrderBy(x => x.Name);
            var bandOptions = Transmitters.Select(x => x.BandCombination).SelectMany(x => x.Bands).Distinct().OrderBy(x => x.Name);

            var options = repo.GetGroup<TxOption>(x => x.Active && x.UserSelectable && x.OptionType != TransmitterOptionType.Hardware).ToList();

            if (model.OptionChecklist.HasSelection)
            {
                var selectedOptions = repo.GetGroup<TxOption>(x => model.OptionChecklist.SelectedValues.Contains(x.Id));

                foreach (var option in selectedOptions)
                {
                    foreach (var conflict in option.Conflicts)
                    {
                        switch (conflict)
                        {
                            case TxOption conflictingOption:
                                options.Remove(conflictingOption);
                                break;
                            case TxComponent conflictingComponent:
                                Transmitters = Transmitters.Where(x => !x.Components.Contains(conflictingComponent)).ToList();
                                break;
                        }
                    }

                    //foreach (var req in option.Requirements)
                    //{
                    //    switch (req)
                    //    {
                    //        case TxOption reqOption:
                    //            options.Remove(conflictingOption);
                    //            break;
                    //        case TxComponent reqComponent:
                    //            Transmitters = Transmitters.Where(x => !x.Components.Contains(conflictingComponent)).ToList();
                    //            break;
                    //    }
                    //}
                }

                model.Options = selectedOptions.ToList();
            }

            FilterTxByComponent(model.BandCombinationChecklist.SelectedValue);
            FilterTxByComponent(model.BasebandChecklist.SelectedValue);
            FilterTxByComponent(model.FootprintChecklist.SelectedValue);
            FilterTxByComponent(model.HeightChecklist.SelectedValue);
            FilterTxByComponent(model.RadioPowerChecklist.SelectedValue);
            FilterTxByComponent(model.SerialChecklist.SelectedValue);

            if (model.DataConnectorPositionChecklist.HasSelection)
            {
                var position = (ConnectorPosition)model.DataConnectorPositionChecklist.SelectedValue;
                Transmitters = Transmitters.Where(x => x.DataConnectorPosition == position).ToList();
            }
            if (model.RadioConnectorPositionChecklist.HasSelection)
            {
                var position = (ConnectorPosition)model.RadioConnectorPositionChecklist.SelectedValue;
                Transmitters = Transmitters.Where(x => x.RadioConnectorPosition == position).ToList();
            }

            // remove option conflicts
            foreach (var component in SelectedComponents)
            {
                options.RemoveAll(x => component.ConflictingOptions.Contains(x));
            }

            if (model.BandChecklist.HasSelection)
            {
                foreach (var bandId in model.BandChecklist.SelectedValues)
                {
                    var band = repo.Get<TxBand>(bandId);
                    Transmitters = Transmitters.Where(x => x.BandCombination.Bands.Contains(band)).ToList();
                }
            }

            if (model.BandCombinationChecklist.HasSelection)
            {
                var bandCombo = repo.Get<TxBandCombination>(x => x.Id == model.BandCombinationChecklist.SelectedValue);
                Transmitters = Transmitters.Where(x => x.BandCombination == bandCombo).ToList();
            }

            if (model.HardwareOptionChecklist.HasSelection)
            {
                foreach (var optionId in model.HardwareOptionChecklist.SelectedValues)
                {
                    var option = repo.Get<TxOption>(optionId);
                    Transmitters = Transmitters.Where(x => x.OptionCombination.Options.Contains(option)).ToList();
                }
            }

            if (model.OptionCombinationChecklist.HasSelection)
            {
                var optionCombo = repo.Get<TxOptionCombination>(x => x.Id == model.OptionCombinationChecklist.SelectedValue);
                Transmitters = Transmitters.Where(x => x.OptionCombination == optionCombo).ToList();
            }

            model.BandCheckmarks = new List<List<bool>>();
            foreach (var bandCombo in BandCombinationOptions.OrderBy(x => x.Name))
            {
                var list = new List<bool>();
                foreach (var band in bandOptions)
                {
                    list.Add(bandCombo.Bands.Contains(band));
                }
                model.BandCheckmarks.Add(list);
            }

            model.HardwareOptionCheckmarks = new List<List<bool>>();
            foreach (var optionCombo in OptionCombinationOptions.OrderBy(x => x.Name))
            {
                var list = new List<bool>();
                foreach (var option in hardwareOptions)
                {
                    list.Add(optionCombo.Options.Contains(option));
                }
                model.HardwareOptionCheckmarks.Add(list);
            }

            model.Modulations = new List<TxModulation>();
            if (model.ModulationChecklist.HasSelection)
            {
                var selectedMods = repo.GetGroup<TxModulation>(x => model.ModulationChecklist.SelectedValues.Contains(x.Id));
                model.Modulations = selectedMods.ToList();
            }

            ModulationOptions = repo.GetAll<TxModulation>().ToList();

            // if all remaining members in any given group are conflicts of an option, remove that option
            var optionsToRemove = new List<TxOption>();
            foreach (var option in options)
            {
                if (!option.ComponentConflicts.Any())
                    continue;

                var comps = option.ComponentConflicts.Select(x => x.Component).ToList();

                if (BandCombinationOptions.Intersect(comps).Count() == BandCombinationOptions.Count())
                    optionsToRemove.Add(option);
                if (BasebandOptions.Intersect(comps).Count() == BasebandOptions.Count())
                    optionsToRemove.Add(option);
                if (FootprintOptions.Intersect(comps).Count() == FootprintOptions.Count())
                    optionsToRemove.Add(option);
                if (HeightOptions.Intersect(comps).Count() == HeightOptions.Count())
                    optionsToRemove.Add(option);
                if (ModulationOptions.Intersect(comps).Count() == ModulationOptions.Count())
                    optionsToRemove.Add(option);
                if (RadioPowerOptions.Intersect(comps).Count() == RadioPowerOptions.Count())
                    optionsToRemove.Add(option);
                if (SerialOptions.Intersect(comps).Count() == SerialOptions.Count())
                    optionsToRemove.Add(option);
            }


            // hard-coded rules for LD6 -- can only work for T4 based
            var optLd6 = repo.Get<TxOption>(x => x.Name == "LD6");
            if (!Transmitters.Any(x => x.Package.Generation == TransmitterGeneration.T4))
            {
                optionsToRemove.Add(optLd6);
            }
            if (model.Options.Contains(optLd6))
            {
                Transmitters = Transmitters.Where(x => x.Package.Generation == TransmitterGeneration.T4).ToList();
            }

            options.RemoveAll(x => optionsToRemove.Contains(x));

            model.BandChecklist = bandOptions
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, model.BandChecklist.SelectedValues);
            model.BandCombinationChecklist = BandCombinationOptions.OrderBy(x => x.Name)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.BandCombinationChecklist.SelectedValue);
            model.BasebandChecklist = BasebandOptions.OrderBy(x => x.Name)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.BasebandChecklist.SelectedValue);
            model.DataConnectorPositionChecklist = DataConnectorPositionOptions
                .ToSingleSelectChecklist(x => x.ToString(), x => (int)x, model.DataConnectorPositionChecklist.SelectedValue);
            model.FootprintChecklist = FootprintOptions.OrderBy(x => x.Area)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.FootprintChecklist.SelectedValue);
            model.HardwareOptionChecklist = hardwareOptions
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, model.HardwareOptionChecklist.SelectedValues);
            model.HeightChecklist = HeightOptions.OrderBy(x => x.Measurement)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.HeightChecklist.SelectedValue);
            model.OptionChecklist = options.OrderBy(x => x.Name)
                .ToMultiSelectChecklist(x => $"{x.Name} - {x.LineItemText}", x => x.Id, model.OptionChecklist.SelectedValues,
                options.Where(x => x.OptionType == TransmitterOptionType.Pinout || x.OptionType == TransmitterOptionType.Accessory)
                .ToDictionary(x => x.Id, x => x.OptionType == TransmitterOptionType.Pinout ? "pin" : "acc"));
            model.OptionCombinationChecklist = OptionCombinationOptions.OrderBy(x => x.Name)
                .ToSingleSelectGuidChecklist(x => "", x => x.Id, model.OptionCombinationChecklist.SelectedValue);
            model.ModulationChecklist = ModulationOptions.OrderBy(x => x.Type)
                .ToMultiSelectChecklist(x => x.Name, x => x.Id, model.ModulationChecklist.SelectedValues);
            model.RadioConnectorPositionChecklist = RadioConnectorPositionOptions
                .ToSingleSelectChecklist(x => x.ToString(), x => (int)x, model.RadioConnectorPositionChecklist.SelectedValue);
            model.RadioPowerChecklist = RadioPowerOptions.OrderBy(x => x.WattagePerOutput)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.RadioPowerChecklist.SelectedValue);
            model.SerialChecklist = SerialOptions.OrderBy(x => x.Name)
                .ToSingleSelectGuidChecklist(x => x.Name, x => x.Id, model.SerialChecklist.SelectedValue);
            model.HardwareOptionDefinitions = hardwareOptions.Select(x => $"{x.PartNumberDescriptor} - {x.LineItemText}").ToList();

            model.BandCombination = SelectedBandCombination;
            model.Baseband = SelectedBaseband;
            model.DataConnectorPosition = SelectedDataConnectorPosition.GetValueOrDefault();
            model.Footprint = SelectedFootprint;
            model.Height = SelectedHeight;
            model.OptionCombination = SelectedOptionCombination;
            model.Package = SelectedPackage;
            model.RadioConnectorPosition = SelectedRadioConnectorPosition.GetValueOrDefault();
            model.RadioPower = SelectedRadioPower;
            model.SerialControlInterface = SelectedSerial;

            if (model.OptionCombination != null)
            {
                model.Options.AddRange(model.OptionCombination.Options);
            }

            // hard-coded rule for 20mW
            if (model.RadioPower?.WattagePerOutput == 0.02d)
            {
                var optionPw = repo.Get<TxOption>(x => x.Name == "PW020");
                model.Options.Add(optionPw);
            }

            // hard-coded rule for D2
            if (model.Options.Any(x => x.Name == "D2"))
            {
                var optionD2 = model.Options.First(x => x.Name == "D2");
                var optionCf = repo.Get<TxOption>(x => x.Name == "CF");

                if (!model.OptionChecklist.SelectedValues.Contains(optionCf.Id))
                {
                    model.Options.Add(optionCf);
                    model.OptionChecklist.GhostSelect(optionCf.Id, $"This option has been \"ghost\" selected as it is required by option {optionD2.Name}");
                }
            }

            // hard-coded rule for Tier I
            if (model.Options.Any(x => x.Name == "LD" || x.Name == "LD6" || x.Name == "STC"))
            {
                var option = model.Options.First(x => x.Name == "LD" || x.Name == "LD6" || x.Name == "STC");
                var mod = repo.Get<TxModulation>(x => x.Type == ModulationType.ARTM1);

                if (!model.ModulationChecklist.SelectedValues.Contains(mod.Id))
                {
                    model.Modulations.Add(mod);
                    model.ModulationChecklist.GhostSelect(mod.Id, $"This modulation has been \"ghost\" selected as it is required by option {option.Name}");
                }
            }

            if (SelectedPackage != null)
            {
                var defaultPinout = repo.Get<TxPinout>(x => x.Name == "XX");

                var requiredPins = SelectedComponents.SelectMany(x => x.Pins).ToList();
                requiredPins.AddRange(model.Options.SelectMany(x => x.Pins));

                // get a list of options that have special pin reqs. pinouts with these pins should only be allowed if the option is selected
                var pinnedOptions = repo.GetGroup<TxOption>(x => x.Active && x.Pins.Count > 0 && !model.Options.Contains(x)).ToList();

                var possiblePinouts = new List<TxPinout>();
               
                foreach (var pinout in SelectedPackage.ConnectorSet.Pinouts)
                {
                    var pins = pinout.PinUsages.Select(x => x.Pin);

                    // check if the pinout has all of the pins required by current selections
                    bool containsReqPins = pins.Intersect(requiredPins).Count() == requiredPins.Count();
                    if (containsReqPins)
                    {
                        bool containsSpecialPins = false;

                        // start with the pins from the primary connector to check special pins against
                        var checkPins = pinout.ConnectorSet.PrimaryConnector.PinUsages.Where(x => x.Pinout == pinout).Select(x => x.Pin);
                        bool hasMdm9 = pinout.ConnectorSet.SecondaryConnector?.Name == "Male MDM-9";

                        // if there is not an MDM-9 present, check all of the pins in the pinout
                        if (!hasMdm9)
                            checkPins = pins;

                        foreach (var option in pinnedOptions)
                        {
                            // if it contains the pins from an option without having that option, it is invalid
                            if (checkPins.Intersect(option.Pins).Count() == option.Pins.Count)
                            {
                                containsSpecialPins = true;
                                break;
                            }
                        }

                        // special rule for EN
                        bool hasEn = model.Options.Any(x => x.Name == "EN");
                        if (!hasEn)
                        {
                            // if EN is not selected, the pinout should not have any pins concerning ethernet
                            containsSpecialPins = pins.Any(x => x.Name.ToLower().Contains("ethernet"));
                        }    

                        if (!containsSpecialPins)
                            possiblePinouts.Add(pinout);
                    }
                }

                if (possiblePinouts.Count > 0)
                {
                    model.Pinout = possiblePinouts.OrderBy(x => x.Name).First();

                    // hard-coded rule for -NRF
                    if (!model.Pinout.IncludedPins.Any(x => x.Name == "RF On/Off"))
                    {
                        var optionNorf = repo.Get<TxOption>(x => x.Name == "NRF");
                        model.Options.Add(optionNorf);
                    }
                }
                else
                    model.Pinout = defaultPinout;
            }
        }

        private List<TxComponent> Components { get; set; } = new List<TxComponent>();

        private void FilterTxByComponent(Guid? id)
        {
            if (id.HasValue)
            {
                var component = repo.Get<TxComponent>(id);
                Transmitters = Transmitters.Where(x => x.Components.Contains(component)).ToList();
            }
        }

        private void InitTransmitterList()
        {
            var packages = repo.GetGroup<TxPackage>(x => x.Active);
            var options = repo.GetAll<TxOption>().ToList();

            foreach (var package in packages)
            {
                foreach (var optionCombo in package.OptionCombinations)
                {
                    foreach (var stack in package.Stacks.Where(x => x.Active))
                    {
                        foreach (var baseband in stack.Basebands)
                        {
                            foreach (var serial in stack.SerialControlInterfaces)
                            {
                                var flatTx = new FlattenedTransmitter()
                                {
                                    Footprint = package.Footprint,
                                    Height = package.Height,
                                    DataConnectorPosition = package.ConnectorSet.PrimaryConnectorPosition,
                                    RadioConnectorPosition = package.RadioConnectorPosition,
                                    RFPower = stack.RadioFrequencyPower,
                                    BandCombination = stack.BandCombination,
                                    Baseband = baseband,
                                    SerialControlInterface = serial,
                                    Package = package,
                                    Stack = stack,
                                    OptionCombination = optionCombo
                                };

                                Transmitters.Add(flatTx);
                            }
                        }
                    }
                }
            }
        }
    }

    public static class Extendarific
    {
        public static bool ContainsAny<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
        {
            return first.Intersect(second).Any();
        }
    }
}
