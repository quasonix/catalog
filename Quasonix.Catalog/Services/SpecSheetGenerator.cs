﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Services
{
    public interface ISpecSheetGenerator
    {
        Stream GenerateTransmitterSpecSheet(Transmitter tx);
    }

    public class SpecSheetGenerator : ISpecSheetGenerator
    {
        private Document document;
        private Transmitter transmitter;

        private Color darkGrey = Color.FromRgb(40, 40, 40);
        private Color lightGrey = Color.FromRgb(200, 200, 200);

        public Stream GenerateTransmitterSpecSheet(Transmitter tx)
        {
            // Create a new MigraDoc document
            document = new Document();

            transmitter = tx;

            // set default style
            var style = document.Styles["Normal"];
            style.Font.Name = "Arial";
            style.Font.Size = 8;
            style.Font.Color = darkGrey;

            PageSetup pageSetup = document.DefaultPageSetup.Clone();
            pageSetup.Orientation = Orientation.Landscape;

            // Add a section to the document
            Section section = document.AddSection();
            section.PageSetup.LeftMargin = "1cm";
            section.PageSetup.RightMargin = "1cm";
            section.PageSetup.TopMargin = "3.4cm";
            section.PageSetup.BottomMargin = "1cm";
            section.PageSetup.Orientation = Orientation.Landscape;
            section.PageSetup.HeaderDistance = ".5cm";

            HeaderFooter header = CreateHeader();
            section.Headers.Primary = header;

            HeaderFooter footer = CreateFooter();
            section.Footers.Primary = footer;

            var table = CreateDataSection();
            section.Add(table);

            section.AddPageBreak();

            section.AddPageBreak();

            document.UseCmykColor = true;
            const bool unicode = false;
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode);
            pdfRenderer.Document = document;

            pdfRenderer.RenderDocument();

            // save the document to stream
            MemoryStream stream = new MemoryStream();
            pdfRenderer.PdfDocument.Save(stream, false);
            return stream;
        }

        private HeaderFooter CreateHeader()
        {
            HeaderFooter hf = new HeaderFooter();

            hf.Format.Font.Color = darkGrey;

            var table = hf.AddTable();
            var column = table.AddColumn("14cm");
            column = table.AddColumn("14cm");

            var row = table.AddRow();
            var logo = row.Cells[0].AddImage(@"C:\Users\nmoore\Pictures\quasonix.png");
            logo.Width = 200;

            // create a second table to represent text
            var subTable = new Table();

            // text columns
            var subColumn = subTable.AddColumn("14cm");
            subColumn.Format.Alignment = ParagraphAlignment.Right;

            var subRow = subTable.AddRow();
            subRow.Cells[0].AddParagraph("Transmitter Specifications");
            subRow.Cells[0].Format.Alignment = ParagraphAlignment.Right;
            subRow.Cells[0].Format.Font.Bold = true;
            subRow.Cells[0].Format.Font.Size = 24;

            subRow = subTable.AddRow();
            subRow.Cells[0].AddParagraph("ISO 9001:2015 Certified");
            subRow.Cells[0].Format.Alignment = ParagraphAlignment.Right;
            subRow.Cells[0].Format.Font.Bold = false;
            subRow.Cells[0].Format.Font.Size = 11;

            subRow = subTable.AddRow();
            Paragraph paragraph = subRow.Cells[0].AddParagraph();
            paragraph.AddText("Page ");
            paragraph.AddPageField();
            paragraph.AddText(" of ");
            paragraph.AddNumPagesField();
            paragraph.AddText($", Generated {DateTime.Now.ToShortDateString()}");
            subRow.Cells[0].Format.Alignment = ParagraphAlignment.Right;
            subRow.Cells[0].Format.Font.Bold = false;
            subRow.Cells[0].Format.Font.Size = 11;

            row.Cells[1].Elements.Add(subTable);

            row = table.AddRow();
            paragraph = row.Cells[0].AddParagraph();
            paragraph.AddFormattedText("Part Number: ", TextFormat.Bold);
            paragraph.AddText("QSX-VCR2-1000-10-04-05AH-AI");
            paragraph.Format.Font.Size = 12;
            paragraph.Format.SpaceBefore = ".1cm";

            return hf;
        }

        private HeaderFooter CreateFooter()
        {
            HeaderFooter hf = new HeaderFooter();

            hf.Format.Font.Color = darkGrey;

            var table = hf.AddTable();
            var column = table.AddColumn("28cm");

            var row = table.AddRow();
            row.Cells[0].AddParagraph("Quasonix, Inc., 6025 Schumacher Park Dr., West Chester, OH 45069  |  (513) 942-1287  |  info@quasonix.com");

            row = table.AddRow();
            var paragraph = row.Cells[0].AddParagraph("No part of the document may be circulated, quoted, or reproduced for distribution without prior written approval from Quasonix, Inc. ");
            paragraph.AddFormattedText(" Copyright Quasonix, Inc., All Rights Reserved.", TextFormat.Bold);
            paragraph.Format.SpaceBefore = ".1cm";

            return hf;
        }

        private Table CreateDataSection()
        {
            // get containing table for top elements
            Table table = new Table();

            // create columns for top elements
            var leftColumn = table.AddColumn("13.6cm");
            var rightColumn = table.AddColumn("13.6cm");
            rightColumn.LeftPadding = ".8cm";

            var row = table.AddRow();

            var leftTable = CreateTable();
            leftTable.AddColumn("6.8cm");
            leftTable.AddColumn("6.8cm");
            leftTable.Columns[0].Borders.Right.Visible = false;
            leftTable.LeftPadding = ".1cm";
            leftTable.RightPadding = ".1cm";
            leftTable.TopPadding = ".1cm";
            var leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Functional");
            leftRow.Cells[0].MergeRight = 1;
            leftRow.Format.Font.Size = 11;
            leftRow.Format.Font.Color = Color.FromRgb(0, 0, 0);
            leftRow.Format.Font.Bold = true;
            leftRow.Shading.Color = lightGrey;

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Frequency Range");

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Carrier Frequency Tuning Increment");

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Frequency Selection");

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Carrier Stability");

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Carrier Frequency Accuracy");

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("RF Output Power");

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Output Load VSWR");

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Modulations Supported");

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Randomizer");

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Clock and Data Interface");

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Serial Control Interface");

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Power Requirements");
            leftRow.Cells[0].MergeRight = 1;
            leftRow.Format.Font.Size = 11;
            leftRow.Format.Font.Color = Color.FromRgb(0, 0, 0);
            leftRow.Format.Font.Bold = true;
            leftRow.Shading.Color = lightGrey;

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Supply Voltage");

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Supply Current");

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Grounding");

            leftRow = leftTable.AddRow();
            leftRow.Cells[0].AddParagraph("Reverse Polarity Protection");

            row.Cells[0].Elements.Add(leftTable);

            var rightTable = CreateTable();
            rightTable.AddColumn("6.8cm");
            rightTable.AddColumn("6.8cm");
            rightTable.Columns[0].Borders.Right.Visible = false;
            rightTable.LeftPadding = ".1cm";
            rightTable.RightPadding = ".1cm";
            rightTable.TopPadding = ".1cm";
            var rightRow = rightTable.AddRow();
            rightRow.Cells[0].AddParagraph("Environmental");
            rightRow.Cells[0].MergeRight = 1;
            rightRow.Format.Font.Size = 11;
            rightRow.Format.Font.Color = Color.FromRgb(0, 0, 0);
            rightRow.Format.Font.Bold = true;
            rightRow.Shading.Color = lightGrey;

            rightRow = rightTable.AddRow();
            rightRow.Cells[0].AddParagraph("Operating Temperature");

            rightRow = rightTable.AddRow();
            rightRow.Cells[0].AddParagraph("Storage Temperature");

            rightRow = rightTable.AddRow();
            rightRow.Cells[0].AddParagraph("Random Vibration");

            rightRow = rightTable.AddRow();
            rightRow.Cells[0].AddParagraph("Shock");

            rightRow = rightTable.AddRow();
            rightRow.Cells[0].AddParagraph("Acceleration");

            rightRow = rightTable.AddRow();
            rightRow.Cells[0].AddParagraph("Humidity");

            rightRow = rightTable.AddRow();
            rightRow.Cells[0].AddParagraph("Altitude");

            rightRow = rightTable.AddRow();
            rightRow.Cells[0].AddParagraph("EMI");

            rightRow = rightTable.AddRow();
            rightRow.Cells[0].AddParagraph("Mechanical");
            rightRow.Cells[0].MergeRight = 1;
            rightRow.Format.Font.Size = 11;
            rightRow.Format.Font.Color = Color.FromRgb(0, 0, 0);
            rightRow.Format.Font.Bold = true;
            rightRow.Shading.Color = lightGrey;

            rightRow = rightTable.AddRow();
            rightRow.Cells[0].AddParagraph("Size");
            var footprint = transmitter.Package.Footprint;
            var sizeString = "";
            if (footprint.Shape == FootprintShape.Rectangular)
                sizeString = $"{footprint.Length}\" x {footprint.Width}\" x {transmitter.Package.Height.Measurement}\"";
            else
                sizeString = $"{footprint.Diameter}\" diameter x {transmitter.Package.Height.Measurement}\"";
            rightRow.Cells[1].AddParagraph(sizeString);

            rightRow = rightTable.AddRow();
            rightRow.Cells[0].AddParagraph("Volume");
            rightRow.Cells[1].AddParagraph(transmitter.Package.Volume + " in³");

            rightRow = rightTable.AddRow();
            rightRow.Cells[0].AddParagraph("Weight");
            rightRow.Cells[1].AddParagraph("Contact Quasonix");

            rightRow = rightTable.AddRow();
            rightRow.Cells[0].AddParagraph("Options");
            rightRow.Cells[0].MergeRight = 1;
            rightRow.Format.Font.Size = 11;
            rightRow.Format.Font.Color = Color.FromRgb(0, 0, 0);
            rightRow.Format.Font.Bold = true;
            rightRow.Shading.Color = lightGrey;

            rightRow = rightTable.AddRow();
            rightRow.Cells[0].AddParagraph("none");

            row.Cells[1].Elements.Add(rightTable);

            return table;
        }

        private Table CreateTable()
        {
            var table = new Table();
            table.Format.Font.Size = 10;
            table.TopPadding = 2;
            table.BottomPadding = 2;
            table.LeftPadding = 2;
            table.RightPadding = 2;
            table.Borders.Color = lightGrey;
            table.Format.Font.Color = darkGrey;
            return table;
        }
    }

    public static class MigradocExtensions
    {
        public static Paragraph AddNonNullParagraph(this Cell cell, string paragraph)
        {
            return cell.AddParagraph(paragraph == null ? "" : paragraph);
        }

        public static Row Format(this Row row, string style)
        {
            switch (style)
            {
                case "header":
                    row.Shading.Color = Color.FromRgb(200, 200, 200);
                    row.Format.Font.Bold = true;
                    row.Format.Font.Size = 9;
                    break;
            }

            return row;
        }
    }
}
