﻿using Microsoft.AspNetCore.Hosting;
using OfficeOpenXml;
using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Services
{
    public interface ISeeder
    {
        void Seed();
    }

    public class Seeder : ISeeder
    {
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly ICatalogRepository repo;
        private readonly ICatalogUnitOfWork unitOfWork;

        public Seeder(IWebHostEnvironment webHostEnvironment,
            ICatalogRepository repo,
            ICatalogUnitOfWork unitOfWork)
        {
            this.webHostEnvironment = webHostEnvironment;
            this.repo = repo;
            this.unitOfWork = unitOfWork;

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        }

        public void Seed()
        {
            try
            {
                unitOfWork.BeginTransaction();
                //SeedAccessory();
                //SeedTxPackage();
                //SeedAntOption();
                //SeedAntPricing();
                //SeedAccPricing();
                //SeedRxBasebandSet();
                //SeedRxPricing();
                //SeedRxConfiguration();
                //SeedTxOption();
                //SeedTxStack();
                //SeedPackageNew();
                //SeedTx();
                //SeedOptionCombo();
                //SeedOptionComboPackage();
                //SeedConflict();
                //SeedPrice();
                //SeedPriceRule();
                //SeedOptionPins();
                //SeedCompPins();
                //SeedGen();
                //FixLines();
                //FixCustInfo();
                unitOfWork.Commit();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void FixCustInfo()
        {
            //var quotes = repo.GetAll<Quote>().ToList();

            //foreach (var quote in quotes)
            //{
            //    if (!string.IsNullOrWhiteSpace(quote.CustomerJson))
            //    {
            //        var info = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.CustomerInfo>(quote.CustomerJson);

            //        quote.AdditionalInformation = info.AdditionalInfo;
            //        quote.CustomerName = info.Name;
            //        quote.CustomerLocation = info.Location;
            //        quote.ShippingZone = ShippingZone.OtherUS;
            //        quote.PurchaserEmail = info.PurchaserEmail;
            //        quote.OrderType = OrderType.COTS;

            //        repo.Update(quote);
            //    }
            //}
        }

        private void FixLines()
        {
            var lines = repo.GetAll<QuoteLine>().ToList();

            string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            foreach (var line in lines)
            {
                var letter = line.Number.ToCharArray().Last();
                line.Position = alphabet.IndexOf(letter);

                repo.Update(line);
            }
        }

        private void SeedAccessory()
        {
            var sheet = OpenSheet("Accessory");

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                var partNumber = sheet.Cells[i, 1].Value.ToString();
                var acc = repo.Get<Accessory>(x => x.PartNumber == partNumber);
                if (acc == null)
                    acc = new Accessory()
                    {
                        PartNumber = partNumber,
                        Description = sheet.Cells[i, 2].Value.ToString(),
                        SignalStandard = (AccessorySignalStandard)Enum.Parse(typeof(AccessorySignalStandard), sheet.Cells[i, 3].Value.ToString(), true)
                    };

                repo.Create(acc);
            }

            sheet = sheet.Workbook.Worksheets[1];
            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                var partNumber = sheet.Cells[i, 1].Value.ToString();
                var acc = repo.Get<Accessory>(x => x.PartNumber == partNumber);
                if (acc != null)
                {
                    var category = repo.Get<AccCategory>(x => x.Name == sheet.Cells[i, 2].Value.ToString());
                    acc.Categories.Add(category);
                    category.Accessories.Add(acc);

                    repo.Update(acc);
                }
            }

            sheet = sheet.Workbook.Worksheets[2];
            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                var partNumber = sheet.Cells[i, 1].Value.ToString();
                var acc = repo.Get<Accessory>(x => x.PartNumber == partNumber);
                if (acc != null)
                {
                    var function = repo.Get<AccFunction>(x => x.Name == sheet.Cells[i, 2].Value.ToString());
                    acc.Functions.Add(function);
                    function.Accessories.Add(acc);

                    repo.Update(acc);
                }
            }
        }

        private class AccPrice
        {
            public string PartNumber { get; set; }
            public int Price { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
        }

        private void SeedAccPricing()
        {
            List<AccPrice> accPrices = new List<AccPrice>();

            var sheet = OpenSheet("AccPricing");

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                if (sheet.Cells[i, 1].Value == null)
                    break;

                accPrices.Add(new AccPrice()
                {
                    PartNumber = sheet.Cells[i, 1].Value.ToString(),
                    Price = Convert.ToInt32(sheet.Cells[i, 2].Value),
                    StartDate = DateTime.Parse(sheet.Cells[i, 3].Value.ToString()),
                    EndDate = DateTime.Parse(sheet.Cells[i, 4].Value.ToString())
                });
            }

            var groups = accPrices.GroupBy(x => x.PartNumber);

            foreach (var group in groups)
            {
                var validPrices = group.Where(x => x.EndDate > DateTime.Now).OrderByDescending(x => x.StartDate);
                if (validPrices.Any())
                {
                    var acc = repo.Get<Accessory>(x => x.PartNumber == group.First().PartNumber);
                    if (acc != null)
                    {
                        acc.Price = validPrices.First().Price;
                        repo.Update(acc);
                    }
                }
            }
        }

        private void SeedAntOption()
        {
            var sheet = OpenSheet("AntOption");

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                var option = new AntOption();
                option.Name = sheet.Cells[i, 1].Value.ToString();
                option.PartNumberDescriptor = sheet.Cells[i, 2].Value.ToString();
                option.LineItemText = sheet.Cells[i, 1].Value.ToString();

                repo.Create(option);
            }
        }

        private void SeedAntPricing()
        {
            var sheet = OpenSheet("AntPricing");
            var priceVersion = repo.GetAll<PriceVersion>().OrderBy(x => x.EffectiveDate).First();

            for (int i = 1; i <= sheet.Dimension.Rows; i++)
            {
                if (sheet.Cells[i, 1].Value == null)
                    break;

                var ped = repo.Get<AntPedestal>(x => x.PartNumberDescriptor == sheet.Cells[i, 1].Value.ToString());
                if (ped != null)
                {
                    ped.Prices.Add(new Price()
                    {
                        PriceVersion = priceVersion,
                        Value = Convert.ToInt32(sheet.Cells[i, 2].Value),
                        Component = ped
                    });

                    repo.Update(ped);
                }
            }

            sheet = sheet.Workbook.Worksheets[1];
            for (int i = 1; i <= sheet.Dimension.Rows; i++)
            {
                if (sheet.Cells[i, 1].Value == null)
                    break;

                var reflector = repo.Get<AntReflectorAssembly>(x => x.PartNumberDescriptor == sheet.Cells[i, 1].Value.ToString());
                if (reflector != null)
                {
                    reflector.Prices.Add(new Price()
                    {
                        PriceVersion = priceVersion,
                        Value = Convert.ToInt32(sheet.Cells[i, 2].Value),
                        Component = reflector
                    });

                    repo.Update(reflector);
                }
            }

            sheet = sheet.Workbook.Worksheets[2];
            for (int i = 1; i <= sheet.Dimension.Rows; i++)
            {
                if (sheet.Cells[i, 1].Value == null)
                    break;

                var wiring = repo.Get<AntWiring>(x => x.PartNumberDescriptor == sheet.Cells[i, 1].Value.ToString());
                if (wiring != null)
                {
                    wiring.Prices.Add(new Price()
                    {
                        PriceVersion = priceVersion,
                        Value = Convert.ToInt32(sheet.Cells[i, 2].Value),
                        Component = wiring
                    });

                    repo.Update(wiring);
                }
            }

            sheet = sheet.Workbook.Worksheets[3];
            for (int i = 1; i <= sheet.Dimension.Rows; i++)
            {
                if (sheet.Cells[i, 1].Value == null)
                    break;

                var feed = repo.Get<AntFeed>(x => x.PartNumberDescriptor == sheet.Cells[i, 1].Value.ToString());
                if (feed != null)
                {
                    feed.Prices.Add(new Price()
                    {
                        PriceVersion = priceVersion,
                        Value = Convert.ToInt32(sheet.Cells[i, 2].Value),
                        Component = feed
                    });

                    repo.Update(feed);
                }
            }

            sheet = sheet.Workbook.Worksheets[4];
            for (int i = 1; i <= sheet.Dimension.Rows; i++)
            {
                if (sheet.Cells[i, 1].Value == null)
                    break;

                var bandCombo = repo.Get<AntBandCombination>(x => x.PartNumberDescriptor == sheet.Cells[i, 1].Value.ToString());
                if (bandCombo != null)
                {
                    bandCombo.Prices.Add(new Price()
                    {
                        PriceVersion = priceVersion,
                        Value = Convert.ToInt32(sheet.Cells[i, 2].Value),
                        Component = bandCombo
                    });

                    repo.Update(bandCombo);
                }
            }

            sheet = sheet.Workbook.Worksheets[5];
            for (int i = 1; i <= sheet.Dimension.Rows; i++)
            {
                if (sheet.Cells[i, 1].Value == null)
                    break;

                var opt = repo.Get<AntOption>(x => x.PartNumberDescriptor == sheet.Cells[i, 1].Value.ToString());
                if (opt != null)
                {
                    opt.Prices.Add(new Price()
                    {
                        PriceVersion = priceVersion,
                        Value = Convert.ToInt32(sheet.Cells[i, 2].Value),
                        Component = opt
                    });

                    repo.Update(opt);
                }
            }
        }

        private void SeedRxBasebandSet()
        {
            var sheet = OpenSheet("RxBasebandSet");

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                var basebandSet = new RxBasebandSet();
                basebandSet.Description = sheet.Cells[i, 1].Value.ToString();
                basebandSet.TtlConnectorSet = repo.Get<RxConnectorSet>(x => x.Description == sheet.Cells[i, 2].Value.ToString());
                basebandSet.RsConnectorSet = repo.Get<RxConnectorSet>(x => x.Description == sheet.Cells[i, 3].Value.ToString());

                repo.Create(basebandSet);
            }
        }

        private void SeedRxConfiguration()
        {
            var sheet = OpenSheet("RxConfiguration");

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                var rxConfig = new RxPackage();
                rxConfig.Name = sheet.Cells[i, 1].Value.ToString();
                rxConfig.Description = sheet.Cells[i, 3].Value.ToString();
                rxConfig.FrontPanelDisplay = repo.Get<RxFrontPanelDisplay>(x => x.Name == sheet.Cells[i, 4].Value.ToString());
                rxConfig.FrontPanelInterface = repo.Get<RxFrontPanelInterface>(x => x.Name == sheet.Cells[i, 5].Value.ToString());
                rxConfig.AgcOutputConnectorSet = repo.Get<RxConnectorSet>(x => x.Description == sheet.Cells[i, 6].Value.ToString());
                rxConfig.AnalogOutputConnectorSet = repo.Get<RxConnectorSet>(x => x.Description == sheet.Cells[i, 7].Value.ToString());
                rxConfig.EthernetConnectorSet = repo.Get<RxConnectorSet>(x => x.Description == sheet.Cells[i, 8].Value.ToString());
                rxConfig.IfInputConnectorSet = repo.Get<RxConnectorSet>(x => x.Description == sheet.Cells[i, 9].Value.ToString());
                rxConfig.IfOutputConnectorSet = repo.Get<RxConnectorSet>(x => x.Description == sheet.Cells[i, 10].Value.ToString());
                rxConfig.RfInputConnectorSet = repo.Get<RxConnectorSet>(x => x.Description == sheet.Cells[i, 11].Value.ToString());
                rxConfig.BasebandSet = repo.Get<RxBasebandSet>(x => x.Description == sheet.Cells[i, 12].Value.ToString());
                rxConfig.AmOutputConnectorSet = repo.Get<RxConnectorSet>(x => x.Description == sheet.Cells[i, 13].Value.ToString());
                rxConfig.ChannelSet = repo.Get<RxChannelSet>(x => x.TotalChannelCount == Convert.ToInt32(sheet.Cells[i, 14].Value));

                repo.Create(rxConfig);
            }
        }

        private void SeedRxPricing()
        {
            var sheet = OpenSheet("RxPricing");
            var priceVersion = repo.GetAll<PriceVersion>().OrderBy(x => x.EffectiveDate).First();

            for (int i = 1; i <= sheet.Dimension.Rows; i++)
            {
                if (sheet.Cells[i, 1].Value == null)
                    break;

                var opt = repo.Get<RxOption>(x => x.PartNumberDescriptor == sheet.Cells[i, 1].Value.ToString());
                if (opt != null)
                {
                    opt.Prices.Add(new Price()
                    {
                        PriceVersion = priceVersion,
                        Value = Convert.ToInt32(sheet.Cells[i, 2].Value),
                        Component = opt
                    });

                    repo.Update(opt);
                }
            }

            sheet = sheet.Workbook.Worksheets[1];
            for (int i = 1; i <= sheet.Dimension.Rows; i++)
            {
                if (sheet.Cells[i, 1].Value == null)
                    break;

                var packages = repo.GetGroup<RxPackage>(x => x.PartNumberDescriptor.EndsWith(sheet.Cells[i, 1].Value.ToString()));
                if (packages.Any())
                {
                    foreach (var package in packages)
                    {
                        package.Prices.Add(new Price()
                        {
                            PriceVersion = priceVersion,
                            Value = Convert.ToInt32(sheet.Cells[i, 2].Value),
                            Component = package
                        });

                        repo.Update(package);
                    }
                }
            }

            sheet = sheet.Workbook.Worksheets[2];
            for (int i = 1; i <= sheet.Dimension.Rows; i++)
            {
                if (sheet.Cells[i, 1].Value == null)
                    break;

                var bc = repo.Get<RxBandCombination>(x => x.PartNumberDescriptor == sheet.Cells[i, 1].Value.ToString());
                if (bc != null)
                {
                    bc.Prices.Add(new Price()
                    {
                        PriceVersion = priceVersion,
                        Value = Convert.ToInt32(sheet.Cells[i, 2].Value),
                        Component = bc
                    });

                    repo.Update(bc);
                }
            }
        }

        private void SeedTxPackage()
        {
            var sheet = OpenSheet("TxPackage");

            List<int> packageStartPos = new List<int>();

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                if (sheet.Cells[i, 1].Value != null)
                    packageStartPos.Add(i);
            }

            foreach (var startPos in packageStartPos)
            {
                var package = new TxPackage();
                package.Active = true;
                package.Name = sheet.Cells[startPos, 2].Value.ToString();

                var rando = new Random();

                var footprints = repo.GetAll<TxFootprint>().ToList();
                package.Footprint = footprints[rando.Next(0, footprints.Count - 1)];

                var heights = repo.GetAll<TxHeight>().ToList();
                package.Height = heights[rando.Next(0, heights.Count - 1)];

                int stackRow = startPos + 1;
                while (sheet.Cells[stackRow, 2].Value != null)
                {
                    var stack = new TxStack();
                    stack.Name = sheet.Cells[stackRow, 2].Value.ToString();

                    var bandCombo = repo.Get<TxBandCombination>(x => x.PartNumberDescriptor == sheet.Cells[stackRow, 7].Value.ToString());
                    stack.BandCombination = bandCombo;

                    string powerString = stack.Name.Substring(10, 2);
                    var power = repo.Get<TxRadioFrequencyPower>(x => x.PartNumberDescriptor == powerString);
                    stack.RadioFrequencyPower = power;

                    string boardString = sheet.Cells[stackRow, 3].Value.ToString();
                    if (boardString != "N/A" && boardString != "NO BOM")
                    {
                        var digBoard = repo.Get<TxBoard>(x => x.ManufacturingName == boardString);
                        if (digBoard == null)
                        {
                            digBoard = new TxBoard()
                            {
                                ManufacturingName = boardString,
                                Type = BoardType.Digital
                            };

                            repo.Create(digBoard);
                        }
                        stack.Boards.Add(digBoard);
                        digBoard.Stacks.Add(stack);
                    }

                    boardString = sheet.Cells[stackRow, 4].Value.ToString();
                    if (boardString != "N/A" && boardString != "NO BOM")
                    {
                        var powerBoard = repo.Get<TxBoard>(x => x.ManufacturingName == boardString);
                        if (powerBoard == null)
                        {
                            powerBoard = new TxBoard()
                            {
                                ManufacturingName = boardString,
                                Type = BoardType.PowerAmplifier
                            };

                            repo.Create(powerBoard);
                        }
                        stack.Boards.Add(powerBoard);
                        powerBoard.Stacks.Add(stack);
                    }

                    boardString = sheet.Cells[stackRow, 5].Value.ToString();
                    if (boardString != "N/A" && boardString != "NO BOM")
                    {
                        var topBoard = repo.Get<TxBoard>(x => x.ManufacturingName == boardString);
                        if (topBoard == null)
                        {
                            topBoard = new TxBoard()
                            {
                                ManufacturingName = boardString,
                                Type = BoardType.TopHat
                            };

                            repo.Create(topBoard);
                        }
                        stack.Boards.Add(topBoard);
                        topBoard.Stacks.Add(stack);
                    }

                    var basebandColl = sheet.Cells[stackRow, 17].Value.ToString().Split(',');
                    foreach (var basebandString in basebandColl)
                    {
                        var baseband = repo.Get<TxBaseband>(x => x.PartNumberDescriptor == basebandString.Trim());
                        stack.Basebands.Add(baseband);
                        baseband.Stacks.Add(stack);
                    }

                    var serialColl = sheet.Cells[stackRow, 18].Value.ToString().Split(',');
                    foreach (var serialString in serialColl)
                    {
                        var serial = repo.Get<TxSerialControlInterface>(x => x.PartNumberDescriptor == serialString.Trim());
                        stack.SerialControlInterfaces.Add(serial);
                        serial.Stacks.Add(stack);
                    }

                    repo.Create(stack);

                    package.Stacks.Add(stack);
                    //stack.Package.Add(package);

                    stackRow++;
                }

                repo.Create(package);
            }
        }

        private void SeedTxOption()
        {
            var sheet = OpenSheet("TxOption");
            //var priceVersion = repo.GetAll<PriceVersion>().OrderBy(x => x.EffectiveDate).First();

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                if (sheet.Cells[i, 1].Value == null)
                    break;
                string nameString = sheet.Cells[i, 1].Value.ToString().Replace("-", "");
                string descString = sheet.Cells[i, 2].Value.ToString();
                string typeString = sheet.Cells[i, 3].Value.ToString();
                bool active = sheet.Cells[i, 4].Value?.ToString() == "1";
                TransmitterOptionType optionType = TransmitterOptionType.Standard;

                switch (typeString)
                {
                    case "Package":
                        optionType = TransmitterOptionType.Hardware;
                        break;
                    case "Pinout":
                        optionType = TransmitterOptionType.Pinout;
                        break;
                    case "Other":
                        optionType = TransmitterOptionType.Standard;
                        break;
                    case "Accessory":
                        optionType = TransmitterOptionType.Accessory;
                        break;
                }

                var option = repo.Get<TxOption>(x => x.Name == nameString);
                if (option == null)
                {
                    option = new TxOption()
                    {
                        Name = nameString,
                        PartNumberDescriptor = nameString,
                        LineItemText = descString,
                        OptionType = optionType,
                        Active = active
                    };

                    repo.Create(option);
                }
                else
                {
                    option.Name = nameString;
                    option.PartNumberDescriptor = nameString;
                    option.LineItemText = descString;
                    option.Active = active;
                    option.OptionType = optionType;

                    repo.Update(option);
                }
            }

            //sheet = sheet.Workbook.Worksheets[1];
            //for (int i = 1; i <= sheet.Dimension.Rows; i++)
            //{
            //    if (sheet.Cells[i, 1].Value == null)
            //        break;

            //    var opt = repo.Get<TxOption>(x => x.PartNumberDescriptor == sheet.Cells[i, 1].Value.ToString());
            //    if (opt != null)
            //    {
            //        opt.Prices.Add(new Price()
            //        {
            //            PriceVersion = priceVersion,
            //            Value = Convert.ToInt32(sheet.Cells[i, 2].Value),
            //            Component = opt
            //        });

            //        repo.Update(opt);
            //    }
            //}

            //sheet = sheet.Workbook.Worksheets[2];
            //for (int i = 1; i <= sheet.Dimension.Rows; i++)
            //{
            //    if (sheet.Cells[i, 1].Value == null)
            //        break;

            //    var opt = repo.Get<TxOption>(x => x.PartNumberDescriptor == sheet.Cells[i, 1].Value.ToString());
            //    var conflict = repo.Get<TxOption>(x => x.PartNumberDescriptor == sheet.Cells[i, 2].Value.ToString());
            //    if (opt != null && conflict != null)
            //    {
            //        //opt.OptionBindings.Add(new OptionBinding()
            //        //{
            //        //    BindingType = OptionBindingType.Conflict,
            //        //    BoundOption = conflict,
            //        //    Option = opt
            //        //});

            //        repo.Update(opt);
            //    }
            //}
        }

        private void SeedTxStack()
        {
            var sheet = OpenSheet("TxStacks");

            List<string> missing = new List<string>();
            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                var packName = sheet.Cells[i, 1].Value.ToString();
                var package = repo.Get<TxPackage>(x => x.Name == packName);

                if (!missing.Contains(packName))
                    missing.Add(packName);
            }

            foreach (var miss in missing)
            {
                System.Diagnostics.Debug.WriteLine(miss);
            }
        }

        public class Conn
        {
            public string Name { get; set; }
            public string Pos { get; set; }
        }

        public class ConnSet
        {
            public Conn Conn1 { get; set; }
            public Conn Conn2 { get; set; }

            public override bool Equals(object obj)
            {
                if (!(obj is ConnSet))
                    return false;

                var otherSet = (ConnSet)obj;

                return Conn1.Name == otherSet.Conn1.Name
                    && Conn1.Pos == otherSet.Conn1.Pos
                    && Conn2.Name == otherSet.Conn2.Name
                    && Conn2.Pos == otherSet.Conn2.Pos;
            }
        }

        private void SeedPackageNew()
        {
            var file = new FileInfo(@"C:\Users\nmoore\Downloads\package_export.xlsx");
            var xlPkg = new ExcelPackage(file);
            var sheet = xlPkg.Workbook.Worksheets[0];

            List<ConnSet> sets = new List<ConnSet>();

            for (int i = 2; i < sheet.Dimension.Rows; i++)
            {
                ConnSet set = new ConnSet();

                var package = new TxPackage();
                package.Name = sheet.Cells[i, 1].Value?.ToString();
                package.Active = sheet.Cells[i, 2].Value?.ToString() == "1";

                string footprintString = sheet.Cells[i, 3].Value.ToString();
                var footprint = repo.Get<TxFootprint>(x => x.Name == footprintString);
                package.Footprint = footprint;

                string heightString = sheet.Cells[i, 4].Value.ToString();
                var height = repo.Get<TxHeight>(x => x.Name == heightString);
                package.Height = height;

                string rfString = sheet.Cells[i, 8].Value.ToString().Replace(" (BOTTOM)", "").Replace(" (TOP)", "");
                var rf = repo.Get<TxRadioConnector>(x => x.Name == rfString);

                var posString = sheet.Cells[i, 9].Value.ToString();
                ConnectorPosition pos;
                Enum.TryParse(posString, out pos);

                string coaxString = sheet.Cells[i, 10].Value.ToString().Replace(" (BOTTOM)", "").Replace(" (TOP)", "");
                var coax = repo.Get<TxCoaxialModulationInput>(x => x.Name == coaxString);
                package.CoaxialModulationInput = coax;

                //repo.Create(package);
                sets.Add(set);
            }

            var distSets = sets.Distinct().ToList();
        }

        private void SeedTx()
        {
            var file = new FileInfo(@"C:\Users\nmoore\Documents\TxSeed.xlsx");
            var xlPkg = new ExcelPackage(file);
            var sheet = xlPkg.Workbook.Worksheets[0];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                try
                {
                    var conn = new TxDataConnector()
                    {
                        Name = sheet.Cells[i, 1].Value.ToString(),
                        PinCount = Convert.ToInt32(sheet.Cells[i, 2].Value)
                    };
                    repo.Create(conn);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            sheet = xlPkg.Workbook.Worksheets[1];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                try
                {
                    var set = new TxConnectorSet()
                    {
                        Name = sheet.Cells[i, 1].Value.ToString()
                    };

                    var conn1 = repo.Get<TxDataConnector>(x => x.Name == sheet.Cells[i, 2].Value.ToString());
                    if (conn1 == null)
                    {

                    }

                    set.ConnectorSetDataConnectors.Add(new TxConnectorSetDataConnector()
                    {
                        ConnectorSet = set,
                        DataConnector = conn1,
                        Rank = ConnectorRank.Primary
                    });

                    if (sheet.Cells[i, 3].Value != null)
                    {
                        var conn2 = repo.Get<TxDataConnector>(x => x.Name == sheet.Cells[i, 3].Value.ToString());
                        if (conn2 == null)
                        {

                        }
                        set.ConnectorSetDataConnectors.Add(new TxConnectorSetDataConnector()
                        {
                            ConnectorSet = set,
                            DataConnector = conn2,
                            Rank = ConnectorRank.Other
                        });
                    }

                    repo.Create(set);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            sheet = xlPkg.Workbook.Worksheets[2];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                try
                {
                    var connSet = repo.Get<TxConnectorSet>(x => x.Name == sheet.Cells[i, 4].Value.ToString());

                    var pinout = new TxPinout()
                    {
                        Name = sheet.Cells[i, 1].Value.ToString(),
                        ConnectorSet = connSet
                    };

                    repo.Create(pinout);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            sheet = xlPkg.Workbook.Worksheets[3];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                try
                {
                    var pinout = repo.Get<TxPinout>(x => x.Name == sheet.Cells[i, 1].Value.ToString());
                    var pin = repo.Get<TxPin>(x => x.Name == sheet.Cells[i, 3].Value.ToString());

                    var rankString = sheet.Cells[i, 4].Value.ToString();
                    var rank = rankString == "Primary" ? ConnectorRank.Primary : ConnectorRank.Other;

                    var connector = pinout.ConnectorSet.ConnectorSetDataConnectors.First(x => x.Rank == rank).DataConnector;

                    var pinUsage = new TxDataConnectorPinUsage()
                    {
                        DataConnector = connector,
                        Pin = pin,
                        Pinout = pinout,
                        PinNumber = Convert.ToInt32(sheet.Cells[i, 2].Value)
                    };

                    repo.Create(pinUsage);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            sheet = xlPkg.Workbook.Worksheets[4];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                try
                {
                    var package = new TxPackage()
                    {
                        Name = sheet.Cells[i, 1].Value.ToString(),
                        Active = sheet.Cells[i, 2].Value?.ToString() == "1",
                        Footprint = repo.Get<TxFootprint>(x => x.Name == sheet.Cells[i, 3].Value.ToString()),
                        Height = repo.Get<TxHeight>(x => x.Name == sheet.Cells[i, 4].Value.ToString()),
                        RadioConnector = repo.Get<TxRadioConnector>(x => x.Name == sheet.Cells[i, 8].Value.ToString()),
                        CoaxialModulationInput = repo.Get<TxCoaxialModulationInput>(x => x.Name == sheet.Cells[i, 10].Value.ToString()),
                        ConnectorSet = repo.Get<TxConnectorSet>(x => x.Name == sheet.Cells[i, 11].Value.ToString())
                    };

                    var posString = sheet.Cells[i, 6].Value.ToString();
                    ConnectorPosition pos;
                    Enum.TryParse(posString, out pos);
                    package.PrimaryConnectorPosition = pos;

                    posString = sheet.Cells[i, 9].Value.ToString();
                    Enum.TryParse(posString, out pos);
                    package.RadioConnectorPosition = pos;

                    repo.Create(package);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

            sheet = xlPkg.Workbook.Worksheets[5];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                try
                {
                    var package = repo.Get<TxPackage>(x => x.Name == sheet.Cells[i, 1].Value.ToString());
                    bool active = sheet.Cells[i, 3].Value?.ToString() != "INACTIVE";
                    var power = repo.Get<TxRadioFrequencyPower>(x => x.WattagePerOutput == Convert.ToDouble(sheet.Cells[i, 4].Value.ToString().Trim())
                        && x.OutputCount == (RadioFrequencyOutputCount)Convert.ToInt32(sheet.Cells[i, 5].Value.ToString().Trim()));
                    var bandCombo = repo.Get<TxBandCombination>(x => x.Name == sheet.Cells[i, 6].Value.ToString());

                    var stack = new TxStack()
                    {
                        Package = package,
                        Active = active,
                        RadioFrequencyPower = power,
                        BandCombination = bandCombo,
                        Name = sheet.Cells[i, 9].Value.ToString(),
                        ManufacturingName = sheet.Cells[i, 10].Value?.ToString(),
                    };

                    var basebands = sheet.Cells[i, 7].Value.ToString().Replace(" ", "").Replace("*", "").Split(',');
                    foreach (var basebandName in basebands)
                    {
                        var baseband = repo.Get<TxBaseband>(x => x.PartNumberDescriptor == basebandName);
                        baseband.Stacks.Add(stack);
                        stack.Basebands.Add(baseband);
                    }

                    var serials = sheet.Cells[i, 8].Value.ToString().Replace(" ", "").Replace("*", "").Split(',');
                    foreach (var serialName in serials)
                    {
                        var serial = repo.Get<TxSerialControlInterface>(x => x.PartNumberDescriptor == serialName);
                        serial.Stacks.Add(stack);
                        stack.SerialControlInterfaces.Add(serial);
                    }

                    for (int y = 11; y <= sheet.Dimension.Columns; y++)
                    {
                        string boardName = sheet.Cells[i, y].Value?.ToString();

                        if (boardName == null)
                            break;

                        var board = repo.Get<TxBoard>(x => x.ManufacturingName == boardName);
                        if (board == null)
                        {
                            board = new TxBoard()
                            {
                                ManufacturingName = boardName
                            };

                            repo.Create(board);
                        }

                        board.Stacks.Add(stack);
                        stack.Boards.Add(board);
                    }

                    repo.Create(stack);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
        }

        private void SeedOptionCombo()
        {
            var file = new FileInfo(@"C:\Users\nmoore\Documents\option_combo.xlsx");
            var xlPkg = new ExcelPackage(file);
            var sheet = xlPkg.Workbook.Worksheets[0];

            var combos = new List<TxOptionCombination>();
            for (int i = 1; i <= sheet.Dimension.Rows; i++)
            {
                string name = sheet.Cells[i, 2].Value.ToString();
                var option = repo.Get<TxOption>(x => x.Name == sheet.Cells[i, 3].Value.ToString().Remove(0, 1));

                var optionCombo = combos.FirstOrDefault(x => x.Name == name);
                if (optionCombo == null)
                {
                    optionCombo = new TxOptionCombination()
                    {
                        Name = name
                    };

                    combos.Add(optionCombo);
                }

                optionCombo.Options.Add(option);
                option.OptionCombinations.Add(optionCombo);
            }

            foreach (var combo in combos)
            {
                repo.Create(combo);
            }
        }

        private void SeedOptionComboPackage()
        {
            var file = new FileInfo(@"C:\Users\nmoore\Documents\combo_x_package.xlsx");
            var xlPkg = new ExcelPackage(file);
            var sheet = xlPkg.Workbook.Worksheets[0];

            var combos = new List<TxOptionCombination>();
            for (int i = 1; i <= sheet.Dimension.Rows; i++)
            {
                string active = sheet.Cells[i, 3].Value?.ToString();
                if (active != "1")
                    continue;

                string packName = sheet.Cells[i, 1].Value.ToString();
                string comboName = sheet.Cells[i, 2].Value.ToString();

                var package = repo.Get<TxPackage>(x => x.Name == packName);
                if (package == null)
                    continue;

                var combo = repo.Get<TxOptionCombination>(x => x.Name == comboName);

                package.OptionCombinations.Add(combo);
                combo.Packages.Add(package);

                repo.Update(combo);
            }
        }

        private void SeedConflict()
        {
            var file = new FileInfo(@"C:\Users\nmoore\Documents\option_bc_conflict.xlsx");
            var xlPkg = new ExcelPackage(file);
            var sheet = xlPkg.Workbook.Worksheets[0];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                string bcName = sheet.Cells[i, 1].Value?.ToString();
                string optionName = sheet.Cells[i, 2].Value?.ToString().Replace("-", "");

                var bandCombo = repo.Get<TxBandCombination>(x => x.Name == bcName);
                var option = repo.Get<TxOption>(x => x.Name == optionName);

                if (bandCombo == null)
                    continue;

                if (option == null)
                    continue;

                var conflict = new TxOptionBinding()
                {
                    Option = option,
                    Component = bandCombo
                };

                repo.Create(conflict);
            }
        }

        private void SeedPrice()
        {
            var file = new FileInfo(@"C:\Users\nmoore\Documents\tx_pricing.xlsx");
            var xlPkg = new ExcelPackage(file);
            var sheet = xlPkg.Workbook.Worksheets[0];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                string name = sheet.Cells[i, 1].Value.ToString();
                int val = Convert.ToInt32(sheet.Cells[i, 2].Value);
                long dateNum = long.Parse(sheet.Cells[i, 3].Value.ToString());
                DateTime pvDate = DateTime.FromOADate(dateNum);

                var comp = repo.Get<TxPackage>(x => x.Name == name);
                if (comp == null)
                    continue;

                var priceVersion = repo.Get<PriceVersion>(x => x.EffectiveDate.Date == pvDate.Date);
                if (priceVersion == null)
                    throw new Exception("what the fuck");

                var price = new Price()
                {
                    Component = comp,
                    PriceVersion = priceVersion,
                    Value = val
                };

                repo.Create(price);
            }

            sheet = xlPkg.Workbook.Worksheets[1];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                string name = sheet.Cells[i, 1].Value.ToString();
                int val = Convert.ToInt32(sheet.Cells[i, 2].Value);
                long dateNum = long.Parse(sheet.Cells[i, 3].Value.ToString());
                DateTime pvDate = DateTime.FromOADate(dateNum);

                var comp = repo.Get<TxBand>(x => x.Name == name);
                if (comp == null)
                    continue;

                var priceVersion = repo.Get<PriceVersion>(x => x.EffectiveDate.Date == pvDate.Date);
                if (priceVersion == null)
                    throw new Exception("what the fuck");

                var price = new Price()
                {
                    Component = comp,
                    PriceVersion = priceVersion,
                    Value = val
                };

                repo.Create(price);
            }

            sheet = xlPkg.Workbook.Worksheets[2];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                string name = sheet.Cells[i, 1].Value.ToString();
                int val = Convert.ToInt32(sheet.Cells[i, 2].Value);
                long dateNum = long.Parse(sheet.Cells[i, 3].Value.ToString());
                DateTime pvDate = DateTime.FromOADate(dateNum);

                var comp = repo.Get<TxBaseband>(x => x.PartNumberDescriptor == name);
                if (comp == null)
                    continue;

                var priceVersion = repo.Get<PriceVersion>(x => x.EffectiveDate.Date == pvDate.Date);
                if (priceVersion == null)
                    throw new Exception("what the fuck");

                var price = new Price()
                {
                    Component = comp,
                    PriceVersion = priceVersion,
                    Value = val
                };

                repo.Create(price);
            }

            sheet = xlPkg.Workbook.Worksheets[3];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                string name = sheet.Cells[i, 1].Value.ToString();
                int val = Convert.ToInt32(sheet.Cells[i, 2].Value);
                long dateNum = long.Parse(sheet.Cells[i, 3].Value.ToString());
                DateTime pvDate = DateTime.FromOADate(dateNum);

                var comp = repo.Get<TxSerialControlInterface>(x => x.PartNumberDescriptor == name);
                if (comp == null)
                    continue;

                var priceVersion = repo.Get<PriceVersion>(x => x.EffectiveDate.Date == pvDate.Date);
                if (priceVersion == null)
                    throw new Exception("what the fuck");

                var price = new Price()
                {
                    Component = comp,
                    PriceVersion = priceVersion,
                    Value = val
                };

                repo.Create(price);
            }

            sheet = xlPkg.Workbook.Worksheets[4];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                string name = sheet.Cells[i, 1].Value.ToString().Replace("-", "");
                int val = Convert.ToInt32(sheet.Cells[i, 2].Value);
                long dateNum = long.Parse(sheet.Cells[i, 3].Value.ToString());
                DateTime pvDate = DateTime.FromOADate(dateNum);

                var comp = repo.Get<TxOption>(x => x.Name == name);
                if (comp == null)
                    continue;

                var priceVersion = repo.Get<PriceVersion>(x => x.EffectiveDate.Date == pvDate.Date);
                if (priceVersion == null)
                    throw new Exception("what the fuck");

                var price = new Price()
                {
                    Component = comp,
                    PriceVersion = priceVersion,
                    Value = val
                };

                repo.Create(price);
            }

            sheet = xlPkg.Workbook.Worksheets[5];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                string name = sheet.Cells[i, 1].Value.ToString();
                int val = Convert.ToInt32(sheet.Cells[i, 2].Value);
                long dateNum = long.Parse(sheet.Cells[i, 3].Value.ToString());
                DateTime pvDate = DateTime.FromOADate(dateNum);

                var comp = repo.Get<TxPinout>(x => x.Name == name);
                if (comp == null)
                    continue;

                var priceVersion = repo.Get<PriceVersion>(x => x.EffectiveDate.Date == pvDate.Date);
                if (priceVersion == null)
                    throw new Exception("what the fuck");

                var price = new Price()
                {
                    Component = comp,
                    PriceVersion = priceVersion,
                    Value = val
                };

                repo.Create(price);
            }

            sheet = xlPkg.Workbook.Worksheets[6];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                string name = sheet.Cells[i, 1].Value.ToString();
                int val = Convert.ToInt32(sheet.Cells[i, 2].Value);
                long dateNum = long.Parse(sheet.Cells[i, 3].Value.ToString());
                DateTime pvDate = DateTime.FromOADate(dateNum);

                var comp = repo.Get<TxModulation>(x => x.Name == name);
                if (comp == null)
                    continue;

                var priceVersion = repo.Get<PriceVersion>(x => x.EffectiveDate.Date == pvDate.Date);
                if (priceVersion == null)
                    throw new Exception("what the fuck");

                var price = new Price()
                {
                    Component = comp,
                    PriceVersion = priceVersion,
                    Value = val
                };

                repo.Create(price);
            }
        }

        private void SeedPriceRule()
        {
            var file = new FileInfo(@"C:\Users\nmoore\Documents\price_rules.xlsx");
            var xlPkg = new ExcelPackage(file);
            var sheet = xlPkg.Workbook.Worksheets[0];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                string opt1Name = sheet.Cells[i, 1].Value?.ToString().Replace("-", "");
                string optionName = sheet.Cells[i, 2].Value?.ToString().Replace("-", "");
                int val = Convert.ToInt32(sheet.Cells[i, 3].Value);
                long dateNum = long.Parse(sheet.Cells[i, 4].Value.ToString());
                DateTime pvDate = DateTime.FromOADate(dateNum);

                var opt1 = repo.Get<TxOption>(x => x.Name == opt1Name);
                var option = repo.Get<TxOption>(x => x.Name == optionName);

                var priceVersion = repo.Get<PriceVersion>(x => x.EffectiveDate.Date == pvDate.Date);
                if (priceVersion == null)
                    throw new Exception("what the fuck");

                if (opt1 == null)
                    continue;

                if (option == null)
                    continue;

                var rule = new PriceRule()
                {
                    PriceVersion = priceVersion,
                    Description = $"Option {opt1Name} with option {optionName} adjustment",
                    Value = val
                };

                rule.Components.Add(opt1);
                rule.Components.Add(option);

                repo.Create(rule);
            }

            sheet = xlPkg.Workbook.Worksheets[1];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                string optionName = sheet.Cells[i, 1].Value?.ToString().Replace("-", "");
                string packName = sheet.Cells[i, 2].Value?.ToString();
                int val = Convert.ToInt32(sheet.Cells[i, 3].Value);
                long dateNum = long.Parse(sheet.Cells[i, 4].Value.ToString());
                DateTime pvDate = DateTime.FromOADate(dateNum);

                var option = repo.Get<TxOption>(x => x.Name == optionName);
                var pack = repo.Get<TxPackage>(x => x.Name == packName);

                var priceVersion = repo.Get<PriceVersion>(x => x.EffectiveDate.Date == pvDate.Date);
                if (priceVersion == null)
                    throw new Exception("what the fuck");

                if (option == null)
                    throw new Exception("what the fuck");

                if (pack == null)
                    throw new Exception("what the fuck");

                var rule = new PriceRule()
                {
                    PriceVersion = priceVersion,
                    Description = $"Option {optionName} with package {packName} adjustment",
                    Value = val
                };

                rule.Components.Add(option);
                rule.Components.Add(pack);

                repo.Create(rule);
            }

            sheet = xlPkg.Workbook.Worksheets[2];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                string bcnName = sheet.Cells[i, 1].Value?.ToString();
                string powName = sheet.Cells[i, 2].Value?.ToString();
                int val = Convert.ToInt32(sheet.Cells[i, 3].Value);
                long dateNum = long.Parse(sheet.Cells[i, 4].Value.ToString());
                DateTime pvDate = DateTime.FromOADate(dateNum);

                var bc = repo.Get<TxBandCombination>(x => x.Name == bcnName);
                var pow = repo.Get<TxRadioFrequencyPower>(x => x.Description == powName);

                var priceVersion = repo.Get<PriceVersion>(x => x.EffectiveDate.Date == pvDate.Date);
                if (priceVersion == null)
                    throw new Exception("what the fuck");

                if (bc == null)
                    throw new Exception("what the fuck");

                if (pow == null)
                    throw new Exception("what the fuck");

                var rule = new PriceRule()
                {
                    PriceVersion = priceVersion,
                    Description = $"Band combination {bcnName} with RF power {pow.Name} adjustment",
                    Value = val
                };

                rule.Components.Add(bc);
                rule.Components.Add(pow);

                repo.Create(rule);
            }

            sheet = xlPkg.Workbook.Worksheets[3];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                string packName = sheet.Cells[i, 1].Value?.ToString();
                string powName = sheet.Cells[i, 2].Value?.ToString();
                string bcnName = sheet.Cells[i, 3].Value?.ToString();
                int val = Convert.ToInt32(sheet.Cells[i, 4].Value);
                long dateNum = long.Parse(sheet.Cells[i, 5].Value.ToString());
                DateTime pvDate = DateTime.FromOADate(dateNum);

                var bc = repo.Get<TxBandCombination>(x => x.Name == bcnName);
                var pow = repo.Get<TxRadioFrequencyPower>(x => x.Description == powName);
                var pack = repo.Get<TxPackage>(x => x.Name == packName);

                var priceVersion = repo.Get<PriceVersion>(x => x.EffectiveDate.Date == pvDate.Date);
                if (priceVersion == null)
                    throw new Exception("what the fuck");

                if (bc == null)
                    throw new Exception("what the fuck");

                if (pow == null)
                    throw new Exception("what the fuck");

                if (pack == null)
                    throw new Exception("what the fuck");

                var rule = new PriceRule()
                {
                    PriceVersion = priceVersion,
                    Description = $"Package {packName} with band combination {bcnName} and RF power {pow.Name} adjustment",
                    Value = val
                };

                rule.Components.Add(pack);
                rule.Components.Add(bc);
                rule.Components.Add(pow);

                repo.Create(rule);
            }

            sheet = xlPkg.Workbook.Worksheets[4];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                string bcnName = sheet.Cells[i, 1].Value?.ToString();
                int val = Convert.ToInt32(sheet.Cells[i, 2].Value);
                long dateNum = long.Parse(sheet.Cells[i, 3].Value.ToString());
                DateTime pvDate = DateTime.FromOADate(dateNum);

                var bc = repo.Get<TxBandCombination>(x => x.Name == bcnName);

                var priceVersion = repo.Get<PriceVersion>(x => x.EffectiveDate.Date == pvDate.Date);
                if (priceVersion == null)
                    throw new Exception("what the fuck");

                if (bc == null)
                    throw new Exception("what the fuck");

                var rule = new PriceRule()
                {
                    PriceVersion = priceVersion,
                    Description = $"L band or S band combined with C band",
                    Value = val
                };

                rule.Components.Add(bc);

                repo.Create(rule);
            }
        }

        private void SeedOptionPins()
        {
            //var file = new FileInfo(@"C:\Users\nmoore\Documents\option_pins.xlsx");
            //var xlPkg = new ExcelPackage(file);
            //var sheet = xlPkg.Workbook.Worksheets[0];

            //for (int i = 2; i <= sheet.Dimension.Rows; i++)
            //{
            //    var option = repo.Get<TxOption>(x => x.Name == sheet.Cells[i, 1].Value.ToString());
            //    string pinName = sheet.Cells[i, 2].Value.ToString();

            //    var pin = repo.Get<TxPin>(x => x.Name == pinName);

            //    if (pin == null || option == null)
            //        throw new Exception();

            //    option.Pins.Add(pin);
            //    pin.Options.Add(option);

            //    repo.Update(pin);
            //}
        }

        private void SeedCompPins()
        {
            var file = new FileInfo(@"C:\Users\nmoore\Documents\serial_pin.xlsx");
            var xlPkg = new ExcelPackage(file);
            var sheet = xlPkg.Workbook.Worksheets[0];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                var comp = repo.Get<TxComponent>(Guid.Parse(sheet.Cells[i, 1].Value.ToString()));
                string pinName = sheet.Cells[i, 2].Value.ToString();

                var pin = repo.Get<TxPin>(x => x.Name == pinName);

                if (pin == null || comp == null)
                    throw new Exception();

                comp.Pins.Add(pin);
                pin.Components.Add(comp);

                repo.Update(pin);
            }
        }

        private void SeedGen()
        {
            var file = new FileInfo(@"C:\Users\nmoore\Documents\t4.xlsx");
            var xlPkg = new ExcelPackage(file);
            var sheet = xlPkg.Workbook.Worksheets[0];

            for (int i = 2; i <= sheet.Dimension.Rows; i++)
            {
                var pack = repo.Get<TxPackage>(x => x.Name == sheet.Cells[i, 1].Value.ToString());

                if (pack == null)
                    continue;

                string genString = sheet.Cells[i, 2].Value.ToString();
                TransmitterGeneration gen = genString == "T3" ? TransmitterGeneration.T3 : TransmitterGeneration.T4;

                pack.Generation = gen;

                repo.Update(pack);
            }
        }

        private ExcelWorksheet OpenSheet(string sheetName)
        {
            var file = new FileInfo($@"{webHostEnvironment.WebRootPath}/seed/{sheetName}.xlsx");
            var package = new ExcelPackage(file);
            return package.Workbook.Worksheets[0];
        }
    }
}
