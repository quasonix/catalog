﻿using Microsoft.AspNetCore.Hosting;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using Newtonsoft.Json;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Services
{
    public interface IQuoteDocumentGenerator
    {
        Stream GenerateQuoteDocument(Quote quote);
    }

    public class QuoteDocumentGenerator : IQuoteDocumentGenerator
    {
        private Quote quote;
        private Document document;
        private Section section;

        private readonly IWebHostEnvironment hostEnvironment;
        private readonly IProductDescriptionBuilder descriptionBuilder;
        private readonly ICatalogRepository repo;

        public QuoteDocumentGenerator(IWebHostEnvironment hostEnvironment,
            IProductDescriptionBuilder descriptionBuilder,
            ICatalogRepository repo)
        {
            this.hostEnvironment = hostEnvironment;
            this.descriptionBuilder = descriptionBuilder;
            this.repo = repo;
        }

        public Stream GenerateQuoteDocument(Quote quote)
        {
            this.quote = quote;

            // create a MigraDoc document
            document = new Document();

            var style = document.Styles["Normal"];
            style.Font.Name = "Arial";
            style.Font.Size = 9;

            // Add a section to the document
            section = document.AddSection();
            section.PageSetup = document.DefaultPageSetup.Clone();
            section.PageSetup.PageWidth = Unit.Empty;
            section.PageSetup.PageHeight = Unit.Empty;
            section.PageSetup.PageFormat = PageFormat.Letter;
            section.PageSetup.LeftMargin = QuoteDocumentProperties.LeftMargin;
            section.PageSetup.RightMargin = QuoteDocumentProperties.RightMargin;
            section.PageSetup.TopMargin = 175;
            section.PageSetup.BottomMargin = QuoteDocumentProperties.BottomMargin;
            section.PageSetup.HeaderDistance = 27.5;

            AddHeader();
            AddFalseSummary();
            AddTerms();
            AddSummary();
            AddNotes();
            AddLineItems();

            document.UseCmykColor = true;
            const bool unicode = false;
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode);
            pdfRenderer.Document = document;

            pdfRenderer.RenderDocument();

            // save the document to stream
            MemoryStream stream = new MemoryStream();
            pdfRenderer.PdfDocument.Save(stream, false);

            return stream;
        }

        private void AddHeader()
        {
            var header = new HeaderFooter();

            // add a table to host two other tables
            var containerTable = new Table();

            // one col for each table
            containerTable.AddColumn(370);
            containerTable.AddColumn(158);

            var containerRow = containerTable.AddRow();

            // create tables
            var leftTable = new Table();

            leftTable.Format.Font.Size = 11;
            leftTable.Format.Font.Color = QuoteDocumentProperties.Colors.TextGrey;

            leftTable.AddColumn(25);
            leftTable.AddColumn(375);

            var row = leftTable.AddRow();
            var logo = row.Cells[0].AddImage(Path.Combine(hostEnvironment.WebRootPath, "img", "qsx_logo.png"));
            logo.Width = 175;

            row = leftTable.AddRow();
            var cell = row.Cells[0];
            cell.AddParagraph("6025 Schumacher Park Drive");
            cell.MergeRight = 1;
            cell.Format.SpaceBefore = 20;

            row = leftTable.AddRow();
            cell = row.Cells[0];
            cell.AddParagraph("West Chester, Ohio, 45069-4812");
            cell.MergeRight = 1;
            cell.Format.SpaceAfter = 15;

            row = leftTable.AddRow();
            cell = row.Cells[0];
            cell.AddParagraph("For");

            cell = row.Cells[1];
            cell.Format.Font.Color = Color.FromCmyk(0, 0, 0, 69);
            cell.AddParagraph(quote.CustomerName);
            cell.AddParagraph(quote.CustomerLocation);
            cell.AddParagraph(quote.PurchaserEmail);
            cell.AddParagraph(quote.AdditionalInformation);

            containerRow.Cells[0].Elements.Add(leftTable);

            var rightTable = new Table();

            rightTable.Format.Font.Size = 11;
            rightTable.Format.Font.Color = QuoteDocumentProperties.Colors.TextGrey;

            rightTable.AddColumn(79);
            rightTable.AddColumn(79);

            rightTable.Columns[1].Format.Font.Color = Color.FromCmyk(0, 0, 0, 69);
            rightTable.Columns[1].Format.Alignment = ParagraphAlignment.Right;

            row = rightTable.AddRow();
            cell = row.Cells[0];
            cell.Format.Font.Size = 25;
            cell.MergeRight = 1;
            cell.Format.Font.Bold = true;
            cell.Format.Alignment = ParagraphAlignment.Right;
            cell.Format.Font.Color = Color.FromCmyk(0, 0, 0, 69);
            cell.AddParagraph("Quotation");

            row = rightTable.AddRow();
            row.Cells[0].AddParagraph("Quote number");
            row.Cells[1].AddParagraph(quote.Number);
            row.Cells[1].Format.Font.Bold = true;

            row = rightTable.AddRow();
            row.Cells[0].AddParagraph("Page");

            var par = new Paragraph();
            par.AddPageField();
            par.AddSpace(1);
            par.AddText("of");
            par.AddSpace(1);
            par.AddNumPagesField();
            row.Cells[1].Add(par);

            row = rightTable.AddRow();
            row.Cells[0].AddParagraph("Sales rep");
            row.Cells[1].AddParagraph(quote.User.DisplayName);

            row = rightTable.AddRow();
            cell = row.Cells[0];
            cell.AddParagraph(quote.User.Email);
            cell.MergeRight = 1;
            cell.Format.Alignment = ParagraphAlignment.Right;
            cell.Format.Font.Color = Color.FromCmyk(0, 0, 0, 69);

            row = rightTable.AddRow();
            row.Cells[0].AddParagraph("Date issued");
            row.Cells[1].AddParagraph(DateTime.Now.ToShortDateString());

            row = rightTable.AddRow();
            row.Cells[0].AddParagraph("Valid through");
            row.Cells[1].AddParagraph(DateTime.Now.AddDays(120).ToShortDateString());

            row = rightTable.AddRow();
            row.Cells[0].AddParagraph("Terms");
            row.Cells[1].AddParagraph($"Net {quote.PaymentTerms}");

            row = rightTable.AddRow();
            row.Cells[0].AddParagraph("FOB");
            row.Cells[1].AddParagraph("Destination");

            containerRow.Cells[1].Elements.Add(rightTable);

            header.Add(containerTable);
            
            section.Headers.Primary = header;
        }

        private void AddFalseSummary()
        {
            var table = new Table();

            table.AddColumn(370);
            table.AddColumn(79);
            table.AddColumn(79);

            var row = table.AddRow();
            row.TopPadding = 3;
            row.BottomPadding = 3;
            row.Format.LeftIndent = 3;
            row.Format.Font.Size = 11;
            row.Format.Font.Bold = true;
            row.Format.Font.Color = QuoteDocumentProperties.Colors.Black;

            row.Cells[0].AddParagraph("Summary");
            row.Cells[1].AddParagraph("Total price:");
            row.Cells[2].AddParagraph(quote.Total.ToString("C0"));
            row.Cells[2].Format.Alignment = ParagraphAlignment.Right;

            row.Shading.Color = Color.FromCmyk(0, 0, 0, 30);

            row = table.AddRow();
            row.Cells[0].AddParagraph("See page 2 for line-item information");
            row.Cells[0].Format.SpaceBefore = 10;
            row.Cells[0].Format.SpaceAfter = 20;

            section.Add(table);
        }

        private void AddTerms()
        {
            var table = new Table();

            table.AddColumn(528);

            var row = table.AddRow();
            row.TopPadding = 3;
            row.BottomPadding = 3;
            row.Format.LeftIndent = 3;
            row.Format.Font.Size = 11;
            row.Format.Font.Bold = true;
            row.Format.Font.Color = QuoteDocumentProperties.Colors.Black;

            row.Cells[0].AddParagraph("Terms - please read carefully");

            row.Shading.Color = Color.FromCmyk(0, 0, 0, 30);

            row = table.AddRow();
            var cell = row.Cells[0];
            var par = cell.AddParagraph("PURCHASE ORDERS");
            par.Format.SpaceBefore = 10;
            par.Format.Font.Size = 11;
            par.Format.Font.Bold = true;

            par = new Paragraph();

            // Use formatted text to specify the color
            FormattedText colorText = new FormattedText();
            colorText.AddText("Please email all purchase orders to");
            colorText.Color = Colors.Red;
            colorText.Bold = true;

            par.Add(colorText);
            par.AddSpace(1);

            colorText = new FormattedText();
            colorText.AddText("sales@quasonix.com.");
            colorText.Color = Colors.Blue;
            colorText.Bold = true;

            par.Add(colorText);
            par.AddSpace(1);

            par.AddText("Delivery to any other address or entity will delay processing.");

            cell.Add(par);
            par.Format.SpaceBefore = 7;

            par = cell.AddParagraph("Quasonix reserves the right to partition orders into multiple delivery lots, as dictated by production capacity and material availability. Invoicing on partial shipments will be considered acceptable unless otherwise noted on your purchase order. Please indicate the requested delivery date and reference Quasonix's quote number on any resulting purchase order.");
            par.Format.SpaceBefore = 7;

            par = cell.AddParagraph("PAYMENT TERMS");
            par.Format.SpaceBefore = 15;
            par.Format.Font.Size = 11;
            par.Format.Font.Bold = true;

            par = new Paragraph();

            colorText = new FormattedText();
            colorText.AddText("Quoted prices are valid ONLY for Net 30 payment terms for established customers.");
            colorText.Color = Colors.Red;
            colorText.Bold = true;

            par.Add(colorText);
            par.AddSpace(1);

            par.AddText("Quasonix will quote extended payment terms up to Net 60 for established accounts in good standing when requested. For payments made on extended terms and for all late payments (based on the day payment arrives at Quasonix), prices increase by 0.05% for each additional day beyond 30 days.All credit card (VISA, MasterCard, AMEX) orders are subject to a 4% convenience fee.");
            par.Format.SpaceBefore = 7;
            cell.Add(par);

            par = cell.AddParagraph("DELIVERY TIMING");
            par.Format.SpaceBefore = 15;
            par.Format.Font.Size = 11;
            par.Format.Font.Bold = true;

            par = new Paragraph();

            colorText = new FormattedText();
            colorText.AddText("Quoted pricing applies only to orders that authorize shipment before the due date, without restriction.");
            colorText.Color = Colors.Red;
            colorText.Bold = true;

            par.Add(colorText);
            par.AddSpace(1);

            par.AddText("Orders that impose limitations on early shipment will be re-quoted with a \"hold until due\" surcharge assessed.");
            par.Format.SpaceBefore = 7;
            cell.Add(par);

            if (quote.OrderType == OrderType.GSA)
            {
                par = cell.AddParagraph("GENERAL SERVICES ADMINISTRATION (GSA) PRICING");
                par.Format.SpaceBefore = 15;
                par.Format.Font.Size = 11;
                par.Format.Font.Bold = true;

                par = new Paragraph();

                colorText = new FormattedText();
                colorText.AddText("Quasonix offers GSA pricing under contract GS-03F0040Y.");
                colorText.Color = Colors.Red;
                colorText.Bold = true;

                par.Add(colorText);
                par.AddSpace(1);

                par.AddText("GSA pricing is available to non-US-government entities only when the purchase order is accompanied by certification that the entity is entitled to buy at GSA prices under FAR 51.102, \"Authorization to Use Government Sources of Supply.\"");
                par.Format.SpaceBefore = 7;
                cell.Add(par);
            }

            par = cell.AddParagraph("QUALITY NOTES");
            par.Format.SpaceBefore = 15;
            par.Format.Font.Size = 11;
            par.Format.Font.Bold = true;

            par = cell.AddParagraph("This quote is for one or more commercial off-the-shelf products designed and manufactured by Quasonix. As a commercial off-the-shelf product, all aspects of product design and manufacture are controlled by Quasonix and updated as necessary to provide youwith industry-leading transmitters, receivers, and other telemetry products.");
            par.Format.SpaceBefore = 7;

            par = new Paragraph();

            colorText = new FormattedText();
            colorText.AddHyperlink("https://www.quasonix.com/f-20.pdf", HyperlinkType.Web)
                .AddFormattedText("https://www.quasonix.com/f-20.pdf");
            colorText.Color = Colors.Blue;

            par.AddText("Quasonix F-20 Quality Notes (");
            par.Add(colorText);
            par.AddText(") and ONLY these Quality Notes apply to the product(s) quoted herein. Any requested deviations, flow-downs and/or pass-throughs will be reviewed, and if possible, quoted separately. Should you have any questions or need additional information, please contact");
            par.AddSpace(1);

            colorText = new FormattedText();
            colorText.AddText("sales@quasonix.com");
            colorText.Color = Colors.Blue;

            par.Add(colorText);
            par.AddText(".");

            par.Format.SpaceBefore = 7;
            cell.Add(par);

            par = new Paragraph();
            par.Format.Font.Bold = true;

            colorText = new FormattedText();
            colorText.AddHyperlink("https://www.quasonix.com/std-terms.pdf", HyperlinkType.Web)
                .AddFormattedText("https://www.quasonix.com/std-terms.pdf");
            colorText.Color = Colors.Blue;
            colorText.Bold = false;

            par.AddText("See our complete terms and conditions (");
            par.Add(colorText);
            par.AddText(") for more information.");

            par.Format.SpaceBefore = 7;
            cell.Add(par);

            section.Add(table);
            section.AddPageBreak();
        }

        private void AddSummary()
        {
            var table = new Table();

            table.AddColumn(70);
            table.AddColumn(300);
            table.AddColumn(79);
            table.AddColumn(79);

            table.Columns[2].Format.Alignment = ParagraphAlignment.Center;
            table.Columns[3].Format.Alignment = ParagraphAlignment.Right;

            var row = table.AddRow();
            row.TopPadding = 3;
            row.BottomPadding = 3;
            row.Format.LeftIndent = 3;
            row.Format.Font.Size = 11;
            row.Format.Font.Bold = true;
            row.Format.Font.Color = QuoteDocumentProperties.Colors.Black;

            row.Cells[0].AddParagraph("Summary");
            row.Cells[2].AddParagraph("Total price:");
            row.Cells[3].AddParagraph(quote.Total.ToString("C0"));

            row.Shading.Color = Color.FromCmyk(0, 0, 0, 30);

            row = table.AddRow();
            row.Format.Font.Underline = Underline.Single;
            row.Cells[0].AddParagraph("Line Item ID");
            row.Cells[1].AddParagraph("Part Number");
            row.Cells[2].AddParagraph("Quantity");
            row.Cells[3].AddParagraph("Extended Price");
            row.TopPadding = 10;
            row.BottomPadding = 10;

            for (int i = 0; i < quote.IncludedLines.Count; i++)
            {
                var line = quote.IncludedLines[i];
                var color = i % 2 == 0 ? QuoteDocumentProperties.Colors.White : QuoteDocumentProperties.Colors.LightGrey;

                row = table.AddRow();
                row.Cells[0].AddParagraph(line.Number);
                row.Cells[1].AddParagraph(line.ProductName);
                row.Cells[2].AddParagraph(line.Quantity.ToString());
                row.Cells[3].AddParagraph(line.Product.ExtendedPrice.ToString("C0"));
                row.TopPadding = 7;
                row.BottomPadding = 7;
                row.Shading.Color = color;
            }

            row.Cells[0].Format.SpaceAfter = 30;

            section.Add(table);
        }

        private void AddNotes()
        {
            var table = new Table();

            table.AddColumn(528);

            var row = table.AddRow();
            row.TopPadding = 3;
            row.BottomPadding = 3;
            row.Format.LeftIndent = 3;
            row.Format.Font.Size = 11;
            row.Format.Font.Bold = true;
            row.Format.Font.Color = QuoteDocumentProperties.Colors.Black;

            row.Cells[0].AddParagraph("Additional notes");

            row.Shading.Color = Color.FromCmyk(0, 0, 0, 30);

            row = table.AddRow();
            row.Cells[0].AddParagraph("notes");
            row.Cells[0].Format.SpaceBefore = 10;

            section.Add(table);
            section.AddPageBreak();
        }

        private void AddLineItems()
        {
            var lines = quote.Lines.Where(x => x.IncludedInQuote).OrderBy(x => x.Number).ToList();
            for (int i = 0; i < lines.Count; i++)
            {
                var line = lines[i];

                var table = new Table();
                table.Format.Font.Size = 11;

                table.AddColumn(370);
                table.AddColumn(93);
                table.AddColumn(65 );

                table.Columns[1].Format.Font.Color = QuoteDocumentProperties.Colors.TextGrey;
                table.Columns[2].Format.Alignment = ParagraphAlignment.Right;

                for (int r = 0; r < 6; r++)
                {
                    table.AddRow();
                }

                var row = table.Rows[0];
                row.TopPadding = 3;
                row.BottomPadding = 3;
                row.Format.LeftIndent = 3;
                row.Format.Font.Bold = true;
                row.Format.Font.Color = QuoteDocumentProperties.Colors.Black;

                row.Cells[0].AddParagraph($"Line item {line.Number}: Part number, delivery, and price");
                row.Cells[0].MergeRight = 2;

                row.Shading.Color = Color.FromCmyk(0, 0, 0, 30);

                row = table.Rows[1];
                var itemNumberPar = row.Cells[0].AddParagraph(line.ProductName);
                itemNumberPar.Format.Font.Bold = true;
                itemNumberPar.Format.Font.Color = Color.FromCmyk(0, 0, 0, 80);
                itemNumberPar.Format.SpaceAfter = 15;

                row.Cells[0].MergeDown = 1;
                row.Format.SpaceBefore = 5;

                if (quote.OrderType == OrderType.GSA)
                {
                    row.Cells[1].AddParagraph("Commercial price");
                    row.Cells[2].AddParagraph(line.Product.UnitPrice.ToString("C0"));

                    row = table.Rows[2];
                    row.Cells[1].AddParagraph("GSA price adj.");
                    row.Cells[2].AddParagraph("");
                }

                row = table.Rows[3];
                var deliveryPar = row.Cells[0].AddParagraph("Typical delivery for this item is 90 to 120 days, but actual lead time depends on the backlog at the time an order is received. To get a more accurate lead time, please contact Quasonix shortly before placing an order.");
                deliveryPar.Format.Font.Size = 8;
                deliveryPar.Format.RightIndent = 25;
                deliveryPar.Format.SpaceAfter = 7;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Bottom;
                row.Cells[0].MergeDown = 2;

                row.Cells[1].AddParagraph("Unit price");
                row.Cells[2].AddParagraph(line.Product.UnitPrice.ToString("C0"));

                row = table.Rows[4];
                row.Cells[1].AddParagraph("Quantity");
                row.Cells[2].AddParagraph(line.Quantity.ToString());

                row = table.Rows[5];
                row.Cells[1].AddParagraph("Extended price");
                row.Cells[2].AddParagraph(line.Product.ExtendedPrice.ToString("C0"));

                table.Rows[5].Format.SpaceAfter = 7;

                section.Add(table);

                table = new Table();

                table.AddColumn(528);

                row = table.AddRow();
                row.TopPadding = 3;
                row.BottomPadding = 3;
                row.Format.LeftIndent = 3;
                row.Format.Font.Size = 11;
                row.Format.Font.Bold = true;
                row.Format.Font.Color = QuoteDocumentProperties.Colors.Black;

                row.Cells[0].AddParagraph($"Line item {line.Number}: Description");
                row.Shading.Color = Color.FromCmyk(0, 0, 0, 30);

                section.Add(table);

                switch (line.ProductType)
                {
                    case ProductType.Transmitter:
                        var tx = repo.Get<Transmitter>(line.Product.Id);
                        AddTransmitterDescription(tx);
                        break;
                    case ProductType.Receiver:
                        var rx = repo.Get<Receiver>(line.Product.Id);
                        AddReceiverDescription(rx);
                        break;
                    case ProductType.Antenna:
                        var ant = repo.Get<Antenna>(line.Product.Id);
                        AddAntennaDescription(ant);
                        break;
                    case ProductType.StatusLogger:
                        var loggger = repo.Get<StatusLogger>(line.Product.Id);
                        AddStatusLoggerDescription(loggger);
                        break;
                    case ProductType.ReceiverAnalyzer:
                        var rxan = repo.Get<ReceiverAnalyzer>(line.Product.Id);
                        AddReceiverAnalyzerDescription(rxan);
                        break;
                    case ProductType.Accessory:
                        var acc = repo.Get<Accessory>(line.Product.Id);
                        AddAccessoryDescription(acc);
                        break;
                    case ProductType.EVTM:
                        var evtm = repo.Get<Evtm>(line.Product.Id);
                        AddEvtmDescription(evtm);
                        break;
                }

                // page break if not last line
                if (i != (lines.Count - 1))
                    section.AddPageBreak();
            }
        }

        private void AddTransmitterDescription(Transmitter tx)
        {
            var txDescription = descriptionBuilder.BuildTransmitterDescription(tx);

            // add a table to host two other tables
            var containerTable = new Table();

            containerTable.AddColumn(300);
            containerTable.AddColumn(228);

            var containerRow = containerTable.AddRow();
            containerRow.TopPadding = 7;

            var partNumberTable = new Table();

            partNumberTable.Format.Font.Size = 7.5;

            partNumberTable.AddColumn(40);
            partNumberTable.AddColumn(260);

            FillDescriptionTable(partNumberTable, txDescription.PartNumberBreakdownSection);

            containerRow.Cells[0].Elements.Add(partNumberTable);

            var pinoutTable = new Table();

            pinoutTable.Format.Font.Size = 7.5;

            pinoutTable.AddColumn(30);
            pinoutTable.AddColumn(198);

            FillDescriptionTable(pinoutTable, txDescription.PinoutSection);

            containerRow.Cells[1].Elements.Add(pinoutTable);

            section.Add(containerTable);
        }

        private void AddReceiverDescription(Receiver rx)
        {
            var rxDescription = descriptionBuilder.BuildReceiverDescription(rx);

            // add a table to host two other tables
            var containerTable = new Table();

            containerTable.AddColumn(300);
            containerTable.AddColumn(228);

            var containerRow = containerTable.AddRow();
            containerRow.TopPadding = 7;

            var partNumberTable = new Table();

            partNumberTable.Format.Font.Size = 7.5;

            partNumberTable.AddColumn(35);
            partNumberTable.AddColumn(265);

            FillDescriptionTable(partNumberTable, rxDescription.PartNumberBreakdownSection);

            containerRow.Cells[0].Elements.Add(partNumberTable);

            var connTable = new Table();

            connTable.Format.Font.Size = 7.5;

            connTable.AddColumn(60);
            connTable.AddColumn(168);

            FillDescriptionTable(connTable, rxDescription.ConnectorSection);

            containerRow.Cells[1].Elements.Add(connTable);

            section.Add(containerTable);
        }

        private void AddAntennaDescription(Antenna ant)
        {
            var antDescription = descriptionBuilder.BuildAntennaDescription(ant);

            // add a table to host two other tables
            var containerTable = new Table();

            containerTable.AddColumn(300);
            containerTable.AddColumn(228);

            var containerRow = containerTable.AddRow();
            containerRow.TopPadding = 7;

            var partNumberTable = new Table();

            partNumberTable.Format.Font.Size = 7.5;

            partNumberTable.AddColumn(35);
            partNumberTable.AddColumn(265);

            FillDescriptionTable(partNumberTable, antDescription.PartNumberBreakdownSection);

            containerRow.Cells[0].Elements.Add(partNumberTable);

            section.Add(containerTable);
        }

        private void AddStatusLoggerDescription(StatusLogger logger)
        {
            var loggerDescription = descriptionBuilder.BuildStatusLoggerDescription(logger);

            // add a table to host two other tables
            var containerTable = new Table();

            containerTable.AddColumn(300);
            containerTable.AddColumn(228);

            var containerRow = containerTable.AddRow();
            containerRow.TopPadding = 7;

            var partNumberTable = new Table();

            partNumberTable.Format.Font.Size = 7.5;

            partNumberTable.AddColumn(35);
            partNumberTable.AddColumn(265);

            FillDescriptionTable(partNumberTable, loggerDescription.SpecSection);

            containerRow.Cells[0].Elements.Add(partNumberTable);

            section.Add(containerTable);
        }

        private void AddReceiverAnalyzerDescription(ReceiverAnalyzer rxan)
        {
            var rxanDescription = descriptionBuilder.BuildReceiverAnalyzerDescription(rxan);

            // add a table to host two other tables
            var containerTable = new Table();

            containerTable.AddColumn(300);
            containerTable.AddColumn(228);

            var containerRow = containerTable.AddRow();
            containerRow.TopPadding = 7;

            var partNumberTable = new Table();

            partNumberTable.Format.Font.Size = 7.5;

            partNumberTable.AddColumn(35);
            partNumberTable.AddColumn(265);

            FillDescriptionTable(partNumberTable, rxanDescription.SpecSection);

            containerRow.Cells[0].Elements.Add(partNumberTable);

            var connTable = new Table();

            connTable.Format.Font.Size = 7.5;

            connTable.AddColumn(60);
            connTable.AddColumn(168);

            FillDescriptionTable(connTable, rxanDescription.ConnectorSection);

            containerRow.Cells[1].Elements.Add(connTable);

            section.Add(containerTable);
        }

        private void AddAccessoryDescription(Accessory acc)
        {
            var accDescription = descriptionBuilder.BuildAccessoryDescription(acc);

            // add a table to host two other tables
            var containerTable = new Table();

            containerTable.AddColumn(300);
            containerTable.AddColumn(228);

            var containerRow = containerTable.AddRow();
            containerRow.TopPadding = 7;

            var partNumberTable = new Table();

            partNumberTable.Format.Font.Size = 7.5;

            partNumberTable.AddColumn(35);
            partNumberTable.AddColumn(265);

            FillDescriptionTable(partNumberTable, accDescription.DescriptionSection);

            containerRow.Cells[0].Elements.Add(partNumberTable);

            section.Add(containerTable);
        }

        private void AddEvtmDescription(Evtm evtm)
        {
            var evtmDescription = descriptionBuilder.BuildEvtmDescription(evtm);

            // add a table to host two other tables
            var containerTable = new Table();

            containerTable.AddColumn(300);
            containerTable.AddColumn(228);

            var containerRow = containerTable.AddRow();
            containerRow.TopPadding = 7;

            var partNumberTable = new Table();

            partNumberTable.Format.Font.Size = 7.5;

            partNumberTable.AddColumn(35);
            partNumberTable.AddColumn(265);

            FillDescriptionTable(partNumberTable, evtmDescription.DescriptionSection);

            containerRow.Cells[0].Elements.Add(partNumberTable);

            section.Add(containerTable);
        }

        private void FillDescriptionTable(Table table, DescriptionSection section)
        {
            var row = table.AddRow();
            row.Cells[0].AddParagraph(section.Title);
            row.Cells[0].MergeRight = 1;
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[0].Format.Font.Size = 9;

            foreach (var lineItem in section.LineItems)
            {
                row = table.AddRow();

                int descriptionCell = 0;

                if (lineItem is DescriptionLineItemWithHeader)
                {
                    row.Cells[0].AddParagraph((lineItem as DescriptionLineItemWithHeader).Header);
                    row.Cells[0].Format.Font.Bold = true;
                    row.Cells[0].Format.Font.Color = Color.FromCmyk(0, 0, 0, 80);
                    descriptionCell = 1;
                }
                else
                {
                    row.Cells[0].MergeRight = 1;
                }

                var par = new Paragraph();
                foreach (var part in lineItem.DescriptionParts)
                {
                    if (part is DescriptionLineBreak)
                    {
                        par.AddLineBreak();
                        continue;
                    }

                    var text = new FormattedText();
                    text.AddText(part.Text);
                    text.Bold = part.Weight == DescriptionPartWeight.Bold;
                    par.Add(text);
                }

                row.Cells[descriptionCell].Add(par);
            }
        }

        private Table CreateTable(bool includeBorders)
        {
            var table = new Table();
            table.Format.Font.Size = 9;
            table.TopPadding = 2;
            table.BottomPadding = 2;
            table.LeftPadding = 2;
            table.RightPadding = 2;
            table.Format.Font.Color = QuoteDocumentProperties.Colors.Black;
            if (includeBorders)
            {
                table.Borders.Color = QuoteDocumentProperties.Colors.Black;
                table.Borders.Visible = true;
                table.Borders.Width = 1;
            }
            return table;
        }
    }

    public static class QuoteDocumentProperties
    {
        public static double LeftMargin = 50;
        public static double RightMargin = 0;
        public static double TopMargin = 100;
        public static double BottomMargin = 0;
        public static double HeaderMargin = 0;

        public static ParagraphFormat TitleFormat = new ParagraphFormat()
        {
            Alignment = ParagraphAlignment.Right,
            SpaceAfter = 3,
            RightIndent = 1.5,
            Font = new Font()
            {
                Size = 21,
                Name = "Arial",
                Bold = true
            }
        };

        public static double TableHeaderTopMargin = 5;
        public static double TableHeaderBottomMargin = 6;
        public static ParagraphFormat TableHeaderFormat = new ParagraphFormat()
        {
            Alignment = ParagraphAlignment.Center,
            Font = new Font()
            {
                Size = 9,
                Name = "Arial"
            }
        };

        public static ParagraphFormat TableDataFormat = new ParagraphFormat()
        {
            Alignment = ParagraphAlignment.Left,
            Font = new Font()
            {
                Size = 9,
                Name = "Times New Roman"
            }
        };

        public static class Colors
        {
            public static Color Black = Color.FromCmyk(60, 60, 60, 100);
            public static Color White = Color.FromCmyk(0, 0, 0, 0);
            public static Color TextGrey = Color.FromCmyk(0, 0, 0, 44);
            public static Color Red = Color.FromCmyk(0, 99, 100, 0);
            public static Color LightGrey = Color.FromCmyk(0, 0, 0, 5);
        }
    }
}
