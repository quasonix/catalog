﻿using Quasonix.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Services
{
    public interface IProductDescriptionBuilder
    {
        TransmitterDescription BuildTransmitterDescription(Transmitter tx);
        ReceiverDescription BuildReceiverDescription(Receiver rx);
        AntennaDescription BuildAntennaDescription(Antenna ant);
        StatusLoggerDescription BuildStatusLoggerDescription(StatusLogger logger);
        ReceiverAnalyzerDescription BuildReceiverAnalyzerDescription(ReceiverAnalyzer rxan);
        AccessoryDescription BuildAccessoryDescription(Accessory rxan);
        EvtmDescription BuildEvtmDescription(Evtm evtm);
    }

    public class ProductDescriptionBuilder : IProductDescriptionBuilder
    {
        public TransmitterDescription BuildTransmitterDescription(Transmitter tx)
        {
            var txDescription = new TransmitterDescription();

            var partSection = new DescriptionSection();
            partSection.Title = "Telemetry Transmitter";

            // standard line
            partSection.AddLineItem("(V)")
                .AddNormalPart("Variable Rate, auto-scaling of deviation and filter BW");

            // band combo
            var bandComboLine = partSection.AddLineItem($"({tx.BandCombination.PartNumberDescriptor})");
            var orderedBands = tx.BandCombination.Bands.OrderBy(x => x.MinimumFrequency);
            foreach (var band in orderedBands)
            {
                bandComboLine.AddNormalPart($"{band.MinimumFrequency} MHz to {band.MaximumFrequency} MHz")
                    .AddLineBreak();
            }
            bandComboLine.AddNormalPart("Tuning in 0.5 MHz steps");

            // baseband
            partSection.AddLineItem($"({tx.Baseband.PartNumberDescriptor})")
                .AddNormalPart(tx.Baseband.LineItemText);

            // serial
            partSection.AddLineItem($"({tx.SerialControlInterface.PartNumberDescriptor})")
                .AddNormalPart(tx.SerialControlInterface.LineItemText);

            // blank line
            partSection.AddLineItem();

            // modulation
            bool hasArtm0 = tx.Modulations.Any(x => x.Type == ModulationType.ARTM0);
            bool hasArtm1 = tx.Modulations.Any(x => x.Type == ModulationType.ARTM1);
            bool hasArtm2 = tx.Modulations.Any(x => x.Type == ModulationType.ARTM2);
            bool hasPsk = tx.Modulations.Any(x => x.Type == ModulationType.PSK);

            partSection.AddLineItem(hasArtm0 ? "(1)" : "(0)")
                .AddNormalPart($"ARTM Tier 0 (PCM/FM), 0.05 - 14 Mbps: {(hasArtm0 ? "YES" : "NO")}");
            partSection.AddLineItem(hasArtm1 ? "(1)" : "(0)")
                .AddNormalPart($"ARTM Tier I (SOQPSK-TG), 0.1 - 28 Mbps: {(hasArtm1 ? "YES" : "NO")}");
            partSection.AddLineItem(hasArtm2 ? "(1)" : "(0)")
                .AddNormalPart($"ARTM Tier II (Multi-h CPM), 0.1 - 28 Mbps: {(hasArtm2 ? "YES" : "NO")}");
            partSection.AddLineItem(hasPsk ? "(1)" : "(0)")
                .AddNormalPart($"BPSK, QPSK, OQPSK 0.05 - 14 Mbps: {(hasPsk ? "YES" : "NO")}");

            // blank line
            partSection.AddLineItem();

            // rf
            partSection.AddLineItem($"({tx.RadioFrequencyPower.PartNumberDescriptor})")
                .AddNormalPart(tx.RadioFrequencyPower.LineItemText);

            // blank line
            partSection.AddLineItem();

            // pinout
            partSection.AddLineItem($"({tx.Pinout.Name})")
                .AddNormalPart("Pin Out Configuration");

            // blank line
            partSection.AddLineItem();

            // package
            partSection.AddLineItem($"({tx.Package.Name})")
                .AddNormalPart($"Package: {tx.Package.Footprint.Length}\" x {tx.Package.Footprint.Width}\" x {tx.Package.Height.Measurement}\" = {Math.Round(tx.Package.Volume.Value, 2)} inches³");

            // blank line
            partSection.AddLineItem();

            // options
            var optionsLine = partSection.AddLineItem()
                .AddBoldPart("Options selected: ");

            if (tx.Options.Count == 0)
                optionsLine.AddNormalPart("none");

            foreach (var option in tx.Options)
            {
                partSection.AddLineItem($"({option.PartNumberDescriptor})")
                    .AddNormalPart(option.LineItemText);
            }

            txDescription.PartNumberBreakdownSection = partSection;

            var pinoutSection = new DescriptionSection();
            pinoutSection.Title = "Primary (Clock/Data/Control) Connector Pins";

            if (tx.Pinout.Name == "XX")
            {
                pinoutSection.AddLineItem("XX")
                    .AddNormalPart("The options chosen require a new/custom pinout; Quasonix will verify feasibility");
            }
            else
            {
                foreach (var connector in tx.Pinout.ConnectorSet.ConnectorSetDataConnectors.OrderBy(x => x.Rank))
                {
                    foreach (var pinUsage in connector.DataConnector.PinUsages.Where(x => x.Pinout == tx.Pinout).OrderBy(x => x.PinNumber))
                    {
                        pinoutSection.AddLineItem(pinUsage.PinNumber.ToString())
                            .AddNormalPart(pinUsage.Pin.Name);
                    }
                }
            }

            txDescription.PinoutSection = pinoutSection;

            return txDescription;
        }
        
        public ReceiverDescription BuildReceiverDescription(Receiver rx)
        {
            var rxDescription = new ReceiverDescription();

            var partSection = new DescriptionSection();
            partSection.Title = "Single-channel compact receiver";

            partSection.AddLineItem("(RDMS)")
                .AddNormalPart("Third-Generation RDMS™ Telemetry Receiver / Demodulator / Synchronizer");

            // package
            partSection.AddLineItem($"({rx.Package.PartNumberDescriptor})")
                .AddNormalPart(rx.Package.Description)
                .AddLineBreak()
                .AddNormalPart($"DISPLAYS: {rx.Package.FrontPanelDisplay.Name}")
                .AddLineBreak()
                .AddNormalPart($"FRONT PANEL CONTROLS: {rx.Package.FrontPanelInterface.Name}")
                .AddLineBreak()
                .AddNormalPart("GRAPHICS: Eye pattern, Constellation, Spectrum")
                .AddLineBreak()
                .AddNormalPart("DATA QUALITY ENCAPSULATION: Included, bypassable");

            // band combo
            var orderedBands = rx.BandCombination.Bands.OrderBy(x => x.MinimumFrequency);
            var bandComboPart = partSection.AddLineItem($"({rx.BandCombination.PartNumberDescriptor})")
                .AddNormalPart("FREQUENCY BANDS, TUNED IN 1Hz STEPS:")
                .AddLineBreak();

            foreach (var band in orderedBands)
            {
                bandComboPart.AddNormalPart(band.RangeText)
                    .AddLineBreak();
            }

            // blank line
            partSection.AddLineItem();

            // demodulations
            bool hasArtm0 = rx.Demodulations.Any(x => x.Type == ModulationType.ARTM0);
            bool hasArtm1 = rx.Demodulations.Any(x => x.Type == ModulationType.ARTM1);
            bool hasArtm2 = rx.Demodulations.Any(x => x.Type == ModulationType.ARTM2);
            bool hasPsk = rx.Demodulations.Any(x => x.Type == ModulationType.PSK);

            partSection.AddLineItem(hasArtm0 ? "(1)" : "(0)")
                .AddNormalPart($"ARTM Tier 0 (PCM/FM), 24 kbps - 23 Mbps: {(hasArtm0 ? "YES" : "NO")}");
            partSection.AddLineItem(hasArtm1 ? "(1)" : "(0)")
                .AddNormalPart($"ARTM Tier I (SOQPSK-TG), 100 kbps - 46 Mbps: {(hasArtm1 ? "YES" : "NO")}")
                .AddLineBreak()
                .AddNormalPart($"Space-Time Coding Demod with SOQPSK-TG: {(hasArtm1 ? "YES" : "NO")}")
                .AddLineBreak()
                .AddNormalPart($"LDPC Decoder with SOQPSK-TG: {(hasArtm1 ? "YES" : "NO")}");
            partSection.AddLineItem(hasArtm2 ? "(1)" : "(0)")
                .AddNormalPart($"ARTM Tier II (Multi-h CPM), 1 Mbps - 46 Mbps: {(hasArtm2 ? "YES" : "NO")}");
            partSection.AddLineItem(hasPsk ? "(1)" : "(0)")
                .AddNormalPart($"BPSK, QPSK, OQPSK, AQPSK, UQPSK, AUQPSK, Digital PM: {(hasPsk ? "YES" : "NO")}");

            // blank line
            partSection.AddLineItem();

            // pinout
            partSection.AddLineItem($"({rx.Pinout.PartNumberDescriptor})")
                .AddNormalPart($"PINOUT CUSTOMIZATION: {rx.Pinout.Name}");

            // blank line
            partSection.AddLineItem();

            // options
            var optionsLine = partSection.AddLineItem()
                .AddBoldPart("Options selected: ");

            if (rx.Options.Count == 0)
                optionsLine.AddNormalPart("none");

            foreach (var option in rx.Options)
            {
                partSection.AddLineItem($"({option.PartNumberDescriptor})")
                    .AddNormalPart(option.LineItemText);
            }

            rxDescription.PartNumberBreakdownSection = partSection;

            // connector section
            var connSection = new DescriptionSection();

            connSection.Title = "I/O Connectors:";

            connSection.AddLineItem("RF IN")
                .AddNormalPart(rx.Package.RfInputConnectorSet.Description);
            connSection.AddLineItem("IF IN")
                .AddNormalPart(rx.Package.IfInputConnectorSet.Description);
            connSection.AddLineItem("IF OUT")
                .AddNormalPart(rx.Package.IfOutputConnectorSet.Description);
            connSection.AddLineItem("TTL C&D")
                .AddNormalPart(rx.Package.BasebandSet.TtlConnectorSet.Description);
            connSection.AddLineItem("RS-422 C&D")
                .AddNormalPart(rx.Package.BasebandSet.RsConnectorSet.Description);
            connSection.AddLineItem("ETHERNET")
                .AddNormalPart(rx.Package.EthernetConnectorSet.Description);
            connSection.AddLineItem("ANALOG OUT")
                .AddNormalPart(rx.Package.AnalogOutputConnectorSet.Description);
            connSection.AddLineItem("AM OUT")
                .AddNormalPart(rx.Package.AmOutputConnectorSet.Description);
            connSection.AddLineItem("AGC OUT")
                .AddNormalPart(rx.Package.AgcOutputConnectorSet.Description);

            rxDescription.ConnectorSection = connSection;

            return rxDescription;
        }

        public AntennaDescription BuildAntennaDescription(Antenna ant)
        {
            var antDescription = new AntennaDescription();

            var partSection = new DescriptionSection();
            partSection.Title = "Antenna";

            // pedestal/standard line
            partSection.AddLineItem($"({ant.Pedestal.PartNumberDescriptor})")
                .AddNormalPart("Precision Drive Antenna Pedestal");

            // reflector
            partSection.AddLineItem($"({ant.ReflectorAssembly.PartNumberDescriptor})")
                .AddNormalPart(ant.ReflectorAssembly.Name);

            // wiring
            partSection.AddLineItem($"({ant.Wiring.PartNumberDescriptor})")
                .AddNormalPart(ant.Wiring.Name);

            // feed
            partSection.AddLineItem($"({ant.Feed.PartNumberDescriptor})")
                .AddNormalPart(ant.Feed.Name);

            // band combo
            partSection.AddLineItem($"({ant.BandCombination.PartNumberDescriptor})")
                .AddNormalPart(ant.BandCombination.Name);

            // blank line
            partSection.AddLineItem();

            // options
            var optionsLine = partSection.AddLineItem()
                .AddBoldPart("Options selected: ");

            if (ant.Options.Count == 0)
                optionsLine.AddNormalPart("none");

            foreach (var option in ant.Options)
            {
                partSection.AddLineItem($"({option.PartNumberDescriptor})")
                    .AddNormalPart(option.LineItemText);
            }

            antDescription.PartNumberBreakdownSection = partSection;

            return antDescription;
        }

        public StatusLoggerDescription BuildStatusLoggerDescription(StatusLogger logger)
        {
            var loggerDescription = new StatusLoggerDescription();

            var specSection = new DescriptionSection();
            specSection.Title = logger.Name;

            // blank line
            specSection.AddLineItem();

            var specLines = logger.Specs.Split(Environment.NewLine, StringSplitOptions.None);

            foreach (var line in specLines)
            {
                specSection.AddLineItem().AddNormalPart(line);
            }

            loggerDescription.SpecSection = specSection;

            return loggerDescription;
        }

        public ReceiverAnalyzerDescription BuildReceiverAnalyzerDescription(ReceiverAnalyzer rxan)
        {
            var rxanDescription = new ReceiverAnalyzerDescription();

            var specSection = new DescriptionSection();
            specSection.Title = rxan.Name;

            // blank line
            specSection.AddLineItem();

            var specLines = rxan.Specs.Split(Environment.NewLine, StringSplitOptions.None);

            foreach (var line in specLines)
            {
                specSection.AddLineItem().AddNormalPart(line);
            }

            rxanDescription.SpecSection = specSection;

            var connSection = new DescriptionSection();
            connSection.Title = rxan.Name;

            // blank line
            connSection.AddLineItem();

            var connLines = rxan.ConnectorDescription.Split(Environment.NewLine, StringSplitOptions.None);

            foreach (var line in connLines)
            {
                connSection.AddLineItem().AddNormalPart(line);
            }

            rxanDescription.ConnectorSection = connSection;

            return rxanDescription;
        }

        public AccessoryDescription BuildAccessoryDescription(Accessory acc)
        {
            var accDescription = new AccessoryDescription();

            var descSection = new DescriptionSection();
            descSection.Title = "Accessory";

            descSection.AddLineItem();

            descSection.AddLineItem().AddNormalPart(acc.Description);

            accDescription.DescriptionSection = descSection;

            return accDescription;
        }

        public EvtmDescription BuildEvtmDescription(Evtm evtm)
        {
            var evtmDescription = new EvtmDescription();

            var descSection = new DescriptionSection();
            descSection.Title = "Accessory";

            descSection.AddLineItem();

            descSection.AddLineItem().AddNormalPart(evtm.Description);

            evtmDescription.DescriptionSection = descSection;

            return evtmDescription;
        }
    }

    public class TransmitterDescription
    {
        public string Title { get; set; }
        public DescriptionSection PartNumberBreakdownSection { get; set; }
        public DescriptionSection PinoutSection { get;set; }
    }

    public class ReceiverDescription
    {
        public string Title { get; set; }
        public DescriptionSection PartNumberBreakdownSection { get; set; }
        public DescriptionSection ConnectorSection { get; set; }
    }

    public class AntennaDescription
    {
        public string Title { get; set; }
        public DescriptionSection PartNumberBreakdownSection { get; set; }
    }

    public class StatusLoggerDescription
    {
        public string Title { get; set; }
        public DescriptionSection SpecSection { get; set; }
    }

    public class ReceiverAnalyzerDescription
    {
        public string Title { get; set; }
        public DescriptionSection SpecSection { get; set; }
        public DescriptionSection ConnectorSection { get; set; }
    }

    public class AccessoryDescription
    {
        public string Title { get; set; }
        public DescriptionSection DescriptionSection { get; set; }
    }

    public class EvtmDescription
    {
        public string Title { get; set; }
        public DescriptionSection DescriptionSection { get; set; }
    }

    public class DescriptionSection
    {
        public string Title { get; set; }
        public List<DescriptionLineItem> LineItems { get; set; } = new List<DescriptionLineItem>();

        public DescriptionLineItem AddLineItem()
        {
            var line = new DescriptionLineItem();
            LineItems.Add(line);
            return line;
        }

        public DescriptionLineItemWithHeader AddLineItem(string header)
        {
            var line = new DescriptionLineItemWithHeader();
            line.Header = header;
            LineItems.Add(line);
            return line;
        }
    }

    public class DescriptionLineItem
    {
        public List<DescriptionPart> DescriptionParts { get; set; } = new List<DescriptionPart>();

        public DescriptionLineItem AddNormalPart(string text)
        {
            DescriptionParts.Add(new DescriptionPart(text, DescriptionPartWeight.Normal));
            return this;
        }

        public DescriptionLineItem AddBoldPart(string text)
        {
            DescriptionParts.Add(new DescriptionPart(text, DescriptionPartWeight.Bold));
            return this;
        }

        public DescriptionLineItem AddLineBreak()
        {
            DescriptionParts.Add(new DescriptionLineBreak());
            return this;
        }
    }

    public class DescriptionLineItemWithHeader : DescriptionLineItem
    {
        public string Header { get; set; } = "";
    }

    public class DescriptionPart
    {
        public string Text { get; set; }
        public DescriptionPartWeight Weight { get; set; } = DescriptionPartWeight.Normal;

        public DescriptionPart() { }

        public DescriptionPart(string text, DescriptionPartWeight weight)
        {
            Text = text;
            Weight = weight;
        }
    }

    public class DescriptionLineBreak : DescriptionPart { }

    public enum DescriptionPartWeight
    {
        Normal, Bold
    }
}
