using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Quasonix.Catalog.Entities;
using Quasonix.Catalog.Identity;
using Quasonix.Catalog.Services;

namespace Quasonix.Catalog
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();

            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(10);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            services.AddAutoMapper(typeof(Startup));

            services.AddControllersWithViews()
                .AddRazorRuntimeCompilation();

            // Add identity types
            services.AddIdentity<User, Role>()
                .AddDefaultTokenProviders();
            services.AddTransient<IUserStore<User>, UserStore>();
            services.AddTransient<IRoleStore<Role>, RoleStore>();

            // DAL
            services.AddTransient<ICatalogRepository, CatalogRepository>();
            services.AddTransient(typeof(ICatalogRepository<>), typeof(CatalogRepository<>));
            services.AddScoped<ICatalogUnitOfWork, CatalogUnitOfWork>();

            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<ISpecSheetGenerator, SpecSheetGenerator>();
            services.AddTransient<IQuoteDocumentGenerator, QuoteDocumentGenerator>();
            services.AddTransient<ISeeder, Seeder>();
            services.AddTransient<IProductDescriptionBuilder, ProductDescriptionBuilder>();

            services.AddTransient<TransmitterFilter>();

            services.ConfigureApplicationCookie(options => options.LoginPath = "/Account/LogIn");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            // REMOVE BEFORE PUBLISH TO PROD!!!!!!!!!!!!!!!
            app.UseDeveloperExceptionPage();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });


            // ensure default roles exist
            //SeedIdentity(serviceProvider).Wait();
            //Seed(serviceProvider);
        }

        private void Seed(IServiceProvider serviceProvider)
        {
            var seeder = serviceProvider.GetService<ISeeder>();
            seeder.Seed();
        }

        private async Task SeedIdentity(IServiceProvider serviceProvider)
        {
            // initialize roles 
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<Role>>();
            var UserManager = serviceProvider.GetRequiredService<UserManager<User>>();
            string[] roleNames = { "admin", "qsx", "rep" };
            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                var roleExist = await RoleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    //create the roles and seed them to the database: Question 1
                    roleResult = await RoleManager.CreateAsync(new Role() { Name = roleName });
                }
            }

            List<string> emails = new List<string>()
            {
                "thill@quasonix.com",
                "mschultz@quasonix.com",
                "dfox@quasonix.com",
                "ryans@quasonix.com",
                "bmallow@quasonix.com",
            };

            foreach (var email in emails)
            {
                // create admin user
                var admin = new User
                {
                    UserName = email,
                    Email = email,
                    EmailConfirmed = true
                };

                string adminPassword = Configuration.GetValue<string>("AdminPassword");
                var user = await UserManager.FindByEmailAsync(email);

                if (user == null)
                {
                    try
                    {
                        var createPowerUser = await UserManager.CreateAsync(admin, "Q5xqu()te");
                        if (createPowerUser.Succeeded)
                        {
                            // add admin to role
                            await UserManager.AddToRoleAsync(admin, "rep");
                        }
                    }
                    catch (Exception ex)
                    {

                        throw;
                    }
                }
            }

        }
    }
}
