﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public static class Alphabet
{
    private static string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static string CharacterAt(int position)
    {
        return alphabet[position].ToString();
    }
}