﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 11/30/2021 3:31:26 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Quasonix.Catalog.Entities
{

    /// <summary>
    /// There are no comments for Quasonix.Catalog.Entities.User, Quasonix.Catalog.Entities in the schema.
    /// </summary>
    public partial class User {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for User constructor in the schema.
        /// </summary>
        public User()
        {
            this.Roles = new HashSet<Role>();
            this.Claims = new HashSet<UserClaim>();
            this.Logins = new HashSet<UserLogin>();
            this.Tokens = new HashSet<UserToken>();
            this.Quotes = new HashSet<Quote>();
            OnCreated();
        }

    
        /// <summary>
        /// There are no comments for DisplayName in the schema.
        /// </summary>
        public virtual string DisplayName
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for RepNumber in the schema.
        /// </summary>
        public virtual string RepNumber
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Roles in the schema.
        /// </summary>
        public virtual ISet<Role> Roles
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Claims in the schema.
        /// </summary>
        public virtual ISet<UserClaim> Claims
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Logins in the schema.
        /// </summary>
        public virtual ISet<UserLogin> Logins
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Tokens in the schema.
        /// </summary>
        public virtual ISet<UserToken> Tokens
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Quotes in the schema.
        /// </summary>
        public virtual ISet<Quote> Quotes
        {
            get;
            set;
        }
    }

}
