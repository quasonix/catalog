﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 11/30/2021 3:31:26 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Quasonix.Catalog.Entities
{

    /// <summary>
    /// There are no comments for Quasonix.Catalog.Entities.TxComponent, Quasonix.Catalog.Entities in the schema.
    /// </summary>
    public partial class TxComponent : Component {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for TxComponent constructor in the schema.
        /// </summary>
        public TxComponent()
        {
            this.OptionConflicts = new HashSet<TxOptionBinding>();
            this.Pins = new HashSet<TxPin>();
            OnCreated();
        }

    
        /// <summary>
        /// There are no comments for OptionConflicts in the schema.
        /// </summary>
        public virtual ISet<TxOptionBinding> OptionConflicts
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Pins in the schema.
        /// </summary>
        public virtual ISet<TxPin> Pins
        {
            get;
            set;
        }
    }

}
