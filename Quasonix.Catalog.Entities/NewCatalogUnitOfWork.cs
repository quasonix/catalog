﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Context;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Quasonix.Catalog.Entities
{
    public interface INewCatalogUnitOfWork
    {
        void Commit();
    }

    public class NewCatalogUnitOfWork : INewCatalogUnitOfWork
    {
        protected static ISessionFactory _sessionFactory;
        private ITransaction _transaction;

        public ISession Session { get; protected set; }

        public NewCatalogUnitOfWork()
        {
            Session = _sessionFactory.OpenSession();
            _transaction = Session.BeginTransaction();
        }

        static NewCatalogUnitOfWork()
        {
#if DEBUG
            _sessionFactory = Fluently.Configure()
                      .Database(MsSqlConfiguration.MsSql2008.ShowSql().ConnectionString("Data Source=server2;Initial Catalog=CatalogDev;User ID=DonaldJTrump;Password=badhombres;MultipleActiveResultSets=True"))
                      .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("Quasonix.Catalog.Entities")))
                      .CurrentSessionContext<WebSessionContext>()
                      .BuildSessionFactory();
#else
            _sessionFactory = Fluently.Configure()
                      .Database(MsSqlConfiguration.MsSql2008.ShowSql().ConnectionString("Data Source=server2;Initial Catalog=Catalog;User ID=DonaldJTrump;Password=badhombres;MultipleActiveResultSets=True"))
                      .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.Load("Quasonix.Catalog.Entities")))
                      .CurrentSessionContext<WebSessionContext>()
                      .BuildSessionFactory();
#endif
        }

        public void BeginTransaction()
        {
            _transaction = Session.BeginTransaction();
        }

        public void Commit()
        {
            try
            {
                // commit transaction if there is one active
                if (_transaction != null && _transaction.IsActive)
                    _transaction.Commit();
            }
            catch
            {
                // rollback if there was an exception
                if (_transaction != null && _transaction.IsActive)
                    _transaction.Rollback();

                throw;
            }
            finally
            {
                Session.Dispose();
                _transaction = Session.BeginTransaction();
            }
        }
    }
}
