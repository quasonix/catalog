﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 11/30/2021 3:31:26 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Quasonix.Catalog.Entities
{

    /// <summary>
    /// There are no comments for Quasonix.Catalog.Entities.Receiver, Quasonix.Catalog.Entities in the schema.
    /// </summary>
    public partial class Receiver : Product {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for Receiver constructor in the schema.
        /// </summary>
        public Receiver()
        {
            this.Options = new HashSet<RxOption>();
            this.Demodulations = new HashSet<RxDemodulation>();
            OnCreated();
        }

    
        /// <summary>
        /// There are no comments for Package in the schema.
        /// </summary>
        public virtual RxPackage Package
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Pinout in the schema.
        /// </summary>
        public virtual RxPinout Pinout
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for BandCombination in the schema.
        /// </summary>
        public virtual RxBandCombination BandCombination
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Options in the schema.
        /// </summary>
        public virtual ISet<RxOption> Options
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Demodulations in the schema.
        /// </summary>
        public virtual ISet<RxDemodulation> Demodulations
        {
            get;
            set;
        }
    }

}
