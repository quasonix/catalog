﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 11/30/2021 3:31:26 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Quasonix.Catalog.Entities
{

    /// <summary>
    /// There are no comments for Quasonix.Catalog.Entities.TxOptionBinding, Quasonix.Catalog.Entities in the schema.
    /// </summary>
    public partial class TxOptionBinding {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for TxOptionBinding constructor in the schema.
        /// </summary>
        public TxOptionBinding()
        {
            OnCreated();
        }

    
        /// <summary>
        /// There are no comments for Id in the schema.
        /// </summary>
        public virtual System.Guid Id
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for BindingType in the schema.
        /// </summary>
        public virtual OptionBindingType BindingType
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Component in the schema.
        /// </summary>
        public virtual TxComponent Component
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Option in the schema.
        /// </summary>
        public virtual TxOption Option
        {
            get;
            set;
        }
    }

}
