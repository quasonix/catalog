﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate Fluent Mapping template.
// Code is generated on: 11/30/2021 3:31:27 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using FluentNHibernate.Mapping;
using FluentNHibernate.MappingModel.Collections;

namespace Quasonix.Catalog.Entities
{
    /// <summary>
    /// There are no comments for TxOptionCombinationMap in the schema.
    /// </summary>
    public partial class TxOptionCombinationMap : ClassMap<TxOptionCombination>
    {
        /// <summary>
        /// There are no comments for TxOptionCombinationMap constructor in the schema.
        /// </summary>
        public TxOptionCombinationMap()
        {
              Table(@"TxOptionCombination");
              LazyLoad();
              Id(x => x.Id)
                .Column("Id")
                .CustomType("Guid")
                .Access.Property()
                .GeneratedBy.Guid();
              Map(x => x.Name)    
                .Column("Name")
                .CustomType("String")
                .Access.Property()
                .Generated.Never();
              HasManyToMany<TxPackage>(x => x.Packages)
                .Access.Property()
                .AsSet()
                .Cascade.SaveUpdate()
                .LazyLoad()
                // .OptimisticLock.Version() /*bug (or missing feature) in Fluent NHibernate*/
                .Generic()
                .Table("TxPackages_TxOptionCombinations")
                .FetchType.Join()
                .ChildKeyColumns.Add("PackageId", mapping => mapping.Name("PackageId")
                                                                     .Nullable())
                .ParentKeyColumns.Add("OptionCombinationId", mapping => mapping.Name("OptionCombinationId")
                                                                     .Nullable());
              HasManyToMany<TxOption>(x => x.Options)
                .Access.Property()
                .AsSet()
                .Cascade.SaveUpdate()
                .LazyLoad()
                // .OptimisticLock.Version() /*bug (or missing feature) in Fluent NHibernate*/
                .Inverse()
                .Generic()
                .Table("TxOptionCombinations_TxOptions")
                .FetchType.Join()
                .ChildKeyColumns.Add("OptionId", mapping => mapping.Name("OptionId")
                                                                     .Nullable())
                .ParentKeyColumns.Add("OptionCombinationId", mapping => mapping.Name("OptionCombinationId")
                                                                     .Nullable());
              ExtendMapping();
        }

        #region Partial Methods

        partial void ExtendMapping();

        #endregion
    }

}
