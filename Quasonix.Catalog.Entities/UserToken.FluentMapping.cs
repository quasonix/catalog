﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate Fluent Mapping template.
// Code is generated on: 11/30/2021 3:31:27 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using FluentNHibernate.Mapping;
using FluentNHibernate.MappingModel.Collections;

namespace Quasonix.Catalog.Entities
{
    /// <summary>
    /// There are no comments for UserTokenMap in the schema.
    /// </summary>
    public partial class UserTokenMap : ClassMap<UserToken>
    {
        /// <summary>
        /// There are no comments for UserTokenMap constructor in the schema.
        /// </summary>
        public UserTokenMap()
        {
              Schema(@"dbo");
              Table(@"UserToken");
              LazyLoad();
              CompositeId()
                .KeyProperty(x => x.UserId, set => {
                    set.Type("String");
                    set.ColumnName("UserId");
                    set.Length(450);
                    set.Access.Property(); } )
                .KeyProperty(x => x.LoginProvider, set => {
                    set.Type("String");
                    set.ColumnName("LoginProvider");
                    set.Length(128);
                    set.Access.Property(); } )
                .KeyProperty(x => x.Name, set => {
                    set.Type("String");
                    set.ColumnName("Name");
                    set.Length(128);
                    set.Access.Property(); } );
              Map(x => x.Value)    
                .Column("Value")
                .CustomType("String")
                .Access.Property()
                .Generated.Never().CustomSqlType("nvarchar(MAX)");
              References(x => x.User)
                .Class<User>()
                .Access.Property()
                .Cascade.SaveUpdate()
                .LazyLoad()
                .Columns("UserId");
              ExtendMapping();
        }

        #region Partial Methods

        partial void ExtendMapping();

        #endregion
    }

}
