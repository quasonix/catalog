﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 11/30/2021 3:31:26 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Quasonix.Catalog.Entities
{

    /// <summary>
    /// There are no comments for Quasonix.Catalog.Entities.ComponentBindingType, Quasonix.Catalog.Entities in the schema.
    /// </summary>
    public enum ComponentBindingType : int
 {
    
        /// <summary>
        /// There are no comments for ComponentBindingType.Requirement in the schema.
        /// </summary>
        Requirement = 1,    
        /// <summary>
        /// There are no comments for ComponentBindingType.Conflict in the schema.
        /// </summary>
        Conflict = 2,    
        /// <summary>
        /// There are no comments for ComponentBindingType.StrictDependency in the schema.
        /// </summary>
        StrictDependency = 3,    
        /// <summary>
        /// There are no comments for ComponentBindingType.LooseDependency in the schema.
        /// </summary>
        LooseDependency = 4,    
        /// <summary>
        /// There are no comments for ComponentBindingType.Prerequisite in the schema.
        /// </summary>
        Prerequisite = 5
    }

}
