﻿
using Microsoft.AspNetCore.Identity;

namespace Quasonix.Catalog.Entities
{
    public partial class AntBandCombination
    {
        public virtual void ClearBands()
        {
            foreach (var band in Bands)
            {
                // clear out other side of the relationship or it will
                // cascade save back in
                band.BandCombinations.Remove(this);
            }

            Bands.Clear();
        }
    }
}
