﻿using System.Linq;

namespace Quasonix.Catalog.Entities
{
    public partial class TxRadioFrequencyPower
    {
        public virtual double TotalWattage => WattagePerOutput * ((int)OutputCount);

        public override int GetPrice(PriceVersion version)
        {
            int slope = 350;
            int intercept = -3000;

            double watts = TotalWattage < .5 ? 0 : TotalWattage;

            double price = (slope * watts) + intercept;

            return (int)price;
        }
    }
}
