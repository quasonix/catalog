﻿
using Microsoft.AspNetCore.Identity;

namespace Quasonix.Catalog.Entities
{
    public partial class TxFootprint : TxComponent
    {
        public virtual double? Radius => Shape == FootprintShape.Round ? (Diameter / 2) : null;
        public virtual double? Area => Shape == FootprintShape.Rectangular ? (Length * Width) : ((Radius * Radius) * System.Math.PI);
    }
}
