﻿
using Microsoft.AspNetCore.Identity;
using System.Linq;

namespace Quasonix.Catalog.Entities
{
    public partial class TxConnectorSet : TxComponent
    {
        public virtual ConnectorPosition PrimaryConnectorPosition => ConnectorSetDataConnectors.First(x => x.Rank == ConnectorRank.Primary).Position;

        public virtual TxDataConnector PrimaryConnector => ConnectorSetDataConnectors.FirstOrDefault(x => x.Rank == ConnectorRank.Primary)?.DataConnector;
        public virtual TxDataConnector SecondaryConnector => ConnectorSetDataConnectors.FirstOrDefault(x => x.Rank == ConnectorRank.Other)?.DataConnector;
    }
}
