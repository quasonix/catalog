﻿
using Microsoft.AspNetCore.Identity;

namespace Quasonix.Catalog.Entities
{
    public partial class AntFeed
    {
        public virtual void ClearBandCombinations()
        {
            foreach (var bandcombo in BandCombinations)
            {
                // clear out other side of the relationship or it will
                // cascade save back in
                bandcombo.Feeds.Remove(this);
            }

            BandCombinations.Clear();
        }
    }
}
