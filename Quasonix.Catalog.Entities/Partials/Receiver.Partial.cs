﻿
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Quasonix.Catalog.Entities
{
    public partial class Receiver : Product
    {
        private int DownConverterCount = 2;
        private int SawFilterCount => Package.ChannelSet.InputChannelCount;
        private int DemodCount => Package.ChannelSet.TotalChannelCount;
        private int BandCount => Package.ChannelSet.InputChannelCount;
        private int ModeCount => Package.ChannelSet.TotalChannelCount;

        public override int GetPrice(PriceVersion version, int quantity, ShippingZone? shippingZone)
        {
            PriceDetails = new List<PriceDetail>();

            CategoryPrice catPrice;
            if (Category.CategoryPrices.Count == 0)
                catPrice = null;
            else
            {
                catPrice = Category.CategoryPrices.FirstOrDefault(x => x.PriceVersion == version);
                if (catPrice == null)
                    catPrice = Category.CategoryPrices.Where(x => x.PriceVersion.EffectiveDate < version.EffectiveDate)
                        .OrderByDescending(x => x.PriceVersion.EffectiveDate).First();
            }

            int basePrice = catPrice == null ? 0 : catPrice.BasePrice;
            AddPriceDetail("Base Price", basePrice, PriceDetailType.Unit);

            int packagePrice = Package.GetPrice(version);
            int pinoutPrice = Pinout.GetPrice(version);
            int bandPrice = BandCombination.GetPrice(version) * BandCount;
            int modPrice = Demodulations.Select(x => x.GetPrice(version)).Sum() * ModeCount;

            int dcPrice = DownConverterCount * 7000;
            int sawPrice = SawFilterCount * 2650;
            int demodPrice = DemodCount * 2800;

            AddPriceDetail("Package", packagePrice, PriceDetailType.Unit);
            AddPriceDetail("Pinout", pinoutPrice, PriceDetailType.Unit);
            AddPriceDetail("Down Converter with PS", dcPrice, PriceDetailType.Unit);
            AddPriceDetail("SAW Filter Board", sawPrice, PriceDetailType.Unit);
            AddPriceDetail("Demod Board Set", demodPrice, PriceDetailType.Unit);
            AddPriceDetail("Bands", bandPrice, PriceDetailType.Unit);
            AddPriceDetail("Modes", modPrice, PriceDetailType.Unit);

            int optPrice = 0;
            foreach (var option in Options)
            {
                int singlePrice = option.GetPrice(version);
                int multiplier = option.MaxInstance.HasValue ? Math.Min(Package.ChannelSet.TotalChannelCount, option.MaxInstance.Value) : Package.ChannelSet.TotalChannelCount;
                int adjPrice = singlePrice * multiplier;
                optPrice += adjPrice;
                AddPriceDetail($"Option {option.Name} [{multiplier}]", adjPrice, PriceDetailType.Unit);
            }

            int compPrices = packagePrice +
                pinoutPrice +
                bandPrice +
                modPrice +
                optPrice +
                dcPrice +
                sawPrice +
                demodPrice;

            int specialPricing = 0;
            var rules = Components.SelectMany(x => x.GetPriceRules(version)).Distinct().ToList();
            foreach (var rule in rules)
            {
                bool satisfied = rule.Components.Intersect(Components).Count() == rule.Components.Count();
                if (satisfied)
                    specialPricing += rule.Value;
            }

            int unitPrice =  compPrices + specialPricing;
            AddPriceDetail("Unit Price", unitPrice, PriceDetailType.Adjustment);

            var discountMultiplier = 1 - (catPrice.MaximumDiscount * (1 - Math.Exp(-(quantity - 1) / (double)catPrice.DiscountTaper)));

            double initPrice = unitPrice * discountMultiplier;

            AddPriceDetail("Unit Price with Qty Discount", (int)initPrice, PriceDetailType.Adjustment);

            ShippingPrice shipPrice;
            if (Category.ShippingPrices.Count == 0)
                shipPrice = null;
            else
            {
                shipPrice = Category.ShippingPrices.FirstOrDefault(x => x.PriceVersion == version && x.ShippingZone == shippingZone);
                if (shipPrice == null)
                    shipPrice = Category.ShippingPrices.Where(x => x.PriceVersion.EffectiveDate < version.EffectiveDate && x.ShippingZone == shippingZone)
                        .OrderByDescending(x => x.PriceVersion.EffectiveDate).First();
            }

            double unitShipping = (shipPrice.FactorPerUnit * initPrice) + shipPrice.AddOnPerUnit;

            AddPriceDetail("Unit Shipping", (int)Math.Ceiling(unitShipping), PriceDetailType.Adjustment);

            PriceMultiplier priceMultiplier;
            if (Category.PriceMultipliers.Count == 0)
                priceMultiplier = null;
            else
            {
                priceMultiplier = Category.PriceMultipliers.FirstOrDefault(x => x.PriceVersion == version);
                if (priceMultiplier == null)
                    priceMultiplier = Category.PriceMultipliers.Where(x => x.PriceVersion.EffectiveDate < version.EffectiveDate)
                        .OrderByDescending(x => x.PriceVersion.EffectiveDate).First();
            }

            UnitPrice = (int)Math.Ceiling(((initPrice + unitShipping) * priceMultiplier.Factor) / 5) * 5;
            ExtendedPrice = UnitPrice * quantity;

            int catAdj = (int)(UnitPrice - (initPrice + unitShipping));

            AddPriceDetail("Unit Category Adjustment", (int)catAdj, PriceDetailType.Adjustment);
            AddPriceDetail($"Extended Price @ Qty {quantity}", (int)ExtendedPrice, PriceDetailType.Summary);

            return UnitPrice;
        }

        public override List<Component> Components
        {
            get
            {
                var compList = new List<Component>()
                    {
                        Package, Pinout, BandCombination
                    };

                compList.AddRange(Demodulations);
                compList.AddRange(Options);

                return compList;
            }
        }
    }
}
