﻿
using Microsoft.AspNetCore.Identity;

namespace Quasonix.Catalog.Entities
{
    public partial class RxConnectorSet
    {
        public virtual void ClearConnectors()
        {
            foreach (var conn in Connectors)
            {
                // clear out other side of the relationship or it will
                // cascade save back in
                conn.Connector.ConnectorsForSet.Remove(conn);
            }

            Connectors.Clear();
        }
    }
}
