﻿
using Microsoft.AspNetCore.Identity;

namespace Quasonix.Catalog.Entities
{
    public partial class AntFocusArrangement
    {
        public virtual void ClearFeeds()
        {
            foreach (var feed in Feeds)
            {
                // clear out other side of the relationship or it will
                // cascade save back in
                feed.FocusArrangements.Remove(this);
            }

            Feeds.Clear();
        }
    }
}
