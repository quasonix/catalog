﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quasonix.Catalog.Entities
{
    public partial class Quote
    {
        public virtual List<QuoteLine> IncludedLines => Lines.Where(x => x.IncludedInQuote)
            .OrderBy(x => x.Number).ToList();

        public virtual int Total => IncludedLines.Sum(x => x.Product.GetPrice(PriceVersion, x.Quantity, ShippingZone) * x.Quantity);

        public virtual void AddLine(QuoteLine line)
        {
            AssignNumber(line);
            Lines.Add(line);
        }

        public virtual void AssignNumber(QuoteLine line)
        {
            string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            line.Position = Lines.Any() ? Lines.Select(x => x.Position).Max() + 1 : 0;

            string alphaPart = "";

            if (line.Position >= alphabet.Length)
            {
                int pos1 = (int)(line.Position / alphabet.Length) - 1;
                int pos2 = line.Position % alphabet.Length;

                alphaPart = alphabet[pos1].ToString() + alphabet[pos2].ToString();
            }
            else
            {
                alphaPart = alphabet[line.Position].ToString();
            }
            
            line.Number = $"{Number}-{alphaPart}";
        }

        public virtual void ClearLines()
        {
            foreach (var line in Lines)
            {
                // clear out other side of the relationship or it will
                // cascade save back in
                line.Quote = null;
            }

            Lines.Clear();
        }

        public virtual void FinalizeQuote()
        {
            Submitted = true;
            FinishDate = DateTime.Now;

            var excludedLines = Lines.Where(x => !x.IncludedInQuote).ToList();

            // remove any excluded lines
            for (int i = 0; i < excludedLines.Count; i++)
            {
                var line = excludedLines[i];
                Lines.Remove(line);
                line.Quote = null;
            }
        }
    }
}
