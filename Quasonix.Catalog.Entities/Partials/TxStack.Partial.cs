﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quasonix.Catalog.Entities
{
    public partial class TxStack
    {
        public virtual void ClearBoards()
        {
            foreach (var board in Boards)
            {
                board.Stacks.Remove(this);
            }

            Boards.Clear();
        }

        public virtual void ClearBasebands()
        {
            foreach (var baseband in Basebands)
            {
                // clear out other side of the relationship or it will
                // cascade save back in
                baseband.Stacks.Remove(this);
            }

            Basebands.Clear();
        }

        public virtual void ClearSerialControlInterfaces()
        {
            foreach (var serialControl in SerialControlInterfaces)
            {
                // clear out other side of the relationship or it will
                // cascade save back in
                serialControl.Stacks.Remove(this);
            }

            SerialControlInterfaces.Clear();
        }
    }
}
