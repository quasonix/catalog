﻿
using Microsoft.AspNetCore.Identity;

namespace Quasonix.Catalog.Entities
{
    public partial class AntPedestal
    {
        public virtual void ClearReflectorAssemblies()
        {
            foreach (var dish in ReflectorAssemblies)
            {
                // clear out other side of the relationship or it will
                // cascade save back in
                dish.Pedestals.Remove(this);
            }

            ReflectorAssemblies.Clear();
        }

        public virtual void ClearWirings()
        {
            foreach (var wiring in Wirings)
            {
                // clear out other side of the relationship or it will
                // cascade save back in
                wiring.Pedestals.Remove(this);
            }

            Wirings.Clear();
        }

        public virtual void ClearOptions()
        {
            foreach (var option in Options)
            {
                // clear out other side of the relationship or it will
                // cascade save back in
                option.Pedestals.Remove(this);
            }

            Options.Clear();
        }
    }
}
