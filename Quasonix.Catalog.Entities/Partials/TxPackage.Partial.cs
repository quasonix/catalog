﻿
using Microsoft.AspNetCore.Identity;

namespace Quasonix.Catalog.Entities
{
    public partial class TxPackage : TxComponent
    {
        public virtual void ClearDataConnectors()
        {
            //foreach (var dataConn in DataConnectors)
            //{
            //    // clear out other side of the relationship or it will
            //    // cascade save back in
            //    dataConn.DataConnector.DataConnectorsForPackages.Remove(dataConn);
            //}

            //DataConnectors.Clear();
        }

        public virtual double? Volume => Footprint?.Area * Height?.Measurement;
    }
}
