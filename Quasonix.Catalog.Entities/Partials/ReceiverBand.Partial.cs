﻿
using Microsoft.AspNetCore.Identity;

namespace Quasonix.Catalog.Entities
{
    public partial class RxBand
    {
        public virtual string RangeText
        {
            get
            {
                return $"{MinimumFrequency} MHz to {MaximumFrequency} MHz ({Name} - Standard Range)";
            }
        }

        public virtual string ExtendedRangeText
        {
            get
            {
                return $"{ExtendedMinimumFrequency} MHz to {ExtendedMaximumFrequency} MHz ({Name} - Extended Range)";
            }
        }
    }
}
