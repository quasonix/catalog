﻿
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Quasonix.Catalog.Entities
{
    public partial class Transmitter : Product
    {
        public override int GetPrice(PriceVersion version, int quantity, ShippingZone? shippingZone)
        {
            PriceDetails = new List<PriceDetail>();

            CategoryPrice catPrice;
            if (Category.CategoryPrices.Count == 0)
                catPrice = null;
            else
            {
                catPrice = Category.CategoryPrices.FirstOrDefault(x => x.PriceVersion == version);
                if (catPrice == null)
                    catPrice = Category.CategoryPrices.Where(x => x.PriceVersion.EffectiveDate < version.EffectiveDate)
                        .OrderByDescending(x => x.PriceVersion.EffectiveDate).First();
            }

            int basePrice = catPrice == null ? 0 : catPrice.BasePrice;
            AddPriceDetail("Base Price", basePrice, PriceDetailType.Unit);

            int bandComboPrice = 0;
            foreach (var band in BandCombination.Bands)
            {
                int bandPrice = band.GetPrice(version);
                bandComboPrice += bandPrice;
                AddPriceDetail($"Band {band.Name}", bandPrice, PriceDetailType.Unit);
            }

            int rfPrice = RadioFrequencyPower.GetPrice(version);
            AddPriceDetail($"RF Power {RadioFrequencyPower.Name}", rfPrice, PriceDetailType.Unit);

            int basebandPrice = Baseband.GetPrice(version);
            AddPriceDetail($"Clock and Data", basebandPrice, PriceDetailType.Unit);

            int serialPrice = SerialControlInterface.GetPrice(version);
            AddPriceDetail($"Serial Control Interface", serialPrice, PriceDetailType.Unit);

            int modComboPrice = 0;
            foreach (var mod in Modulations)
            {
                int modPrice = mod.GetPrice(version);
                modComboPrice += modPrice;
                AddPriceDetail($"Modulation {mod.Name}", modPrice, PriceDetailType.Unit);
            }

            int pinoutPrice = Pinout.GetPrice(version);
            AddPriceDetail($"Pinout {Pinout.Name}", pinoutPrice, PriceDetailType.Unit);

            int packagePrice = Package.GetPrice(version);
            AddPriceDetail($"Package {Package.Name}", packagePrice, PriceDetailType.Unit);

            int optionComboPrice = 0;
            foreach (var option in Options)
            {
                int optionPrice = option.GetPrice(version);
                optionComboPrice += optionPrice;
                AddPriceDetail($"Option {option.Name}", optionPrice, PriceDetailType.Unit);
            }

            int compPrices = bandComboPrice +
                rfPrice +
                basebandPrice +
                serialPrice +
                modComboPrice +
                pinoutPrice +
                packagePrice +
                optionComboPrice;

            int specialPricing = 0;
            var rules = Components.SelectMany(x => x.GetPriceRules(version)).Distinct().ToList();
            foreach (var rule in rules)
            {
                bool satisfied = rule.Components.Intersect(Components).Count() == rule.Components.Count();
                if (satisfied)
                {
                    specialPricing += rule.Value;
                    AddPriceDetail(rule.Description, rule.Value, PriceDetailType.Unit);
                }
            }

            int unitPrice = basePrice + compPrices + specialPricing;
            AddPriceDetail("Unit Price", unitPrice, PriceDetailType.Adjustment);

            var discountMultiplier = 1 - (catPrice.MaximumDiscount * (1 - Math.Exp(-(quantity - 1) / (double)catPrice.DiscountTaper)));

            double initPrice = unitPrice * discountMultiplier;

            if (quantity > 1)
                AddPriceDetail("Unit Price with Qty Discount", (int)initPrice, PriceDetailType.Adjustment);

            ShippingPrice shipPrice;
            if (Category.ShippingPrices.Count == 0)
                shipPrice = null;
            else
            {
                shipPrice = Category.ShippingPrices.FirstOrDefault(x => x.PriceVersion == version && x.ShippingZone == shippingZone);
                if (shipPrice == null)
                    shipPrice = Category.ShippingPrices.Where(x => x.PriceVersion.EffectiveDate < version.EffectiveDate && x.ShippingZone == shippingZone)
                        .OrderByDescending(x => x.PriceVersion.EffectiveDate).First();
            }

            double unitShipping = (shipPrice.FactorPerUnit * initPrice) + shipPrice.AddOnPerUnit;

            AddPriceDetail("Unit Shipping", (int)Math.Ceiling(unitShipping), PriceDetailType.Adjustment);

            PriceMultiplier priceMultiplier;
            if (Category.PriceMultipliers.Count == 0)
                priceMultiplier = null;
            else
            {
                priceMultiplier = Category.PriceMultipliers.FirstOrDefault(x => x.PriceVersion == version);
                if (priceMultiplier == null)
                    priceMultiplier = Category.PriceMultipliers.Where(x => x.PriceVersion.EffectiveDate < version.EffectiveDate)
                        .OrderByDescending(x => x.PriceVersion.EffectiveDate).First();
            }

            UnitPrice = (int)Math.Ceiling(((initPrice + unitShipping) * priceMultiplier.Factor) / 5) * 5;
            ExtendedPrice = UnitPrice * quantity;

            int catAdj = (int)(UnitPrice - (initPrice + unitShipping));

            AddPriceDetail("Unit Category Adjustment", (int)catAdj, PriceDetailType.Adjustment);
            AddPriceDetail($"Extended Price @ Qty {quantity}", (int)ExtendedPrice, PriceDetailType.Summary);

            return UnitPrice;
        }

        public override List<Component> Components
        {
            get
            {
                var compList = new List<Component>()
                    {
                        Package.Footprint, Package.Height, RadioFrequencyPower, SerialControlInterface, Baseband, BandCombination, Package,
                        Package.CoaxialModulationInput, Package.ConnectorSet
                    };

                compList.AddRange(BandCombination.Bands);
                compList.AddRange(Modulations);
                compList.AddRange(Package.ConnectorSet.ConnectorSetDataConnectors.Select(x => x.DataConnector));
                compList.AddRange(Options);

                return compList;
            }
        }
    }
}
