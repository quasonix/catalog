﻿
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Collections.Generic;

namespace Quasonix.Catalog.Entities
{
    public partial class TxOption : TxComponent
    {
        public virtual IEnumerable<TxComponent> Conflicts => ComponentConflicts.Where(x => x.BindingType == OptionBindingType.Conflict).Select(X => X.Component);
        public virtual IEnumerable<TxComponent> Requirements => ComponentConflicts.Where(x => x.BindingType == OptionBindingType.Requirement).Select(X => X.Component);
    }
}
