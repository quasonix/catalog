﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Quasonix.Catalog.Entities
{
    public partial class User : IdentityUser<string>
    {
        public virtual long? LockoutEndUnixTimeMilliseconds { get; set; }

        public override DateTimeOffset? LockoutEnd
        {
            get
            {
                if (!LockoutEndUnixTimeMilliseconds.HasValue)
                {
                    return null;
                }
                var offset = DateTimeOffset.FromUnixTimeMilliseconds(
                    LockoutEndUnixTimeMilliseconds.Value
                );
                return TimeZoneInfo.ConvertTime(offset, TimeZoneInfo.Local);
            }
            set
            {
                if (value.HasValue)
                {
                    LockoutEndUnixTimeMilliseconds = value.Value.ToUnixTimeMilliseconds();
                }
                else
                {
                    LockoutEndUnixTimeMilliseconds = null;
                }
            }
        }
    }
}
