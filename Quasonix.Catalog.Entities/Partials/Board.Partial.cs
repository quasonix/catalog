﻿
using Microsoft.AspNetCore.Identity;

namespace Quasonix.Catalog.Entities
{
    public partial class TxBoard
    {
        public virtual string FullName => ManufacturingName + (string.IsNullOrWhiteSpace(EngineeringName) ? "" : $" ({EngineeringName})");
    }
}
