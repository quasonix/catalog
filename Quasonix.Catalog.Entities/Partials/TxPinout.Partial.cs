﻿using System.Collections.Generic;
using System.Linq;

namespace Quasonix.Catalog.Entities
{
    public partial class TxPinout
    {
        public virtual IEnumerable<TxPin> IncludedPins => PinUsages.Select(x => x.Pin);
    }
}
