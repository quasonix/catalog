﻿
using Microsoft.AspNetCore.Identity;

namespace Quasonix.Catalog.Entities
{
    public partial class RxFormFactor
    {
        public virtual void ClearOptions()
        {
            foreach (var option in Options)
            {
                option.FormFactors.Remove(this);
            }

            Options.Clear();
        }

        public virtual void ClearPinouts()
        {
            foreach (var pinout in Pinouts)
            {
                pinout.FormFactors.Remove(this);
            }

            Pinouts.Clear();
        }
    }
}