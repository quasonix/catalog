﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace Quasonix.Catalog.Entities
{
    public partial class Product
    {
        public virtual int UnitPrice { get; set; }
        public virtual int ExtendedPrice { get; set; }

        public virtual int GetPrice(PriceVersion version, int quantity, ShippingZone? shippingZone)
        {
            return 0;
        }

        public virtual List<Component> Components { get; set; }

        public virtual List<PriceDetail> PriceDetails { get; set; }

        public virtual void AddPriceDetail(string message, int price, PriceDetailType type)
        {
            if (PriceDetails == null)
                PriceDetails = new List<PriceDetail>();

            PriceDetails.Add(new PriceDetail()
            {
                Message = message,
                Price = price,
                Type = type
            });
        }
    }
}
