﻿
using System;
using System.Linq;

namespace Quasonix.Catalog.Entities
{
    public partial class StatusLogger : Product
    {
        public override int GetPrice(PriceVersion version, int quantity, ShippingZone? shippingZone)
        {
            CategoryPrice catPrice;
            if (Category.CategoryPrices.Count == 0)
                catPrice = null;
            else
            {
                catPrice = Category.CategoryPrices.FirstOrDefault(x => x.PriceVersion == version);
                if (catPrice == null)
                    catPrice = Category.CategoryPrices.Where(x => x.PriceVersion.EffectiveDate < version.EffectiveDate)
                        .OrderByDescending(x => x.PriceVersion.EffectiveDate).First();
            }

            int basePrice = catPrice == null ? 0 : catPrice.BasePrice;
            AddPriceDetail("Base Price", basePrice, PriceDetailType.Unit);

            int unitPrice = basePrice + Price;
            AddPriceDetail("Unit Price", unitPrice, PriceDetailType.Adjustment);

            var discountMultiplier = 1 - (catPrice.MaximumDiscount * (1 - Math.Exp(-(quantity - 1) / (double)catPrice.DiscountTaper)));

            double initPrice = unitPrice * discountMultiplier;

            if (quantity > 1)
                AddPriceDetail("Unit Price with Qty Discount", (int)initPrice, PriceDetailType.Adjustment);

            ShippingPrice shipPrice;
            if (Category.ShippingPrices.Count == 0)
                shipPrice = null;
            else
            {
                shipPrice = Category.ShippingPrices.FirstOrDefault(x => x.PriceVersion == version && x.ShippingZone == shippingZone);
                if (shipPrice == null)
                    shipPrice = Category.ShippingPrices.Where(x => x.PriceVersion.EffectiveDate < version.EffectiveDate && x.ShippingZone == shippingZone)
                        .OrderByDescending(x => x.PriceVersion.EffectiveDate).First();
            }

            double unitShipping = (shipPrice.FactorPerUnit * initPrice) + shipPrice.AddOnPerUnit;

            AddPriceDetail("Unit Shipping", (int)Math.Ceiling(unitShipping), PriceDetailType.Adjustment);

            PriceMultiplier priceMultiplier;
            if (Category.PriceMultipliers.Count == 0)
                priceMultiplier = null;
            else
            {
                priceMultiplier = Category.PriceMultipliers.FirstOrDefault(x => x.PriceVersion == version);
                if (priceMultiplier == null)
                    priceMultiplier = Category.PriceMultipliers.Where(x => x.PriceVersion.EffectiveDate < version.EffectiveDate)
                        .OrderByDescending(x => x.PriceVersion.EffectiveDate).First();
            }

            UnitPrice = (int)Math.Ceiling(((initPrice + unitShipping) * priceMultiplier.Factor) / 5) * 5;
            ExtendedPrice = UnitPrice * quantity;

            int catAdj = (int)(UnitPrice - (initPrice + unitShipping));

            AddPriceDetail("Unit Category Adjustment", (int)catAdj, PriceDetailType.Adjustment);
            AddPriceDetail($"Extended Price @ Qty {quantity}", (int)ExtendedPrice, PriceDetailType.Summary);

            return UnitPrice;
        }
    }
}
