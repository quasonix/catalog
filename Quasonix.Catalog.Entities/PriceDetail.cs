﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quasonix.Catalog.Entities
{
    public class PriceDetail
    {
        public string Message { get; set; }
        public int Price { get; set; }
        public PriceDetailType Type { get; set; }
    }

    public enum PriceDetailType
    {
        Unit, Adjustment, Summary
    }
}
