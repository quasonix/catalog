﻿using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Quasonix.Catalog.Entities
{
    public interface ICatalogRepository<TEntity>
    {
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> GetGroup(Expression<Func<TEntity, bool>> predicate);
        TEntity Get(object id);
        TEntity Get(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> GetAsync(object id);
        Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate);
        object Create(TEntity entity);
        object Update(TEntity entity);
        void Delete(object id);
        void Delete(TEntity entity);
    }

    public class CatalogRepository<TEntity> : ICatalogRepository<TEntity>
    {
        public CatalogUnitOfWork _unitOfWork;

        public CatalogRepository(ICatalogUnitOfWork unitOfWork)
        {
            _unitOfWork = (CatalogUnitOfWork)unitOfWork;
        }

        protected ISession Session => _unitOfWork.Session;

        public IQueryable<TEntity> GetAll()
        {
            return Session.Query<TEntity>();
        }

        public IQueryable<TEntity> GetGroup(Expression<Func<TEntity, bool>> predicate)
        {
            return Session.Query<TEntity>().Where(predicate);
        }

        public TEntity Get(object id)
        {
            return Session.Get<TEntity>(id);
        }

        public TEntity Get(Expression<Func<TEntity, bool>> predicate)
        {
            return Session.Query<TEntity>().FirstOrDefault(predicate);
        }

        public async Task<TEntity> GetAsync(object id)
        {
            return await Session.GetAsync<TEntity>(id);
        }

        public async Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await Session.Query<TEntity>().FirstOrDefaultAsync(predicate);
        }

        public object Create(TEntity entity)
        {
            return Session.Save(entity);
        }

        public object Update(TEntity entity)
        {
            return Session.Save(entity);
        }

        public void Delete(object id)
        {
            Session.Delete(Session.Load<TEntity>(id));
        }

        public void Delete(TEntity entity)
        {
            Session.Delete(entity);
        }
    }
    public interface ICatalogRepository
    {
        IQueryable<TEntity> GetAll<TEntity>();
        IQueryable<TEntity> GetGroup<TEntity>(Expression<Func<TEntity, bool>> predicate);
        TEntity Get<TEntity>(object id);
        TEntity Get<TEntity>(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> GetAsync<TEntity>(object id);
        Task<TEntity> GetAsync<TEntity>(Expression<Func<TEntity, bool>> predicate);
        object Create<TEntity>(TEntity entity);
        object Update<TEntity>(TEntity entity);
        void Delete<TEntity>(object id);
        void Delete<TEntity>(TEntity entity);
    }

    public class CatalogRepository: ICatalogRepository
    {
        public CatalogUnitOfWork _unitOfWork;

        public CatalogRepository(ICatalogUnitOfWork unitOfWork)
        {
            _unitOfWork = (CatalogUnitOfWork)unitOfWork;
        }

        protected ISession Session => _unitOfWork.Session;

        public IQueryable<TEntity> GetAll<TEntity>()
        {
            return Session.Query<TEntity>();
        }

        public IQueryable<TEntity> GetGroup<TEntity>(Expression<Func<TEntity, bool>> predicate)
        {
            return Session.Query<TEntity>().Where(predicate);
        }

        public TEntity Get<TEntity>(object id)
        {
            return Session.Get<TEntity>(id);
        }

        public TEntity Get<TEntity>(Expression<Func<TEntity, bool>> predicate)
        {
            return Session.Query<TEntity>().FirstOrDefault(predicate);
        }

        public async Task<TEntity> GetAsync<TEntity>(object id)
        {
            return await Session.GetAsync<TEntity>(id);
        }

        public async Task<TEntity> GetAsync<TEntity>(Expression<Func<TEntity, bool>> predicate)
        {
            return await Session.Query<TEntity>().FirstOrDefaultAsync(predicate);
        }

        public object Create<TEntity>(TEntity entity)
        {
            return Session.Save(entity);
        }

        public object Update<TEntity>(TEntity entity)
        {
            return Session.Save(entity);
        }

        public void Delete<TEntity>(object id)
        {
            Session.Delete(Session.Load<TEntity>(id));
        }

        public void Delete<TEntity>(TEntity entity)
        {
            Session.Delete(entity);
        }
    }
}
