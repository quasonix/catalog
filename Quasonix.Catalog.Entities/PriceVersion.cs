﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 11/30/2021 3:31:26 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Quasonix.Catalog.Entities
{

    /// <summary>
    /// There are no comments for Quasonix.Catalog.Entities.PriceVersion, Quasonix.Catalog.Entities in the schema.
    /// </summary>
    public partial class PriceVersion {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for PriceVersion constructor in the schema.
        /// </summary>
        public PriceVersion()
        {
            this.Quotes = new HashSet<Quote>();
            this.Prices = new HashSet<Price>();
            this.PriceRules = new HashSet<PriceRule>();
            this.CategoryPrices = new HashSet<CategoryPrice>();
            this.ShippingPrices = new HashSet<ShippingPrice>();
            this.PriceMultipliers = new HashSet<PriceMultiplier>();
            OnCreated();
        }

    
        /// <summary>
        /// There are no comments for Id in the schema.
        /// </summary>
        public virtual System.Guid Id
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for EffectiveDate in the schema.
        /// </summary>
        public virtual System.DateTime EffectiveDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Note in the schema.
        /// </summary>
        public virtual string Note
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Type in the schema.
        /// </summary>
        public virtual PriceVersionType Type
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Quotes in the schema.
        /// </summary>
        public virtual ISet<Quote> Quotes
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Prices in the schema.
        /// </summary>
        public virtual ISet<Price> Prices
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for PriceRules in the schema.
        /// </summary>
        public virtual ISet<PriceRule> PriceRules
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for CategoryPrices in the schema.
        /// </summary>
        public virtual ISet<CategoryPrice> CategoryPrices
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for ShippingPrices in the schema.
        /// </summary>
        public virtual ISet<ShippingPrice> ShippingPrices
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for PriceMultipliers in the schema.
        /// </summary>
        public virtual ISet<PriceMultiplier> PriceMultipliers
        {
            get;
            set;
        }
    }

}
