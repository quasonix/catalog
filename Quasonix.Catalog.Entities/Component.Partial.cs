﻿
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;

namespace Quasonix.Catalog.Entities
{
    public partial class Component
    {
        public virtual void ClearPrices()
        {
            foreach (var price in Prices)
            {
                // clear out other side of the relationship or it will
                // cascade save back in
                price.PriceVersion.Prices.Remove(price);
            }

            Prices.Clear();
        }

        public virtual int GetPrice(PriceVersion version)
        {
            if (Prices.Count == 0)
                return 0;

            var matchingPrice = Prices.FirstOrDefault(x => x.PriceVersion == version);
            if (matchingPrice != null)
                return matchingPrice.Value;

            return Prices.Where(x => x.PriceVersion.EffectiveDate < version.EffectiveDate)
                .OrderByDescending(x => x.PriceVersion.EffectiveDate).First().Value;
        }

        public virtual List<PriceRule> GetPriceRules(PriceVersion version)
        {
            List<PriceRule> rulesInEffect = new List<PriceRule>();

            var ruleGroups = PriceRules.GroupBy(x => x.Description);
            foreach (var group in ruleGroups)
            {
                var matchingRule = group.Where(x => x.PriceVersion.EffectiveDate <= version.EffectiveDate)
                    .OrderByDescending(x => x.PriceVersion.EffectiveDate).FirstOrDefault();

                if (matchingRule != null)
                    rulesInEffect.Add(matchingRule);
            }

            return rulesInEffect;
        }
    }
}
