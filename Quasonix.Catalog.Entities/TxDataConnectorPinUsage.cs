﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 11/30/2021 3:31:26 PM
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Quasonix.Catalog.Entities
{

    /// <summary>
    /// There are no comments for Quasonix.Catalog.Entities.TxDataConnectorPinUsage, Quasonix.Catalog.Entities in the schema.
    /// </summary>
    public partial class TxDataConnectorPinUsage {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for TxDataConnectorPinUsage constructor in the schema.
        /// </summary>
        public TxDataConnectorPinUsage()
        {
            OnCreated();
        }

    
        /// <summary>
        /// There are no comments for Id in the schema.
        /// </summary>
        public virtual System.Guid Id
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for PinNumber in the schema.
        /// </summary>
        public virtual int PinNumber
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Pin in the schema.
        /// </summary>
        public virtual TxPin Pin
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for DataConnector in the schema.
        /// </summary>
        public virtual TxDataConnector DataConnector
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Pinout in the schema.
        /// </summary>
        public virtual TxPinout Pinout
        {
            get;
            set;
        }
    }

}
